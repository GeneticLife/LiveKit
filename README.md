# Genetic Live
## Installation with root permissions

### CMake
- Note that different Boost versions require different CMake versions
- Build essentials with:
    ```sudo apt-get install -y build-essential```

- If you have not already, install the latest version of CMake with the following command:
    ```
    wget http://www.cmake.org/files/v3.14/cmake-3.14.2.tar.gz &&
    tar xf cmake-3.14.2.tar.gz &&
    cd cmake-3.14.2 &&
    ./configure &&
    make &&
    sudo make install
    ```

- Check the CMake version with the following command:
    ```
    cmake --version
    ```

### Zlib
- Simply run the following command:
    ```
    sudo apt-get install zlib1g-dev
    ```

### Lz4
- Run the following command:
    ```
    wget https://github.com/lz4/lz4/archive/v1.8.3.tar.gz &&
    tar -xvzf v1.8.3.tar.gz &&
    cd  lz4-1.8.3 &&
    make &&
    sudo make install &&
    cd ..
    ```
- Lz4 will be usually be installed in your /usr/local directory
- If this is not the case, you can change the path to Lz4 in the CMakeLists.txt under the section "Setup Zlib and lz4 libraries"

### Seqan
- Run the following command:
    ```
    wget 'packages.seqan.de/seqan-src/seqan-src-2.3.2.tar.gz' &&
    tar -xvzf seqan-src-2.3.2.tar.gz
    ```

- Adjust the /util/cmake-Path and the /include-Path in the "Setup seqan libraries"-Section in the CMakeLists.txt to the corresponding path:
[pathToSeqan]/util/cmake and [pathToSeqan]/seqan-seqan-v2.3.2/include

### Boost

- boost.1.69.0 (system, filesystem, program_options)
- do the following steps:
  - create the file `user-config.jam` with the following content:
    ```
    ##### content of user-config.jam #####

        # Configure specific Python version.
        using python : 3.6 ; # Make both versions of Python available
        using python : 2.7 ; # To build with python 2.4, add python=2.4
                             # to your command line.

    ```
- Run the following command in the directory where you created `user-config.jam`:
    ```
    wget 'http://sourceforge.net/projects/boost/files/boost/1.69.0/boost_1_69_0.tar.bz2/download' &&
    tar xf download &&
    cd boost_1_69_0 &&
    ./bootstrap.sh --with-python=/usr/bin/python3 &&
    mv ../user-config.jam ./user-config.jam &&
    sudo ./b2 cxxflags=-fPIC cflags=-fPIC -a install &&
    cd ..
    ```

    (Alternatively use a different source e.g. https://dl.bintray.com/boostorg/release/1.69.0/source/boost_1_69_0.tar.gz)

    If you get the error `Could not find the following static boost libraries: boost_python36` when compiling, you need to adjust the CMakeList by specifieing the correct python3 version:
    1. Adjust `include_directories("/usr/include/python3.6/")`
    (e.g. to `include_directories("/usr/include/python3.5/")`)
    2. Adjust `find_package(Boost COMPONENTS python36 system filesystem program_options iostreams REQUIRED)`
    (to e.g `find_package(Boost COMPONENTS python35 system filesystem program_options iostreams REQUIRED)`)

### LiveKit Installation
- Check out the LiveKit source code from the project website and make the project. 
    ```
    git clone https://gitlab.com/GeneticLife/LiveKit &&
    cd LiveKit &&
    mkdir build &&
    cd build &&
    cmake .. &&
    make 
    ```
- Start the framework with the example config: 
    1. unzip the example Basecalls:`tar xzf ../tutorial/BaseCalls.tar.gz -C ../tutorial/`
    2. start the framework: `./main ../config/framework.json`
    



# Local installation without root permissions

### CMake
- Note that different Boost versions require different CMake versions
- If you have not already, install the latest version of CMake with the following command:
    ```
    wget http://www.cmake.org/files/v3.14/cmake-3.14.2.tar.gz &&
    tar -xvzf cmake-3.14.2.tar.gz &&
    cd cmake-3.14.2/ &&
    ./configure --prefix=**[PathOfYourChoice]** &&
    make &&
    make install &&
    cd ..
    ```
- Check the CMake version with the following command:
    ```
    cd bin/ &&
    cmake --version &&
    cd ..
    ```

### Zlib
- Run the following command:
    ```
    wget http://www.zlib.net/zlib-1.2.11.tar.gz &&
    tar -xvzf zlib-1.2.11.tar.gz &&
    cd zlib-1.2.11 &&
    ./configure --prefix=[PathOfYourChoice]/zlib &&
    make &&
    make install &&
    cd ..
    ```

### Lz4
- Run the following command:
    ```
    wget https://github.com/lz4/lz4/archive/v1.8.3.tar.gz &&
    tar -xvzf v1.8.3.tar.gz &&
    cd  lz4-1.8.3 &&
    make &&
    make install PREFIX=[pathOfYourChoice]/lz4-1.8.3 &&
    cd ..
    ```
- Change the path in the CMakeLists to [pathOfYourChoice]/lz4-1.8.3/lib under the section "Setup Zlib and lz4 libraries"
- Delete the # at the beginning of the line

### Seqan
- Run the following command:
```
wget 'packages.seqan.de/seqan-src/seqan-src-2.3.2.tar.gz' &&
tar -xvzf seqan-src-2.3.2.tar.gz
```
- Adjust the /util/cmake-Path and the /include-Path in the "Setup seqan libraries"-Section in the CMakeLists.txt to the corresponding path:
[pathToSeqan]/util/cmake and [pathToSeqan]/seqan-seqan-v2.3.2/include

### Boost
- boost.1.69.0 (system, filesystem, program_options)
- do the following steps:
  - create the file `user-config.jam` with the following content:
    ```
    ##### content of user-config.jam #####

        # Configure specific Python version.
        using python : 3.6 ; # Make both versions of Python available
        using python : 2.7 ; # To build with python 2.4, add python=2.4
                             # to your command line.

    ```
- Run the following command in the directory where you created `user-config.jam`:
    ```
    wget 'http://sourceforge.net/projects/boost/files/boost/1.69.0/boost_1_69_0.tar.bz2/boost' &&
    tar xf boost &&
    cd boost_1_69_0 &&
    ./bootstrap.sh --prefix=[PathOfYourChoice] --with-python=/usr/bin/python3 &&
    mv ../user-config.jam ./user-config.jam &&
    ./b2 cxxflags=-fPIC cflags=-fPIC -a install &&
    cd ..
    ```

    (Alternatively use a different source e.g. https://dl.bintray.com/boostorg/release/1.69.0/source/boost_1_69_0.tar.gz)

    Specify the path to the boost directory by adding `set(BOOST_ROOT <PathOfYourChoice>/include)` to the CMakeList.
    If you get the error `Could not find the following static boost libraries: boost_python36` when compiling, you need to adjust the CMakeList by specifieing the correct python3 version:
    1. Adjust `include_directories("/usr/include/python3.6/")`
    (e.g. to `include_directories("/usr/include/python3.5/")`)
    2. Adjust `find_package(Boost COMPONENTS python36 system filesystem program_options iostreams REQUIRED)`
    (to e.g `find_package(Boost COMPONENTS python35 system filesystem program_options iostreams REQUIRED)`)

### LiveKit Installation
- Check out the LiveKit source code from the project website and make the project. 
    ```
    git clone https://gitlab.com/GeneticLife/LiveKit &&
    cd LiveKit &&
    mkdir build &&
    cd build &&
    cmake .. &&
    make 
    ```
- Start the framework with the example config: 
    1. unzip the example Basecalls:`tar xzf ../tutorial/BaseCalls.tar.gz -C ../tutorial/`
    2. start the framework: `./main ../config/framework.json`
    
## Additional Dependencies
- There might be additional dependencies for plugins
- Check the READMEs of the plugins to resolve potential dependencies


## Branch Prefixes

- design
- misc
- master
- develop
- feature
- bugfix
- refactoring
- test

## Additional Notes

- To achieve runtime performance, compile with *Release* - option enabled
    - To do that change CMAKE_BUILD_TYPE in the CMakeLists.txt from *Debug* to *Release*

