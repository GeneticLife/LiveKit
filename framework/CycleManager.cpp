#include "CycleManager.h"
#include <boost/algorithm/string/split.hpp>

using namespace std;

CycleManager::CycleManager(vector<pair<int,char>> reads) : reads(reads), cycleCount(0), currentCycle(0), mateCount(0), cyclesToSkip() {
    for (auto read : this->reads) {
        this->cycleCount += read.first;

        if (read.second == 'R') {
            this->cyclesToSkip.push(this->cycleCount);
            this->mateCount++;
        }
    }
}

int CycleManager::getCurrentCycle() {
    return this->currentCycle;
}

void CycleManager::incrementCurrentCycle() {
    this->currentCycle++;

    if (this->currentCycle == this->cyclesToSkip.front()) {
        this->cyclesToSkip.pop();
        // TODO: understand the consequences of the content of 'cyclesToSkip'!
        // this->incrementCurrentCycle();
    }
}

int CycleManager::getCycleCount() {
    return this->cycleCount;
}

int CycleManager::getCurrentReadCycle(int currentReadCycle) {
    for (auto readPair : this->reads){
        if (currentReadCycle > readPair.first)
            currentReadCycle -= readPair.first;
        else break;
    }
    return currentReadCycle;
}

int CycleManager::getCurrentReadId(int cycle) {
    int readId = 0;
    int tempReadSum = 0;
    for (auto readPair : this->reads) {
        tempReadSum += readPair.first;
        if (tempReadSum >= cycle) break;
        readId ++;
    }
    return readId;
}

int CycleManager::getCurrentReadLength(int cycle) {
    return this->reads[this->getCurrentReadId(cycle)].first;
}

bool CycleManager::isBarcodeCycle(int cycle) {
    return 'B' == this->reads[this->getCurrentReadId(cycle)].second;
}

int CycleManager::getCurrentMateId(int cycle) {
    if (this->reads[this->getCurrentReadId(cycle)].second == 'B') return 0;

    int mateId = 1;
    int tempReadSum = 0;
    for (auto readPair: this->reads){
        tempReadSum += readPair.first;
        if (tempReadSum >= cycle) break;
        if (readPair.second == 'R') mateId ++;
    }
    return mateId;
}

int CycleManager::getMateCount() {
    return this->mateCount;
}
