#ifndef LIVEKIT_CYCLEMANAGER_H
#define LIVEKIT_CYCLEMANAGER_H

#include "configuration/Configurable.h"
#include <queue>
#include <string>
#include <vector>

class CycleManager {
private:

    std::vector<std::pair<int, char>> reads;

    int cycleCount;

    int currentCycle;

    int mateCount;

    std::queue<int> cyclesToSkip;

public:

    explicit CycleManager(std::vector<std::pair<int,char>> reads);

    virtual int getCurrentCycle();

    virtual void incrementCurrentCycle();

    virtual int getCycleCount();

    virtual int getCurrentReadCycle(int currentReadCycle);

    virtual int getCurrentReadId(int cycle);

    virtual int getCurrentReadLength(int cycle);

    virtual bool isBarcodeCycle(int cycle);

    virtual int getCurrentMateId(int cycle);

    virtual int getMateCount();

    virtual ~CycleManager() = default;
};

#endif //LIVEKIT_CYCLEMANAGER_H
