#ifndef LIVEKIT_EXECUTIONSYNCHRONIZER_H
#define LIVEKIT_EXECUTIONSYNCHRONIZER_H

#include <atomic>
#include <condition_variable>

class ExecutionSynchronizer {
    std::mutex cycleAvailableMutex;
    std::condition_variable cycleAvailableConditionVariable;

    std::mutex stepAvailableMutex;
    std::condition_variable stepAvailableConditionVariable;

    int numThreads;
public:
    explicit ExecutionSynchronizer(int numThreads);

    void waitForStepAvailable();
    void notifyStepAvailable();

    void waitForCycleAvailable();
    void notifyCycleAvailable();
};


#endif //LIVEKIT_EXECUTIONSYNCHRONIZER_H
