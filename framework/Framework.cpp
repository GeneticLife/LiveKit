#include <memory>
#include <new>
#include <time.h>
#include <stdlib.h>
#include <map>
#include <set>
#include <vector>
#include <thread>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/assign.hpp>

#include "Framework.h"
#include "execution_planning/ExecutionScheduler.h"
#include "utils.h"
#include "ExecutionSynchronizer.h"
#include "output_management/TUIOutput.h"
#include "output_management/ConsoleOutput.h"

using namespace std;

Framework::Framework(const string &manifestFilePath) {
    cout << "-- Loading framework with manifest file: '" << manifestFilePath << "'" << endl;

    srand(time(nullptr)); // required for hashing the fragments

    this->setConfigFromFile(manifestFilePath);

    string baseCallsDirectory = this->getConfigEntry<string>("BaseCallsDirectory");
    utils::validateExistanceOfFile(baseCallsDirectory);
    vector<uint16_t> lanes = this->getConfigEntry<vector<uint16_t>>("lanes");
    vector<uint16_t> tiles = this->getConfigEntry<vector<uint16_t>>("tiles");
    string bclParserType = this->getConfigEntry<string>("BclParserType");
    auto readStructure = this->getConfigEntry<vector<pair<int,char>>>("reads");

    this->cycleManager = new CycleManager(readStructure);

    OutputInterface& outputInterface = (this->getConfigEntry<bool>("enableTUI"))
                                       ? TUIOutput::getInstance()
                                       : ConsoleOutput::getInstance();
    outputInterface.setCycleCount(this->cycleManager->getCycleCount());
    this->outputManager = new OutputManager(outputInterface);
    this->outputManager->captureStream({"cout"}, cout);
    this->outputManager->captureStream({"cerr"}, cerr);

    this->baseManager = new BaseManager(baseCallsDirectory, lanes, tiles, bclParserType, readStructure);

    this->loadSerializablesFactory(this->getConfigEntry<string>("SerializableFactoryPath"));

    this->setupFromManifestFile();
}

Framework::~Framework() {

    delete this->cycleManager;
    delete this->baseManager;
    delete this->graph;
    delete this->executionScheduler;
    delete this->memoryManager;
    delete this->outputManager;
}

Plugin *Framework::loadPlugin(const string &pluginPath) {
    if (utils::hasEnding(pluginPath, "py"))
        return this->loadPythonPlugin();
    else
        return this->loadBinaryPlugin(pluginPath);
}

void Framework::run() {
    string baseCallsDirectory = this->getConfigEntry<string>("BaseCallsDirectory");

    FileDetector fileDetector(baseCallsDirectory);
    fileDetector.waitForDirectory(baseCallsDirectory);

    auto lanes = this->getConfigEntry<vector<uint16_t>>("lanes");
    fileDetector.waitForAllLanes(lanes);

    int numThreads = this->getConfigEntry<int>("numThreads");

    ExecutionSynchronizer executionSynchronizer(numThreads);

    int nextCycle, cycleCount = this->cycleManager->getCycleCount();

    // First cycle has to be parsed before preprocessing step are executed
    fileDetector.waitForCycleFiles(this->getBasecallFiles(1));
    this->baseManager->parseFirstCycle();
    this->cycleManager->incrementCurrentCycle();

    vector<thread> executors;
    executors.reserve(numThreads);

    // Distribute Alignment extension on multiple Threads
    for (int threadId = 0; threadId < numThreads; threadId++) {
        executors.emplace_back([&](){
            while (true) {
                // Force outputManager to refresh it's output (cout does not refresh automatically).
                this->outputManager->notify();

                switch (this->executionScheduler->nextStep()) {
                    case ExecutionStatus::DONE: {
                        executionSynchronizer.notifyStepAvailable();
                        continue;
                    }
                    case ExecutionStatus::WAIT_FOR_NEXT_CYCLE: {
                        executionSynchronizer.waitForCycleAvailable();
                        break;
                    }
                    case ExecutionStatus::NOT_ENOUGH_MEMORY:
                    case ExecutionStatus::NO_STEP_AVAILABLE: {
                        executionSynchronizer.waitForStepAvailable();
                        break;
                    }
                    case ExecutionStatus::ALL_STEPS_DONE:
                    default:
                        executionSynchronizer.notifyStepAvailable();
                        return;
                }
            }
        });
    }

    while ((nextCycle = this->cycleManager->getCurrentCycle() + 1) <= cycleCount) {
        // TODO: Check one cycle in advance!
        fileDetector.waitForCycleFiles(this->getBasecallFiles(nextCycle));

        this->baseManager->setLatestAvailableCycle(nextCycle);
        this->baseManager->parseAllTiles();

        // Increment the cycle information only AFTER the BaseManager has parsed the BCL data!
        this->cycleManager->incrementCurrentCycle();
        this->executionScheduler->setLatestAvailableCycle(nextCycle);
        this->memoryManager->setLatestAvailableCycle(nextCycle);


        executionSynchronizer.notifyCycleAvailable();
    }

    // Unlock postprocessing step
    this->executionScheduler->setLatestAvailableCycle(this->cycleManager->getCurrentCycle() + 1);
    executionSynchronizer.notifyCycleAvailable();

    for (auto &executor : executors) {
        executionSynchronizer.notifyStepAvailable();
        executor.join();
    }
}

void Framework::unloadPlugins() {
    for (auto pluginInfo : this->pluginInfos) {
        pluginInfo.libraryInstance->finalize();
        delete pluginInfo.libraryInstance->specification;
        pluginInfo.destroy(pluginInfo.libraryInstance);
    }
}

shared_ptr<Fragment> Framework::createNewFragment(string name) {
    return shared_ptr<Fragment>(new Fragment(name, this->serializableFactory.libraryInstance));
}

shared_ptr<FragmentContainer> Framework::createNewFragmentContainer() {
    return std::make_shared<FragmentContainer>();
}

void Framework::setupFromManifestFile() {
    try {
        set<Plugin *> plugins;

        // Insert plugins that can be successfully instantiated
        for (auto &pluginChild : this->parsedConfigTree.get_child("plugins")) {
            try {
                PluginSpecification spec(pluginChild.second);
                Plugin *plugin = this->setupPlugin(spec);
                plugins.insert(plugin);
            } catch (exception &e) {
                cerr << e.what() << endl;
            }
        }

        auto lanes = this->getConfigEntry<vector<uint16_t>>("lanes"),
             tiles = this->getConfigEntry<vector<uint16_t>>("tiles");

        ExecutionGraphBuilder graphFactory(lanes, tiles, this->cycleManager->getCycleCount());

        this->graph = graphFactory.build(plugins);
        this->outputManager->setupPlugins(lanes, tiles, plugins);

        // Print out graph structure
        // use http://www.webgraphviz.com/ to visualize
        if (this->getConfigEntry<bool>("printGraphStructure"))
            graphFactory.write_graphviz();

        this->memoryManager = new MemoryManager(
                this->baseManager,
                this->graph,
                lanes,
                tiles,
                this->getConfigEntry<int>("ramLimit")
        );

        this->executionScheduler = new ExecutionScheduler(this, this->graph, this->memoryManager);

        cout << "Finished setup from manifest-file" << endl;

    } catch (exception &e) {
        cerr << "Setup of graph failed: " << e.what() << endl;
        throw (string) "Could not finish the setup from manifest-file 'framework.json': " + e.what();
    }
}

void Framework::setConfig() {
    /* Define default config options here */

    this->registerConfigEntry<bool>("enableTUI", false);

    this->registerConfigEntry<int, string>("ramLimit", "500GB", Configurable::toBytes('M'));
    cout << "Memory limit: " << this->getConfigEntry<int>("ramLimit") << " MB" << endl;

    this->registerConfigEntry<string>("RunInfoPath", "");
    this->registerConfigEntry<string>("SampleSheetPath", "");
    this->registerConfigEntry<string>("BaseCallsDirectory", "");
    this->registerConfigEntry<string>("SerializableFactoryPath", "", EntryOptions::SHOW_WARNINGS);

    this->registerConfigEntry<string>("outDir", "./out/");

    this->registerConfigEntry<int>("numThreads", 1);

    this->registerConfigEntry<bool>("printGraphStructure", false);

    this->registerConfigEntry<vector<uint16_t>, string>("barcodeErrors", "2, 2", Configurable::toVector<uint16_t>(','));

    // BEGIN: RunInfo entries
    // these are parsed from the RunInfo.xml, but overwritten by the json-Config Entries

    this->registerConfigEntry<string>("run-id", "0");
    this->registerConfigEntry<string>("flowcell-id", "Default");
    this->registerConfigEntry<string>("instrument-id", "0");
    this->registerConfigEntry<int>("surfaceCount", 1);
    this->registerConfigEntry<int>("swathCount", 1);
    this->registerConfigEntry<int>("tileCountPerSwath", 1);

    this->registerConfigEntry<vector<pair<int, char>>, string>("reads", "", [](string input) {
        vector<pair<int, char>> result;
        vector<string> intermediateResult;
        boost::split(intermediateResult, input, boost::is_any_of(","), boost::token_compress_on);
        for (auto el : intermediateResult) {

            if (el.substr(0, el.size() - 1).find_first_not_of("0123456789") != string::npos)
                throw runtime_error(
                        "Invalid length for read fragment " + input + ". Please only use unsigned integer values.");

            if (el[el.size() - 1] != 'B' && el[el.size() - 1] != 'R')
                throw runtime_error(
                        "'" + to_string(el[el.size() - 1])
                        + "' is no valid read type. Please use 'R' for sequencing reads or 'B' for barcode reads."
                );

            result.emplace_back(stoi(el.substr(0, el.size() - 1)), el[el.size() - 1]);
        }
        return result;
    }, THROW_ERROR);

    this->registerConfigEntry<vector<uint16_t>, string>("lanes", "", Configurable::toVector<uint16_t>(','), THROW_ERROR);
    this->registerConfigEntry<vector<uint16_t>, string>("tiles", "", Configurable::toVector<uint16_t>(','), THROW_ERROR);

    this->registerConfigEntry<string>("BclParserType", "BclParser");

    this->registerCalculatedConfigEntry<uint16_t>("laneCount", [&] { return this->getConfigEntry<vector<uint16_t>>("lanes").size(); } );
    this->registerCalculatedConfigEntry<uint16_t>("tileCount", [&] { return this->getConfigEntry<vector<uint16_t>>("tiles").size(); } );

    // END: RunInfo entries

    // BEGIN: SampleSheet entries

    this->registerConfigEntry<vector<vector<string>>, string>("barcodeVector", "", Configurable::toVectorVector<string>(';',','));

    if (this->parsedConfigTree.count("SampleSheetPath")) {
        vector<string> barcodeFragmentsRead1 = sampleSheetParser.getSampleEntries("index");
        vector<string> barcodeFragmentsRead2 = sampleSheetParser.getSampleEntries("index2");
        string barcode;
        auto reads = this->getConfigEntry<vector<pair<int, char>>>("reads");

        for (int i = 0; i < (int) barcodeFragmentsRead1.size(); i++) {
            if (reads.size() == 4) {
                // If read structure contains two barcodes (RBBR)
                barcode = barcodeFragmentsRead1[i] + barcodeFragmentsRead2[i];
            } else {
                // If read structure contains only one barcode (e.g. RBR, RB)
                barcode = barcodeFragmentsRead1[i];
            }
            this->registerConfigEntry<vector<string>, string>(barcode, "N,N", Configurable::toVector<string>(','));
        }
    }
    else {
        auto barcodeVector = this->getConfigEntry<vector<vector<string>>>("barcodeVector");
        for (auto &barcode : barcodeVector) {
            string concatinatedBarcode;
            for (auto &fragment : barcode) concatinatedBarcode += fragment;
            this->registerConfigEntry<vector<string>, string>(concatinatedBarcode, "[N/A], [N/A]", Configurable::toVector<string>(','));
        }
    }
    // END: SampleSheet entries
}

Plugin *Framework::setupPlugin(PluginSpecification &spec) {
    utils::validateExistanceOfFile(spec.pluginPath);
    utils::validateExistanceOfFile(spec.pluginConfigPath);
    Plugin *plugin = this->loadPlugin(spec.pluginPath);
    plugin->setFramework(new FrameworkInterface(this));

    try {
        cout << "-- Loading plugin '" << spec.getDisplayName() << "'";
        cout << " with config-file '" << spec.pluginConfigPath << "'" << endl;

        plugin->specification = new PluginSpecification(spec);
        plugin->setConfigFromFile(spec.pluginConfigPath, this->configMap);
        plugin->init();

        return plugin;
    } catch (const runtime_error &e) {
        throw runtime_error("Could not setup plugin " + spec.getDisplayName() + "\n" + e.what());
    }
}

void Framework::setConfigFromFile(const string &configFilePath) {
    boost::property_tree::read_json(configFilePath, this->parsedConfigTree);

    // Parse xml file
    if (this->parsedConfigTree.count("RunInfoPath")){
        utils::validateExistanceOfFile(this->parsedConfigTree.get<string>("RunInfoPath"));
        this->runInfoParser.parseRunInfo(this->parsedConfigTree.get<string>("RunInfoPath"));
    }

    // Parse csv file
    if (this->parsedConfigTree.count("SampleSheetPath")) {
        utils::validateExistanceOfFile(this->parsedConfigTree.get<string>("SampleSheetPath"));
        this->sampleSheetParser.parseSampleSheet(this->parsedConfigTree.get<string>("SampleSheetPath"));
    }

    this->setConfig();
}

Plugin *Framework::loadBinaryPlugin(const string &pluginPath) {
    PluginInfo pluginInfo;
    void *libraryHandle;
    Plugin *(*create)();
    void (*destroy)(Plugin *);

    libraryHandle = dlopen(pluginPath.c_str(), RTLD_LAZY);
    create = (Plugin *(*)()) dlsym(libraryHandle, "create");
    destroy = (void (*)(Plugin *)) dlsym(libraryHandle, "destroy");

    if (create != nullptr && destroy != nullptr) {
        Plugin *pluginInstance = create();

        pluginInfo = {destroy, pluginInstance};
        this->pluginInfos.push_back(pluginInfo);

        return pluginInstance;
    } else {
        cerr << "The plugin binary could not be located at '" << pluginPath.c_str() << "'" << endl;
        return nullptr;
    }
}

Plugin *Framework::loadPythonPlugin() {
    if (this->pythonPlugins.empty())
        PythonDependencies::initPython();

    auto pyPlugin = new PythonPlugin();
    this->pythonPlugins.push_back(pyPlugin);

    return (Plugin *) pyPlugin;
}

void Framework::loadSerializablesFactory(const string &libraryPath) {
    void *libraryHandle;
    SerializableFactory *(*create)();
    void (*destroy)(SerializableFactory *);

    libraryHandle = dlopen(libraryPath.c_str(), RTLD_LAZY);
    create = (SerializableFactory *(*)()) dlsym(libraryHandle, "create");
    destroy = (void (*)(SerializableFactory *)) dlsym(libraryHandle, "destroy");

    if (create != nullptr && destroy != nullptr) {
        SerializableFactory *factory = create();

        this->serializableFactory = {destroy, factory};
    } else {
        cerr << "The SerializableFactory couldn't be initialized." << endl;
        cerr << "(Please check whether the path to the shared object of the SerializableFactory is correctly specified "
                "as \"SerializableFactoryPath\"-config entry in the framework config)" << endl;
        throw runtime_error("The SerializableFactory couldn't be initialized");
    }
}

OutputManager *Framework::getOutputManager() {
    return this->outputManager;
}

std::vector<std::string> Framework::getBasecallFiles(int cycle) {
    vector<string> filenames;
    vector<uint16_t> lanes = this->getConfigEntry<vector<uint16_t>>("lanes");
    vector<uint16_t> tiles = this->getConfigEntry<vector<uint16_t>>("tiles");
    for(auto lane : lanes)
        for(auto tile : tiles){
            auto tmpFilenames = this->baseManager->bclParser->getFilenames(lane, tile, cycle, this->getConfigEntry<string>("BaseCallsDirectory"));
            filenames.insert( filenames.end(), tmpFilenames.begin(), tmpFilenames.end() );
        }

    // remove duplicates
    filenames.erase( unique( filenames.begin(), filenames.end() ), filenames.end() );

    return filenames;
}
