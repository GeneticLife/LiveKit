#include "FrameworkInterface.h"
#include "Framework.h"

using namespace std;

FrameworkInterface::FrameworkInterface(Framework *framework) : framework(framework), currentPluginCycle(0) {}

FrameworkInterface::FrameworkInterface(const FrameworkInterface &frameworkInterface) {
    this->framework = frameworkInterface.framework;
    this->currentPluginCycle = frameworkInterface.currentPluginCycle;
}

int FrameworkInterface::getCurrentCycle() {
    return this->currentPluginCycle;
}

SequenceIterators FrameworkInterface::getSequenceIterators(uint16_t lane, uint16_t tile, int sequence) {
    return this->framework->baseManager->getSequenceIterators(lane, tile, sequence, this->getCurrentCycle());
}

Read FrameworkInterface::getSequence(uint16_t lane, uint16_t tile, int sequence) {
    return this->framework->baseManager->getSequence(lane, tile, sequence, this->currentPluginCycle);
}

Read FrameworkInterface::getRead(uint16_t lane, uint16_t tile, int sequence, int readId) {
    return this->framework->baseManager->getRead(lane, tile, sequence, readId, this->getCurrentCycle());
}

ReadIterators FrameworkInterface::getReadIterators(uint16_t lane, uint16_t tile, int sequence, int readId) {
    return this->framework->baseManager->getReadIterators(lane, tile, sequence, readId, this->getCurrentCycle());
}

const BCL &FrameworkInterface::getMostRecentBcl(uint16_t lane, uint16_t tile) {
    return this->framework->baseManager->getBcl(lane, tile, this->getCurrentCycle());
}

deque<BCL> FrameworkInterface::getMostRecentBcls(uint16_t lane) {
    return this->framework->baseManager->getMostRecentBcls(lane, this->getCurrentCycle());
}

bool FrameworkInterface::filterBasecall(uint16_t lane, uint16_t tile, unsigned long position) {
    return this->framework->baseManager->filterBasecall(lane, tile, position);
}

vector<char>& FrameworkInterface::getFilterData(uint16_t lane, uint16_t tile) {
    return this->framework->baseManager->getFilterData(lane, tile);
}

uint32_t FrameworkInterface::getNumSequences(uint16_t lane, uint16_t tile) {
    return this->framework->baseManager->getNumSequences(lane, tile);
}

int FrameworkInterface::getCycleCount() {
    return this->framework->cycleManager->getCycleCount();
}

int FrameworkInterface::getCurrentReadCycle() {
    return this->framework->cycleManager->getCurrentReadCycle(this->currentPluginCycle);
}

int FrameworkInterface::getCurrentReadId() {
    return this->framework->cycleManager->getCurrentReadId(this->currentPluginCycle);
}

int FrameworkInterface::getCurrentReadLength() {
    return this->framework->cycleManager->getCurrentReadLength(this->currentPluginCycle);
}

bool FrameworkInterface::isBarcodeCycle() {
    return this->framework->cycleManager->isBarcodeCycle(this->currentPluginCycle);
}

int FrameworkInterface::getCurrentMateId() {
    return this->framework->cycleManager->getCurrentMateId(this->currentPluginCycle);
}

int FrameworkInterface::getMateCount() {
    return this->framework->cycleManager->getMateCount();
}

shared_ptr<Fragment> FrameworkInterface::createNewFragment(string name) {
    return this->framework->createNewFragment(name);
}

std::shared_ptr<FragmentContainer> FrameworkInterface::createNewFragmentContainer() {
    return this->framework->createNewFragmentContainer();
}

vector<char> FrameworkInterface::getSampleBarcode (Read barcode, vector<vector<string>> barcodeVector, vector<uint16_t> barcodeErrors, bool isSingleEnd) {
    return this->framework->baseManager->getSampleBarcode(barcode, barcodeVector, barcodeErrors, isSingleEnd);
}

void FrameworkInterface::serializeUnusedData() {
    this->framework->memoryManager->serializeUnusedData(true);
}

void FrameworkInterface::setCurrentPluginCycle(int cycle) {
    this->currentPluginCycle = cycle;
}
