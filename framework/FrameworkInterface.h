#ifndef LIVEKIT_FRAMEWORKINTERFACE_H
#define LIVEKIT_FRAMEWORKINTERFACE_H

#include <memory>
#include "basecalls/data_representation/SequenceContainer.h"
#include "basecalls/data_representation/BclRepresentation.h"
#include "basecalls/management/BaseManager.h"
#include "CycleManager.h"


class Framework;
class FragmentContainer;
class Fragment;

/**
 * Interface of the whole framework.
 * Plugins have access to theses functions.
 */
class FrameworkInterface {
protected:
    Framework *framework;
    int currentPluginCycle;

public:
    explicit FrameworkInterface(Framework* framework);

    FrameworkInterface(const FrameworkInterface &frameworkInterface);

    /**
    * Return the current cycle (starting by 1). In FullReadPostProcess the currentCycle is equal to getCycleCount + 1.
    * @return
    */
    virtual int getCurrentCycle();

    /**
     * Get start and end iterators for a specific Sequence from a specific lane and tile.
     * Advantage:   This has a good performance because the data is not copied
     * Alternative: 'getSequence'
     * @param lane
     * @param tile
     * @param sequence Index of the wanted sequence
     * @return
     */
    virtual SequenceIterators getSequenceIterators(uint16_t lane, uint16_t tile, int sequence);

    /**
     * Get a specific Sequence as copy from a specific lane and tile.
     * Warning:     This method passes the data by copy. This may lead to a bigger memory usage or a higher runtime
     * Advantage:   This method returns sequences with the correct size. (Nucleotides that have been parsed but belong to
     *              a cycle that is higher that the current cycle of the plugin are cut off)
     * Alternative: 'getSequenceRef'
     * @param lane
     * @param tile
     * @param sequence Index of the wanted sequence
     * @return
     */
    virtual Read getSequence(uint16_t lane, uint16_t tile, int sequence);

    /**
     * Returns the Read (as a vector of uint8_t) for the specified sequence and readId.
     * When accessing a read that is not fully parsed yet, only the parsed contend is returned.
     * When accessing a read that is not parsed at all, an empty Read is  returned.
     * @param lane
     * @param tile
     * @param sequence
     * @param readId starts at 0
     * @return
     */
    virtual Read getRead(uint16_t lane, uint16_t tile, int sequence, int readId);

    /**
     * Get start and end iterators for a Read from a specific sequence.
     * Advantage:   This has a good performance because the data is not copied
     * @param lane
     * @param tile
     * @param sequence Index of the wanted sequence
     * @param readId
     * @return
     */
    ReadIterators getReadIterators(uint16_t lane, uint16_t tile, int sequence, int readId);

    /**
     * Gets the latest bcl to a specific tile.
     * @param lane
     * @param tile
     * @return
     */
    virtual const BCL &getMostRecentBcl(uint16_t lane, uint16_t tile);

    /**
     * Get the latest basecalls of all tiles of one specific lane.
     * @param lane
     * @return
     */
    virtual std::deque<BCL> getMostRecentBcls(uint16_t lane);

    /**
     * Uses .filter files to determine if a read should be filtered or not.
     * @param lane
     * @param tile
     * @param position of the read in the sequence representation
     * @return true if the read is filtered and false otherwise
     */
    virtual bool filterBasecall(uint16_t lane, uint16_t tile, unsigned long position);

    /**
     * Provides the filter information of the .filter file for a whole tile
     * 0 means: should be filtered, 1 means: should not be filtered
     * @param lane
     * @param tile
     * @return a vector with the filter flag for each read
     */
    virtual std::vector<char>& getFilterData(uint16_t lane, uint16_t tile);

    /**
     * Get the number of reads (sequences) of one specific tile and lane.
     *
     * @param lane
     * @param tile
     * @return
     */
    virtual uint32_t getNumSequences(uint16_t lane, uint16_t tile);

    /**
     * Get the total number of cycles.
     * Is calculated by the read structure (e.g. 100R 8B 8B 100R = 216 cycles).
     * @return
     */
    virtual int getCycleCount();

    /**
     * Get the current cycle of the current read. (e.g. when the read structure ist 100R 8B 8B 100R and the current
     * cycle is 123, the current read cycle is 7, because it is the 7-th cycle of the fourth read.
     * @return
     */
    virtual int getCurrentReadCycle();

    /**
     * Get the index of the current read.
     * @return
     */
    virtual int getCurrentReadId();

    /**
     * Get the length of the current read.
     * The read length does not depend on the length of the other reads in that sequence.
     * @return
     */
    virtual int getCurrentReadLength();

    /**
     * LiveKit distinguish between read and barcode.
     * @return true if a barcode was sequenced this cycle or false otherwise
     */
    virtual bool isBarcodeCycle();

    /**
     * A mate is a single read, that is not a barcoe. In a paired end read structure, the two reads are two mates.
     * All mates are numbered and 1-based.
     * @return mate id of the current read. If the current cycle is an barcode cycle, then this method returns 0
     */
    virtual int getCurrentMateId();

    /**
     * Get the total number of mates.
     * @return
     */
    virtual int getMateCount();


    /**
     * This method serializes all fragments and all tiles, that are currently not used to free as much space as possible.
     */
    virtual void serializeUnusedData();

    /**
     * Create a new fragment, that can be received by other plugins.
     * @param name of the new fragment
     * @return
     */
    virtual std::shared_ptr<Fragment> createNewFragment(std::string name);

    /**
     * Create a new Fragment Container.
     * Contains all Fragments of one Plugin.
     * @return
     */
    virtual std::shared_ptr<FragmentContainer> createNewFragmentContainer();

    /**
     * Check if a read is part of the barcodeVector given a maximum number of errors and if the read is is single-end.
     * @param barcode the read to check
     * @param barcodeVector the vector with all barcodes to check
     * @param barcodeErrors the number of allowed errors per barcode
     * @param isSingleEnd bool which specifies if the read is single-end
     * @return the correct barcode from the vector if the barcode matches otherwise an empty vector is returned
     */
    virtual std::vector<char> getSampleBarcode(Read barcode, std::vector<std::vector<std::string>> barcodeVector, std::vector<uint16_t> barcodeErrors, bool isSingleEnd);

    /**
     * This methods sets the 'currentPluginCyle'
     * This is needed because different plugins can be in different cycles. However they share the same FrameworkInterface.
     * The FrameworkInterface encapsulates the state of different cycles from the plugins so they don't have to worry about it.
     * Once the 'currentPluginCycle' is set, all methods of this class can be used by the plugin and they behave accordingly.
     * @param cycle
     */
    virtual void setCurrentPluginCycle(int cycle);

    virtual ~FrameworkInterface() = default;
};


#endif //LIVEKIT_FRAMEWORKINTERFACE_H
