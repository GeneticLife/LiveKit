#include "MemoryManager.h"
#include "execution_planning/ExecutionGraphBuilder.h"
#include <algorithm>

#if defined(__APPLE__)
#include <mach/mach.h>
#elif defined(__linux__)
#include <malloc.h>
#endif

using namespace std;

MemoryManager::MemoryManager(
        BaseManager *baseManager,
        ExecutionGraph *graph,
        std::vector<uint16_t> &lanes,
        std::vector<uint16_t> &tiles,
        int maxRamSize
) : baseManager(baseManager), graph(graph), lanes(lanes), tiles(tiles), maxRamSize(maxRamSize), allTilesUsedCount(0), latestAvailableCycle(1) {
    for (auto &lane : lanes) {
        this->executionStepsPerCycle[lane] = map<uint16_t, vector<uint16_t>>();
        for (auto &tile : tiles) {
            this->executionStepsPerCycle[lane][tile] = vector<uint16_t>(this->graph->cycleCount + 2, 0);
            this->currentlyUsedTiles[lane][tile] = 0;
        }
    }
}

pair<double, double> MemoryManager::processMemoryUsage() {
    // Resident Set Size: number of pages the process has
    //    in real memory.  This is just the pages which count
    //    toward text, data, or stack space.  This does not
    //    include pages which have not been demand-loaded in,
    //    or which are swapped out.
    //    (memory in RAM)
    // Virtual memory size in bytes
    //    (total memory which can be accessed by this process)

    double vm_usage, resident_set;

    #if defined(__APPLE__)
        struct task_basic_info t_info;
        mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT;
        if (KERN_SUCCESS != task_info(mach_task_self(),
                                      TASK_BASIC_INFO, (task_info_t)&t_info,
                                      &t_info_count)) {
            cerr << "Reading current memory usage failed.";
            vm_usage = numeric_limits<double>::max();
            resident_set = numeric_limits<double>::max();
        } else {
            vm_usage = t_info.virtual_size / (1024 * 1024);
            resident_set = t_info.resident_size / (1024 * 1024);
        }
    #elif defined(__linux__)
        // "stat" contains a lot of information that we are not interested in
        unsigned long vsize, rss;

        string ignore;
        ifstream ifs("/proc/self/stat", ios_base::in);
        ifs >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore
            >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore
            >> ignore >> ignore >> vsize >> rss;

        long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages

        vm_usage = vsize / (1024.0 * 1024.0);
        resident_set = rss * page_size_kb / 1024.0;
    #endif

    return make_pair(vm_usage, resident_set);
}

double MemoryManager::getRSS() {
    return this->processMemoryUsage().second;
}

double MemoryManager::getVSize() {
    return this->processMemoryUsage().first;
}

double MemoryManager::getHeapSize() {
#if defined(__linux__)
    struct mallinfo info = mallinfo();
    double totalUsage = (double)info.uordblks / ( 1024 * 1024);
    return totalUsage;
#else
    return 0;
#endif
}

unsigned long
MemoryManager::getSerializedSizeForVertexIdentifier(ExecutionGraph::Vertex vertex, VertexIdentifier identifier) {
    unsigned long serializedSize = 0;

    // permanentFragment
    if (identifier.plugin->permanentFragment)
        serializedSize += identifier.plugin->permanentFragment->getSerializedSize();

    if (identifier.lane != this->graph->ALL_LANES && identifier.tile != this->graph->ALL_TILES) {
        auto &tileFragments = identifier.plugin->tileFragments;
        if (tileFragments.find(identifier.lane) != tileFragments.end()
            && tileFragments[identifier.lane].find(identifier.tile) != tileFragments[identifier.lane].end()
            && tileFragments[identifier.lane][identifier.tile])
            serializedSize += identifier.plugin->tileFragments[identifier.lane][identifier.tile]->getSerializedSize();
    }

    // inputFragments
    for (auto &fragment : this->preparedInputContainer[vertex]->getAllFragments())
        serializedSize += fragment->getSerializedSize();

    // SequenceData
    auto lanesToCalculate = (identifier.lane == this->graph->ALL_LANES) ? this->lanes : vector<uint16_t>{ identifier.lane };
    auto tilesToCalculate = (identifier.tile == this->graph->ALL_TILES) ? this->tiles : vector<uint16_t>{ identifier.tile };
    for (auto lane : lanesToCalculate)
        for (auto tile :  tilesToCalculate)
            serializedSize += this->baseManager->areSequencesOfTileSerialized(lane, tile) ?
                              this->baseManager->getNumSequences(lane, tile) * identifier.cycle // more precise: multiply with the latest parsed cycle
                              : 0;

    // TODO: BCL data

    return serializedSize;
}

bool MemoryManager::hasFreeMemory() {
    return this->getFreeMemory() > 0;
}

double MemoryManager::getFreeMemory() {
    return this->maxRamSize - this->getRSS();
}

void MemoryManager::trim() {
#if defined(__linux__)
    malloc_trim(4096);
#endif
}

void MemoryManager::serializeUnusedData(bool serializeEverything) {
    if (!serializeEverything && this->hasFreeMemory())  return;
    lock_guard<mutex> lock(this->serializationMutex);

    double memoryBefore = this->getRSS();

    // Serialize unused sequences
    if (this->allTilesUsedCount == 0) {
        for (auto &lane : lanes) {
            for (auto &tile : tiles) {
                if (this->currentlyUsedTiles[lane][tile] > 0) continue;
                if (!serializeEverything && this->hasFreeMemory()) break;
                if (this->baseManager->serializeSequencesOfTile(lane, tile))
                    this->trim();
            }
        }
    }

    // Serialize unused BCLs
    for(auto& laneEntry : this->currentlyUsedCycles)
        for(auto& tileEntry : laneEntry.second)
            for(auto& cycleEntry : tileEntry.second) {
                uint16_t lane = laneEntry.first;
                uint16_t tile = tileEntry.first;
                uint16_t cycle = cycleEntry.first;
                if (this->currentlyUsedCycles[lane][tile][cycle] > 0) continue;
                if (!serializeEverything && this->hasFreeMemory()) break;
                if (this->baseManager->serializeBCL(lane, tile, cycle))
                    this->trim();
            }




    lock_guard<mutex> canBeSerializedLock(this->canBeSerializedMutex);
    for (auto &fragment : this->canBeSerialized) {
        if (!serializeEverything && this->hasFreeMemory()) break;
        if (fragment->serialize())
            this->trim();  // Only trim, if the fragment has actually been serialized
    }

    double memoryAfter = this->getRSS();

    this->trim();

    if (memoryAfter != memoryBefore) {
        cout    << "Reduced memory: "
                << to_string(memoryBefore) + " Mb"
                << " --> ";

        cout    <<  to_string(memoryAfter) + " Mb"
                << "." << endl;
    }
}

void MemoryManager::markAllDependenciesAsUsed(ExecutionGraph::Vertex vertex) {
    auto identifier = this->graph->getVertexIdentifier(vertex);

    for (auto &fragment : this->preparedInputContainer[vertex]->getAllFragments())
        this->incrementCurrentlyUsedCount(fragment);

    if (identifier.lane == this->graph->ALL_LANES && identifier.tile == this->graph->ALL_TILES) {
        this->allTilesUsedCount++;
        for(auto lane: this->lanes)
            for(auto tile : this->tiles)
                this->currentlyUsedCycles[lane][tile][identifier.cycle]++;
    } else {
        // Mark tile-based fragment as currently used
        this->incrementCurrentlyUsedCount(identifier.plugin->tileFragments[identifier.lane][identifier.tile]);

        // Mark Sequences of current tile as currently used
        this->currentlyUsedTiles[identifier.lane][identifier.tile]++;
        // Mark BCLs of current tile as currently used
        this->currentlyUsedCycles[identifier.lane][identifier.tile][identifier.cycle]++;
    }

    // Mark permanentFragment of the Plugin as currently used
    this->incrementCurrentlyUsedCount(identifier.plugin->permanentFragment);
}

void MemoryManager::markAllDependenciesAsUnused(ExecutionGraph::Vertex vertex) {
    auto identifier = this->graph->getVertexIdentifier(vertex);

    for (auto &fragment : this->preparedInputContainer[vertex]->getAllFragments())
        this->decrementCurrentlyUsedCount(fragment);


    if (identifier.lane == this->graph->ALL_LANES && identifier.tile == this->graph->ALL_TILES) {
        this->allTilesUsedCount--;
        for(auto lane: this->lanes)
            for(auto tile : this->tiles)
                this->currentlyUsedCycles[lane][tile][identifier.cycle]--;
    } else {
        // Mark tile-based fragment as not used anymore
        this->decrementCurrentlyUsedCount(identifier.plugin->tileFragments[identifier.lane][identifier.tile]);

        // Mark Sequences of current tile as not used anymore
        this->currentlyUsedTiles[identifier.lane][identifier.tile]--;
        // Mark BCLs of current tile as not used anymore
        this->currentlyUsedCycles[identifier.lane][identifier.tile][identifier.cycle]--;
    }

    // TODO: Only a hotfix
    // When preprocessing works on tileFragments, they also have to be serialized
    if (identifier.cycle == this->graph->PRE_CYCLE) {
        for (auto lane : this->lanes)
            for (auto tile : this->tiles)
                this->decrementCurrentlyUsedCount(identifier.plugin->tileFragments[lane][tile]);
    }

    // Mark permanentFragment of the Plugin as not used anymore
    this->decrementCurrentlyUsedCount(identifier.plugin->permanentFragment);
}

void MemoryManager::addToCanBeSerialized(std::shared_ptr<Fragment> &fragment) {
    lock_guard<mutex> lock(this->canBeSerializedMutex);
    this->canBeSerialized.insert(fragment);
}

void MemoryManager::incrementCurrentlyUsedCount(std::shared_ptr<Fragment> &fragment) {
    if (fragment && ++fragment->currentlyUsedCount == 1) {
        // Fragment is not allowed to be serialized anymore, since it is beeing used by at least one thread
        lock_guard<mutex> lock(this->canBeSerializedMutex);
        this->canBeSerialized.erase(fragment);
    }
}

void MemoryManager::decrementCurrentlyUsedCount(std::shared_ptr<Fragment> &fragment) {
    if (fragment && --fragment->currentlyUsedCount <= 0) {
        fragment->currentlyUsedCount = 0;
        // Fragment is currently not in use, so it can be serialized
        this->addToCanBeSerialized(fragment);
    }
}

uint16_t MemoryManager::getOldestUnfinishedCycle(uint16_t lane, uint16_t tile) {
    int numberOfHooksPerTileCycle = this->graph->numberOfHooksPerTileCycle;

    int totalOldestUnfinishedCycle = 0;

    auto lanesToConsider = (lane == this->graph->ALL_LANES) ? this->lanes : vector<uint16_t>{lane};
    auto tilesToConsider = (tile == this->graph->ALL_TILES) ? this->tiles : vector<uint16_t>{tile};

    for (auto currentLane : lanesToConsider) {
        for (auto currentTile : tilesToConsider) {
            auto &stepCounts = this->executionStepsPerCycle[currentLane][currentTile];

            int oldestUnfinishedCycle = 0;
            for (unsigned long i = 1; i < stepCounts.size(); i++)
                if (stepCounts[i] != numberOfHooksPerTileCycle) {
                    oldestUnfinishedCycle = i;
                    break;
                }
            totalOldestUnfinishedCycle = max(totalOldestUnfinishedCycle, oldestUnfinishedCycle);
        }
    }

    return totalOldestUnfinishedCycle;
}

void MemoryManager::updateUsedCycleInterval(VertexIdentifier identifier){
    auto lanesToUpdate = (identifier.lane == this->graph->ALL_LANES) ? this->lanes : vector<uint16_t>{identifier.lane};
    auto tilesToUpdate = (identifier.tile == this->graph->ALL_TILES) ? this->tiles : vector<uint16_t>{identifier.tile};

    for (auto lane : lanesToUpdate) {
        for (auto tile : tilesToUpdate) {
            this->executionStepsPerCycle[lane][tile][identifier.cycle]++;

            // try increasing latest executed cycle
            this->baseManager->tryIncreasingLatestExecutedCycle(lane, tile, identifier.cycle);

            // try increasing oldest unfinished cycle
            int oldestUnfinishedCycle = getOldestUnfinishedCycle(lane, tile);
            this->baseManager->tryIncreasingOldestUnfinishedCycle(lane, tile, oldestUnfinishedCycle);
        }
    }
}

void MemoryManager::setLatestAvailableCycle(int cycleNumber) {
    this->latestAvailableCycle = cycleNumber;
}

int MemoryManager::getMaxRamSize(){
    return this->maxRamSize;
}
