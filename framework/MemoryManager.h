#ifndef LIVEKIT_MEMORYMANAGER_H
#define LIVEKIT_MEMORYMANAGER_H

#include <iostream>
#include <fstream>
#include <unistd.h>
#include <vector>

#include "fragments/FragmentContainer.h"
#include "basecalls/management/BaseManager.h"
#include "execution_planning/ExecutionGraph.h"

class BaseManager;
class ExecutionGraph;

class MemoryManager {
private:
    BaseManager *baseManager;
    ExecutionGraph *graph;

    std::vector<uint16_t> lanes;
    std::vector<uint16_t> tiles;
    int maxRamSize;

    std::mutex canBeSerializedMutex;
    std::set<std::shared_ptr<Fragment>> canBeSerialized;

    std::atomic<int> allTilesUsedCount;

    std::mutex serializationMutex;

    /**
     * This map contains information about how many execution-steps are already executed for each cycle (broken down by tile).
     * These information are used to calculate the oldest cycle for a tile which is not fully processed.
     * After a cycle is full processed, the number stored in 'executionStepsPerCycle[lane][tile][cycle]' is numPlugins*2,
     * because each plugin is executed with 'runCycle' and 'runCycleForTile'.
     * The index "0" represents "preprocessing", index "1" represents the first cycle, ...
     */
    std::map<uint16_t, std::map<uint16_t, std::vector<uint16_t>>> executionStepsPerCycle;

    int latestAvailableCycle;

    /**
     * Reads the 'virtual memory size' and the 'resident set size' of the current process
     * @return a pair { first: 'virtual memory size', second: resident set size' }
     */
    std::pair<double, double> processMemoryUsage();


    void incrementCurrentlyUsedCount(std::shared_ptr<Fragment> &fragment);

    void decrementCurrentlyUsedCount(std::shared_ptr<Fragment> &fragment);

    /**
     * Uses 'executionStepsPerCycle' to calculate the oldest unfinished cycle
     * @param lane
     * @param tile
     * @return oldest unfinished cycle
     */
    uint16_t getOldestUnfinishedCycle(uint16_t lane, uint16_t tile);

    /**
     * Uses malloc_trim to give unused memory back to the Operating System
     */
    void trim();

public:
    std::map<uint16_t, std::map<uint16_t, std::atomic<int>>> currentlyUsedTiles;
    std::map<uint16_t, std::map<uint16_t, std::map<uint16_t, std::atomic<int>>>> currentlyUsedCycles;

    /**
    * Holds one fragment container for every plugin.
    */
    std::vector<std::shared_ptr<FragmentContainer>> preparedInputContainer;

    MemoryManager(BaseManager *baseManager, ExecutionGraph *graph, std::vector<uint16_t> &lanes, std::vector<uint16_t> &tiles,
                           int maxRamSize);

    /**
     * RSS ('resident set size') specifies the number of pages the process has in real memory. We already multiply this
     * with the size of one page to get the size in kilobytes.
     * @return rss in Mb (Mebibytes)
     */
    double getRSS();

    /**
     * VSize ('virtual memory size') describes the amount of memory that the process has access to.
     * This including memory that is swapped out, memory that is allocated, but not used, and memory
     * that is from shared libraries.
     * @return virtual memory size in Mb (Mebibytes)
     */
    double getVSize();

    double getHeapSize();

    /**
     * Calculates the amount of data that is serialized for a given VertexIdentifier
     * This includes: inputFragements, permanentFragments and the sequence data
     * The size is 0, if all data of the given VertexIdentifier is already in memory
     * @param identifier
     * @return
     */
    unsigned long getSerializedSizeForVertexIdentifier(ExecutionGraph::Vertex vertex, VertexIdentifier identifier);

    /**
     * Calculates the amount of free memory until the RAM limit is met
     * If more memory is being used as allowed, the return value is negative
     * @return amount of free memory in Mb (Mebibytes)
     */
    double getFreeMemory();

    /**
     * Returns true if the currently used memory is under the defined 'ramSize'-limit.
     * Otherwise it returns false.
     * @return
     */
    bool hasFreeMemory();

    /**
     * Goes through all fragments and sequence data that is currently not in use
     * and serializes it until the memory usage is below the limit.
     * @param serializeEverything flag, whether serialization should be continued even if memory usage is below the limit.
     */
    void serializeUnusedData(bool serializeEverything = false);

    /**
     * Declares all the dependencies (inputFragments, permantentFragments and sequence Data) of an execution step as
     * in use, to prevent it from being serialized by the MemoryManager.
     * @param vertex the vertex of the ExecutionGraph whose dependencies should be declared as in use.
     */
    void markAllDependenciesAsUsed(ExecutionGraph::Vertex vertex);

    /**
     * Declares all the dependencies (inputFragments, permantentFragments and sequence Data) of an execution step as
     * in unused by this execution step, to allow the MemoryManager to serialize them if necessary.
     * (unless the dependencies are being used by another execution step)
     * @param vertex the vertex of the ExecutionGraph whose dependencies should be declared as not used by this step.
     */
    void markAllDependenciesAsUnused(ExecutionGraph::Vertex vertex);

    /**
     * Informs the MemoryManager about the existence of a fragment that can be serialized, since it is currently not used
     * @param fragment that can be serialized
     */
    void addToCanBeSerialized(std::shared_ptr<Fragment> &fragment);

    /**
     * Checks if (after the execution of the specified 'identifier') the 'latestExecutedCycle' or the 'oldestUnfinishedCycle' have changed.
     * If they have changed the cycles are updated.
     * Increments 'executionStepsPerCycle' for the specified cycle at the correct tiles, before the cycles are checked.
     * @param identifier the identifier of the executionStep that finished execution
     */
    void updateUsedCycleInterval(VertexIdentifier identifier);

    /**
     * Returns the maximum allowed ram size in MB (this is a soft limit)
     * @return
     */
    int getMaxRamSize();

    void setLatestAvailableCycle(int cycleNumber);
};


#endif //LIVEKIT_MEMORYMANAGER_H
