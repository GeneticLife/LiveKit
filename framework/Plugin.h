#ifndef LIVEKIT_PLUGIN_H
#define LIVEKIT_PLUGIN_H

#include <exception>
#include <iostream>
#include <string>
#include <map>

#include "configuration/Configurable.h"
#include "FrameworkInterface.h"
#include "fragments/Fragment.h"
#include "fragments/FragmentContainer.h"
#include "execution_planning/PluginSpecification.h"
#include "TileBasedFrameworkInterface.h"
#include "output_management/OutputWrapper.h"

class SequenceContainer;
class OutputWrapper;

class Plugin : public Configurable {
protected:
    /// pointer to the FrameworkInterface, which can be used to access framework functions
    FrameworkInterface *framework = nullptr;

public:

    /** To enable parallelization and for convenience, the TileContext class acts as a wrapper for the execution of the
     *  runCycleForTile hook. Inside the TileContext, tile-specific data is encapsulated from the plugin.
     *  To implement a TileContext for a concrete Plugin, create a class that inherits from TileContext and overrides
     *  the runCycleForTile method. Use the LIVEKIT_TILE_CONTEXT(CONTEXT_CLASS) - preprocessor macro!
     */
    class TileContext {
    public:
        /// A reference to the plugin, to access config entries as well as the permanentFragment of the plugin
        Plugin *plugin = nullptr;

        /** Every TileContext receives its own TileBasedFrameworkInterface, that has the information about lane, tile
         *  and cycle already bound to itself. When accessing bcl Data, the parameters for lane and tile can be omitted.
         */
        TileBasedFrameworkInterface *framework = nullptr;

        /// The lane and tile, that this TileContext should process
        uint16_t lane = 0, tile = 0;

        /// A reference to the tile-based plugin of this TileContext
        std::shared_ptr<Fragment> tileFragment = nullptr;

        /**
         * Output which is piped into this stream will be printed in the tab (tile) of this plugin in the TUI.
         * If the TUI is disabled, the content will be printed by "cout".
         */
        OutputWrapper out;

    public:
        /**
         * This method works as a 'life cycle hook' which is called by the framework in a defined order.
         * A plugin developer can choose to override this function and implement the respective actions which should be carried out in this step.
         * This method gets called every cycle and should be used for cycle-wise computations.
         * Each call of this function should only do the processing of one tile of one lane for one cycle.
         * Using this method instead of runCycle can improve performance, since the framework cares about paralellization and
         * scheduling. The framework automatically tries to execute these function calls in the most efficient order.
         * If you want to return no Fragment%s, return an empty FragmentContainer: `this->framework->createNewFragmentContainer()`.
         * @param inputFragments pointer on a FragmentCointainer containing the input Fragment%s of the Plugin. Modify the inputFragments wisely because they might be used elsewhere.
         * @return FragmentContainer containing the output Fragment%s of the Plugin
         */
        virtual std::shared_ptr<FragmentContainer> runCycleForTile(std::shared_ptr<FragmentContainer> inputFragments) {
            (void) inputFragments;
            return this->framework->createNewFragmentContainer();
        };

        virtual ~TileContext() = default;
    };

    /**
     * This method can be overwritten by Plugins if they want to implement the `runCycleForTile`-hook.
     * If the Plugin uses the macro `LIVEKIT_TILE_CONTEXT` this method will be automatically overwritten so that is returns true.
     * @return
     */
    virtual bool hasTileContext() { return false; };

    /**
     * A factory method that returns a new instance of a tile context is required so the framework can execute these
     * TileContexts. When creating a TileContext for a concrete Plugin, this method is automatically created when using
     * the LIVEKIT_TILE_CONTEXT(CONTEXT_CLASS) preprocessor macro!
     * @return TileContext a new Instance of the TileContext that belongs to this plugin.
     */
    virtual TileContext *getTileContext() {
        return new TileContext();
    }

    /// pointer to the permanent Fragment, which can be used to maintain local state
    std::shared_ptr<Fragment> permanentFragment = nullptr;

    /// this map stores the permanentFragments for each tile, so every runCycleForTile call has its own fragment
    std::map<uint16_t, std::map<uint16_t, std::shared_ptr<Fragment>>> tileFragments;

    // TODO: Documentation
    std::map<uint16_t, std::map<uint16_t, OutputWrapper>> tileOutputs;

    /// pointer to the PluginSpecification, which is used to describe inputFragments and outputFragments
    PluginSpecification *specification = nullptr;

    /**
     * Output which is piped into this stream will be printed in the tab (main) of this plugin in the TUI.
     * If the TUI is disabled, the content will be printed by "cout".
     */
    OutputWrapper out;

    /**
     * This method sets the framework pointer to the current instance of the FrameworkInterface.
     * This gets called before the init() function so you can use it there.
     */
    void setFramework(FrameworkInterface *frameworkInterface) {
        this->framework = frameworkInterface;
    };

    /**
     * This method can be used to register config entries to use later on in the Plugin.
     * This is done by registerConfigEntry() and you can only use registered config entries.
     */
    void setConfig() override {};

    /**
     * This method works as a 'life cycle hook' which is called by the Framework in a defined order.
     * A plugin developer can choose to override this function and implement the respective actions which should be carried out in this step.
     * In this step the Plugin should run its initialization routine.
     * This includes setting up the permanent Fragment.
     */
    virtual void init() {};

    /**
     * This method works as a 'life cycle hook' which is called by the framework in a defined order.
     * A plugin developer can choose to override this function and implement the respective actions which should be carried out in this step.
     * This method gets called before any reads are processed.
     * This method can only access output%Fragment%s of other preprocessing steps.
     * If you want to return no Fragment%s, return an empty FragmentContainer: `this->framework->createNewFragmentContainer()`.
     * @param inputFragments pointer on a FragmentContainer containing the input Fragment%s of the Plugin. Modify the inputFragments wisely because they might be used elsewhere.
     * @return FragmentContainer containing the output Fragment%s of the Plugin
     */
    virtual std::shared_ptr<FragmentContainer> runPreprocessing(std::shared_ptr<FragmentContainer> inputFragments) {
        (void) inputFragments;
        return this->framework->createNewFragmentContainer();
    };

    /**
     * This method works as a 'life cycle hook' which is called by the framework in a defined order.
     * A plugin developer can choose to override this function and implement the respective actions which should be carried out in this step.
     * This method gets called every cycle and should be used for cycle-wise computations.
     * If you want to return no Fragment%s, return an empty FragmentContainer: `this->framework->createNewFragmentContainer()`.
     * @param inputFragments pointer on a FragmentCointainer containing the input Fragment%s of the Plugin. Modify the inputFragments wisely because they might be used elsewhere.
     * @return FragmentContainer containing the output Fragment%s of the Plugin
     */
    virtual std::shared_ptr<FragmentContainer> runCycle(std::shared_ptr<FragmentContainer> inputFragments) {
        (void) inputFragments;
        return this->framework->createNewFragmentContainer();
    };

    /**
     * This method works as a 'life cycle hook' which is called by the framework in a defined order.
     * A plugin developer can choose to override this function and implement the respective actions which should be carried out in this step.
     * This method gets called after all cycles are finished and the full read can be processed.
     * This method can still access Fragment%s of the last cycle and output%Fragment%s of other fullReadPostprocessing steps.
     * If you want to return no Fragment%s, return an empty FragmentContainer: `this->framework->createNewFragmentContainer()`.
     * @param inputFragments pointer on a FragmentContainer containing the input Fragment%s of the Plugin. Modify the inputFragments wisely because they might be used elsewhere.
     * @return FragmentContainer containing the output Fragment%s of the Plugin
     */
    virtual std::shared_ptr<FragmentContainer> runFullReadPostprocessing(std::shared_ptr<FragmentContainer> inputFragments) {
        (void) inputFragments;
        return this->framework->createNewFragmentContainer();
    };

    /**
     * This method works as a 'life cycle hook' which is called by the framework in a defined order.
     * A plugin developer can choose to override this function and implement the respective actions which should be carried out in this step.
     * In this step the Plugin should run its cleanup routine and finalize its activity.
     */
    virtual void finalize() {};

    /**
     * This method can be overwritten by a concrete Plugin to handle low memory situations.
     * An idea might be to write (intermediate) results on the disk instead of keeping them in memory. This would potentially take longer but use less RAM.
     * This feature is optional, therefore this method does not have to be overwritten.
     */
    virtual void switchToMemorySaveMode() {};

    /**
     * Different plugins can be in different cycles and therefore get different data when calling the FrameworkInterface
     * The PluginInterface need to know, in which cycle the Plugin is to return the correct data
     * The Framework calls this method before executing the pluginHook (e.g. runCycle) to set the correct cycle.
     * @param cycle
     */
    virtual void setCurrentPluginCycle(int cycle){
        this->framework->setCurrentPluginCycle(cycle);
        this->out.setCycle(cycle);
    }

    virtual ~Plugin() {
        delete this->framework;
    };
};


// TODO: Try to automate the creation of the create and destroy function by using the LIVEKIT_PLUGIN macro
//    extern "C" CLASSNAME *create() { return new CLASSNAME; }
//    extern "C" void destroy(CLASSNAME *plugin) { delete plugin; }

// Provides a macro for the class definition of a new plugin
#define LIVEKIT_PLUGIN(CLASSNAME) \
    class CLASSNAME : public Plugin

// Provides a macro for the class definition of a new TileContext of a concrete plugin
// The TileContext of a plugin should be defined inside the class definition of the plugin itself.
// To allow the framework to create a new instance of a TileContext for a concrete Plugin, the plugin has to provide
// the factory method 'getTileContext()'. The implementation of this factory method can be omitted, when using this
// macro since it automatically creates the implementation.
#define LIVEKIT_TILE_CONTEXT(CONTEXT_CLASS) \
    bool hasTileContext() override { return true; } \
    TileContext *getTileContext() override { \
        return new CONTEXT_CLASS(); \
    }; \
    class CONTEXT_CLASS : public Plugin::TileContext


#endif //LIVEKIT_PLUGIN_H
