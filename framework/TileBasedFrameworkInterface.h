#ifndef LIVEKIT_TILEBASEDFRAMEWORKINTERFACE_H
#define LIVEKIT_TILEBASEDFRAMEWORKINTERFACE_H

#include "FrameworkInterface.h"

/**
 * The TileBasedFrameworkInterface acts as a specialization of the FrameworkInterface.
 * It provides a superset of it's functionality. Unlike the FrameworkInterface,
 * the TileBasedFrameworkInterface also stores the information about a lane and a tile.
 * All interface methods of the FrameworkInterface that require the selection of a tile have counterparts in the
 * TileBasedFrameworkInterface where lane and tile of the specific instance are already hardcoded.
 */
class TileBasedFrameworkInterface : public FrameworkInterface {
    uint16_t lane, tile;
public:
    TileBasedFrameworkInterface(Framework *framework, int cycle, uint16_t lane, uint16_t tile)
    : FrameworkInterface(framework)
    , lane(lane)
    , tile(tile) { this->setCurrentPluginCycle(cycle); }

    // Mark member function of parent class as used to suppress compiler warnings
    using FrameworkInterface::getSequenceIterators;
    virtual SequenceIterators getSequenceIterators(int sequence) { return this->getSequenceIterators(this->lane, this->tile, sequence); };

    using FrameworkInterface::getSequence;
    virtual Read getSequence(int sequence) { return this->getSequence(this->lane, this->tile, sequence); };

    using FrameworkInterface::getRead;
    virtual Read getRead(int sequence, int readId) { return this->getRead(this->lane, this->tile, sequence, readId); };

    using FrameworkInterface::getReadIterators;
    virtual ReadIterators getReadIterators(int sequence, int readId) { return this->getReadIterators(this->lane, this->tile, sequence, readId); };

    using FrameworkInterface::getMostRecentBcl;
    virtual const BCL &getMostRecentBcl() { return this->getMostRecentBcl(this->lane, this->tile); }

    using FrameworkInterface::getMostRecentBcls;
    virtual std::deque<BCL> getMostRecentBcls() { return this->getMostRecentBcls(lane); };

    using FrameworkInterface::filterBasecall;
    virtual bool filterBasecall(unsigned long position) { return this->filterBasecall(this->lane, this->tile, position); };

    using FrameworkInterface::getFilterData;
    virtual std::vector<char>& getFilterData() { return this->getFilterData(this->lane, this->tile); };

    using FrameworkInterface::getNumSequences;
    virtual uint32_t getNumSequences() { return this->getNumSequences(lane, tile); };

    virtual ~TileBasedFrameworkInterface() = default;
};


#endif //LIVEKIT_TILEBASEDFRAMEWORKINTERFACE_H
