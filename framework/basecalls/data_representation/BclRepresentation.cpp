#include <utility>

#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include "BclRepresentation.h"

using namespace std;

BclRepresentation::BclRepresentation(vector<uint16_t> &lanes, vector<uint16_t> &tiles) {
    // initialize the empty datastructure
    for (auto lane : lanes) {
        this->bclData[lane] =  map<uint16_t, map<uint16_t, BCL>>();
        for (auto tile : tiles) {
            this->bclData[lane][tile] =  map<uint16_t, BCL>();
        }
    }
};

const BCL &BclRepresentation::getBcl(uint16_t lane, uint16_t tile, uint16_t cycle) {
    if(this->bclData[lane][tile].count(cycle) == 0) {
        string errorMessage =
                "The BCL representation does not store the BCL of the requested cycle ( " + to_string(cycle) + " ).";
        __throw_out_of_range(errorMessage.c_str());
    }

    return this->bclData[lane][tile][cycle];
}

void BclRepresentation::addBcl(uint16_t lane, uint16_t tile, uint16_t cycle, BCL& newBcl) {
    this->bclData[lane][tile][cycle] = move(newBcl);
    this->isBclSerialzied[lane][tile][cycle] = false;
}

uint32_t BclRepresentation::getBclSize(uint16_t lane, uint16_t tile) {
    return (*this->bclData[lane][tile].begin()).second.size();
}

bool BclRepresentation::isSerialized(uint16_t lane, uint16_t tile, uint16_t cycle) {
    return this->isBclSerialzied[lane][tile][cycle];
}

void BclRepresentation::serialize(uint16_t lane, uint16_t tile, uint16_t cycle) {
    cout << "Serialize BCL (lane " + to_string(lane) + ", tile " + to_string(tile) + ", cycle " + to_string(cycle) + ")" << endl;
    this->bclData[lane][tile].erase(cycle);
    this->isBclSerialzied[lane][tile][cycle] = true;
}
