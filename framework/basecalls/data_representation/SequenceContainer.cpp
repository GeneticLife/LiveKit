#include <cstring>
#include <fstream>
#include "SequenceContainer.h"

using namespace std;

#define revtwobit_repr(n) ((n) == 0 ? 'A' : \
                           (n) == 1 ? 'C' : \
                           (n) == 2 ? 'G' : 'T')


Sequence::Sequence(char *serializedSpace) {
    this->deserialize(serializedSpace);
}

void Sequence::serialize(char *serializedSpace) {
    unsigned long size = this->serializableSize();
    SERIALIZE_VALUE(size);

    unsigned long sequenceLength = this->sequence.size();
    SERIALIZE_VALUE(sequenceLength);

    // serialize the data of the reads
    memcpy(serializedSpace, this->sequence.data(), this->sequence.size());
    //serializedSpace += this->sequence.size();

    this->sequence.clear();
    this->sequence.shrink_to_fit();
}

unsigned long Sequence::serializableSize() {
    unsigned long size = 0;
    size += sizeof(unsigned long); // for storing the total size
    size += sizeof(unsigned long); // for storing the sequenceLength
    size += this->sequence.size() * sizeof(uint8_t);

    return size;
}

void Sequence::deserialize(char *serializedSpace) {
    // make sure that old data is removed
    this->sequence.clear();

    unsigned long size;
    DESERIALIZE_VALUE(size);

    unsigned long sequenceLength;
    DESERIALIZE_VALUE(sequenceLength);

    this->sequence.resize(sequenceLength);

    memcpy(this->sequence.data(), serializedSpace, sequenceLength);
    //serializedSpace += readLength;
}

bool Sequence::operator==(Sequence s) const {
    return this->sequence == s.sequence;
}

void Sequence::appendNucletide(uint8_t nucleotide) {
    this->sequence.push_back(nucleotide);
}

void SequenceContainer::serialize(char *serializedSpace) {
    // TODO: consider maxReadLength
    unsigned long size = this->serializableSize();
    unsigned long bufferPosition = 0;

    memcpy(serializedSpace, &size, sizeof(unsigned long));
    bufferPosition += sizeof(unsigned long);

    // serialize each sequence
    for(auto &sequence: this->sequences){
        // we need to store the size here, because the data could be cleared after it is serialized
        unsigned long sequenceSize = sequence.serializableSize();
        sequence.serialize(serializedSpace + bufferPosition);
        bufferPosition += sequenceSize;
    }

    this->sequences.clear();
    // Since 'clear' doesn't reduce the actual capacity of the vector,
    // 'shrink_to_fit' is called to give back memory to the operating system.
    this->sequences.shrink_to_fit();

    this->isSerialized = true;
}

void SequenceContainer::deserialize(char *serializedSpace) {
    // clear old data
    this->sequences.clear();

    unsigned long size = 0;
    unsigned long bufferPosition = 0;

    memcpy(&size, serializedSpace, sizeof(unsigned long));
    bufferPosition += sizeof(unsigned long);

    while(bufferPosition < size){
        unsigned long sequenceSize = 0;
        memcpy(&sequenceSize, serializedSpace + bufferPosition, sizeof(unsigned long));

        auto tempSequence = Sequence(serializedSpace + bufferPosition);
        this->sequences.push_back(tempSequence);

        bufferPosition += sequenceSize;
    }

    this->isSerialized = false;
}

void SequenceContainer::extendSequence(unsigned long sequenceNumber, char nucleotide) {
    this->sequences[sequenceNumber].appendNucletide(nucleotide);
}

void SequenceContainer::extendSequences(const vector<char> &nucleotides) {
    lock_guard<mutex> lock(this->serializationMutex);

    for (unsigned long i = 0; i < nucleotides.size(); i++) {
        this->extendSequence(i, nucleotides[i]);
    }
}

unsigned long SequenceContainer::serializableSize() {
    unsigned long size = 0;
    size += sizeof(unsigned long); // for storing the totalLength at the beginning

    for (auto &sequence : this->sequences) // this->sequences.size() == number of sequences per tile
        size += sequence.serializableSize();

    return size;
}

SequenceIterators SequenceContainer::getSequenceIterators(int index, int maxCycle) {
    if(maxCycle > this->sequences.at(index).sequence.size()){
        string errMsg = "The sequence is shorter than " + to_string(maxCycle) + " nucleotides";
        __throw_out_of_range(errMsg.c_str());
    }

    auto &seq = this->sequences.at(index);

    auto startPosition = seq.sequence.begin();
    auto endPosition = seq.sequence.begin() + maxCycle;

    return make_pair(startPosition, endPosition);
}


Read SequenceContainer::getSequence(int index, int cycle) {
    return Read(this->sequences[index].sequence.begin(), this->sequences[index].sequence.begin() + cycle);
}

Read SequenceContainer::getRead(int sequence, int readId, int maxCycle) {
    auto iterators = this->getReadIterators(sequence, readId, maxCycle);
    return Read(iterators.first, iterators.second);
}

ReadIterators SequenceContainer::getReadIterators(int sequence, int readId, int maxCycle) {
    if(readId >= this->maxReadLength.size()){
        string errMsg = "The sequence does not have a read with the readId" + to_string(readId);
        __throw_out_of_range(errMsg.c_str());
    }

    auto &seq = this->sequences[sequence];
    int startIndex = 0;

    for(int i = 0; i < readId; i++) {
        startIndex += this->maxReadLength[i];
    }

    int length = min((uint16_t)(maxCycle - startIndex), this->maxReadLength[readId]);

    startIndex = min(startIndex, (int)seq.sequence.size());
    auto endIndex = min(startIndex + length, (int)seq.sequence.size());
    auto startPosition = seq.sequence.begin() + startIndex;
    auto endPosition = seq.sequence.begin() + endIndex;

    return make_pair(startPosition, endPosition);
}

void SequenceContainer::setSize(int size) {
    this->sequences.resize(size, Sequence());
}

void SequenceContainer::setReadStructure(const vector<pair<int, char>>& readstructure) {
    for (auto pair : readstructure)
        this->maxReadLength.push_back(pair.first);
}
