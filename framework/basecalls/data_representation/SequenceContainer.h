#ifndef LIVEKIT_SEQUENCEREPRESENTATION_H
#define LIVEKIT_SEQUENCEREPRESENTATION_H

#include <vector>
#include <string>
#include <algorithm>
#include <mutex>
#include <iostream>

#include "../../Serializable.h"

/**
 * Container for all bases and qualities of a single read.
 * This `Read` gets extended each cycle.
 */
typedef std::vector<uint8_t> Read;

/**
 * Start and end-iterator of a Read
 */
typedef  std::pair<Read::const_iterator, Read::const_iterator> ReadIterators;

/**
 * Start and end-iterator of a Sequence
 */
typedef  std::pair<std::vector<uint8_t>::const_iterator, std::vector<uint8_t>::const_iterator> SequenceIterators;

class Sequence {
private:

    /**
     * Contains all nucleotides of this sequence.
     */
    Read sequence;

    friend class SequenceContainer;

public:


    explicit Sequence() = default;

    /**
     * Create a Sequence based on the state (serialized data) of a serialized Sequence
     * This is needed because for serialization and deserialization of the SequenceContainer
     * @param serializedSpace is a pointer to the serialized Data
     */
    explicit Sequence(char *serializedSpace);

    bool operator==(Sequence s) const;

    void appendNucletide(uint8_t nucleotide);

    void serialize(char *serializedSpace);

    void deserialize(char *serializedSpace);

    unsigned long serializableSize();

    virtual ~Sequence() = default;
};

/**
 * A `SequenceContainer` object holds all reads for a single tile and exposes interaction possibilities as well as a serialize interface.
 */
class SequenceContainer {
    // TODO: Inherit from Serializable - Interface
private:

    std::vector<Sequence> sequences;

    std::vector<uint16_t> maxReadLength;

    void extendSequence(unsigned long sequenceNumber, char nucleotides);

#ifdef TEST_DEFINITIONS
    friend class SequenceContainerAccessHelper;
#endif

public:

    std::mutex serializationMutex;
    bool isSerialized;

    SequenceContainer() : isSerialized(false) {};

    /**
     * Calls extendSequence for each sequence in sequences with the associated nucleotide
     * @param nucleotide is a vector<char> where each char is a new nucleotide for this specific tile. (nucleotide.size() == sequences.size())
     */
    void extendSequences(const std::vector<char> &nucleotide);

    void serialize(char *serializedSpace);

    void deserialize(char *serializedSpace);

    virtual SequenceIterators getSequenceIterators(int index, int maxCycle);

    virtual Read getSequence(int index, int cycle);

    Read getRead(int sequence, int readId, int maxCycle);

    ReadIterators getReadIterators(int sequence, int readId, int maxCycle);

    /**
     * Sets the size of the vector which holds the sequences and initialize each sequence
     * @param size specifies how many sequences this tile has
     */
    void setSize(int size);

    void setReadStructure(const std::vector<std::pair<int, char>>& readstructure);

    unsigned long serializableSize();

    virtual ~SequenceContainer() = default;
};

#endif //LIVEKIT_SEQUENCEREPRESENTATION_H
