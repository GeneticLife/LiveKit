#include <utility>

#include "../parser/AggregatedBclParser.h"
#include "../parser/CompressedBclParser.h"
#include "../parser/BgzfBclParser.h"

#include "BaseManager.h"

using namespace std;

#define revtwobit_repr(n) ((n) == 0 ? 'A' : \
                           (n) == 1 ? 'C' : \
                           (n) == 2 ? 'G' : 'T')


BaseManager::BaseManager(
        string baseCallRootDirectory,
        vector<uint16_t> lanes,
        vector<uint16_t> tiles,
        const string& bclParserType,
        vector<pair<int, char>> readStructure
    ) :
          baseCallRootDirectory(move(baseCallRootDirectory)),
          lanes(std::move(lanes)),
          tiles(std::move(tiles)),
          bclData(this->lanes, this->tiles),
          filterData(this->lanes, this->tiles),
          filterParser(this->lanes, this->tiles, &filterData),
          readStructure(std::move(readStructure)),
          sequenceManager(this->lanes, this->tiles, this->readStructure),
          latestAvailableCycle(0) {
    if (bclParserType == "BclParser") {
        this->bclParser = new BclParser(&bclData);
    } else if (bclParserType == "CompressedBclParser"){
        this->bclParser = new CompressedBclParser(&bclData);
    } else if (bclParserType == "AggregatedBclParser") {
        this->bclParser = new AggregatedBclParser(&bclData, &filterData);
    } else if (bclParserType == "BgzfBclParser") {
        this->bclParser = new BgzfBclParser(&bclData);
    } else {
        throw std::runtime_error("Wrong BclParser configured");
    }

    for (auto lane : this->lanes)
        for (auto tile : this->tiles)
            this->usedCycleInterval[lane][tile] =  {1, 1, 1};
}

void BaseManager::parseFirstCycle() {
    for (auto &lane : lanes) {
        for (auto &tile : tiles) {
            this->bclParser->parse(lane, tile, 1, this->baseCallRootDirectory);

            uint32_t bclSize = bclData.getBclSize(lane, tile);
            sequenceManager.getSequences(lane, tile).setSize(bclSize);
            filterParser.parse(lane, tile, this->baseCallRootDirectory, bclSize);
            if (this->filterData.getNumSequences(lane, tile) != bclSize)
                cerr << "The filter file in lane "
                     << to_string(lane) << ", tile " << to_string(tile)
                     << " has a different number of reads as the corresponding bcl file in the first cycle."
                     << endl;

            const BCL& lastBaseCall = bclData.getBcl(lane, tile, 1);

            // TODO: The sequenceManager takes a significant part of the BaseManger-runtime - can this be improved?
            sequenceManager.getSequences(lane, tile).extendSequences(lastBaseCall);
        }
    }
}

void BaseManager::parseAllTiles() {
    for (auto &lane : lanes)
        for (auto &tile : tiles)
            parseTile(lane, tile);
}

void BaseManager::setLatestAvailableCycle(uint16_t cycle) {
    this->latestAvailableCycle = cycle;
}

void BaseManager::parseTile(uint16_t lane, uint16_t tile) {
    lock_guard<mutex> lock(parseMutexes[lane][tile]);

    auto &cycleState = this->usedCycleInterval[lane][tile];
    while (cycleState.parsed < cycleState.latest + 1) {
        uint16_t cycleToParse = cycleState.parsed + 1;
        if (this->latestAvailableCycle < cycleToParse) break;
        this->bclParser->parse(lane, tile, cycleToParse, this->baseCallRootDirectory);
        const BCL &latestBaseCall = bclData.getBcl(lane, tile, cycleToParse);
        sequenceManager.getSequences(lane, tile).extendSequences(latestBaseCall);
        cycleState.parsed++;
    }
}

SequenceContainer &BaseManager::getSequences(uint16_t lane, uint16_t tile) {
    lock_guard<mutex> lock(parseMutexes[lane][tile]);
    return this->sequenceManager.getSequences(lane, tile);
}

SequenceIterators BaseManager::getSequenceIterators(uint16_t lane, uint16_t tile, int sequence, int cycle) {
    lock_guard<mutex> lock(parseMutexes[lane][tile]);
    return this->sequenceManager.getSequences(lane, tile).getSequenceIterators(sequence, cycle);
}

Read BaseManager::getSequence(uint16_t lane, uint16_t tile, int sequence, int cycle) {
    lock_guard<mutex> lock(parseMutexes[lane][tile]);
    return this->sequenceManager.getSequences(lane, tile).getSequence(sequence, cycle);
}

Read BaseManager::getRead(uint16_t lane, uint16_t tile, int sequence, int readId, int cycle) {
    lock_guard<mutex> lock(parseMutexes[lane][tile]);
    return this->sequenceManager.getSequences(lane, tile).getRead(sequence, readId, cycle);
}

ReadIterators BaseManager::getReadIterators(uint16_t lane, uint16_t tile, int sequence, int readId, int cycle) {
    lock_guard<mutex> lock(parseMutexes[lane][tile]);
    return this->sequenceManager.getSequences(lane, tile).getReadIterators(sequence, readId, cycle);
}

const BCL &BaseManager::getBcl(uint16_t lane, uint16_t tile, uint16_t cycle) {
    lock_guard<mutex> parseLock(parseMutexes[lane][tile]);
    lock_guard<mutex> serializationLock(this->bclData.serializationMutex);
    // The BCL might be serialized (parsing it again is the equivalent of 'deserialize')
    if(bclData.isSerialized(lane, tile, cycle))
        this->bclParser->parse(lane, tile, cycle, this->baseCallRootDirectory);
    return bclData.getBcl(lane, tile, cycle);
}

deque<BCL> BaseManager::getMostRecentBcls(uint16_t lane, uint16_t cycle) {

    // returns the most recent BCLs for each tile of a lane
    deque<BCL> mostRecent;
    for (auto &tileEntry : this->bclData.bclData[lane]) {
        lock_guard<mutex> lock(this->parseMutexes[lane][tileEntry.first]);
        if (!tileEntry.second.empty())
            mostRecent.push_back(tileEntry.second[cycle]);
    }

    return mostRecent;
}

bool BaseManager::filterBasecall(uint16_t lane, uint16_t tile, unsigned long position) {
    return 0 == this->filterData.getFilterDataForPosition(lane, tile, position);
}

vector<char>& BaseManager::getFilterData(uint16_t lane, uint16_t tile) {
    return this->filterData.getFilterData(lane, tile);
}

uint32_t BaseManager::getNumSequences(uint16_t lane, uint16_t tile) {
    return this->filterData.getNumSequences(lane, tile);
}

bool BaseManager::areSequencesOfTileSerialized(uint16_t lane, uint16_t tile) {
    return this->sequenceManager.areSequencesOfTileSerialized(lane, tile);
}

bool BaseManager::serializeSequencesOfTile(uint16_t lane, uint16_t tile) {
    lock_guard<mutex> lock(parseMutexes[lane][tile]);
    return this->sequenceManager.serializeSequencesOfTile(lane, tile);
}

bool BaseManager::serializeBCL(uint16_t lane, uint16_t tile, uint16_t cycle) {
    lock_guard<mutex> lock(this->bclData.serializationMutex);
    if(this->bclData.isSerialized(lane, tile, cycle)) return false;
    this->bclData.serialize(lane, tile, cycle);
    return true;
}

bool BaseManager::deserializeSequencesOfTile(uint16_t lane, uint16_t tile) {
    lock_guard<mutex> lock(parseMutexes[lane][tile]);
    return this->sequenceManager.deserializeSequencesOfTile(lane, tile);
}

void BaseManager::tryIncreasingOldestUnfinishedCycle(uint16_t lane, uint16_t tile, uint16_t newOldestUnfinishedCycle) {
    if(this->usedCycleInterval[lane][tile].oldest < newOldestUnfinishedCycle) {
        this->usedCycleInterval[lane][tile].oldest = newOldestUnfinishedCycle;
    }
}

void BaseManager::tryIncreasingLatestExecutedCycle(uint16_t lane, uint16_t tile, uint16_t newLatestExecutedCycle) {
    if(this->usedCycleInterval[lane][tile].latest < newLatestExecutedCycle) {
        this->usedCycleInterval[lane][tile].latest = newLatestExecutedCycle;
        this->parseTile(lane, tile);
    }
}

BaseManager::~BaseManager() {
    delete this->bclParser;
}

uint16_t BaseManager::globalLatestExecutedCycle() {
    uint16_t globalLatestExecutedCycle = 0;
    for (auto& tileMapEntry : this->usedCycleInterval)
        for (auto pairEntry : tileMapEntry.second)
            globalLatestExecutedCycle = std::max(pairEntry.second.latest, globalLatestExecutedCycle);

    return globalLatestExecutedCycle;
}

vector<char> BaseManager::getSampleBarcode(Read barcode, vector<vector<string>> barcodeVector, vector<uint16_t> barcodeErrors, bool isSingleEnd) {

    int faultCounter = 0;
    bool isSampleBarcode = true;

    // Compare each barcode of the barcodeVector with the barcode of the current read
    for (int i = 0; i < (int) barcodeVector.size(); i++) {
        string concatedBarcode;
        if (isSingleEnd) {
            concatedBarcode = barcodeVector[i][0];
        }
        else {
            concatedBarcode = barcodeVector[i][0] + barcodeVector[i][1];
        }
        // Compare each base
        for (int j = 0; j < (int) barcode.size(); j++) {
            char base = barcode[j];
            if (base != concatedBarcode.at(j)) {
                faultCounter++;
            }

            int fragmentNumber = 0;
            if (j > (int)barcodeVector[i][0].size()) { fragmentNumber += 1;}

            if (faultCounter > barcodeErrors[fragmentNumber]) {
                isSampleBarcode = false;
                // Try the next barcode of the barcodeVector
                break;
            }
        }
        faultCounter = 0;
        if (isSampleBarcode) {
            vector<char> charBarcode(concatedBarcode.begin(), concatedBarcode.end());
            return charBarcode;
        }
        isSampleBarcode = true;
    }

    return {};
}
