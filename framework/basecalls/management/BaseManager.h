#ifndef LIVEKIT_BASEMANAGER_H
#define LIVEKIT_BASEMANAGER_H

#include <mutex>
#include <condition_variable>
#include "SequenceManager.h"
#include "../parser/BclParser.h"
#include "../data_representation/BclRepresentation.h"
#include "../../configuration/Configurable.h"
#include "../parser/FilterParser.h"

/**
 * The `BaseManager` bundles the interface of the `BclRepresentation` for data of newer cycles and the `SequenceManager` for complete sequences.
 * It also implements behavior to parse new BCL files and extend the `Sequences`.
 */
class BaseManager {
private:
    std::string baseCallRootDirectory;

    std::vector<uint16_t> lanes;

    std::vector<uint16_t> tiles;

    BclRepresentation bclData;

    FilterRepresentation filterData;

    FilterParser filterParser;

    BclParser *bclParser;

    std::vector<std::pair<int, char>> readStructure;

    SequenceManager sequenceManager;

    uint16_t latestAvailableCycle;

    typedef struct {
        uint16_t oldest;
        uint16_t latest;
        uint16_t parsed;
    } CycleState;

    std::map<uint16_t, std::map<uint16_t, std::mutex>> parseMutexes;

    friend class MemoryManager;
    friend class Framework;
public:
    BaseManager(
        std::string baseCallRootDirectory,
        std::vector<uint16_t> lanes,
        std::vector<uint16_t> tiles,
        const std::string& bclParserType,
        std::vector<std::pair<int, char>> readStructure
    );

    /**
     * This map contains for every tile
     * - the oldest unfinished cycle
     * - the latest executed cycle
     * - the latest parsed cycle
     * This specifies, which BCLs are currently used.
     * BCLs for cycles outside of this interval do not need to be held in memory (or do not need to be parsed yet).
     */
    std::map<uint16_t, std::map<uint16_t, CycleState>> usedCycleInterval;

    /**
     * Calls parseTile for all tiles of all lanes.
     */
    void parseAllTiles();

    /**
     * Extends BCL and Sequence data of one tile until one cycle past the latest cycle
     * that this tile was processed by any of the plugins. (Except the BCL data of that cycle is not yet available.)
     * @param lane
     * @param tile
     */
    void parseTile(uint16_t lane, uint16_t tile);

    /**
     * Informs the BaseManager about the latest cycle of BCL data by the sequencer that is available on the disk.
     * This is neccessary to prevent the BaseManager from trying to parse cycle files, that are not yet available.
     * @param cycle the latest available cycle
     */
    void setLatestAvailableCycle(uint16_t cycle);

    /**
     * Processes the first cycle of BCL data for every tile of every lane. The first parsing cycle is different from the
     * other ones, because is sets the size of the sequences and also parses the filter data.
     */
    virtual void parseFirstCycle();


    virtual SequenceContainer &getSequences(uint16_t lane, uint16_t tile);

    /**
     * Returns a reference to a vector of uint8_t which contains all parsed nucleotides of the specified sequence
     * @param lane
     * @param tile
     * @param sequence
     * @return
     */
    virtual SequenceIterators getSequenceIterators(uint16_t lane, uint16_t tile, int sequence, int cycle);

    /**
     * Get a single sequence (as a vector of uint8_t) of a specific tile.
     * The returned sequence has a size of 'cycle'. In case more cycles have benn parsed, the additional data is cut off
     * @param lane The lane number
     * @param tile The tile number
     * @param read The read number starting at 0
     * @return Sequence Object which holds all reads of the tile
     */
    virtual Read getSequence(uint16_t lane, uint16_t tile, int sequence, int cycle);

    /**
     * Returns the specified Read (as a vector of uint8_t)
     * Example: If the read-structure is 100R,4B,4B,100R this method would
     * return a Read with lenght 100 if the readId=0 is specified (and the first 100 cycles are processed).
     * @param lane
     * @param tile
     * @param sequence
     * @param readId
     * @param cycle
     * @return
     */
    Read getRead(uint16_t lane, uint16_t tile, int sequence, int readId, int cycle);

    // TODO: documentation
    ReadIterators getReadIterators(uint16_t lane, uint16_t tile, int sequence, int readId, int cycle);

    /**
     * Get a single BCL representation of a specific tile
     * @param lane The lane number
     * @param tile The tile number
     * @param cycle The cycle of the BCL data.
     * @return BCL representation with one base for each read on the tile
     */
    virtual const BCL &getBcl(uint16_t lane, uint16_t tile, uint16_t cycle);

    /**
     * Get the BCL data of one cycle for all tiles of one lane.
     * @param lane
     * @param cycle
     * @return
     */
    std::deque<BCL> getMostRecentBcls(uint16_t lane, uint16_t cycle);

    virtual bool filterBasecall(uint16_t lane, uint16_t tile, unsigned long position);

    virtual std::vector<char>& getFilterData(uint16_t lane, uint16_t tile);

    virtual uint32_t getNumSequences(uint16_t lane, uint16_t tile);

    bool areSequencesOfTileSerialized(uint16_t lane, uint16_t tile);

    bool deserializeSequencesOfTile(uint16_t lane, uint16_t tile);

    bool serializeSequencesOfTile(uint16_t lane, uint16_t tile);

    bool serializeBCL(uint16_t lane, uint16_t tile, uint16_t cycle);

    /**
     * Compare the sequenced barcode with the barcodeVector.
     * Does not use any baseManager data but belongs semantically to the baseManager.
     * @param barcode
     * @param barcodeVector
     * @param barcodeFaultTolerance
     * @param isSingleEnd
     * @return A sample barcode if the sequenced barcode maps on an barcodes from the barcodeVector and an empty vector if the barcode does not map on a sample barcode
     */
    virtual std::vector<char> getSampleBarcode(Read barcode, std::vector<std::vector<std::string>> barcodeVector, std::vector<uint16_t> barcodeFaultTolerance, bool isSingleEnd);

    /**
     * Updates 'usedCycleInterval' by setting the 'newOldestUnfinishedCycle' and remove all BCLs from even older cycles,
     * if 'newOldestUnfinishedCycle' is bigger than the previous value
     * (which are already fully processed) to save memory.
     * @param lane
     * @param tile
     * @param newOldestUnfinishedCycle
     * @param latestParsedCycle specifies the lates cycle in which an bcl is parsed
     */
    void tryIncreasingOldestUnfinishedCycle(uint16_t lane, uint16_t tile, uint16_t newOldestUnfinishedCycle);

    /**
     * Updates 'usedCycleInterval' by setting the 'newLatestExecutedCycle', if 'newLatestExecutedCycle' is bigger than the previous value.
     * @param lane
     * @param tile
     * @param newLatestExecutedCycle
     */
    void tryIncreasingLatestExecutedCycle(uint16_t lane, uint16_t tile, uint16_t newLatestExecutedCycle);

    /**
     * Returns the maximum latest executed cycle of all tiles
     * @return
     */
    uint16_t globalLatestExecutedCycle();

    virtual ~BaseManager();
};

#endif //LIVEKIT_BASEMANAGER_H
