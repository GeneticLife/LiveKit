#include <fstream>
#include <iostream>
#include <mutex>

#include "SequenceManager.h"

using namespace std;

SequenceManager::SequenceManager(const vector<uint16_t>& lanes, const vector<uint16_t>& tiles, vector<pair<int, char>> readstructure) {
    for (auto lane : lanes)
        for(auto tile : tiles){
            this->sequenceRepresentations[lane][tile]; // initializes the SequenceContainer by calling the default constructor
            this->sequenceRepresentations[lane][tile].setReadStructure(readstructure);
        }
}

SequenceContainer &SequenceManager::getSequences(uint16_t lane, uint16_t tile) {
    if(this->sequenceRepresentations[lane][tile].isSerialized)
        this->deserializeSequencesOfTile(lane, tile);
    return this->sequenceRepresentations[lane][tile];
}

bool SequenceManager::serializeSequencesOfTile(uint16_t lane, uint16_t tile) {
    lock_guard<mutex> lock(this->sequenceRepresentations[lane][tile].serializationMutex);
    if (this->areSequencesOfTileSerialized(lane, tile)) return false;
    cout << "Serializing Sequence of lane " + to_string(lane) + " and tile " + to_string(tile) << endl;

    ofstream outputFile;
    outputFile.open("./temp/sequences/" + to_string(lane) + "_" + to_string(tile));

    SequenceContainer &sequenceContainer = this->getSequences(lane, tile);
    auto size = sequenceContainer.serializableSize();
    char *serializedReads = (char *) malloc(size);
    sequenceContainer.serialize(serializedReads);
    outputFile.write(serializedReads, size);
    free(serializedReads);

    outputFile.close();

     return true;
}

bool SequenceManager::deserializeSequencesOfTile(uint16_t lane, uint16_t tile) {
    lock_guard<mutex> lock(this->sequenceRepresentations[lane][tile].serializationMutex);
    if (!this->areSequencesOfTileSerialized(lane, tile)) return false;
    cout << "Deserializing Sequence of lane " << lane << " and tile " << tile << endl;

    ifstream inputFile;
    inputFile.open("./temp/sequences/" + to_string(lane) + "_" + to_string(tile));

    // get length of file:
    inputFile.seekg (0, inputFile.end);
    int length = inputFile.tellg();
    inputFile.seekg (0, inputFile.beg);

    char* serializedSpace = new char[length];

    // read data as a block:
    inputFile.read(serializedSpace,length);

    SequenceContainer &sequenceContainer = this->sequenceRepresentations[lane][tile];
    sequenceContainer.deserialize(serializedSpace);

    delete[] serializedSpace;

    return true;
}

bool SequenceManager::areSequencesOfTileSerialized(uint16_t lane, uint16_t tile) {
    return this->sequenceRepresentations[lane][tile].isSerialized;
}
