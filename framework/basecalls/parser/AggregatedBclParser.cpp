#include "AggregatedBclParser.h"
#include "../../utils.h"

using namespace std;

CBCL_header AggregatedBclParser::load_CBCL_header(FILE *&f_stream) {

    // Set pointer position to the beginning of the file
    fseek(f_stream, 0, SEEK_SET);

    // Declare header struct
    CBCL_header header;

    // Load version number
    fread(&(header.version), sizeof(uint16_t), 1, f_stream);

    // Load header size
    fread(&(header.header_size), sizeof(uint32_t), 1, f_stream);

    // Load bits per basecall and quality score
    fread(&(header.bc_bits), sizeof(uint8_t), 1, f_stream);
    fread(&(header.qual_bits), sizeof(uint8_t), 1, f_stream);

    // Load quality score mappings
    fread(&(header.qual_map_size), sizeof(uint32_t), 1, f_stream);

    uint32_t bin_number, qual_value;
    for (uint32_t bin = 0; bin < header.qual_map_size; bin++) {
        fread(&bin_number, sizeof(uint32_t), 1, f_stream);
        fread(&qual_value, sizeof(uint32_t), 1, f_stream);
        header.qual_map[bin_number] = qual_value;
    }

    // Load data information about the tiles
    fread(&(header.tiles_size), sizeof(uint32_t), 1, f_stream);
    for (uint32_t tile = 0; tile < header.tiles_size; ++tile) {
        CBCL_tile tile_data;
        fread(&(tile_data.tile_number), sizeof(uint32_t), 1, f_stream);
        fread(&(tile_data.num_clusters), sizeof(uint32_t), 1, f_stream);
        fread(&(tile_data.uncompressed_size), sizeof(uint32_t), 1, f_stream);
        fread(&(tile_data.compressed_size), sizeof(uint32_t), 1, f_stream);
        header.tiles.push_back(tile_data);
    }

    fread(&header.pf_excluded, sizeof(uint8_t), 1, f_stream);

    return header;
}

CBCL_header AggregatedBclParser::getCBCLHeader(FILE *&fStream) {
    struct stat tmpStats;
    fstat(fileno(fStream), &tmpStats);
    if (!utils::sameFile(tmpStats, this->cachedFileStat)) {
        this->cachedHeader = load_CBCL_header(fStream);
        this->cachedFileStat = tmpStats;
    }
    return this->cachedHeader;
}

void AggregatedBclParser::parse(uint16_t lane, uint16_t tile, uint16_t cycle, const string &bclPath) {
    int currentSurface = tile/1000; // the surface is encoded as the first digit in the tile
    string bclFileName = this->getFilenames(lane, currentSurface, cycle, bclPath)[0];

    cout << "Parsing lane " + to_string(lane) + ", tile " + to_string(tile) << ", cycle " << to_string(cycle) << endl;

    // Open cbcl file
    FILE *fstream = fopen(bclFileName.c_str(), "rb");

    // Check if file was opened correctly
    if (!fstream) {
        throw runtime_error("Error while reading CBCL file");
    }

    CBCL_header header = getCBCLHeader(fstream);

    uint64_t skip_bytes = 0; // bytes to skip
    bool success = false;
    auto found_tile = header.tiles.begin();

    // Get the gzip virtual file offsets
    for (; found_tile != header.tiles.end(); ++found_tile) {

        // Other tile than searched for
        if (found_tile->tile_number != tile) {
            skip_bytes += found_tile->compressed_size;
        }

            // Searched tile was found
        else {
            success = true;
            break;
        }
    }

    // Check if tile was found in the header. If not, throw error
    if (!success) {
        throw runtime_error("The specified tile wasn't found in the BCL file");
    }

    // Skip all compressed data of the other tiles
    fseek(fstream, header.header_size + skip_bytes, SEEK_SET);

    // Load compressed tile data
    std::vector<char> compressed_data;
    std::vector<char> data_;
    compressed_data.resize(found_tile->compressed_size);
    fread(&compressed_data[0], sizeof(char), found_tile->compressed_size, fstream);

    // Decompress gzipped data using boost and store in data vector
    boost::iostreams::filtering_ostream os;
    os.push(boost::iostreams::gzip_decompressor());
    os.push(bcl_byte_converter_and_back_inserter(data_, header.qual_bits, header.bc_bits, header.qual_map));
    int written = boost::iostreams::write(os, &compressed_data[0], compressed_data.size());

    if (written < 0) { // error
        throw runtime_error("Error while decompressing data");
    }

    // Push the last data from the buffer to the vector
    os.flush();


    // When the cbcl file contains 2 BaseCalls per Byte and there is an uneven number of BaseCalls, then the
    // decompressing-pipeline generates one byte of overhead at the end of data_ (since it always pushes two
    // entries per byte). To fix this, we simply remove the last element.
    if (found_tile->num_clusters % 2 != 0 && data_.size() == found_tile->num_clusters + sizeof(uint32_t) + 1)
        data_.pop_back();
    
    // Check the size of the data vector
    if (data_.size() != found_tile->num_clusters)
        throw runtime_error("Found an unexpected number of bytes");

    if (header.pf_excluded && filterRepresentation && filterRepresentation->getNumSequences(lane, tile)) {
        vector<char> dataWithFilteredBaseCalls;

        // Reserve space for BaseCalls
        dataWithFilteredBaseCalls.reserve(filterRepresentation->getNumSequences(lane, tile) + sizeof(uint32_t));

        auto bclDataIt = data_.begin() + sizeof(uint32_t);

        auto filterData = filterRepresentation->getFilterData(lane, tile);
        for (char & filterIt : filterData) {
            if (filterIt == 0) {
                // BaseCall has been filtered, so it does not exist in the CBCL data vector
                // -> replace it with Null Values
                dataWithFilteredBaseCalls.push_back(0);
            } else {
                // BaseCall exists in CBCL data -> simply copy it
                dataWithFilteredBaseCalls.push_back(*bclDataIt++);
            }
        }

        this->bclRepresentation->addBcl(lane, tile, cycle, dataWithFilteredBaseCalls);
    } else {
        this->bclRepresentation->addBcl(lane, tile, cycle, data_);
    }
}

vector<string> AggregatedBclParser::getFilenames(uint16_t lane, uint16_t surface, uint16_t cycle, const string &path) {
    vector<string> filenames;
    ostringstream pathStream;
    pathStream << path << "/L" << utils::toNDigits(lane, 3) << "/C" << cycle << ".1/L" << utils::toNDigits(lane, 3)
               << "_" << surface << ".cbcl";
    filenames.push_back(pathStream.str());
    return filenames;
}
