#include "BclParser.h"
#include "../../utils.h"

using namespace std;

void BclParser::parse(uint16_t lane, uint16_t tile, uint16_t cycle, const string &bclPath) {

    cout << "Parsing lane " + to_string(lane) + ", tile " + to_string(tile) << ", cycle " << to_string(cycle) << endl;

    string bclFileName = this->getFilenames(lane, tile, cycle, bclPath)[0];
    vector<char> bclData;
    int position = 4; // the real data starts at index 4
    utils::readBinaryFile(bclFileName, bclData, position);
    this->bclRepresentation->addBcl(lane, tile, cycle, bclData);
}

std::vector<std::string> BclParser::getFilenames(uint16_t lane, uint16_t tile, uint16_t cycle, const string &path) {
    vector<string> filenames;
    ostringstream path_stream;
    path_stream << path << "/L" << utils::toNDigits(lane, 3) << "/C" << cycle << ".1/s_" << lane << "_"
    << tile << ".bcl";
    filenames.push_back(path_stream.str());
    return filenames;
}