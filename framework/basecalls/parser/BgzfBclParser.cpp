#include "BgzfBclParser.h"
#include "../../utils.h"
#include <zlib.h>

using namespace std;

void BgzfBclParser::parse(uint16_t lane, uint16_t tile, uint16_t cycle, const string &bclPath) {
    // Parse files
    auto bciPath = this->getFilenames(lane, tile, cycle, bclPath)[0];
    auto bclBgzfPath = this->getFilenames(lane, tile, cycle, bclPath)[1];

    cout << "Parsing lane " + to_string(lane) + ", tile " + to_string(tile) << ", cycle " << to_string(cycle) << endl;

    vector<char> reads = this->parseReadsByDecompressingFile(bclBgzfPath, bciPath, tile);
    this->bclRepresentation->addBcl(lane, tile, cycle, reads);
}

/// Parses a .bci file for tile numbers and the associated number of reads.
map<uint32_t, uint32_t> BgzfBclParser::parseTileNumberToReadCount(const string &bciPath) {
    FILE *file = fopen(bciPath.c_str(), "rb");

    if (file == nullptr)
        throw runtime_error("Cannot read file: " + bciPath);

    uint16_t i = 0;
    uint32_t parsedNumber = 0;

    uint32_t tileNumber;
    map<uint32_t, uint32_t> tileNumberToReadCount;

    size_t bytesPerElement = 1, numberOfElements = 4;
    while (fread(&parsedNumber, bytesPerElement, numberOfElements, file)) {
        switch (i++ % 2) {
            case 0:
                tileNumber = parsedNumber;
                break;
            case 1:
                tileNumberToReadCount[tileNumber] = parsedNumber;
                break;
        }
    }

    fclose(file);
    return tileNumberToReadCount;
}

vector<char> BgzfBclParser::parseReadsByDecompressingFile(const string bclBgzfPath, const string bciPath,
                                                          uint16_t tile) {
    // Decompress bgzf file
    gzFile file = gzopen(bclBgzfPath.c_str(), "rb");

    if (file == nullptr)
        throw runtime_error("Cannot read file: " + bclBgzfPath);

    // Extract position of reads in decompressed file and readCount of tile
    auto tileNumberToReadCount = this->parseTileNumberToReadCount(bciPath);
    auto readCountsToSkip = 0;
    for (auto mapIterator = tileNumberToReadCount.begin();
         mapIterator != tileNumberToReadCount.end() && mapIterator->first != tile; ++mapIterator) {
        uint32_t tileNumber = mapIterator->first;
        readCountsToSkip += tileNumberToReadCount.at(tileNumber);
    }
    uint32_t readCount = tileNumberToReadCount.at(tile);

    // Seek until beginning of file
    gzseek(file, 0, SEEK_SET);

    // Add number of reads to base call vector
    vector<char> baseCalls;
    baseCalls.reserve(readCount);

    // Skip total number of reads and seek to base calls of tile
    gzseek(file, 4 + readCountsToSkip, SEEK_CUR);

    // Extract base calls of tile
    for (unsigned int i = 0; i < readCount; i++) {
        uint8_t parsedRead = 0;
        gzread(file, &parsedRead, sizeof(parsedRead));
        baseCalls.push_back(parsedRead);
    }

    gzclose(file);
    return baseCalls;
}

vector<string> BgzfBclParser::getFilenames(uint16_t lane, uint16_t tile, uint16_t cycle, const std::string &path) {
    (void) tile; // We parse the whole files and therefore do not need the tile

    vector<string> filenames;

    // .bci
    ostringstream bci_path_stream;
    bci_path_stream << path << "/L" << utils::toNDigits(lane, 3) << "/s_" << lane << ".bci";
    filenames.push_back(bci_path_stream.str());

    // .bcl.bgzf
    ostringstream bcl_bgzf_path_stream;
    bcl_bgzf_path_stream << path << "/L" << utils::toNDigits(lane, 3) << "/" << utils::toNDigits(cycle, 4) << ".bcl.bgzf";
    filenames.push_back(bcl_bgzf_path_stream.str());

    // .bcl.bgzf.bci
    ostringstream bcl_bgzf_bci_path_stream;
    bcl_bgzf_bci_path_stream << path << "/L" << utils::toNDigits(lane, 3) << "/" << utils::toNDigits(cycle, 4) << ".bcl.bgzf.bci";
    filenames.push_back(bcl_bgzf_bci_path_stream.str());

    return filenames;
}
