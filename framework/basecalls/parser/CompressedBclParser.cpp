#include "CompressedBclParser.h"
#include <cstring>
#include <zlib.h>
#include "../../utils.h"

using namespace std;

void CompressedBclParser::parse(uint16_t lane, uint16_t tile, uint16_t cycle, const string &bclPath) {
    string bclFileName = this->getFilenames(lane, tile, cycle, bclPath)[0];

    cout << "Parsing lane " + to_string(lane) + ", tile " + to_string(tile) << ", cycle " << to_string(cycle) << endl;

    // decompress the bcl files
    gzFile file = gzopen(bclFileName.c_str(), "rb");
    if(file == nullptr){
        cerr << "Cannot read file: " + bclFileName << endl;
    }

    // skip first 4 bytes which only contain the size
    gzseek(file, 4, 0);

    char unzipBuffer[8192];
    int unzippedBytes;
    vector<char> unzippedBcl;
    while (true) {
        unzippedBytes = gzread(file, unzipBuffer, 8192);
        if (unzippedBytes > 0) unzippedBcl.insert(unzippedBcl.end(), unzipBuffer, unzipBuffer + unzippedBytes);
        else break;
    }

    gzclose(file);

    this->bclRepresentation->addBcl(lane, tile, cycle, unzippedBcl);
}

std::vector<std::string> CompressedBclParser::getFilenames(uint16_t lane, uint16_t tile, uint16_t cycle, const string &path) {
    vector<string> filenames;
    ostringstream path_stream;
    path_stream << path << "/L" << utils::toNDigits(lane, 3) << "/C" << cycle << ".1/s_" << lane << "_"
                << tile << ".bcl.gz";
    filenames.push_back(path_stream.str());
    return filenames;
}
