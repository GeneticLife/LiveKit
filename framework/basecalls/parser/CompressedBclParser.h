#ifndef LIVEKIT_COMPRESSEDBCLPARSER_H
#define LIVEKIT_COMPRESSEDBCLPARSER_H

#include <sstream>
#include <fstream>
#include <iomanip>
#include <cstring>
#include "../data_representation/BclRepresentation.h"
#include "BclParser.h"

class CompressedBclParser : public BclParser {
public:
    explicit CompressedBclParser(BclRepresentation *bclRepresentation) : BclParser(bclRepresentation){}

    void parse(uint16_t lane, uint16_t tile, uint16_t cycle, const std::string &bclPath) override;

    std::vector<std::string> getFilenames(uint16_t lane, uint16_t tile, uint16_t cycle, const std::string &path) override;
};

#endif //LIVEKIT_COMPRESSEDBCLPARSER_H
