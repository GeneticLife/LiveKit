#ifndef LIVEKIT_CONFIGURABLE_H
#define LIVEKIT_CONFIGURABLE_H

#include <iostream>
#include <map>
#include <functional>

#include "RunInfoParser.h"
#include "SampleSheetParser.h"

#include <boost/lexical_cast.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/variant.hpp>
#include <boost/any.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>


class Configurable {
protected:
    enum EntryOptions {
        NO_WARNING = 0x00,
        SHOW_WARNINGS = 0x01, // Show a warning when the config entry falls back to the default parameter
        THROW_ERROR = 0x02 // Ignore the default parameter, throw an error, if no config value is specified
    };

    boost::property_tree::ptree parsedConfigTree;

    RunInfoParser runInfoParser;

    SampleSheetParser sampleSheetParser;

protected:
    std::map<std::string, boost::any> configMap;

public:
    /**
     * This method must be overwritten by the childclass
     * The Method in the childclass should register every config-entry (key-value pair) of the childclass
     * in its respective configMap by calling the method "registerConfigEntry
     */
    virtual void setConfig() = 0;

    /**
     * This Method loads the content from the given file into the property tree,
     * afterwards it registers all its entries as config-entries in the configMap by calling the  "setConfig"-Method
     * @param configFilePath The path to the config-file
     */
    virtual void setConfigFromFile(const std::string &configFilePath);

    /**
     * This Method first adds the values of the fallbackConfigMap (most likely the ConfigMap of the Framework)
     * into the ConfigMap,
     * then it loads the content from the given file into the property tree,
     * and registers all its entries as config-entries in the configMap by calling the  "setConfig"-Method
     * @param configFilePath The path to the config-file
     * @param fallbackConfigMap The ConfigMap of the Framework
     */
    virtual void
    setConfigFromFile(const std::string &configFilePath, std::map<std::string, boost::any> fallbackConfigMap);

    /**
     * Checks if the configMap contains an entry with the given key
     * @param key The key of the config-entry in the configMap
     * @return Returns 'true' if the configMap contains an entry with the given key, 'false' otherwhise
     */
    bool containsConfigEntry(const std::string &key) const;

    /**
     * This method registers a config-entry (key-value pair) in the configMap
     * Only config-entries that are registered can be used
     * Config-entries that are specified in the config-file but not registered will be ignored
     * @tparam T Type of the value that is stored in the configMap
     * @param key The key of the config-entry in the configMap
     * @param defaultValue The default value that is used if the config-entry with the specified key is not configured in the config-file.
     */
    template<typename T>
    void registerConfigEntry(const std::string &key, const T &defaultValue, EntryOptions options = SHOW_WARNINGS) {
        // Best way to set config settings without default-warnings:
        // this->configMap[key] = this->parsedConfigTree.get(key, defaultValue);

        try {
            auto configValueFromFile = this->parsedConfigTree.get<T>(key);
            this->configMap[key] = configValueFromFile;
        } catch (std::exception &e) {
            // fallback to runInfo or default value
            try {
                // Get data from RunInfo.xml
                this->configMap[key] = boost::any_cast<T>(this->runInfoParser.getRunInfo()[key]);

            } catch (std::exception &e) {
                // fallback to SampleSheet.csv or default value
                try {
                    // Get data from SampleSheet.csv

                    auto sampleData = this->sampleSheetParser.getSampleData();
                    if (sampleData.find(key) == sampleData.end())
                        throw e;
                    this->configMap[key] = this->sampleSheetParser.getSampleData()[key];

                } catch (std::exception &e) {
                    if (options & THROW_ERROR) {
                      std::cerr << "Config parameter " << key << " has not been specified! " << std::endl;
                      throw std::invalid_argument("No parameter specified for: " + key);
                    } else if (options & SHOW_WARNINGS)
                        std::cout << "Use default value for config parameter: " << key << std::endl;
                    this->configMap[key] = defaultValue;
                }
            }
        }
    }

    /**
     * This method registers a config-entry (key-value pair) in the configMap
     * The value of the config-entry is the value that is returned by the converter-function.
     * This allows data-manipulation and type-casting.
     * Only config-entries that are registered can be used
     * Config-entries that are specified in the config-file but not registered will be ignored
     * @tparam T Type of the value that is stored in the configMap
     * @tparam primitiveT Type of the initial value in the config file
     * @param key The key of the config-entry in the configMap
     * @param defaultValue The default value that is used if the config-entry with the specified key is not configured in the config-file.
     * @param converter A function that converts the data from Type primitiveT to Type T
     */
    template<typename T, typename primitiveT>
    void
    registerConfigEntry(const std::string &key, const primitiveT &defaultValue, std::function<T(primitiveT)> converter,
                        EntryOptions options = SHOW_WARNINGS) {
        this->registerConfigEntry<primitiveT>(key, defaultValue, options);
        this->configMap[key] = converter(this->getConfigEntry<primitiveT>(key));
    }

    /**
     * This method registers a config-entry (key-value pair) in the configMap
     * The value of the config-entry is the value that is returned by the calculation-function.
     * This method is register config-entries that are purely calculated e.g. based on other config entries.
     * Only config-entries that are registered can be used
     * Config-entries that are specified in the config-file but not regiestered will be ignored
     * @tparam T Type of the value that is stored in the configMap
     * @param key The key of the config-entry in the configMap
     * @param calculation A function that returns the data
     */
    template<typename T>
    void registerCalculatedConfigEntry(const std::string &key, std::function<T()> calculation) {
        // this is a bit dirty but necessary because the boost::ptree can't handle a vector directly
        this->registerConfigEntry<std::string>(key, "", NO_WARNING);
        this->configMap[key] = calculation();
    }

    /**
     * This method can be used as the 'converter'-function as input parameter in the method 'registerConfigEntry'
     * The method converts the input (given as input param. in 'registerConfigEntry') into a vector, which elements are converted to primitive types,
     * and delimited by the delimiter variable.
     * if the input does not exist, the method returns an empty Vector of Type T
     * @tparam T Type of the vector which is returned
     * @param delimiter A variable which delimits the elements of the vector (usually ',')
     * @return converter function which converts the data to a vector of Type T
     */

    template<typename T>
    static std::function<std::vector<T>(std::string)> toVector(char delimiter) {
        return [delimiter](std::string input) {
            if (!input.size()) return std::vector<T>{};

            std::vector<std::string> intermediateResult;
            boost::split(intermediateResult, input, boost::is_any_of(std::string(1, delimiter)));
            std::vector<T> result;
            std::transform(
                    intermediateResult.begin(),
                    intermediateResult.end(),
                    std::back_inserter(result),
                    Configurable::toPrimitiveType<T>
            );
            return result;
        };
    }

    /**
    * This method can be used as the 'converter'-function as input parameter in the method 'registerConfigEntry'
    * The Method converts the input (given as input param. in 'registerConfigEntry') into a vector,
    * which elements are converted to vectors and delimited by the 'outerDelimiter' variable,
    * the elements of the inner vectors are converted to primitive Types and delimited by the 'innerDelimiter' variable
    * @tparam T Type of the inner vector(s) which are returned
    * @param outerDelimiter A variable which delimits the elements of the outer vector (usually ';')
    * @param innerDelimiter A variable which delimits the elements of the outer vector (usually ',')
    * @return converter function which converts the data to a vector of Vectors of Type T
    */
    template<typename T>
    static std::function<std::vector<std::vector<T>>(std::string)>
    toVectorVector(char outerDelimiter, char innerDelimeter) {
        return [outerDelimiter, innerDelimeter](std::string input) {
            // split into outer vectors
            std::vector<std::string> outerVector = Configurable::toVector<std::string>(outerDelimiter)(input);

            // split outer vectors into inner vectors
            std::vector<std::vector<std::string>> intermediateResults;
            std::vector<std::vector<T>> result;
            for (unsigned long i = 0; i < outerVector.size(); i++) {
                intermediateResults.push_back(Configurable::toVector<std::string>(innerDelimeter)(outerVector[i]));

                // transform to correct type
                result.push_back(std::vector<T>());
                std::transform(
                        intermediateResults[i].begin(),
                        intermediateResults[i].end(),
                        std::back_inserter(result[i]),
                        Configurable::toPrimitiveType<T>
                );
            }
            return result;
        };
    }

    /**
    * This method can be used as the 'converter'-function as input parameter in the method 'registerConfigEntry'
    * This method can convert data size disclosures from any byte based unit into the desired byte based data unit.
    * @param the unit that the data size should be converted to (can be B (Bytes), K (Kibibytes), M (Mebibytes), G (Gibibytes)
    * @return converter function which converts the an input string (e.g. 5GB, 100K, 512) into the data size of the desired unit.
    */
    static std::function<unsigned long (std::string)>toBytes(char toUnit = 'B') {
        return [toUnit](std::string input) {
            std::map<char, unsigned long> conversion{
                {'B', 1}, {'K', 1024}, {'M', 1024 * 1024}, {'G', 1024 * 1024 * 1024 }
            };

            char fromUnit = 'B';

            auto unitPosition = input.find_first_of("GMKB");
            if (unitPosition != std::string::npos) { // Split value to size and type
                fromUnit = input[unitPosition];
                input = input.substr(0, unitPosition);
            }

            if (input.find_first_not_of("0123456789") != std::string::npos)
                throw std::runtime_error("Data size unit is invalid!");

            double bytes = atol(input.c_str());

            return (unsigned long)(bytes * conversion[fromUnit] / conversion[toUnit]);
        };
    }

    /**
    * This method converts the type of the input string to a primitive type
    * @param element The element string which should be converted to a primitive type
    * @return Element converted to the primitive Type T
    */
    template<typename T>
    static T toPrimitiveType(std::string element) {
        boost::any value;
        if (std::is_same<T, std::string>::value) {
            value = element;
        } else if (std::is_same<T, int>::value) {
            value = stoi(element);
        } else if (std::is_same<T, float>::value) {
            value = stof(element);
        } else if (std::is_same<T, bool>::value) {
            value = boost::lexical_cast<bool>(element);
        } else if (std::is_same<T, uint16_t>::value) {
            value = (uint16_t) stoi(element);
        } else {
            std::cerr << "No viable conversion found for: " << element << std::endl;
            throw std::runtime_error("ConversionError");
        }
        return boost::any_cast<T>(value);
    }

    /**
     * This method returns the value of a key-value pair in the Config Map which can be identified by the key
     * if the key is not found by iterating over the map, an error is thrown.
     * @param key The key of the config-entry in the configMap
     * @return value of type T which is paired with 'key' in the Config Map
     */
    template<typename T>
    const T getConfigEntry(const std::string &key) {
        if (!this->containsConfigEntry(key))
            std::cerr << "Can't find key: " << key << std::endl;

        try {
            return boost::any_cast<T>(this->configMap[key]);
        } catch (std::exception e) {
            std::cerr << "Error on reading configEntry with key: " << key << " : " << e.what() << std::endl;
            std::cerr << "(Check for type mismatches!)" << std::endl;
            throw std::runtime_error("Error on reading configEntry with key: " + key + ".");
        }
    }
};

#endif //LIVEKIT_CONFIGURABLE_H
