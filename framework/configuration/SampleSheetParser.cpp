#include <fstream>

#include "SampleSheetParser.h"

#include <iterator>
#include <boost/algorithm/string.hpp>
#include <iostream>

using namespace std;

void SampleSheetParser::parseSampleSheet(string filename, string delm) {
    ifstream file(filename);

    string line = "";
    // Iterate through each line and split the content using delimeter
    while (getline(file, line)) {
        vector<string> vec;
        boost::algorithm::split(vec, line, boost::is_any_of(delm));
        this->dataList.push_back(vec);
    }
    // Close the File
    file.close();

    // We only parse the barcodes(Indices), SampleID/SampleName
    // Store all information in data in order to get access to it via getConfigEntry<>()
    this->data["barcodeVector"] = this->getBarcodeVector();
    this->fillConfigVector();
}

string SampleSheetParser::getBarcodeVector() {
    // Get index and index2 column
    vector<string> row = this->dataList[this->getHeaderRow()];
    auto posIndex = find(row.begin(), row.end(), "index") - row.begin();

    // When sample sheet file does not contain index2
    long posIndex2;

    if (find(row.begin(), row.end(), "index2") != row.end()) {
        // There is a second barcode
        posIndex2 = find(row.begin(), row.end(), "index2") - row.begin();
    } else {
        posIndex2 = row.size() + 1;
    }

    string barcodes;

    for (int i = this->getHeaderRow() + 1; i < (int) this->dataList.size(); i++) {
        barcodes.append(this->dataList[i][posIndex] + ",");
        if (posIndex2 <= (int) row.size()) {
            barcodes.append(this->dataList[i][posIndex2] + ";");
        }
    }

    return barcodes.substr(0, barcodes.size() - 1);
}

void SampleSheetParser::fillConfigVector() {
    /*
     * The Sample_Name tag is optional. If it is present, than it is part of fastq file name.
     * return The name of the given Sample_ID
     */
    vector<string> sampleIdentification = this->getSampleEntries("Sample_Name");

    for (auto sampleIDs : sampleIdentification) {
        if (sampleIDs == "") {
            sampleIdentification = this->getSampleEntries("Sample_ID");
        }
    }

    vector<string> index = this->getSampleEntries("index");
    vector<string> index2;

    if (this->getHeaderRow() != -1) {
        index2 = this->getSampleEntries("index2");
    }

    vector<string> barcodes;
    for (int i = 0; i < (int) index.size(); i++) {
        if (index2.empty()) {
            barcodes.push_back(index[i]);
        } else {
            barcodes.push_back(index[i] + index2[i]);
        }
        int sampleNumber = this->getSampleNumber(sampleIdentification[i]);
        this->data[barcodes[i]] = sampleIdentification[i] + "," + to_string(sampleNumber);
    }
}

// Function to fetch all parsed data from a CSV File
std::map<std::string, string> SampleSheetParser::getSampleData() {
    return this->data;
}

int SampleSheetParser::getHeaderRow() {
    // Find the header row of the [Data] block
    // Sample_ID is required in the header row
    for (int i = 0; i < int(this->dataList.size()); i++) {
        for (int j = 0; j < int(this->dataList[i].size()); j++) {
            if (this->dataList[i][j] == "Sample_ID") {
                return i;
            }
        }
    }
    return -1;
}

int SampleSheetParser::getSampleRow(string sample) {
    // Find the row of a specific sample
    // return -1 if the sample does not exists
    for (int i = 0; i < int(this->dataList.size()); i++) {
        for (int j = 0; j < int(this->dataList[i].size()); j++) {
            if (this->dataList[i][j] == sample) {
                return i;
            }
        }
    }

    cerr << sample << " does not exists (Check your SampleSheet if this is intended)" << endl;

    return -1;
}

vector<string> SampleSheetParser::getSampleEntries(string columnName) {

    vector<string> headerRow;
    try {
        // Get the row with the header information
        headerRow = this->dataList[this->getHeaderRow()];
    } catch (exception &e) {}

    if ((int) headerRow.size() == 0) {
        return {};
    }

    auto sampleEntrieStart = find(headerRow.begin(), headerRow.end(), columnName) - headerRow.begin();

    vector<string> entries;
    for (int i = this->getHeaderRow() + 1; i < (int) this->dataList.size(); i++) {
        // Get column entries
        entries.push_back(this->dataList[i][sampleEntrieStart]);
    }


    return entries;
}

int SampleSheetParser::getSampleNumber(string sampleID) {
    return this->getSampleRow(sampleID) - this->getHeaderRow();
}

int SampleSheetParser::getNumberOfSamples() {
    return (int) this->dataList.size() - this->getHeaderRow() - 1;
}
