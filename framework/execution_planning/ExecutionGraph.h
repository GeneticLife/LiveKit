#ifndef LIVEKIT_EXECUTIONGRAPH_H
#define LIVEKIT_EXECUTIONGRAPH_H

#include "../Plugin.h"
#include "Graph.h"

class HelperPlugin : public Plugin {
public:
    explicit HelperPlugin(PluginSpecification *specification) {
        this->specification = specification;
    }

    void setConfig() override {};

    void init() override {};

    std::shared_ptr<FragmentContainer> runPreprocessing(std::shared_ptr<FragmentContainer> inputFragments) override { return inputFragments; };

    std::shared_ptr<FragmentContainer> runCycle(std::shared_ptr<FragmentContainer> inputFragments) override { return inputFragments; };

    std::shared_ptr<FragmentContainer> runFullReadPostprocessing(std::shared_ptr<FragmentContainer> inputFragments) override { return inputFragments; };

    void finalize() override {};
};

struct VertexIdentifier;
typedef struct VertexIdentifier VertexIdentifier;

struct VertexIdentifier {
    Plugin *plugin;
    uint16_t lane;
    uint16_t tile;
    int cycle;

    // Add operator< to make the struct usable as the key of a map
    bool operator<(const VertexIdentifier &id) const {
        return plugin < id.plugin
               || (plugin == id.plugin && lane < id.lane)
               || (plugin == id.plugin && lane == id.lane && tile < id.tile)
               || (plugin == id.plugin && lane == id.lane && tile == id.tile && cycle < id.cycle);
    }

    bool operator==(const VertexIdentifier &id) const {
        return plugin == id.plugin && lane == id.lane && tile == id.tile && cycle == id.cycle;
    }
};

class ExecutionGraph : public Graph<VertexIdentifier, PluginSpecification::FragmentSpecification> {
public:
    explicit ExecutionGraph (int cycleCount)
            : cycleCount(cycleCount)
            , numberOfHooksPerTileCycle(0)
            , PRE_CYCLE(0)
            , POST_CYCLE(this->cycleCount + 1)
            , ALL_LANES(0)
            , ALL_TILES(0) {
        // StartPlugin
        this->startPluginVertex = this->addVertex({
            static_cast<Plugin *>(new HelperPlugin(new PluginSpecification("START", ""))),
            this->ALL_LANES, this->ALL_TILES, this->PRE_CYCLE
        });

        // EndPlugin
        this->endPluginVertex = this->addVertex({
            static_cast<Plugin *>(new HelperPlugin(new PluginSpecification("END", ""))),
            this->ALL_LANES, this->ALL_TILES, this->POST_CYCLE
        });
    }

    int cycleCount;
    int numberOfHooksPerTileCycle;


    const int PRE_CYCLE, POST_CYCLE;
    const uint16_t ALL_LANES, ALL_TILES;

    Vertex startPluginVertex, endPluginVertex;

    std::string toEdgeCategory(int cycle) {
        return (cycle == 0) ? "preprocessing"
                            :  (cycle == this->cycleCount + 1) ? "postprocessing"
                                                               :  "cycle";
    }

    ~ExecutionGraph() {
        // Delete start and end - Plugin
        for (auto vertex : {this->startPluginVertex, this->endPluginVertex } ) {
            delete this->getVertexIdentifier(vertex).plugin->specification;
            delete this->getVertexIdentifier(vertex).plugin;
        }
    }
};


#endif //LIVEKIT_EXECUTIONGRAPH_H
