#include <stdlib.h>

#include "ExecutionGraphBuilder.h"
#include "../fragments/FragmentContainer.h"

using namespace std;

ExecutionGraphBuilder::ExecutionGraphBuilder(std::vector<uint16_t> lanes, std::vector<uint16_t> tiles, int cycleCount)
    : graph(new ExecutionGraph(cycleCount))
    , lanes(std::move(lanes))
    , tiles(std::move(tiles)) {
    this->pluginVertices[this->graph->getVertexIdentifier(this->graph->startPluginVertex)] = this->graph->startPluginVertex;
    this->pluginVertices[this->graph->getVertexIdentifier(this->graph->endPluginVertex)] = this->graph->endPluginVertex;
}

ExecutionGraphBuilder::Vertex ExecutionGraphBuilder::toVertex(VertexIdentifier identifier) {
    return this->pluginVertices[identifier];
}

void ExecutionGraphBuilder::registerOutputSpecification(
    PluginSpecification::FragmentSpecification outputFragmentSpecification,
    Vertex vertex,
    VertexIdentifier identifier) {
    // Insert if not yet present
    auto insertionResult = this->registeredOutputSpecifications.insert(make_pair(
        OutputSpecificationKey{
            outputFragmentSpecification.fragmentName,
            identifier.lane,
            identifier.tile,
            identifier.cycle
        },
        OutputSpecificationValue{
            outputFragmentSpecification.fragmentType,
            vertex
        }
    ));

    bool wasInserted = insertionResult.second;

    if (!wasInserted) {
        auto retrievedKeyValuePair = insertionResult.first;
        auto retrievedFragmentType = retrievedKeyValuePair->second.fragmentType;
        auto retrievedVertex = retrievedKeyValuePair->second.vertex;

        string vertexName = this->graph->getVertexIdentifier(vertex).plugin->specification->getDisplayName(),
            retrievedVertexName = this->graph->getVertexIdentifier(retrievedVertex).plugin->specification->getDisplayName();

        if (outputFragmentSpecification.fragmentType == retrievedFragmentType) {
            // Name and type matched previously with another output fragment specification
            string message = "Output fragment '" + outputFragmentSpecification.fragmentName
                             + "' of type '" + outputFragmentSpecification.fragmentType
                             + "' was already specified in plugins '" + vertexName
                             + "' and '" + retrievedVertexName
                             + "'";
            throw runtime_error(message);
        } else {
            // Name matches with another output fragment specification, but type differs
            string message = "Output fragment '" + outputFragmentSpecification.fragmentName
                             + "' was specified in plugin '" + vertexName
                             + "' with type '" + outputFragmentSpecification.fragmentType
                             + "' and plugin '" + retrievedVertexName
                             + "' with type '" + retrievedFragmentType
                             + "'";
            throw runtime_error(message);
        }
    }
}

ExecutionGraphBuilder::Vertex ExecutionGraphBuilder::addVertex(VertexIdentifier identifier) {
    Vertex v = this->graph->addVertex(identifier);

    this->pluginVertices[identifier] = v;

    string edgeCategory = this->graph->toEdgeCategory(identifier.cycle);
    for (auto &outputFragmentSpecification : identifier.plugin->specification->outputFragmentSpecifications[edgeCategory])
        this->registerOutputSpecification(outputFragmentSpecification, v, identifier);

    return v;
}

ExecutionGraphBuilder::Edge ExecutionGraphBuilder::addEdge(Vertex sourceVertex, Vertex targetVertex,
                                                         PluginSpecification::FragmentSpecification fragmentSpecification) {
    if (fragmentSpecification.fragmentType.empty())
        return this->graph->addEdge(sourceVertex, targetVertex);
    else
        return this->graph->addEdge(sourceVertex, targetVertex, fragmentSpecification);
}

void ExecutionGraphBuilder::addInputEdges(Vertex targetVertex) {
    auto targetVertexIdentifier = this->graph->getVertexIdentifier(targetVertex);
    auto targetPlugin           = targetVertexIdentifier.plugin;
    int targetVertexCycle       = targetVertexIdentifier.cycle;

    string edgeCategory = this->graph->toEdgeCategory(targetVertexCycle);
    for (auto &inputSpecification : targetPlugin->specification->inputFragmentSpecifications[edgeCategory]) {
        vector<OutputSpecificationKey> keysToSearch;


        if (inputSpecification.fromPrecedingCycle) {
            // If the fromPrecedingCycle flag is set, the required fragment must come from the previous cycle
            // it cannot be a preprocessing fragment

            // In the first cycle we can not get the fragment from the preceeding cycle
            if (targetVertexCycle == 1) continue;
            keysToSearch = {
                    {inputSpecification.fragmentName, targetVertexIdentifier.lane, targetVertexIdentifier.tile,
                            targetVertexCycle - 1}
            };
        } else if (targetVertexCycle == this->graph->PRE_CYCLE)
            // In preprocessing, the input fragments can only come from other preprocessing plugins
            keysToSearch = {
                { inputSpecification.fragmentName, this->graph->ALL_LANES, this->graph->ALL_TILES, this->graph->PRE_CYCLE }
            };
        else if (targetVertexCycle == this->graph->POST_CYCLE)
            // In postprocessing, input fragments can come from preprocessing,
            // from the last runCycle call or from postprocessing
            keysToSearch = {
                { inputSpecification.fragmentName, this->graph->ALL_LANES, this->graph->ALL_TILES, this->graph->PRE_CYCLE },
                { inputSpecification.fragmentName, this->graph->ALL_LANES, this->graph->ALL_TILES, this->graph->cycleCount },
                { inputSpecification.fragmentName, this->graph->ALL_LANES, this->graph->ALL_TILES, this->graph->POST_CYCLE },
            };
        else
            // Otherwise the fragment can be from the current cycle or from preprocessing
            keysToSearch = {
                { inputSpecification.fragmentName, this->graph->ALL_LANES, this->graph->ALL_TILES, this->graph->PRE_CYCLE },
                { inputSpecification.fragmentName, targetVertexIdentifier.lane, targetVertexIdentifier.tile, targetVertexCycle }
            };

        OutputSpecificationValue outputSpecification = searchForOutputSpecification(
            inputSpecification,
            keysToSearch
        );

        Vertex sourceVertex = outputSpecification.vertex;

        this->addEdge(sourceVertex, targetVertex, inputSpecification);
    }
}

ExecutionGraphBuilder::OutputSpecificationValue ExecutionGraphBuilder::searchForOutputSpecification(
    PluginSpecification::FragmentSpecification inputSpecification,
    vector<OutputSpecificationKey> specificationKeys
) {
    map<OutputSpecificationKey, OutputSpecificationValue>::iterator mapKeyValuePair;

    for (auto &specificationKey : specificationKeys) {
        mapKeyValuePair = this->registeredOutputSpecifications.find( specificationKey );

        if (mapKeyValuePair != this->registeredOutputSpecifications.end()) {
            if (mapKeyValuePair->second.fragmentType != inputSpecification.fragmentType)
                throw runtime_error(
                    "Fragment '"
                        + inputSpecification.fragmentName
                        + "' has mismatching types ('"
                        + mapKeyValuePair->second.fragmentType
                        + "' and '"
                        + inputSpecification.fragmentType
                        + "')"
                );
            else return mapKeyValuePair->second;
        }
    }

    throw runtime_error(
        "Output-fragment '"
        + inputSpecification.fragmentName
        + "' of type '"
        + inputSpecification.fragmentType
        + "' could not be found"
    );
}

ExecutionGraph *ExecutionGraphBuilder::build(set<Plugin *> plugins) {
    // Setup Preprocessing
    for (auto plugin : plugins)
        this->addVertex({ plugin, this->graph->ALL_LANES, this->graph->ALL_TILES, this->graph->PRE_CYCLE });

    for (auto plugin : plugins) {
        Vertex v = this->toVertex({ plugin, this->graph->ALL_LANES, this->graph->ALL_TILES, this->graph->PRE_CYCLE });

        // Every Vertex has a an immediate predecessor even without fragment dependencies
        this->addEdge(this->graph->startPluginVertex, v);

        // Add edges for fragment dependencies
        this->addInputEdges(v);
    }

    // Setup Cycles
    for (int currentCycle = 1; currentCycle <= this->graph->cycleCount; currentCycle++) {
        // Build up graph structure for runCycle Hooks

        // Add all plugins of the current cycle to the graph first, to register the outputSpecifications
        for (auto plugin : plugins)
            this->addVertex({plugin, this->graph->ALL_LANES, this->graph->ALL_TILES, currentCycle});

        for (auto plugin : plugins) {
            Vertex v = this->toVertex({ plugin, this->graph->ALL_LANES, this->graph->ALL_TILES, currentCycle });

            // Every Vertex has a an immediate predecessor even without fragment dependencies
            this->addEdge(this->toVertex({plugin, this->graph->ALL_LANES, this->graph->ALL_TILES, currentCycle - 1}), v);

            // Add edges for fragment dependencies
            this->addInputEdges(v);
        }

        // Build up graph structure for tile based hooks
        for (auto lane : this->lanes)
            for (auto tile : this->tiles) {
                for (auto plugin : plugins) {
                    // Only if the Plugin has a TileContext, tile-based steps have to be created

                    if (!plugin->hasTileContext()) continue;
                    this->addVertex({plugin, lane, tile, currentCycle});
                }

                for (auto plugin : plugins) {
                    if (!plugin->hasTileContext()) continue;

                    Vertex v = this->toVertex({ plugin, lane, tile, currentCycle });

                    if (currentCycle == 1)
                        // In the first cycle the the predecessor vertex of the tile based cycle hook is the
                        // runPreprocessing step for all tiles of that plugin
                        this->addEdge(this->toVertex({plugin, this->graph->ALL_LANES, this->graph->ALL_TILES, this->graph->PRE_CYCLE }), v);
                    else
                        this->addEdge(this->toVertex({plugin, lane, tile, currentCycle - 1}), v);

                    // runCycle should be called AFTER each tile based cycle hook is done
                    this->addEdge(v, this->toVertex({plugin, this->graph->ALL_LANES, this->graph->ALL_TILES, currentCycle}));

                    // Add edges for fragment dependencies
                    this->addInputEdges(v);
               }
            }
    }

    // count the number of hooks per cycle
    for (auto plugin : plugins) {
        // increase 'numberOfHooksPerTileCycle' because every plugin has the runCylce-hook
        this->graph->numberOfHooksPerTileCycle++;
        // increase 'numberOfHooksPerTileCycle' again only if the plugin has the runCycleForTile-hook
        if(plugin->hasTileContext()) this->graph->numberOfHooksPerTileCycle++;
    }


    // Setup Postprocessing
    for (auto plugin : plugins)
        this->addVertex({ plugin, this->graph->ALL_LANES, this->graph->ALL_TILES, this->graph->POST_CYCLE });

    for (auto plugin : plugins) {
        Vertex v = this->toVertex({ plugin, this->graph->ALL_LANES, this->graph->ALL_TILES, this->graph->POST_CYCLE });

        // Add last runCycle vertex as predecessor
        this->addEdge(this->toVertex({plugin, this->graph->ALL_LANES, this->graph->ALL_TILES, this->graph->cycleCount }), v);

        // the tile based runCycle Steps should also be predecessors of the postprocessing step
        for (auto lane : this->lanes)
            for (auto tile : this->tiles) {
                if (!plugin->hasTileContext()) continue;
                this->addEdge(this->toVertex({plugin, lane, tile, this->graph->cycleCount }), v);
            }

        // Add edge to the endVertex
        this->addEdge(v, this->graph->endPluginVertex);

        // Add edges for fragment dependencies
        this->addInputEdges(v);
    }

    return this->graph;
}

void ExecutionGraphBuilder::write_graphviz() {

    function<string (Vertex v, VertexIdentifier identifier)> toVertexName = [&](Vertex v, VertexIdentifier identifier) {
        string
            lane = (identifier.lane == this->graph->ALL_LANES)  ? "ALL_LANES" : to_string(identifier.lane),
            tile = (identifier.tile == this->graph->ALL_TILES)  ? "ALL_TILES" : to_string(identifier.tile),
            cycle = (identifier.cycle == this->graph->PRE_CYCLE)
                    ? "PRE"     : (identifier.cycle == this->graph->POST_CYCLE)
                    ? "POST"    : to_string(identifier.cycle);

        return (string) (
            + "V" + to_string(v)
            + "_" + lane
            + "_" + tile
            + "_" + cycle
        );
    };

    map<Plugin *, vector<string>> subgraphs;

    string mainGraph;

    for (unsigned int u = 0; u < this->graph->getNumVertices(); u++) {
        for (auto v : this->graph->successors(u)) {
            auto    uIdentifier = this->graph->getVertexIdentifier(u),
                    vIdentifier = this->graph->getVertexIdentifier(v);

            string edgeName = toVertexName(u, uIdentifier) + " -> " + toVertexName(v, vIdentifier);

            auto fragmentSpecifications = this->graph->getEdgePayload(this->graph->getEdge(u, v));
            if (!fragmentSpecifications.empty()) {
                string fragmentNames;
                for (auto fragmentSpecification : fragmentSpecifications) {
                    if (!fragmentNames.empty()) fragmentNames += + "_";
                    fragmentNames += fragmentSpecification.fragmentName;
                }
                edgeName += " [label=\"" + fragmentNames + "\"]";
            }

            edgeName += ";\n";

            if (uIdentifier.plugin == vIdentifier.plugin)
                subgraphs[uIdentifier.plugin].push_back(edgeName);
            else
                mainGraph += edgeName;
        }
    }

    cout << "digraph G {" << endl;

    int clusterId = 0;
    for (auto &subgraph : subgraphs) {
        cout << "subgraph cluster"  << clusterId++ << " {" << endl;
        for (auto &edge : subgraph.second)
            cout << edge;

        cout << "label = \"" <<  subgraph.first->specification->getDisplayName() << "\";" << endl;
        cout << "}" << endl;
    }

    cout << mainGraph;

    cout << toVertexName(this->graph->startPluginVertex, this->graph->getVertexIdentifier(this->graph->startPluginVertex)) << " [shape=Mdiamond];" << endl;
    cout << toVertexName(this->graph->endPluginVertex, this->graph->getVertexIdentifier(this->graph->endPluginVertex)) << " [shape=Mdiamond];" << endl;

    cout << "}" << endl;
}
