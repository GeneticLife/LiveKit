#include <memory>

#include "ExecutionScheduler.h"

#include "../TileBasedFrameworkInterface.h"

#include <string>

using namespace std;

ExecutionScheduler::ExecutionScheduler(Framework *framework, ExecutionGraph *graph, MemoryManager *memoryManager)
        : framework(framework)
        , graph(graph)
        , memoryManager(memoryManager)
        , latestAvailableCycle(0)
        , processedVertices(0)
        , finished(false)
        , waitForCycleDataSteps(this->cycleCompare)
        , numStepsRunning(0) {

    for (unsigned int i = 0; i < this->graph->getNumVertices(); i++) {
        this->memoryManager->preparedInputContainer.push_back(make_shared<FragmentContainer>());
        this->isProcessed[i] = false;
    }

    availableSteps.push_back(this->graph->startPluginVertex);
}

bool ExecutionScheduler::notEnoughMemoryToExecuteNextStep(VertexIdentifier currentVertexIdentifier) {
    if ((float) this->memoryManager->getRSS() > (float) this->memoryManager->getMaxRamSize() * 1.1) {
        // there is not enough free memory to start the nextStep in a new thread
        if (this->numStepsRunning > 0) // wait with execution of nextStep until other threads finished execution and there is more free memory
            return true;
        else // we allow the execution of the nextStep (even though there is not enough freeMemory) to make assure that at least one thread can always run
        if (currentVertexIdentifier.cycle != 0) { // we cannot switch to memorySaveMode before runPreprocessing is finished (some values might not be initialized yet)
            currentVertexIdentifier.plugin->switchToMemorySaveMode();
        }
    }

    return false;
}

ExecutionStatus ExecutionScheduler::nextStep() {
    ExecutionStep step(this);
    step.prepareMemory();
    step.execute();
    step.releaseMemory();
    step.prepareNextSteps();

    return step.result();
}

void ExecutionScheduler::updateExecutionQueue(Vertex currentPluginVertex, std::shared_ptr<FragmentContainer> outputFragmentContainer) {

    for (auto targetPluginVertex : this->graph->successors(currentPluginVertex)) {
        Edge edge = this->graph->getEdge(currentPluginVertex, targetPluginVertex);

        for (auto const &fragmentSpecification : this->graph->getEdgePayload(edge)) {
            string fragmentType = fragmentSpecification.fragmentType;

            if (!fragmentType.empty() && outputFragmentContainer->has(fragmentType)) {
                auto outputFragment = outputFragmentContainer->get(fragmentType);

                // Move required fragments over to the input containers of the following plugins
                this->memoryManager->preparedInputContainer[targetPluginVertex]->add(outputFragment);

                this->memoryManager->addToCanBeSerialized(outputFragment);
            }
        }

        this->isProcessed[currentPluginVertex] = true;

        if (this->allEdgesProcessed(targetPluginVertex))
            this->enqueueStep(targetPluginVertex);
    }
}

void ExecutionScheduler::enqueueStep(Vertex pluginVertex) {
    if (this->graph->getVertexIdentifier(pluginVertex).cycle > this->latestAvailableCycle) {
        lock_guard<mutex> lock(this->waitForCycleDataStepsMutex);
        this->waitForCycleDataSteps.push(pluginVertex);
    } else {
        lock_guard<mutex> lock(this->availableStepsMutex);
        this->availableSteps.push_back(pluginVertex);
    }
}

void ExecutionScheduler::setLatestAvailableCycle(int cycleNumber) {
    this->latestAvailableCycle = cycleNumber;

    lock_guard<mutex> availableLock(this->availableStepsMutex);
    lock_guard<mutex> waitLock(this->waitForCycleDataStepsMutex);

    while (!this->waitForCycleDataSteps.empty()) {
        Vertex vertex = this->waitForCycleDataSteps.top();
        if (this->graph->getVertexIdentifier(vertex).cycle <= this->latestAvailableCycle) {
            this->waitForCycleDataSteps.pop();
            this->availableSteps.push_back(vertex);
        } else break;
    }
}

ExecutionGraph::Vertex ExecutionScheduler::getNextStep() {
    //lock_guard<mutex> lock(this->availableStepsMutex);
    auto vertexIterator = min_element(
        this->availableSteps.begin(),
        this->availableSteps.end(),
        [&](Vertex v1, Vertex v2) {
            return
                    this->memoryManager->getSerializedSizeForVertexIdentifier(v1, this->graph->getVertexIdentifier(v1))
                < this->memoryManager->getSerializedSizeForVertexIdentifier(v2, this->graph->getVertexIdentifier(v2));
        }
    );

    auto vertex = *vertexIterator;
    this->availableSteps.erase(vertexIterator);

    return vertex;
}

bool ExecutionScheduler::allEdgesProcessed(Vertex pluginVertex) {
    for (Vertex v : this->graph->predecessors(pluginVertex))
        if (!this->isProcessed[v]) return false;
    return true;
}

ExecutionScheduler::PluginHook ExecutionScheduler::toPluginHook(VertexIdentifier identifier) {
    if (identifier.cycle == this->graph->PRE_CYCLE)
        return [identifier](shared_ptr<FragmentContainer> inputFragments) -> std::shared_ptr<FragmentContainer>  {
            return identifier.plugin->runPreprocessing(inputFragments);
        };
    if (identifier.cycle == this->graph->POST_CYCLE)
        return [identifier](shared_ptr<FragmentContainer> inputFragments) -> std::shared_ptr<FragmentContainer>  {
            return identifier.plugin->runFullReadPostprocessing(inputFragments);
        };
    if (identifier.lane == this->graph->ALL_LANES && identifier.tile == this->graph->ALL_TILES)
        return [identifier](shared_ptr<FragmentContainer> inputFragments) -> std::shared_ptr<FragmentContainer>  {
            identifier.plugin->setCurrentPluginCycle(identifier.cycle);
            return identifier.plugin->runCycle(inputFragments);
        };
    // tile - based Cycle hooks
    return [&, identifier](std::shared_ptr<FragmentContainer> inputFragments) -> std::shared_ptr<FragmentContainer>  {
        auto tileContext = identifier.plugin->getTileContext();
        tileContext->plugin = identifier.plugin;
        tileContext->framework = new TileBasedFrameworkInterface(this->framework, identifier.cycle, identifier.lane, identifier.tile);
        tileContext->out = identifier.plugin->tileOutputs[identifier.lane][identifier.tile];
        tileContext->out.setCycle(identifier.cycle);
        tileContext->lane = identifier.lane;
        tileContext->tile = identifier.tile;
        tileContext->tileFragment = identifier.plugin->tileFragments[identifier.lane][identifier.tile];

        auto outputContainer = tileContext->runCycleForTile(inputFragments);

        delete tileContext->framework;
        delete tileContext;

        return outputContainer;
    };
}

void ExecutionScheduler::increaseProcessedVertices(){
    this->processedVertices++;
    float progress = (float)this->processedVertices / (float)this->graph->getNumVertices();
    this->framework->getOutputManager()->updateProgress(progress);
}

ExecutionScheduler::~ExecutionScheduler() = default;