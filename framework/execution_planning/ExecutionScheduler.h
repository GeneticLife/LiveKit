#ifndef LIVEKIT_EXECUTIONSCHEDULER_H
#define LIVEKIT_EXECUTIONSCHEDULER_H

#include <vector>

#include "../fragments/FragmentContainer.h"
#include "../MemoryManager.h"
#include "../Framework.h"
#include "ExecutionGraphBuilder.h"
#include "ExecutionStep.h"

enum ExecutionStatus : uint8_t {
    PREPARING,
    NO_STEP_AVAILABLE,
    WAIT_FOR_NEXT_CYCLE,
    WAIT_FOR_MEMORY,
    NOT_ENOUGH_MEMORY,
    READY_FOR_EXECUTION,
    RELEASE_MEMORY,
    PREPARE_NEXT_STEPS,
    DONE,
    ALL_STEPS_DONE
};

// a successful execution goes like this:
// WAIT_FOR_MEMORY => READY_FOR_EXECUTION => RELEASE_MEMORY => PREPARE_NEXT_STEPS => DONE


class ExecutionScheduler {
    Framework *framework;
    ExecutionGraph *graph;
    MemoryManager *memoryManager;

    int latestAvailableCycle;
    
    int processedVertices;

    bool finished;

    typedef ExecutionGraph::Vertex Vertex;
    typedef ExecutionGraph::Edge Edge;

    std::map<Vertex, bool> isProcessed;

    std::mutex checkEnoughMemoryMutex;

    std::mutex availableStepsMutex;
    std::list<Vertex> availableSteps;

    std::function<bool (Vertex, Vertex)> cycleCompare =
            [&](Vertex left, Vertex right) {
                return
                      this->graph->getVertexIdentifier(left).cycle
                    < this->graph->getVertexIdentifier(right).cycle;
            };
    std::mutex waitForCycleDataStepsMutex;
    std::priority_queue<Vertex, std::vector<Vertex>, decltype(cycleCompare)> waitForCycleDataSteps;

    std::atomic<int> numStepsRunning;

    void updateExecutionQueue(Vertex currentPluginVertex, std::shared_ptr<FragmentContainer> outputFragmentContainer);

    bool allEdgesProcessed(Vertex pluginVertex);

    typedef std::function<std::shared_ptr<FragmentContainer> (std::shared_ptr<FragmentContainer>)> PluginHook;
    PluginHook toPluginHook(VertexIdentifier identifier);

    void enqueueStep(Vertex pluginVertex);

    bool notEnoughMemoryToExecuteNextStep(VertexIdentifier currentVertexIdentifier);

    friend class ExecutionStep;

public:
    ExecutionScheduler(Framework *framework, ExecutionGraph *graph, MemoryManager *memoryManager);

    ExecutionStatus nextStep();

    void setLatestAvailableCycle(int cycleNumber);

    void increaseProcessedVertices();

    /**
     * Choose the VertexIdentifier with the lowest 'serializedSize' out of all Vertexs in 'executionQueue'
     * @return the VertexIdentifier that has the lowest 'serializedSize' and therefore should be executed
     */
    ExecutionGraph::Vertex getNextStep();

    ~ExecutionScheduler();
};


#endif //LIVEKIT_EXECUTIONSCHEDULER_H
