#include "ExecutionStep.h"

using namespace std;

ExecutionStatus ExecutionStep::determineInitialStatus() {
    if (this->executionScheduler->finished)
        return ExecutionStatus::ALL_STEPS_DONE;

    lock_guard<mutex> lock(this->executionScheduler->availableStepsMutex);

    if (this->executionScheduler->availableSteps.empty())
        if (this->executionScheduler->waitForCycleDataSteps.empty())
            return ExecutionStatus::NO_STEP_AVAILABLE;
        else
            return ExecutionStatus::WAIT_FOR_NEXT_CYCLE;
    else {
        this->vertex = this->executionScheduler->getNextStep();
        this->identifier = this->executionScheduler->graph->getVertexIdentifier(this->vertex);
        return ExecutionStatus::WAIT_FOR_MEMORY;
    }
}

void ExecutionStep::prepareMemory() {
    if (this->status != ExecutionStatus::WAIT_FOR_MEMORY) return;

    this->executionScheduler->checkEnoughMemoryMutex.lock();

    this->executionScheduler->memoryManager->markAllDependenciesAsUsed(this->vertex);
    this->executionScheduler->memoryManager->serializeUnusedData();


    if (this->executionScheduler->notEnoughMemoryToExecuteNextStep(this->identifier)){
        this->status = ExecutionStatus::NOT_ENOUGH_MEMORY;
        this->executionScheduler->checkEnoughMemoryMutex.unlock();
    }
    else
        this->status = ExecutionStatus::READY_FOR_EXECUTION;
}

void ExecutionStep::execute() {
    if (this->status != ExecutionStatus::READY_FOR_EXECUTION) return;

    /*
    cout    << "this->numPluginslugin: "
            << this->identifier.plugin->specification->getDisplayName()
            << " on lane "  << this->identifier.lane
            << " and tile " << this->identifier.tile
            << " in cycle " << this->identifier.cycle
            << "." << endl;
    */

    this->executionScheduler->numStepsRunning++;
    this->executionScheduler->checkEnoughMemoryMutex.unlock();

    ExecutionScheduler::PluginHook pluginHook = this->executionScheduler->toPluginHook(this->identifier);
    this->outputFragmentContainer = pluginHook(this->executionScheduler->memoryManager->preparedInputContainer[this->vertex]);

    this->executionScheduler->numStepsRunning--;
    this->executionScheduler->increaseProcessedVertices();

    this->status = ExecutionStatus::RELEASE_MEMORY;

}

void ExecutionStep::releaseMemory() {
    if (this->status != ExecutionStatus::RELEASE_MEMORY && this->status != ExecutionStatus::NOT_ENOUGH_MEMORY)
        return;

    if (this->status == ExecutionStatus::NOT_ENOUGH_MEMORY)
        this->executionScheduler->enqueueStep(this->vertex);
    else {
        // Clear preparedInputContainer of last executed plugin - so the fragments that are unused can be freed
        this->executionScheduler->memoryManager->preparedInputContainer[this->vertex]->clear();
        this->executionScheduler->memoryManager->updateUsedCycleInterval(this->identifier);
        this->status = PREPARE_NEXT_STEPS;
    }

    this->executionScheduler->memoryManager->markAllDependenciesAsUnused(this->vertex);
}

void ExecutionStep::prepareNextSteps() {
    if (this->status != ExecutionStatus::PREPARE_NEXT_STEPS) return;

    this->executionScheduler->updateExecutionQueue(this->vertex, this->outputFragmentContainer);

    this->status = ExecutionStatus::DONE;
}

ExecutionStatus ExecutionStep::result() {
    if (this->status != ExecutionStatus::DONE)
         return this->status;

    if (this->vertex == this->executionScheduler->graph->endPluginVertex) {
        this->executionScheduler->finished = true;
        this->status = ExecutionStatus::ALL_STEPS_DONE;
    }

    return this->status;
}
