#ifndef LIVEKIT_EXECUTIONSTEP_H
#define LIVEKIT_EXECUTIONSTEP_H

#include "ExecutionScheduler.h"

enum ExecutionStatus : uint8_t;
class ExecutionScheduler;

class ExecutionStep {
    ExecutionScheduler *executionScheduler;
    ExecutionStatus status;
    std::shared_ptr<FragmentContainer> outputFragmentContainer = nullptr;
    ExecutionGraph::Vertex vertex;
    VertexIdentifier identifier;
public:
    explicit ExecutionStep(ExecutionScheduler *executionScheduler)
        : executionScheduler(executionScheduler)
        , status(this->determineInitialStatus()) {};

    ExecutionStatus determineInitialStatus();
    void prepareMemory();
    void execute();
    void releaseMemory();
    void prepareNextSteps();
    ExecutionStatus result();
};

#endif //LIVEKIT_EXECUTIONSTEP_H
