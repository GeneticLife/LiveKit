#ifndef LIVEKIT_GRAPH_H
#define LIVEKIT_GRAPH_H

#include <iostream>
#include <vector>
#include <map>
#include <functional>

template <typename VertexIdentifier, typename EdgePayload>
class Graph {
public:
    typedef unsigned int Vertex;
    typedef std::pair<Vertex, Vertex> Edge;
private:
    unsigned int numVertices;
    unsigned int numEdges;

    std::vector<std::vector<Vertex>> adjacencyList;
    std::vector<std::vector<Vertex>> invertedAdjacencyList;
    std::vector<VertexIdentifier> vertexIdentifier;
    std::map<Edge, std::vector<EdgePayload>> edgePayload;
    std::map<Vertex, std::map<Vertex, bool>> isEdgeExisting;
public:
    Graph()
        : numVertices(0)
        , numEdges(0)
        , adjacencyList()
        , invertedAdjacencyList()
        , vertexIdentifier()
        , edgePayload()
        , isEdgeExisting() {};

    Vertex addVertex(VertexIdentifier identifier) {
        Vertex v = numVertices++;

        this->adjacencyList.emplace_back();
        this->invertedAdjacencyList.emplace_back();
        this->isEdgeExisting[v] = std::map<Vertex, bool>();

        this->vertexIdentifier.push_back(identifier);
        return v;
    };

    VertexIdentifier &getVertexIdentifier(Vertex v) {
        return this->vertexIdentifier[v];
    };

    std::vector<EdgePayload> &getEdgePayload(Edge e) {
        return this->edgePayload[e];
    };

    unsigned int getNumVertices() {
        return this->numVertices;
    };

    unsigned int getNumEdges() {
        return this->numEdges;
    }

    Edge addEdge(Vertex from, Vertex to) {
        Edge e = this->getEdge(from, to);

        if (!this->hasEdge(from, to)) {
            this->adjacencyList[from].push_back(to);
            this->invertedAdjacencyList[to].push_back(from);
            this->isEdgeExisting[from][to] = true;

            this->edgePayload[e] = {};

            this->numEdges++;
        }

        return e;
    };

    Edge addEdge(Vertex from, Vertex to, EdgePayload payload) {
        Edge e = this->addEdge(from, to);
        this->edgePayload[e].push_back(payload);
        return e;
    }

    bool hasEdge(Vertex from, Vertex to) {
        return this->isEdgeExisting[from].count(to) && this->isEdgeExisting[from][to];
    };

    Edge getEdge(Vertex from, Vertex to) {
        return std::make_pair(from, to);
    };

    std::vector<Vertex> &successors(Vertex v) {
        return this->adjacencyList[v];
    };

    std::vector<Vertex> &predecessors(Vertex v) {
        return this->invertedAdjacencyList[v];
    };
};


#endif //LIVEKIT_GRAPH_H
