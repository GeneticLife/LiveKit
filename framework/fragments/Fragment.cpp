#ifndef LIVEKIT_FRAGMENT_CPP
#define LIVEKIT_FRAGMENT_CPP

#include "Fragment.h"
#include "../utils.h"
#include <boost/filesystem.hpp>

using namespace std;

unsigned long Fragment::getFilesize(const string &fileName) {

    std::ifstream file(fileName.c_str(), std::ifstream::in | std::ifstream::binary);

    if(!file.is_open()) { return 0; }

    file.seekg(0, ios::end);
    int fileSize = file.tellg();
    file.close();

    return fileSize;
}

unsigned long Fragment::getDirectorySize(const string &directoryPath) {
    unsigned long size = 0;
    for (boost::filesystem::recursive_directory_iterator it(directoryPath);
         it!=boost::filesystem::recursive_directory_iterator();
         ++it) {
        if (!boost::filesystem::is_directory(*it))
            size+=boost::filesystem::file_size(*it);
    }

    return size;
}

template<typename T>
T Fragment::get(string name) {
    return *this->getPointer<T>(name);
};

template<typename T>
T *Fragment::getPointer(string name) {
    if (this->isSerialized)
        this->deserialize();
    if (this->map.count(name)) {
        return (T *) this->map[name];
    } else {
        throw runtime_error("Element does not exist");
    }
}

template<>
string Fragment::get(string name) {
    return (string) getPointer<char>(name);
}

template<typename T>
void Fragment::set(string name, T value) {
    if (this->isSerialized) this->deserialize();

    if (this->map.count(name)) {
        *(T *) this->map[name] = value;
    } else {
        auto storage = (T *) malloc(sizeof(T));
        *storage = value;
        this->map[name] = (void *) storage;
        this->sizes[name] = sizeof(T);
    }
}

template<>
void Fragment::set(string name, string value) {
    if (this->isSerialized) this->deserialize();

    if(map.count(name)) this->erase(name);

    char *someArray = this->initArray<char>(name, value.length()+1); // +1 for null byte
    value += '\0';
    const char *valuePointer = value.c_str();
    memcpy(someArray, valuePointer, value.length());
}

void Fragment::erase(string name) {
    if (this->isSerialized) this->deserialize();

    if (this->map.count(name)) {
        free(this->map[name]);
        this->map.erase(name);
        this->sizes.erase(name);
    }
}

template<typename T>
T *Fragment::initArray(string name, size_t size) {
    if (this->isSerialized) this->deserialize();

    if (!this->map.count(name)) {
        auto storageAddress = (T *) malloc(sizeof(T) * size);
        this->map[name] = (void *) storageAddress;
        this->sizes[name] = sizeof(T) * size;
        return storageAddress;
    } else {
        throw runtime_error("Element already initialized");
    }
}

void Fragment::setSerializable(string name, Serializable *serializable) {
    if (this->isSerialized) this->deserialize();

    if (!this->serializables.count(name)) {
        this->serializables[name] = serializable;
    } else {
        throw runtime_error("Serializable element already exists");
    }
}

Serializable *Fragment::getSerializable(string name) {
    if (this->isSerialized) this->deserialize();

    return this->serializables[name];
}

bool Fragment::serialize() {
    lock_guard<mutex> lock(this->serializationMutex);
    if (this->isSerialized)
        return false;

    cout << "Serializing Fragment " << this->name << " with hash " << this->hash << endl;

    FILE *serializeFile;
    string filename = this->getSerializedFileName();
    serializeFile = fopen(filename.c_str(), "wb");

    unsigned long size = this->map.size();
    fwrite(&size, sizeof(unsigned long), 1, serializeFile);

    // Primitives
    for (auto it = this->map.cbegin(); it != this->map.cend();) {
        this->serializeName(it->first, serializeFile);
        this->serializePrimitive(it->second, this->sizes[it->first], serializeFile);
        this->sizes.erase(it->first);
        it = this->map.erase(it);
    }

    size = this->serializables.size();
    fwrite(&size, sizeof(size), 1, serializeFile);

    // Serializables
    for (auto it = this->serializables.cbegin(); it != this->serializables.cend();) {
        this->serializeName(it->first, serializeFile);
        this->serializeSerializable(it->first, it->second, serializeFile);
        delete it->second;
        it = this->serializables.erase(it);
    }

    fclose(serializeFile);

    this->serializedSize += getFilesize(filename);
    this->isSerialized = true;

    return true;
}

void Fragment::serializeName(const std::string &name, FILE *serializeFile) {
    unsigned long size = name.size();
    fwrite(&size, sizeof(unsigned long), 1, serializeFile);
    fwrite(name.c_str(), size, 1, serializeFile);
}

void Fragment::serializePrimitive(void *primitive, unsigned long size, FILE *serializeFile) {
    fwrite(&size, sizeof(unsigned long), 1, serializeFile);
    fwrite(primitive, size, 1, serializeFile);
    free(primitive);
}

void Fragment::serializeSerializable(const string &name, Serializable *serializable, FILE *serializeFile) {
    unsigned long size = serializable->serializableSize(); // TODO: Consider caching this value
    char *serializedSpace = (char *) malloc(sizeof(char) * size);

    string serializationDirectory = "./temp/" + this->name + "_" + this->hash + "/" + name + "_serializable_" + this->hash + "/";
    boost::filesystem::create_directories(serializationDirectory);

    serializable->serialize(serializedSpace, serializationDirectory);

    this->serializedSize += getDirectorySize(serializationDirectory);

    serializePrimitive(serializedSpace, size, serializeFile);

}

bool Fragment::deserialize() {
    lock_guard<mutex> lock(this->serializationMutex);
    if (!this->isSerialized) return false;

    cout << "Deserializing Fragment " << this->name << " with hash " << this->hash << endl;

    FILE *serializedFile;
    string filename = this->getSerializedFileName();
    serializedFile = fopen(filename.c_str(), "rb");

    unsigned long count;
    fread(&count, sizeof(unsigned long), 1, serializedFile);

    // deserialize primitives
    for (unsigned long i = 0; i < count; i++) {
        string name = deserializeName(serializedFile);

        unsigned long size;
        fread(&size, sizeof(unsigned long), 1, serializedFile);

        void *primitive = malloc(size);
        fread(primitive, size, 1, serializedFile);

        this->map[name] = primitive;
        this->sizes[name] = size;
    }

    fread(&count, sizeof(unsigned long), 1, serializedFile);

    // deserialize serializables
    for (unsigned long i = 0; i < count; i++) {
        string name = deserializeName(serializedFile);

        unsigned long size;
        fread(&size, sizeof(unsigned long), 1, serializedFile);

        string serializableType = deserializeName(serializedFile);

        long offset = sizeof(unsigned long) + serializableType.size() * sizeof(char);

        fseek(serializedFile, -offset, SEEK_CUR);

        auto data = (char *) malloc(size);
        fread(data, size, 1, serializedFile);

        string serializationDirectory = "./temp/" + this->name + "_" + this->hash + "/" + name + "_serializable_" + this->hash + "/";

        auto tmpSerializable = this->serializableFactory->createSerializable(serializableType, size, data, serializationDirectory);

        this->serializables[name] = tmpSerializable;
    }

    boost::filesystem::remove_all("./temp/" + this->name + "_" + this->hash + "/");

    fclose(serializedFile);
    remove(filename.c_str());

    this->serializedSize = 0;
    this->isSerialized = false;

    return true;
}

string Fragment::deserializeName(FILE *serializedFile) {
    unsigned long size;
    fread(&size, sizeof(unsigned long), 1, serializedFile);

    auto tmpName = (char *) malloc(size * sizeof(char));
    fread(tmpName, size, 1, serializedFile);

    string name(tmpName, tmpName + size);
    free(tmpName);
    return name;
}

string Fragment::generateHash() {
    string hash;
    hash.resize(10);
    static const char alphanum[] =
            "0123456789"
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < 10; ++i) {
        hash[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    return hash;
}

string Fragment::getHash() {
    return this->hash;
}

unsigned long Fragment::getSerializedSize() {
    return this->serializedSize;
}

string Fragment::getSerializedFileName() {
    return "temp/" + this->name + "_" + this->hash + ".fragment";
}

bool Fragment::has(std::string key) {
    return this->map.count(key) > 0;
}

bool Fragment::hasSerializable(std::string key) {
    return this->serializables.count(key) > 0;
}

bool Fragment::isFragmentSerialized() {
    return this->isSerialized;
}

Fragment::~Fragment() {
    if (!this->isSerialized) {
        for (auto const &element : this->map) {
            free(element.second);
        }

        for (auto const &serializable : this->serializables) {
            delete serializable.second;
        }
    } else {
        // No need to deserialize before destruction - simply remove the serialized fragment file
        remove(this->getSerializedFileName().c_str());
    }
}

#endif // LIVEKIT_FRAGMENT_CPP