#ifndef LIVEKIT_FRAGMENTCONTAINER_H
#define LIVEKIT_FRAGMENTCONTAINER_H

#include <map>
#include <memory>
#include <mutex>

#include "Fragment.h"

class FragmentContainer {
private:
    std::mutex writingMutex;

    friend class Framework;

public:
    virtual std::vector<std::shared_ptr<Fragment>> getAllFragments();

    std::map<std::string, std::shared_ptr<Fragment>> fragments;

    virtual std::shared_ptr<Fragment> add(std::shared_ptr<Fragment> fragment);

    virtual std::shared_ptr<Fragment> get(std::string name);

    virtual bool has(std::string name);

    virtual void clear();

    virtual ~FragmentContainer() = default;
};

#endif //LIVEKIT_FRAGMENTCONTAINER_H
