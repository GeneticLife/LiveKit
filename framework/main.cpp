#include "main.h"
#include <boost/filesystem.hpp>
#include <iostream>

#include "Framework.h"

using namespace std;

int main(int argc, char **argv) {
    // Create a temp directory for all output files
    boost::filesystem::create_directory(boost::filesystem::path("./temp"));
    boost::filesystem::create_directories("./temp/sequences/");

    if(argc != 2 || strcmp(argv[1], "--help") == 0){
        printHelpPage();
        return 0;
    }

    string frameworkManifestFilePath = argv[1];
    Framework framework(frameworkManifestFilePath);
    framework.run();

    framework.unloadPlugins();

    return 0;
}

void printHelpPage(){
    cout << "Open help page:" << endl;
    cout << "   ./main" << endl;
    cout << "   ./main --help" << endl;
    cout << endl;

    cout << "Run with config" << endl;
    cout << "   ./main \"path/to/framework.json\"" << endl;
    cout << endl;

    cout << "Run the framework with an example config:" << endl;
    cout << "   .main \"../config/framework.json\"" << endl;
    cout << endl;

    cout << "Parameter for framework.json" << endl;
    cout << "   Important parameter for specifying the plugins:" << endl;
    cout << "       NAME                        DEFAULT VALUE   DESCRIPTION" << endl;
    cout << "       plugins                     []              A list of all plugins that will be executed" << endl << endl;
    cout << "       example:" << endl;
    cout << "       {\n"
            "         \"pluginPath\": \"./../path/to/plugin.so\",\n"
            "         \"configPath\": \"./../path/to/config.json\",\n"
            "         \"inputFragments\": {},\n"
            "         \"outputFragments\": {\n"
            "           \"preprocessing\": [\n"
            "             {\n"
            "               \"name\": \"index\",\n"
            "               \"type\": \"FMIndex\"\n"
            "             }\n"
            "           ]\n"
            "         }\n"
            "       }" << endl << endl;
    cout << "       inputFragments and outputFragments both have three categories (preprocessing, cycle and postprocessing). " << endl;
    cout << "       In those categories the developer can specify which fragments are required as input (or as output). " << endl;
    cout << "       A category does not have to be specified if the plugin has no fragments for this category." << endl;
    cout << endl;
    cout << "   Main parameters:" << endl;
    cout << "       NAME                        DEFAULT VALUE   DESCRIPTION" << endl;
    cout << "       ramLimit                    \"500GB\"         The size of the RAM that should be utilized (B/KB/MB/GB)" << endl;
    cout << "       numThreads                  1               The number of threads that can be used" << endl;
    cout << "       RunInfoPath                 \"\"              The path to the RunInfo.xml file" << endl;
    cout << "       SampleSheetPath             \"\"              [optional] The path to the sampleSheet.csv file" << endl;
    cout << "       BaseCallsDirectory          \"\"              Path to the BaseCall directory" << endl;
    cout << "       SerializableFactoryPath     \"\"              Path to the libSerializableFactory.so" << endl;
    cout << "       BclParserType               \"BclParser\"     The type of the BclParser (BclParser (.bcl) / CompressedBclParser (.bcl.gz) / AggregatedBclParser (.cbcl) / BgzfBclParser (.bgzf)" << endl;
    cout << "       barcodeErrors               \"2, 2\"          How many errors are allowed for the barcodes. (Parsed from the SampleSheet)enableTUIfalseFlag whether to enable the text-based user interface" << endl;
    cout << "       outDir                      \"./out/\"        Path to the framework-wide default output directory" << endl;
    cout << "       printGraphStructure         false           Flag whether to print the ExecutionGraph structure as a dot graph" << endl;
    cout << endl;
    cout << "   Other parameter (these are parsed from the RunInfo.xml but can be overwritten in the config):" << endl;
    cout << "      NAME                         DEFAULT VALUE   DESCRIPTION" << endl;
    cout << "      reads                        \"100R\"          Specifies the read structure e.g. \"100R,8B,8B,100R\" " << endl;
    cout << "      lanes                        \"1\"             Specifies the lanes e.g. \"1,2\" " << endl;
    cout << "      tiles                        \"1101\"          Specifies the tiles e.g. \"1101,1102\" " << endl;
    cout << "      run-id                       \"0\"             Id of this specific run " << endl;
    cout << "      flowcell-id                  \"Default\"       Id of the flowcell " << endl;
    cout << "      instrument-id                \"0\"             Id of the sequencer " << endl;
    cout << "      surfaceCount                 1               Number of surfaces " << endl;
    cout << "      swathCount                   1               Number of swaths " << endl;
    cout << "      tileCountPerSwath            1               Number of tiles per swath " << endl;
    cout << endl;

    cout << "For more information visit https://gitlab.com/GeneticLife/LiveKit/wikis/configs/Framework-Config" << endl;
}
