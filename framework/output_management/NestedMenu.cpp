#ifndef NESTED_MENU_CPP
#define NESTED_MENU_CPP

#include <string>
#include "NestedMenu.h"

NestedMenu::NestedMenu() {
    this->parentItem = nullptr;
    // TODO
}

void NestedMenu::recreateMenu() {
    // delete old menu
    unpost_menu(this->menu);
    free_menu(this->menu);

    // create it again
    this->initializeMenu();
}

void NestedMenu::addItem(ItemObject *item) {
    item->parentMenu = this;
    this->items.push_back(item);
    this->pureItems.push_back(item->item); // insert pure item
}

void NestedMenu::initializeMenu() {
    this->pureItems.push_back(0); // has to be terminated for 'new_menu'
    this->menu = new_menu((ITEM **) this->pureItems.data());
    set_menu_format(menu, LINES - 5, 1);

    for (auto item : this->items) {
        if (item->subMenu)
            item->subMenu->initializeMenu();
    }

    refresh();

    this->initialized = true;
}

bool NestedMenu::isEmpty() {
    return this->items.empty();
}

NestedMenu::~NestedMenu() {
    if (this->initialized) {
        unpost_menu(this->menu);
        free_menu(this->menu);
    }
    for (auto item : this->items)
        delete item;
    // no need to delete pointers in 'pureItems' because they are deleted when 'items' gets destroyed
}

#endif