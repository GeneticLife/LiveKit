#ifndef NESTED_MENU_WRAPPER_H
#define NESTED_MENU_WRAPPER_H

#include "NestedMenu.h"
#include "ItemObject.h"
#include <deque>

class NestedMenuWrapper {
private:
    NestedMenu *currentMenu;
public:

    WINDOW *menuWindow;

    NestedMenuWrapper(NestedMenu *menu, WINDOW *menuWindow);

    MENU *getMenu();

    void drawWindow();

    std::string handleKey(int c);

    std::vector<std::string> getSelectedMenuPath();

    ~NestedMenuWrapper();
};

#endif
