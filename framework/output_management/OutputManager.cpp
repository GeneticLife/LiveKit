#include "OutputManager.h"
#include "OutputWrapper.h"
#include "../Plugin.h"

using namespace std;

OutputManager::OutputManager(OutputInterface& outputInterface) : outputInterface(outputInterface) {}

void OutputManager::setupPlugins(vector<uint16_t> lanes, vector<uint16_t> tiles, set<Plugin*> plugins) {

    for (Plugin* plugin : plugins) {
        vector<string> entry = {plugin->specification->getDisplayName(), "Main"};
        plugin->out = OutputWrapper(
            entry,
            this,
            this->outputInterface.registerOStream(entry)
        );

        if (!plugin->hasTileContext()) continue;

        for (auto lane : lanes) {
            for (auto tile : tiles) {
                entry = {
                    plugin->specification->getDisplayName(),
                    to_string(lane) + "_" + to_string(tile)
                };

                plugin->tileOutputs[lane][tile] = OutputWrapper(
                    entry,
                    this,
                    this->outputInterface.registerOStream(entry)
                );
            }
        }
    }

    this->outputInterface.start();
}

void OutputManager::captureStream(vector<string> menuPath, ostream &stream) {
    this->outputInterface.captureStream(menuPath, stream);
};

void OutputManager::updateProgress(float progress) {
    this->outputInterface.updateProgress(progress);
}

void OutputManager::updateCycle(std::vector<std::string> menuPath, int cycle) {
    this->outputInterface.updateCycle(menuPath, cycle);
}

void OutputManager::notify(std::vector<std::string> menuName) {
    this->outputInterface.notify(menuName);
}