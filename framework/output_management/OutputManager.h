#ifndef LIVEKIT_OUTPUTMANAGER_H
#define LIVEKIT_OUTPUTMANAGER_H

#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <sstream>
#include "OutputInterface.h"

using namespace std;

class Plugin;

class OutputManager {
private:
    OutputInterface& outputInterface;
public:
    OutputManager(OutputInterface& outputInterface);

    void setupPlugins(std::vector<uint16_t> lanes, std::vector<uint16_t> tiles, std::set<Plugin*> plugins);

    void captureStream(std::vector<std::string> menuName, std::ostream &stream);

    virtual void notify(std::vector<std::string> menuName = {});

    void updateProgress(float progress);

    virtual void updateCycle(std::vector<std::string> menuName, int cycle);

    virtual ~OutputManager() = default;
};

#endif //LIVEKIT_OUTPUTMANAGER_H
