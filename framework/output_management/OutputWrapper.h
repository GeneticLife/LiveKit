#ifndef LIVEKIT_OUTPUTWRAPPER_H
#define LIVEKIT_OUTPUTWRAPPER_H

#include <string>
#include <sstream>

#include "OutputManager.h"

class OutputManager;

class OutputWrapper {
    std::vector<std::string> menuPath;

    OutputManager *outputManager;

    std::ostream *stream;

    friend class OutputManager;
public:
    OutputWrapper() : menuPath{}, outputManager(nullptr), stream(nullptr) {}

    OutputWrapper(std::vector<std::string> menuPath, OutputManager *outputManager, std::ostream* stream)
        : menuPath(menuPath)
        , outputManager(outputManager)
        , stream(stream) {}


    virtual OutputWrapper &operator<<(std::string input) { return this->printOutput(input); }

    template<typename T>
    OutputWrapper &operator<<(T input) { return this->printOutput(input); }

    template<typename T>
    OutputWrapper &printOutput(T input) {
        (*stream) << input;
        if (this->outputManager)
            outputManager->notify(this->menuPath);
        return *this;
    }

    virtual OutputWrapper &operator<<( std::ostream&(*endl)(std::ostream&) ) {
        (*stream) << endl;
        return *this;
    }

    virtual void setCycle(uint16_t cycle) {
        if (this->outputManager) this->outputManager->updateCycle(this->menuPath, cycle);
    }

    virtual ~OutputWrapper() = default;
};



#endif //LIVEKIT_OUTPUTWRAPPER_H
