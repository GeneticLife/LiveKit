#include <iostream>
#include <signal.h>
#include <cstring>
#include <thread>
#include <algorithm>
#include <zconf.h>

#include "TUIOutput.h"

using namespace std;

TUIOutput::TUIOutput() : progress(0), cycleCount(0) {
    // disable cout
    cout.setstate(ios_base::failbit);
    this->nullStream << "Go to submenu by pressing Enter!" << endl;
}

void static_handle_winch(int sig) {
    TUIOutput::getInstance().handle_winch(sig);
}

void static_quit() {
    TUIOutput::getInstance().quit();
}

void
draw_box(WINDOW *window, string title = "", chtype color = COLOR_PAIR(1), int borderType1 = 0, int borderType2 = 0) {
    wattron(window, color);
    box(window, borderType1, borderType2);
    if (title != "")
        mvwaddstr(window, 0, 3, title.c_str());
    wattroff(window, color);
}

void TUIOutput::quit(void) {
    sleep(1); // TODO: why is this needed?
    mvaddstr(LINES - 2, 1, "Done! Press 'q' to quit.");
    refresh();
    this->finalize = true;
    this->outputThread.join();

    cout.clear();

    delete this->wrapper;

    delwin(this->menuBoxWindow);
    delwin(this->menuWindow);
    delwin(this->mainBoxWindow);
    delwin(this->mainWindow);
    delwin(this->progressBarWindow);

    endwin();
}

void TUIOutput::uptdateDimensions(int columns, int rows) {
    this->x_mainSection = 3;
    this->y_mainSection = menuWidth + 1;
    this->height_mainSection = rows - 6;
    this->width_mainSection = columns - this->y_mainSection;
}

void TUIOutput::resizeWindow() {
    // menuWindow
    wclear(this->menuBoxWindow);
    wclear(this->menuWindow);
    wresize(this->menuBoxWindow, LINES - 3, this->menuWidth);
    wresize(this->menuWindow, LINES - 5, this->menuWidth - 2);
    draw_box(this->menuBoxWindow, "Available Streams");

    // mainWindow
    wclear(this->mainBoxWindow);
    wclear(this->mainWindow);
    wresize(this->mainBoxWindow, this->height_mainSection, this->width_mainSection);
    wresize(this->mainWindow, this->height_mainSection - 2, this->width_mainSection - 2);
    draw_box(this->mainBoxWindow, "Output");

    //progressBarWindow;
    wclear(this->progressBarWindow);
    wresize(this->progressBarWindow, 2, this->width_mainSection);
    mvwin(this->progressBarWindow, this->x_mainSection + this->height_mainSection, this->y_mainSection);
    this->updateProgress(this->progress);

    refresh();
    wrefresh(this->menuBoxWindow);
    wrefresh(this->menuWindow);
    wrefresh(this->mainBoxWindow);
    wrefresh(this->mainWindow);
    wrefresh(this->progressBarWindow);

    this->writeStream();
    this->drawOutputOverview();
    this->drawCycleOverview();
}

void TUIOutput::handle_winch(int sig) {
    (void) sig; // UNUSED
    endwin();
    refresh();
    clear();

    // resize mainWindow
    uptdateDimensions(COLS, LINES);
    resizeWindow();

    // redraw menu
    this->wrapper->drawWindow();

    refresh();
}

void TUIOutput::initMenu() {

    // initialize windows
    this->menuWidth = 30;
    this->menuBoxWindow = newwin(LINES - 3, menuWidth, 0, 0);
    this->menuWindow = newwin(LINES - 5, menuWidth - 2, 1, 1);
    draw_box(this->menuBoxWindow, "Available Streams");

    this->mainMenu.initializeMenu();

    // draw the windows
    refresh();
    wrefresh(this->menuBoxWindow);
    wrefresh(this->menuWindow);

    // add the mainMenu to the wrapper
    this->wrapper = new NestedMenuWrapper(&this->mainMenu, this->menuWindow);
}

void TUIOutput::initMainWindow() {
    // setup the mainWindow
    struct sigaction sa;
    memset(&sa, 0, sizeof(struct sigaction));
    sa.sa_handler = static_handle_winch;
    sigaction(SIGWINCH, &sa, NULL);

    // initialize variables
    uptdateDimensions(COLS, LINES);

    // initialize windows
    this->mainBoxWindow = newwin(this->height_mainSection, this->width_mainSection, this->x_mainSection, this->y_mainSection);
    this->mainWindow = newwin(this->height_mainSection - 2, this->width_mainSection - 2, this->x_mainSection + 1, this->y_mainSection + 1);

    draw_box(mainBoxWindow, "Output");
    scrollok(this->mainWindow, TRUE);

    refresh();
    wrefresh(this->mainWindow);
    wrefresh(this->mainBoxWindow);
}

void TUIOutput::initProgressBarWindow() {
    // initialize window
    // the width of the progressBar is equal to the width of the mainWindow
    this->progressBarWindow = newwin(2, this->width_mainSection, this->x_mainSection + this->height_mainSection, this->y_mainSection);

    this->updateProgress(this->progress);

    refresh();
    wrefresh(this->progressBarWindow);
}

void TUIOutput::setStream(stringstream *newOStream) {
    this->mu.lock();
    this->os = newOStream;
    this->mu.unlock();
}

stringstream *TUIOutput::getStream() {
    stringstream *currentStream;
    this->mu.lock(); // is this useful// ?
    currentStream = this->os;
    this->mu.unlock();
    return currentStream;
}

void TUIOutput::updateProgress(float newProgress) {
    lock_guard<mutex> lock(this->updateMutex);
    this->progress = min(1.f, newProgress);

    int progressBarSize =
            this->width_mainSection - 8; // number of characters between '[' ']' so that everything fits in the window
    int numberOfEquals = max(0, (int) (progressBarSize * this->progress));
    int numberOfSpaces = max(0, (int) (progressBarSize * (1 - this->progress)));
    string equalString = string(numberOfEquals, '=');
    string spaceString = string(numberOfSpaces, ' ');
    int percentage = this->progress * 100;
    werase(this->progressBarWindow);
    mvwaddstr(this->progressBarWindow, 0, 0, "Progress:");
    mvwaddstr(this->progressBarWindow, 1, 0, (to_string(percentage) + "% [" + equalString + ">" + spaceString + "]").c_str());

    refresh();
    wrefresh(this->progressBarWindow);
}

// g++ tui_menu.cpp Plugin.cpp -pthread -lncurses -lmenu -o tui_menu && ./tui_menu
void TUIOutput::run() {
    int ch;

    while ((ch = getch()) != KEY_F(1)) {
        if (this->finalize && ch == 'q') return;

        lock_guard<mutex> lock(this->updateMutex);
        string name;
        wclear(this->mainWindow);

        if ((name = this->wrapper->handleKey(ch)) == "NOTHING") {
            this->setStream(&this->nullStream);
        } else {
            this->setStream(this->ostreams[this->wrapper->getSelectedMenuPath()]);
        }

        this->drawOutputOverview();
        this->drawCycleOverview();
        this->writeStream();

        // redraw the content of the windows
        wrefresh(this->mainWindow);
        wrefresh(this->menuWindow);
        wrefresh(this->progressBarWindow);
    }
}

void TUIOutput::start(){
    this->numPlugins = this->ostreams.size();

    //setStream(plugins[0].getStream());

    initscr();
    start_color();
    atexit(static_quit);
    clear();
    noecho();
    curs_set(0);
    cbreak();
    nl();
    keypad(stdscr, TRUE);

    // init colors
    // Color pair 0 is hard-wired to white on black
    init_pair(1, COLOR_CYAN, COLOR_BLACK); // for borders of boxes

    initMenu();
    initMainWindow();
    initProgressBarWindow();

    this->setStream(this->ostreams[{"cout"}]);

    refresh();

    this->outputThread = thread([&] {this->run();}); // listen on key inputs
}

void TUIOutput::registerMenuEntry(vector<string> &menuPath) {
    NestedMenu *currentMenu = &this->mainMenu;
    for (auto menuEntry : menuPath) {
        auto menuItemIterator = find_if(currentMenu->items.begin(), currentMenu->items.end(), [&](auto &item){ return item->name == menuEntry; });
        if (menuItemIterator != currentMenu->items.end()) {
            currentMenu = ((*menuItemIterator)->subMenu);
        } else {
            auto *subMenu = new NestedMenu();
            currentMenu->addItem(new ItemObject(menuEntry, subMenu));
            currentMenu = subMenu;
        }
    }
}

ostream *TUIOutput::registerOStream(vector<string> menuPath) {
    this->registerMenuEntry(menuPath);

    if(this->ostreams.count(menuPath) == 0){
        // entry or subEntry not registered yet
        this->ostreams[menuPath] = new stringstream;
    }
    return this->ostreams[menuPath];
}

void TUIOutput::captureStream(vector<string> menuPath, ostream &stream) {
    this->registerMenuEntry(menuPath);

    auto replacementStream = new stringstream;
    stream.rdbuf(replacementStream->rdbuf());
    this->ostreams[menuPath] = replacementStream;
}

void TUIOutput::notify(vector<string> menuPath = {}) {
    lock_guard<mutex> lock(this->updateMutex);
    if (this->wrapper->getSelectedMenuPath() == menuPath || menuPath.empty()){
        this->writeStream();
    }

}

void TUIOutput::writeStream(){
    mvwaddstr(this->mainWindow, 0, 0, this->getStream()->str().c_str());
    refresh();
    wrefresh(mainWindow);
}

void TUIOutput::updateCycle(std::vector<std::string> menuPath, int newCycle) {
    lock_guard<mutex> lock(this->updateMutex);
    this->cycles[menuPath] = newCycle;

    this->drawCycleOverview();
}

void TUIOutput::drawOutputOverview(){
    auto menuPath = this->wrapper->getSelectedMenuPath();
    move(1, this->menuWidth + 1);
    clrtoeol(); // clear line 1
    string outputOverview;
    outputOverview += "Output of: ";
    //mvaddstr(1, this->menuWidth + 1, "Output of");
    //addstr(" : ");

    for (auto &menuEntry : menuPath) {
        outputOverview += menuEntry;
        //addstr(menuEntry.c_str());
        if (menuEntry != menuPath.back())
            outputOverview += " > ";
            //addstr(" > ");
    }
    outputOverview = outputOverview.substr(0, this->width_mainSection);
    mvaddstr(1, this->menuWidth + 1, outputOverview.c_str());
}

void TUIOutput::drawCycleOverview(){
    auto menuPath = this->wrapper->getSelectedMenuPath();
    move(2, this->menuWidth + 1);
    clrtoeol(); // clear line 2
    if(this->cycles[menuPath] > 0) {
        mvaddstr(2, this->menuWidth + 1, ("Cycle " + to_string(this->cycles[menuPath]) + "/" + to_string(this->cycleCount)).c_str());
        refresh();
    }
}

void TUIOutput::setCycleCount(int newCycleCount) {
    this->cycleCount = newCycleCount;
}
