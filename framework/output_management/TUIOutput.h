#ifndef TUI_OUTPUT_H
#define TUI_OUTPUT_H

#include <menu.h>
#include <mutex>
#include <sstream>
#include <vector>
#include <map>
#include <stdlib.h>
#include "OutputInterface.h"
#include "NestedMenuWrapper.h"

class TUIOutput : public OutputInterface {
private:
    std::stringstream nullStream;

    bool finalize = false;

    std::thread outputThread;

    NestedMenuWrapper *wrapper = nullptr;

    std::stringstream *os = nullptr;
    std::mutex mu; // for save access for the stringstream

    std::map<std::vector<std::string>, std::stringstream*> ostreams;
    std::map<std::vector<std::string>, int> cycles;

    WINDOW *menuWindow = nullptr;
    WINDOW *menuBoxWindow = nullptr;

    WINDOW *mainWindow = nullptr;
    WINDOW *mainBoxWindow = nullptr;

    WINDOW *progressBarWindow = nullptr;

    NestedMenu mainMenu;

    std::mutex updateMutex;

    int menuWidth{};
    int numPlugins{};
    int x_mainSection{};
    int y_mainSection{};
    int height_mainSection{};
    int width_mainSection{};
    float progress;
    int cycleCount;

    void uptdateDimensions(int columns, int rows);

    void resizeWindow();

    void initMenu();

    void initMainWindow();

    void initProgressBarWindow();

    void setStream(std::stringstream *newOStream);

    std::stringstream *getStream();

    void drawCycleOverview();

    void drawOutputOverview();

    void writeStream();

    void run();

public:
    TUIOutput();

    void registerMenuEntry(std::vector<std::string> &menuPath);

    std::ostream *registerOStream(std::vector<std::string> menuPath) override;

    void captureStream(std::vector<std::string> menuPath, std::ostream &stream) override;

    void quit() override;

    void handle_winch(int sig) override;

    void start() override;

    void setCycleCount(int cycleCount) override;

    void notify(std::vector<std::string> menuPath) override;

    void updateProgress(float newProgress) override;

    void updateCycle(std::vector<std::string> menuPath, int newCycle) override;

    static OutputInterface &getInstance() {
        static TUIOutput instance;
        return instance;
    }
};

#endif