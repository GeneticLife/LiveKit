#include "PythonDependencies.h"
#include <iostream>
#include <vector>
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem.hpp>
#include "../Framework.h"
#include "PythonFrameworkInterface.h"
#include "PythonFrameworkConfig.h"

// we convert 'char' to 'uint8_t' because 'char' might be signed (depending on implementation)
// this would cause problems with python, therefore we play it safe and use 'uint8_t' (which is unsigned)
typedef std::vector<std::string> StringList;
typedef std::vector<uint8_t> NucleotideList;
typedef std::vector<NucleotideList> SequenceList;

namespace bp = boost::python;

BOOST_PYTHON_MODULE (PluginLibrary) {
    bp::class_<StringList>("StringList")
            .def(bp::vector_indexing_suite<StringList>());

    bp::class_<NucleotideList>("NucleotideList")
            .def(bp::vector_indexing_suite<NucleotideList>());

    bp::class_<SequenceList>("SequenceList")
            .def(bp::vector_indexing_suite<SequenceList>());

    bp::class_<PythonFrameworkInterface>("PythonFrameworkInterface", bp::init<FrameworkInterface *>())
            .def("getSequence", &PythonFrameworkInterface::getSequence)
            //.def("getSequenceIterators", &PythonFrameworkInterface::getSequenceIterators)
            .def("getRead", &PythonFrameworkInterface::getRead)
            .def("getMostRecentBcl", &PythonFrameworkInterface::getConvertedMostRecentBcl)
            .def("getMostRecentBcls", &PythonFrameworkInterface::getMostRecentBclsVector)
            .def("filterBasecall", &PythonFrameworkInterface::filterBasecall)
            .def("getNumSequences", &PythonFrameworkInterface::getNumSequences)
            .def("getCurrentCycle", &PythonFrameworkInterface::getCurrentCycle)
            .def("getCycleCount", &PythonFrameworkInterface::getCycleCount)
            .def("getCurrentReadCycle", &PythonFrameworkInterface::getCurrentReadCycle)
            .def("getCurrentReadId", &PythonFrameworkInterface::getCurrentReadId)
            .def("getCurrentReadLength", &PythonFrameworkInterface::getCurrentReadLength)
            .def("isBarcodeCycle", &PythonFrameworkInterface::isBarcodeCycle)
            .def("getCurrentMateId", &PythonFrameworkInterface::getCurrentMateId)
            .def("getMateCount", &PythonFrameworkInterface::getMateCount)
            .def("setCurrentPluginCycle", &PythonFrameworkInterface::setCurrentPluginCycle)
            .def("getFilterData", &PythonFrameworkInterface::getConvertedFilterData);
}

void PythonDependencies::initPython() {
    try {
        PyImport_AppendInittab("PluginLibrary", &PyInit_PluginLibrary);
        Py_Initialize();

        boost::filesystem::path workingDir = boost::filesystem::absolute("./../plugins/python").normalize();
        PyObject * sysPath = PySys_GetObject((char *) "path");
        PyList_Insert(sysPath, 0, PyUnicode_FromString(workingDir.string().c_str()));

    } catch (const bp::error_already_set &) {
        std::cerr << "Python Error: ";
        PyErr_Print();
        throw std::runtime_error("<<< Python exception caught!");
    }
}
