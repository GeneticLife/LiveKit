#ifndef LIVEKIT_PYTHONFRAMEWORKCONFIG_H
#define LIVEKIT_PYTHONFRAMEWORKCONFIG_H

#include "../configuration/Configurable.h"
#include "../Plugin.h"

class PythonFrameworkConfig : public Configurable{
public:
    explicit PythonFrameworkConfig(Plugin* plugin){
        // To gain access to the configMap of the plugin, we cast it to a 'PythonFrameworkConfig' (both inherit from 'Configurable')
        this->configMap = ((PythonFrameworkConfig*)plugin)->configMap;
    }

    void setConfig() override {
        // No configEntries are registered
    };

    void exportConfigToJson(std::string jsonName){
        /*
         * I apologize for this ugly code but for some scenarios it is important to get access to the config from the framework.
         * Parsing json is very nice in Python but relatively complicated in C++.
         * This allows us to export the configMap from the framwork into a json (so that we can use it in python plugins).
         * This all is needed because the interface between C++ and python does not allow template functions (like 'getConfigEntry<T>').
         * We would need to define a method for each type that we use (e.g. 'getConfigEntry_Int') which would be ugly for the plugin developer.
         * We cannot write the elements from the config directly into the json, because our configMap stores element of type 'boost::any' (which first need to be casted to the correct type).
         * Just reading the framework.json that is already on disk is not enough because a lot of entries use the default value (which are not in the framework.json)
         *
         * Conclusion: This is not beautiful code and might need to be extended if there are new types of configEntries in the framwork.
         *             This ugly code allows python developers to write beautiful code
         */
        boost::property_tree::ptree pt;
        for (auto& entryAny: this->configMap){
            if (entryAny.second.type() == typeid(uint16_t)){
                auto value = boost::any_cast<uint16_t>(entryAny.second);
                pt.put (entryAny.first, value);
            } else if (entryAny.second.type() == typeid(int)) {
                auto value = boost::any_cast<int>(entryAny.second);
                pt.put(entryAny.first, value);
            } else if (entryAny.second.type() == typeid(bool)) {
                auto value = boost::any_cast<bool>(entryAny.second);
                pt.put(entryAny.first, value);
            } else if (entryAny.second.type() == typeid(std::string)) {
                auto value = boost::any_cast<std::string>(entryAny.second);
                pt.put(entryAny.first, value);
            } else if (entryAny.second.type() == typeid(float)) {
                auto value = boost::any_cast<float>(entryAny.second);
                pt.put(entryAny.first, value);
            } else if (entryAny.second.type() == typeid(std::vector<uint16_t>)) {
                auto valueVector = boost::any_cast<std::vector<uint16_t>>(entryAny.second);
                boost::property_tree::ptree newNode;

                for (auto v : valueVector) {
                    boost::property_tree::ptree child;
                    child.put("", v);
                    newNode.push_back(std::make_pair("", child));
                }
                pt.push_back(std::make_pair(entryAny.first, newNode));
            } else if (entryAny.second.type() == typeid(std::vector<std::vector<std::string>>)) {
                auto valueVectorVector = boost::any_cast<std::vector<std::vector<std::string>>>(
                        entryAny.second);

                boost::property_tree::ptree newNode;

                for (auto &valueVector : valueVectorVector) {
                    boost::property_tree::ptree child;
                    for (auto &v : valueVector) {
                        boost::property_tree::ptree child_child;
                        child_child.put("", v);
                        child.push_back(std::make_pair("", child_child));
                    }
                    newNode.push_back(std::make_pair("", child));
                }
                pt.push_back(std::make_pair(entryAny.first, newNode));
            } else if (entryAny.second.type() == typeid(std::vector<std::pair<int, char>>)) {
                auto pairVector = boost::any_cast<std::vector<std::pair<int, char>>>(
                        entryAny.second);

                boost::property_tree::ptree newNode;

                for (auto &pair : pairVector) {
                    boost::property_tree::ptree child;

                    boost::property_tree::ptree first_child;
                    first_child.put("", pair.first);
                    child.push_back(std::make_pair("", first_child));

                    boost::property_tree::ptree second_child;
                    second_child.put("", pair.second);
                    child.push_back(std::make_pair("", second_child));

                    newNode.push_back(std::make_pair("", child));
                }
                pt.push_back(std::make_pair(entryAny.first, newNode));
            } else {
                std::cerr << "Cannot cast framework-configEntry '" + entryAny.first + "'" << std::endl;
            }
        }

        write_json (jsonName, pt);
    }

};

#endif //LIVEKIT_PYTHONFRAMEWORKCONFIG_H
