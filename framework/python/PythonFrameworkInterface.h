#ifndef LIVEKIT_PYTHONFRAMEWORKINTERFACE_H
#define LIVEKIT_PYTHONFRAMEWORKINTERFACE_H

#include "../Framework.h"

class PythonFrameworkInterface : public FrameworkInterface {
public:
    explicit PythonFrameworkInterface(FrameworkInterface *frameworkInterface) : FrameworkInterface(
            *frameworkInterface) {};

    std::vector<std::vector<uint8_t>> getMostRecentBclsVector(uint16_t lane) {
        auto deque = FrameworkInterface::getMostRecentBcls(lane);
        std::vector<std::vector<uint8_t>> bclVector;
        for(auto bcl: deque){
            bclVector.emplace_back(bcl.begin(), bcl.end());
        }
        return bclVector;
    };

    std::vector<uint8_t> getConvertedMostRecentBcl(uint16_t lane, uint16_t tile) {
        BCL bcl = FrameworkInterface::getMostRecentBcl(lane, tile);
        return std::vector<uint8_t>(bcl.begin(), bcl.end());
    }

    std::vector<uint8_t> getConvertedFilterData(uint16_t lane, uint16_t tile) {
        auto filterData = FrameworkInterface::getFilterData(lane, tile);
        return std::vector<uint8_t>(filterData.begin(), filterData.end());
    }
};


#endif //LIVEKIT_PYTHONFRAMEWORKINTERFACE_H
