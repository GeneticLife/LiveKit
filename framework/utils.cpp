#include "utils.h"

#include <boost/filesystem.hpp>

using namespace std;

namespace utils {
    unsigned long getFilesize(const string &fileName) {

        ifstream file(fileName.c_str(), ifstream::in | ifstream::binary);

        if(!file.is_open()) { return 0; }

        file.seekg(0, ios::end);
        int fileSize = file.tellg();
        file.close();

        return fileSize;
    }

    unsigned long getDirectorySize(const string &directoryPath) {
        unsigned long size = 0;
        for (boost::filesystem::recursive_directory_iterator it(directoryPath);
             it!=boost::filesystem::recursive_directory_iterator();
             ++it) {
            if (!boost::filesystem::is_directory(*it))
                size+=boost::filesystem::file_size(*it);
        }

        return size;
    }

    bool hasEnding(std::string const &fullString, std::string const &ending) {
        return (fullString.length() >= ending.length())
            && (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
    }


    void readBinaryFile(const string &fileName, vector<char>& data, int position) {
        // get file size
        auto size = (uint64_t) getFilesize(fileName) - position;

        // open binary file
        FILE *file;
        file = fopen(fileName.c_str(), "rb");

        if (!file) {
            stringstream error;
            error << "Error reading binary file " << fileName << ": Could not open file.";
            throw ios::failure(error.str());
        }

        // allocate memory
        data.resize(size);

        fseek(file, position, 0); // skip the first few bytes which only contain meta information

        // read all data at once
        uint64_t read = fread(data.data(), 1, size, file);

        if (read != size) {
            stringstream error;
            error << "Error reading binary file " << fileName << ": Read " << read << " bytes while file has " << size
                  << " bytes.";
            throw ios::failure(error.str());
        }

        fclose(file);
    }

    string toNDigits(int value, int N, char fill_char) {
        stringstream ss;
        ss << setw(N) << setfill(fill_char) << value;
        return ss.str();
    }

    string getFilterFilename(uint16_t lane, uint16_t tile, const string &path) {
        ostringstream path_stream;
        path_stream << path << "/L" << toNDigits(lane, 3) << "/s_" << lane << "_"
                    << tile << ".filter";
        return path_stream.str();
    }

    bool sameFile(struct stat stat1, struct stat stat2) {
        return (stat1.st_dev == stat2.st_dev) && (stat1.st_ino == stat2.st_ino);
    }

    void serializeString(const std::string &string, FILE *serializeFile) {
        unsigned long size = string.size();
        fwrite(&size, sizeof(unsigned long), 1, serializeFile);
        fwrite(string.c_str(), size, 1, serializeFile);
    }

    void serializeString(const std::string &string, char **serializedSpace) {
        unsigned long size = string.size();
        memcpy(*serializedSpace, &size, sizeof(unsigned long));
        *serializedSpace += sizeof(unsigned long);
        memcpy(*serializedSpace, string.c_str(), size);
        *serializedSpace += size;
    }

    void validateExistanceOfFile(const std::string &name) {
        if(!boost::filesystem::exists(name)) {
            cerr << "The file " + name + " does not exist" << endl;
            exit(1);
        }
    }
}

