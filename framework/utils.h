#ifndef LIVEKIT_UTILS_H
#define LIVEKIT_UTILS_H

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstring>
#include <sys/stat.h>

namespace utils {
    bool hasEnding(std::string const &fullString, std::string const &ending);

    void readBinaryFile(const std::string &fileName, std::vector<char>& data, int position);

    std::string toNDigits(int value, int N, char fill_char = '0');

    std::string getFilterFilename(uint16_t lane, uint16_t tile, const std::string &path);

    bool sameFile(struct stat stat1, struct stat stat2);

    void serializeString(const std::string &string, FILE *serializeFile);

    void serializeString(const std::string &string, char **serializedSpace);

    void validateExistanceOfFile(const std::string &name);
}

#endif //LIVEKIT_UTILS_H
