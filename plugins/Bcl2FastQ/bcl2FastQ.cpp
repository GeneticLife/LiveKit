#include "bcl2FastQ.h"
#include "../../framework/fragments/Fragment.cpp"
#include <boost/filesystem.hpp>

using namespace std;

void Bcl2FastQ::init() {
    // Check if prefix directory exists and create it otherwise
    string prefix = "./out/Bcl2FastQ/";
    boost::filesystem::path dir(prefix);
    if (!boost::filesystem::is_directory(dir) || !boost::filesystem::exists(dir)) {
        boost::filesystem::create_directory(dir);
    }
}

string Bcl2FastQ::toNDigits(int value, int N, char fill_char) {
    stringstream ss;
    ss << setw(N) << setfill(fill_char) << value;
    return ss.str();
}

std::shared_ptr<FragmentContainer> Bcl2FastQ::runCycle(std::shared_ptr<FragmentContainer> inputFragments) {

    auto reads = this->getConfigEntry<vector<pair<int, char>>>("reads");

    // Call bcl2FastQPlugin in the cycles we specified in the framework config
    auto outputCycles = this->getConfigEntry<vector<uint16_t >>("PrintFastQAfterCycles");

    int currentCycle = this->framework->getCurrentCycle();

    // Do we have a singleEnd or pairedRead structure?
    bool isPairedEnd = true;
    if (reads.size() == 2) {
        isPairedEnd = false;
    }

    // Do we want to print this cycle or is it the last cycle of read1?
    if (reads.size() == 4) {
        if ((reads[0].first + reads[1].first + reads[2].first == currentCycle) ||
            !(find(outputCycles.begin(), outputCycles.end(), currentCycle) == outputCycles.end())) {
            this->processFastQ(inputFragments, outputCycles, currentCycle, isPairedEnd, false);
        }
    }
    else if (reads.size() == 3) {
        if ((reads[0].first + reads[1].first == currentCycle) ||
            !(find(outputCycles.begin(), outputCycles.end(), currentCycle) == outputCycles.end())) {
            this->processFastQ(inputFragments, outputCycles, currentCycle, isPairedEnd, false);
        }
    }
    else if (!(find(outputCycles.begin(), outputCycles.end(), currentCycle) == outputCycles.end())) {
        processFastQ(inputFragments, outputCycles, currentCycle, isPairedEnd, false);
    }

    return inputFragments;
}

void Bcl2FastQ::processFastQ(std::shared_ptr<FragmentContainer> inputFragments, vector<uint16_t> outputCycles, int currentCycle, bool isPairedEnd, bool isFullReadPostProcess) {

    auto reads = this->getConfigEntry<vector<pair<int, char>>>("reads");

    seqan::StringSet<string> identifier;
    seqan::StringSet<string> qualities;
    // Dna5: 'A', 'C', 'G', 'T', 'N'
    seqan::StringSet<seqan::Dna5String> sequences;

    // if we want to print the last Cycle we skip run cycle because it is created anyway in the runFullReadPostprocessing method
    if ((reads.size() == 3 && currentCycle == reads[0].first + reads[1].first + reads[2].first + 1) ||
        (!isPairedEnd && currentCycle == reads[0].first + reads[1].first)) {
        return;
    }

    this->out << to_string(currentCycle) << endl;
    this->out << "------------------------------------------------------------------\n Bcl2FastQ Plugin" << endl;
    this->out << "------------------------------------------------------------------ \nCreating FastQ file ... ";

    for (auto &lane : this->getConfigEntry<vector<uint16_t >>("lanes")) {

        string output;

        // Get all sequences from the sequence representation
        vector<Read> allSequences;
        for (auto &tile : this->getConfigEntry<vector<uint16_t >>("tiles")) {
            for (int i = 0; i < (int) this->framework->getNumSequences(lane, tile); i++) {
                allSequences.push_back(this->framework->getSequence(lane, tile, i));
            }
        }

        int numberOfSequences = allSequences.size();
        // Create new sequence strings
        seqan::resize(sequences, numberOfSequences);
        // Create new strings for the qualities
        seqan::resize(qualities, numberOfSequences);
        // Create new strings for the identifier
        seqan::resize(identifier, numberOfSequences);

        uint32_t sequenceCounter = 0;

        map<vector<char>, vector<unsigned int>> barcodes;
        int readFlag = 0;

        for (auto &tile : this->getConfigEntry<vector<uint16_t >>("tiles")) {
            // Create always an fastq file for the first read before the second read started
            // Read1 and at the most both barcodes are sequenced
            if (isPairedEnd && reads[0].first + reads[1].first + reads[2].first == currentCycle) {
                // process as read1 and call the file as normal
                barcodes = this->createFastQRead1AndRead2(inputFragments, identifier, qualities, sequences, lane, tile,
                                                          sequenceCounter, allSequences, true);
                readFlag = 1;
            }
                // If we have not reached a barcode yet
            else if ((isPairedEnd && reads[0].first + reads[1].first + reads[2].first > currentCycle) ||
                     (!isPairedEnd && reads[0].first + reads[1].first > currentCycle)) {
                // process and call the file "Cyclexx_R1_001.fastq"
                output = this->createFastQFileWithoutBarcode(inputFragments, identifier, qualities, sequences, lane,
                                                             tile, sequenceCounter, allSequences);
            }
                // The sequencing of the second read started
                // process read2 and call the file as normal
            else if (find(outputCycles.begin(), outputCycles.end(), currentCycle) != outputCycles.end() || isFullReadPostProcess) {
                bool isRead1 = reads.size() <= 2;

                barcodes = this->createFastQRead1AndRead2(inputFragments, identifier, qualities, sequences, lane, tile,
                                                          sequenceCounter,
                                                          allSequences, isRead1);
                readFlag = 2;
            }

            sequenceCounter += (int) this->framework->getNumSequences(lane, tile);
        }

        if (readFlag == 1 || readFlag == 2) {

            for (auto barcodeSequence : barcodes) {
                auto barcodeVector = this->getConfigEntry<vector<vector<string>>>("barcodeVector");

                string stringBarcode(barcodeSequence.first.begin(), barcodeSequence.first.end());
                string sampleName;
                string sampleNumber;

                if (stringBarcode == "undetermined") {
                    sampleName = "Undetermined";
                    sampleNumber = "0";
                } else {
                    sampleName = this->getConfigEntry<vector<string>>(stringBarcode)[0];
                    sampleNumber = this->getConfigEntry<vector<string>>(stringBarcode)[1];
                }

                // Check if prefix directory exists and create it otherwise
                string prefix = "./out/Bcl2FastQ/";
                boost::filesystem::path dir(prefix);

                // Path to the fastq file
                // fileName format: SampleName_S1(SampleNumber)_L001_R1_001.fastq.gz
                string output = prefix + sampleName + "_S" + sampleNumber + "_L" + toNDigits(lane, 3) + "_R" +
                                to_string(readFlag) + "_Cycle" +
                                to_string(this->framework->getCurrentCycle()) + "_001.fastq";


                int barcodeLength = 0;
                for (auto read : reads) {
                    if (read.second == 'B') {
                        barcodeLength += read.first;
                    }
                }

                if (this->framework->getCurrentReadCycle() == reads[this->framework->getCurrentReadId()].first ||
                    this->framework->getCurrentCycle() - barcodeLength == reads[this->framework->getCurrentMateId()].first) {
                    output = prefix + sampleName + "_S" + sampleNumber + "_L" + toNDigits(lane, 3) + "_R" +
                             to_string(readFlag) + "_001.fastq";
                }

                ofstream outputFile(output, ofstream::out);
                if (outputFile.fail()) {
                    cerr << "Couldn't open file!" << endl;
                    return;
                }

                // Go through all reads and save them in the stream (outputFile)
                for (auto reads : barcodeSequence.second) {
                    identifier[reads].append(sampleNumber);
                    seqan::writeRecord(outputFile, identifier[reads], sequences[reads], qualities[reads],
                                       seqan::Fastq());
                }
            }
        }
        else {

            ofstream outputFile(output, ofstream::out);
            if (outputFile.fail()) {
                cerr << "Couldn't open file!" << endl;
                return;
            }

            // Go through all reads and save them in the stream (outputFile)
            for (unsigned i = 0; i < length(identifier); ++i) {
                identifier[i].append("0");
                seqan::writeRecord(outputFile, identifier[i], sequences[i], qualities[i], seqan::Fastq());
            }
        }

        // Clear all strings after every tile
        clear(sequences);
        clear(identifier);
        clear(qualities);
    }

    this->out << " Done" << endl;
}

string
Bcl2FastQ::createFastQFileWithoutBarcode(std::shared_ptr<FragmentContainer> inputFragments, seqan::StringSet<string> &identifier,
                                         seqan::StringSet<string> &qualities,
                                         seqan::StringSet<seqan::Dna5String> &sequences, uint16_t lane, uint16_t tile,
                                         int sequenceCounter, vector<Read> allSequences) {

    string instrumentID = this->getConfigEntry<string>("instrument-id");
    string runID = this->getConfigEntry<string>("run-id");
    string flowcellID = this->getConfigEntry<string>("flowcell-id");

    Read currentSequence;

    // <read> = 1 if we have a single read structure, or it is the first read of a paired read structure
    auto reads = this->getConfigEntry<vector<pair<int, char>>>("reads");

    // Get the fragment that stores the position of every read and cast it into a vector
    auto readPositionFragment = inputFragments->get("POSITION");
    auto positionVector = (SerializableVector<float> *) readPositionFragment->getSerializable("positionVector");

    int numberOfSequences = this->framework->getNumSequences(lane, tile);

    // Run through all sequences from our representation and create the string sets for seqan
    unsigned long positionCounter = 0;

    for (uint32_t i = sequenceCounter; i < (uint32_t) sequenceCounter + numberOfSequences; i++) {
        bool isFiltered = this->framework->filterBasecall(lane, tile, i);
        this->addIdentifier(identifier[i], instrumentID, runID, flowcellID, lane, tile, positionVector, 1, positionCounter, isFiltered);
        positionCounter += 2;

        currentSequence = allSequences[i];

        // extract read 1
        Read::const_iterator first = currentSequence.begin();
        Read::const_iterator last = currentSequence.begin() + reads[0].first;
        Read firstRead(first, last);

        if (int(currentSequence.size()) <= reads[0].first) {
            firstRead = currentSequence;
        }

        this->addBasesAndQualities(firstRead, sequences[i], qualities[i]);
    }

    // Check if prefix directory exists and create it otherwise
    string prefix = "./out/Bcl2FastQ/";
    boost::filesystem::path dir(prefix);

    int barcodeLength = 0;
    for (auto read : reads) {
        if (read.second == 'B') {
            barcodeLength += read.first;
        }
    }

    // Path to our fastq file
    // fileName format: SampleName_S(SampleNumber)_L001_R1_001.fastq.gz
    return prefix + "L" + toNDigits(lane, 3) + "_R1_Cycle" + to_string(this->framework->getCurrentCycle()) +
           "_001.fastq";
}

map<vector<char>, vector<unsigned int>>
Bcl2FastQ::createFastQRead1AndRead2(std::shared_ptr<FragmentContainer> inputFragments, seqan::StringSet<std::string> &identifier,
                                    seqan::StringSet<std::string> &qualities,
                                    seqan::StringSet<seqan::Dna5String> &sequences,
                                    uint16_t lane, uint16_t tile, uint32_t sequenceCounter,
                                    vector<Read> allSequences,
                                    bool isRead1) {
    // We have the Barcode and can create a normal FastQ file for read1 and read2

    string instrumentID = this->getConfigEntry<string>("instrument-id");
    string runID = this->getConfigEntry<string>("run-id");
    string flowcellID = this->getConfigEntry<string>("flowcell-id");

    auto barcodeErrors = this->getConfigEntry<vector<uint16_t>>("barcodeErrors");

    Read currentSequence;

    // <read> = 1 if we have a single read structure, or it is the first read of a paired read structure
    auto reads = this->getConfigEntry<vector<pair<int, char>>>("reads");

    map<vector<char>, vector<unsigned int>> barcodes;

    // Get the fragment that stores the position of every read and cast it into a vector
    auto readPositionFragment = inputFragments->get("POSITION");
    auto positionVector = (SerializableVector<float> *) readPositionFragment->getSerializable("positionVector");

    // Get all sequences from the sequence representation
    unsigned long numberOfSequences = this->framework->getNumSequences(lane, tile);

    // Run through all sequences from our representation and create the string sets for seqan
    unsigned long positionCounter = 0;

    for (uint32_t i = sequenceCounter; i < (uint32_t) sequenceCounter + numberOfSequences; i++) {
        bool isFiltered = this->framework->filterBasecall(lane, tile, i);
        currentSequence = allSequences[i];

        if (isRead1) {
            this->addIdentifier(identifier[i], instrumentID, runID, flowcellID, lane, tile, positionVector, 1, positionCounter, isFiltered);
        } else {
            this->addIdentifier(identifier[i], instrumentID, runID, flowcellID, lane, tile, positionVector, 2, positionCounter, isFiltered);
        }

        positionCounter += 2;

        // extract the barcodes
        bool firstB = true;

        Read::const_iterator first = currentSequence.begin();
        Read::const_iterator last = currentSequence.begin();

        for (int j = 0; j < (int)reads.size(); j++) {
            if (reads[j].second == 'B') {
                last += reads[j].first;

                firstB = false;
            }
            else if (firstB) {
                last += reads[j].first;
                first += reads[j].first;
            }
        }

        vector<char> barcodeSequence(first, last);

        auto barcodeVector = this->getConfigEntry<vector<vector<string>>>("barcodeVector");
        string stringBarcode = this->getBaseFromCharVector(barcodeSequence);
        Read charBarcode(stringBarcode.begin(), stringBarcode.end());

        // Determine if the barcode is one of the sample or not
        bool isSingleEnd;
        if (reads.size() == 4) {
            isSingleEnd = false;
        } else {
            isSingleEnd = true;
        }
        barcodeSequence = this->framework->getSampleBarcode(charBarcode, barcodeVector, barcodeErrors,
                                                                       isSingleEnd);

        if (barcodeSequence.size() == 0) {
            barcodes[{'u', 'n', 'd', 'e', 't', 'e', 'r', 'm', 'i', 'n', 'e', 'd'}].push_back(i);
        } else {
            barcodes[barcodeSequence].push_back(i);
        }

        if (isRead1) {
            // extract read1
            first = currentSequence.begin();
            last = currentSequence.begin() + reads[0].first;
            Read firstReadSequence(first, last);
            this->addBasesAndQualities(firstReadSequence, sequences[i], qualities[i]);
        } else {
            // extract read2
            if (reads.size() == 4) {
                first = currentSequence.begin() + reads[0].first + reads[1].first + reads[2].first;
            }
            else {
                first = currentSequence.begin() + reads[0].first + reads[1].first;
            }

            last = currentSequence.begin() + currentSequence.size();
            Read secondReadSequence(first, last);

            this->addBasesAndQualities(secondReadSequence, sequences[i], qualities[i]);
        }
    }

    return barcodes;
}

string Bcl2FastQ::getBaseFromCharVector(vector<char> barcodeSequence) {
    string sequence;

    for (size_t i = 0; i < barcodeSequence.size(); i++) {
        bitset<8> binary(barcodeSequence.at((int) i));

        int base = this->getBaseFromBits(binary);

        switch (base) {
            case (0):
                sequence.push_back('A');
                break;
            case (1):
                sequence.push_back('C');
                break;
            case (2):
                sequence.push_back('G');
                break;
            case (3):
                sequence.push_back('T');
                break;
            default:
                break;
        }
    }
    return sequence;
}


int Bcl2FastQ::getBaseFromBits(bitset<8> byte) {
    return (int) byte.to_ulong() & 0b11;
}

char Bcl2FastQ::getQualityFromBits(bitset<8> byte) {
    // shift the byte 2 bits to the right (111111AB -> 00111111)
    bitset<8> binaryQuality = byte >> 2;

    // return the qualities (starts at 33 in ascii table)
    return (char) binaryQuality.to_ulong() + 33;
}

void Bcl2FastQ::addBasesAndQualities(Read currentSequence, seqan::Dna5String &sequence, string &qualities) {
    for (size_t i = 0; i < currentSequence.size(); i++) {
        bitset<8> binary(currentSequence.at((int) i));

        // extract the base and quality from the byte
        char quality = this->getQualityFromBits(binary);
        int base = this->getBaseFromBits(binary);

        // Append the base to the sequence
        // If the quality is too bad ('!'), we append a 'N' to the sequence
        if (quality == '!')
            sequence += 4;
        else
            sequence += base;

        // Append the quality to the sequence (and convert the char to a string)
        qualities.append(string(1, quality));
    }
}

void Bcl2FastQ::setConfig() {
    this->registerConfigEntry<vector<uint16_t>, string>("PrintFastQAfterCycles", "0", Configurable::toVector<uint16_t>(','));
    this->registerConfigEntry<int>("controlNumber", 1);
}

void
Bcl2FastQ::addIdentifier(string &identifier, const string instrumentID, const string runID, const string flowcellID, int lane, int tile,
                         SerializableVector<float> *positionVector,
                         int read, int positionCounter, bool isFiltered) {
    // @<instrument-id>:<run-id>:<flowcell ID>:<lane>:<tile>:<x-pos>:<y-pos> <read>:<is filtered>:<control number>:<sample number>

    string isFilteredString;
    if (isFiltered) {
        isFilteredString = "Y";
    } else {
        isFilteredString = "N";
    }

    // Is needed in order to print the position values with a precision of 2
    ostringstream positionX;
    ostringstream positionY;
    positionX.precision(2);
    positionY.precision(2);

    try {
        positionX << fixed << positionVector->at(positionCounter);
        positionY << fixed << positionVector->at(positionCounter + 1);
    } catch (exception &e) {
        cerr << "PositionVector run out of elements" << endl;
        positionX << 0;
        positionY << 0;
    }

    for (auto el : vector<string>{
            instrumentID, ":",
            runID, ":",
            flowcellID, ":",
            to_string(lane), ":",
            to_string(tile), ":",
            positionX.str(), ":",
            positionY.str(), " ",
            to_string(read), ":",
            isFilteredString, ":",
            to_string(this->getConfigEntry<int>("controlNumber")), ":"
    }) {
        identifier.append(el);
    }

}

std::shared_ptr<FragmentContainer> Bcl2FastQ::runFullReadPostprocessing(std::shared_ptr<FragmentContainer> inputFragments) {

    auto reads = this->getConfigEntry<vector<pair<int, char>>>("reads");

    // Call bcl2FastQPlugin in the cycles we specified in the framework config
    auto outputCycles = this->getConfigEntry<vector<uint16_t >>("PrintFastQAfterCycles");

    int currentCycle = this->framework->getCurrentCycle();

    // Do we have a singleEnd or pairedRead structure?
    bool isPairedEnd = true;
    if (reads.size() == 2) {
        isPairedEnd = false;
    }

    // Do we want to print this cycle or is it the last cycle of read1?
    if ((reads[0].first + reads[1].first + reads[2].first == currentCycle) ||
        !(find(outputCycles.begin(), outputCycles.end(), currentCycle) == outputCycles.end())) {
        this->processFastQ(inputFragments, outputCycles, currentCycle, isPairedEnd, false);
    }

    this->processFastQ(inputFragments, outputCycles, currentCycle, isPairedEnd, true);

    return inputFragments;
}

extern "C" Bcl2FastQ *create() {
    return new Bcl2FastQ;
}

extern "C" void destroy(Bcl2FastQ *plugin) {
    delete plugin;
}
