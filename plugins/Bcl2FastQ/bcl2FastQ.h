#ifndef LIVEKIT_BCL2FASTQ_H
#define LIVEKIT_BCL2FASTQ_H

#include <bitset>

#include <seqan/sequence.h>
#include <seqan/stream.h>
#include <seqan/seq_io.h>

#include "../../framework/Plugin.h"
#include "../../framework/FrameworkInterface.h"
#include "../../serializables/SerializableVector.h"

/** Plugin that turns bcls to a fastQ file */
LIVEKIT_PLUGIN(Bcl2FastQ) {
public:
    std::string toNDigits(int value, int N, char fill_char = '0');

    std::shared_ptr<FragmentContainer> runCycle(std::shared_ptr<FragmentContainer> inputFragments) override;

    void init() override;

    /** Run the Bcl2FastQ postprocess */
    std::shared_ptr<FragmentContainer> runFullReadPostprocessing(std::shared_ptr<FragmentContainer> inputFragments) override;

    void finalize() override {};

    /** Set default header information */
    void setConfig() override;

    /** Add the bases and qualities from our currentSequence (SequenceRepresentation) (binary) to the seqan sequence (ASCII)
    * @param currentSequence One read, that we currently convert
    * @param sequence The fastQ sequence, where we add the bases
    * @param qualities The fastQ sequence, where we add the qualities
    */
    void addBasesAndQualities(Read currentSequence, seqan::Dna5String &sequence, std::string &qualities);

    /** Parses the bits and return the corresponding base
    * @param byte The Byte that gets parsed
    * @return The corresponding base
    */
    int getBaseFromBits(std::bitset<8> byte);

    /** Parse the bits to extract the quality
    * @param byte The Byte that gets parsed
    * @return The corresponding quality
    */
    char getQualityFromBits(std::bitset<8> byte);

    /** Add the headerinformation to the fastQ file
    * @param Identifier StringSet, that store all headers for the reads
    */
    void
    addIdentifier(std::string &identifiers, const std::string instrumentID, const std::string runID, const std::string flowcellID, int lane, int tile, SerializableVector<float> *positionVector, int read,
                  int positionCounter, bool isFiltered);

    /**
     * Whenever the user wants to create a fastq file before we sequenced the barcodes, we call this method.
     * @param inputFragments ReadPositionFragment
     * @param identifier
     * @param qualities
     * @param sequences
     * @param lane Current lane
     * @param tile Current tile
     * @param sequenceCounter Counts the sequences. Holds the sum of all reads to the current tile
     * @param allSequences Holds all sequences of all tiles of one lane
     * @return fastq filename
     */
    std::string
    createFastQFileWithoutBarcode(std::shared_ptr<FragmentContainer> inputFragments, seqan::StringSet<std::string> &identifier,
                                  seqan::StringSet<std::string> &qualities,
                                  seqan::StringSet<seqan::Dna5String> &sequences, uint16_t lane, uint16_t tile,
                                  int sequenceCounter, std::vector<Read> allSequences);

    /**
     * Core-structure of the bcl2FastQ plugin. Is called every time we want to create a fastq file.
     * Determine what kind of fastq file should be created
     * @param inputFragments
     * @param outputCycles
     * @param currentCycle
     */
    void processFastQ(std::shared_ptr<FragmentContainer> inputFragments, std::vector<uint16_t> outputCycles, int currentCycle, bool isPairedEnd, bool isFullReadPostProcess);

    /**
     * Creates a fastq file either for read1 or read2.
     * @param inputFragments ReadPositionFragment
     * @param identifier
     * @param qualities
     * @param sequences
     * @param lane Current lane
     * @param tile Current tile
     * @param sequenceCounter Counts the sequences. Holds the sum of all reads to the current tile
     * @param allSequences Holds all sequences of all tiles of one lane
     * @param isRead1 Indicates if we want to create a fastq file for read1 or read2
     * @return Barcodes and their belonging reads (index of the read)
     */
    std::map<std::vector<char>, std::vector<unsigned int>>
    createFastQRead1AndRead2(std::shared_ptr<FragmentContainer> inputFragments, seqan::StringSet<std::string> &identifier,
                seqan::StringSet<std::string> &qualities, seqan::StringSet<seqan::Dna5String> &sequences, uint16_t lane,
                uint16_t tile, uint32_t sequenceCounter, std::vector<Read> allSequences, bool isRead1);

    /**
     * Transform a read into a string consists of its bases. Is used to compare the barcode with the barcodeVector from the sampleSheet
     * @param barcodeSequence the read that is transformed
     * @return the string of bases
     */
    std::string getBaseFromCharVector(std::vector<char> barcodeSequence);
};

#endif //LIVEKIT_BCL2FASTQ_H
