#include "BclExport.h"

#include <boost/filesystem.hpp>
#include <stdio.h>

using namespace std;

std::shared_ptr<FragmentContainer> BclExport::runPreprocessing(std::shared_ptr<FragmentContainer> inputFragments) {
    (void) inputFragments; // UNUSED

    string exportDirectory = this->getConfigEntry<string>("outDir")
        + this->getConfigEntry<string>("exportDirectoryName") + "/";
    string exportBaseCallsDirectory = exportDirectory + "Data/Intensities/BaseCalls/";

    boost::filesystem::create_directories(exportBaseCallsDirectory);

    for (auto lane : this->getConfigEntry<vector<uint16_t>>("lanes")) {
        boost::filesystem::create_directory(exportBaseCallsDirectory + "L00" + to_string(lane));
    }

    // Copy RunInfo and SampleSheet

    boost::filesystem::path runInfoPath(this->getConfigEntry<string>("RunInfoPath"));
    if (!runInfoPath.empty()) {
        string targetRunInfoPath = exportDirectory + runInfoPath.filename().string();
        boost::filesystem::copy_file(
            runInfoPath,
            targetRunInfoPath,
            boost::filesystem::copy_option::overwrite_if_exists
        );
    }

    boost::filesystem::path sampleSheetPath(this->getConfigEntry<string>("SampleSheetPath"));
    if (!sampleSheetPath.empty()) {
        string targetSampleSheetPath = exportDirectory + sampleSheetPath.filename().string();
        boost::filesystem::copy_file(
            sampleSheetPath,
            targetSampleSheetPath,
            boost::filesystem::copy_option::overwrite_if_exists
        );
    }

    // Copy Filter files

    string baseCallsDirectory = this->getConfigEntry<string>("BaseCallsDirectory");
    for (auto lane : this->getConfigEntry<vector<uint16_t>>("lanes")) {
        string laneDirectory = baseCallsDirectory + "/L00" + to_string(lane) + "/";
        string exportLaneDirectory = exportBaseCallsDirectory + "L00" + to_string(lane) + "/";

        for (auto tile : this->getConfigEntry<vector<uint16_t>>("tiles")) {
            boost::filesystem::path filterFilePath(laneDirectory + "s_" + to_string(lane) + "_" + to_string(tile) + ".filter");
            if (!filterFilePath.empty()) {
                string targetFilterFilePath = exportLaneDirectory + filterFilePath.filename().string();
                boost::filesystem::copy_file(
                    filterFilePath,
                    targetFilterFilePath,
                    boost::filesystem::copy_option::overwrite_if_exists
                );
            }
        }
    }

    return this->framework->createNewFragmentContainer();
};

std::shared_ptr<FragmentContainer> BclExport::runCycle(std::shared_ptr<FragmentContainer> inputFragments) {
    (void) inputFragments; // UNUSED

    string exportDirectory = this->getConfigEntry<string>("outDir")
        + this->getConfigEntry<string>("exportDirectoryName") + "/";
    string exportBaseCallsDirectory = exportDirectory + "Data/Intensities/BaseCalls/";

    
    for (auto lane : this->getConfigEntry<vector<uint16_t>>("lanes")) {
        string laneDirectory = exportBaseCallsDirectory + "L00" + to_string(lane) + "/";
        string cycleDirectory = laneDirectory + "C" + to_string(this->framework->getCurrentCycle()) + ".1/";
        boost::filesystem::create_directory(cycleDirectory);

        for (auto tile : this->getConfigEntry<vector<uint16_t>>("tiles")) {
            BCL bcl = this->framework->getMostRecentBcl(lane, tile);
            FILE* tileFile;
            tileFile = fopen((cycleDirectory + "s_" + to_string(lane) + "_" + to_string(tile) + ".bcl").c_str(), "wb");

            uint32_t numBaseCalls = bcl.size();
            fwrite(&numBaseCalls, 1, sizeof(uint32_t), tileFile);

            fwrite(bcl.data(), sizeof(uint8_t), bcl.size(), tileFile);

            fclose(tileFile);
        }
    }
    
    return this->framework->createNewFragmentContainer();
};

std::shared_ptr<FragmentContainer> BclExport::runFullReadPostprocessing(std::shared_ptr<FragmentContainer> inputFragments)  {
    (void) inputFragments; // UNUSED

    return this->framework->createNewFragmentContainer();
};

void BclExport::setConfig() {
    this->registerConfigEntry<string>("exportDirectoryName", "bclExport");
};

extern "C" BclExport *create() {
    return new BclExport;
}

extern "C" void destroy(BclExport *plugin) {
    delete plugin;
}

