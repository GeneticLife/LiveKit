#ifndef LIVEKIT_ALIGNER_H
#define LIVEKIT_ALIGNER_H

#include "../HiLive_sharedLibraries/alnread.h"
#include "../../serializables/SerializableAlignment.h"
#include "../../serializables/SerializableIndex.h"
#include "../../framework/TileBasedFrameworkInterface.h"

/**
 * This class is the abstract base class for strategies that implement the "extendAlignment"- and "extendBarcode"-
 * Functionality. The Aligner should extend the Alignment of the "SerializableAlignment" that it gets a pointer to.
 */
class Aligner {
protected:
    Configurable* settings;
    SerializableIndex *index;
    TileBasedFrameworkInterface *framework;
public:
    SerializableAlignment *alignment;

    Aligner(Configurable *settings, SerializableIndex *index, TileBasedFrameworkInterface *framework);

    void setAlignment(SerializableAlignment *alignment);

    /**
     * Initialize empty alignments.
     */
    virtual void initAlignment() = 0;

    /**
     * Extend the alignments for all reads of the Alignments by one cycle.
     * @param cycle Current cycle, i.e. the cycle that will be extended.
     * @param keepAlnFile Flag whether the according Align-File should be kept.
     * @return Total number of seeds (for all reads).
     */
    virtual uint64_t extendAlignment(uint16_t cycle, bool keepAlnFile) = 0;

    /**
     * Extend the barcode for all reads with the information of the current sequencing cycle.
     * @param bc_cycle The cycle of the barcode read.
     * @param currentReadLength The length of the current Read.
     */
    virtual void extendBarcode(uint16_t bc_cycle, uint16_t currentReadLength) = 0;

    virtual ~Aligner() = default;
};


#endif //LIVEKIT_ALIGNER_H
