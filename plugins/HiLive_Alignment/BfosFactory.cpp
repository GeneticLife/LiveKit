#include "BfosFactory.h"

using namespace std;

BamFileOutDeque *BfosFactory::initBfos(uint16_t cycle) {
    boost::filesystem::create_directories(this->settings->getConfigEntry<string>("outDir"));

    vector<string> barcodes = this->get_barcode_string_vector();

    auto bfos = new BamFileOutDeque;

    // Init the bamIOContext (the same object can be used for all output streams)
    bfos->set_context(this->index->getSeqNames(), this->index->getSeqLengths());

    // Init the header (the same object can be used for all output streams)
    seqan::BamHeader header = getBamHeader();

    // Init output stream for each barcode (plus undetermined if keep_all_barcodes is set)
    for ( unsigned int barcode=0; barcode < (barcodes.size() + 1); barcode ++) {
        if ( barcode < barcodes.size() || this->settings->getConfigEntry<bool>("keepAllBarcodes") ) {

            string barcode_string = ( barcode == barcodes.size() ) ? "undetermined" : barcodes[barcode];

            // Open file in Bam output stream and write the header
            bfos->emplace_back( this->getBamTempFileName(barcode_string, cycle) );
            (*bfos)[barcode].writeHeader(header);

        }
    }

    return bfos;
}


void BfosFactory::finalizeBfos(BamFileOutDeque *bfos, uint16_t cycle) {
    bfos->clear();

    vector<string> barcodes = get_barcode_string_vector();

    // Move all output files to their final location.
    for ( unsigned int barcode=0; barcode < barcodes.size() + 1; barcode ++) {
        if ( barcode < barcodes.size() || this->settings->getConfigEntry<bool>("keepAllBarcodes") ) {

            string barcode_string = ( barcode == barcodes.size() ) ? "undetermined" : barcodes[barcode];

            int rename = atomic_rename(this->getBamTempFileName(barcode_string, cycle).c_str(), this->getBamFileName(barcode_string, cycle).c_str());
            if ( rename == -1 ) {
                cerr << "Renaming temporary output file " << this->getBamTempFileName(barcode_string, cycle).c_str() << " to " << getBamFileName(barcode_string, cycle).c_str() << " failed." << endl;
            }
        }
    }

    delete bfos;

}

string BfosFactory::getBamFileName(string barcode, CountType cycle) {
    ostringstream fname;
    string file_suffix = getOutputFormat() == OutputFormat::BAM ? ".bam" : ".sam";
    fname << settings->getConfigEntry<string>("outDir") << "hilive_out_" << "cycle" << to_string(cycle) << "_" << barcode << file_suffix;
    return fname.str();
}

string BfosFactory::getBamTempFileName(string barcode, CountType cycle) {
    ostringstream fname;
    string file_suffix = getOutputFormat() == OutputFormat::BAM ? ".bam" : ".sam";
    fname << settings->getConfigEntry<string>("outDir") << "hilive_out_" << "cycle" << to_string(cycle) << "_" << barcode << ".temp" << file_suffix;
    return fname.str();
}

OutputFormat BfosFactory::getOutputFormat() {
    return this->settings->getConfigEntry<OutputFormat>("outputFormat");
}

vector<string> BfosFactory::get_barcode_string_vector() {
    vector<string> bc_strings;
    for (CountType i = 0; i < this->settings->getConfigEntry<vector<vector<string>>>("barcodeVector").size(); i++) {
        bc_strings.push_back(this->get_barcode_string(i));
    }
    return bc_strings;
}

string BfosFactory::get_barcode_string(CountType index) const {

    // invalid index
    if (index >= this->settings->getConfigEntry<vector<vector<string>>>("barcodeVector").size()) {
        return "";
    } else {

        vector<string> bc_vec = this->settings->getConfigEntry<vector<vector<string>>>("barcodeVector")[index];

        stringstream ss;
        for (auto fragment : bc_vec) {
            ss << fragment;
        }
        string barcode_string = ss.str();
        return this->format_barcode(barcode_string);
    }
}

string BfosFactory::format_barcode(string unformatted_barcode) const {
    CountType pos = 0;
    for (auto el : this->sequenceElements->getSeqs()) {
        if (el.mate == 0) {
            pos += el.length;
            if (unformatted_barcode.length() >= pos)
                unformatted_barcode.insert(pos++, "-");
            else
                break;
        }
    }

    return unformatted_barcode.substr(0, pos - 1);
}