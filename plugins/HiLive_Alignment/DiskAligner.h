#ifndef LIVEKIT_DISKALIGNER_H
#define LIVEKIT_DISKALIGNER_H

#include "Aligner.h"

/**
 * The MemoryAligner is a concrete implementation of the "extendAlignment"-Functionality, that utilizes mostly the
 * disk in order to save memory.
 */
class DiskAligner : public Aligner {
public:
    DiskAligner(Configurable *settings, SerializableIndex *index, TileBasedFrameworkInterface *framework);

    void initAlignment() override;

    uint64_t extendAlignment(uint16_t cycle, bool keepAlnFile) override;

    void extendBarcode(uint16_t bc_cycle, uint16_t currentReadLength) override;
};


#endif //LIVEKIT_DISKALIGNER_H
