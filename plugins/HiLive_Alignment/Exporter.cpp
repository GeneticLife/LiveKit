#include "Exporter.h"

using namespace std;

Exporter::Exporter(
        Configurable *settings,
        SerializableIndex *index,
        TileBasedFrameworkInterface *framework,
        SerializableSequenceElements *sequenceElements,
        uint16_t lane,
        uint16_t tile,
        uint16_t cycle,
        int mateCount,
        vector<SerializableAlignment *> alignmentFiles,
        BamFileOutDeque *bfos)
    : settings(settings)
    , index(index)
    , framework(framework)
    , sequenceElements(sequenceElements)
    , lane(lane)
    , tile(tile)
    , cycle(cycle)
    , alignmentFiles(alignmentFiles)
    , bfos(bfos)
    , numBarcodes(this->settings->getConfigEntry<vector<vector<string>>>("barcodeVector").size())
    , outputMode(this->settings->getConfigEntry<OutputMode>("mode")) {


    // Get the finished cycles for each mate
    for ( CountType mate = 1; mate <= mateCount; mate++ ) {
        this->mateCycles.push_back( this->sequenceElements->getMateCycle( mate, cycle ) );
    }
}

string Exporter::format_barcode(string unformatted_barcode) const {
    CountType pos = 0;
    for (auto el : this->sequenceElements->getSeqs()) {
        if (el.mate == 0) {
            pos += el.length;
            if (unformatted_barcode.length() >= pos)
                unformatted_barcode.insert(pos++, "-");
            else
                break;
        }
    }

    return unformatted_barcode.substr(0, pos - 1);
}

void Exporter::writeTileToBam() {
    ReadAlignmentSettings readAlignmentSettings = ReadAlignment::generateSettingsfromConfigurable(this->settings);


    ////////////////////////////////////////////////////
    //  Main loop //////////////////////////////////////
    ////////////////////////////////////////////////////

    // load alignment into memory
    for(auto alignment : this->alignmentFiles)
        alignment->disableMemorySaveMode(this->settings->getConfigEntry<string>("tempDir"));

    unsigned numberOfAlignments = this->alignmentFiles[0]->numReads;

    bool    keepAllBarcodes     = this->settings->getConfigEntry<bool>("keepAllBarcodes"),
            reportUnmapped      = this->settings->getConfigEntry<bool>("reportUnmapped"),
            keepAllSequences    = this->settings->getConfigEntry<bool>("keepAllSequences");

    ScoreType   minAs               = this->settings->getConfigEntry<ScoreType>("minAs");
    float       maxSoftclipRatio    = this->settings->getConfigEntry<float>("maxSoftclipRatio");
    uint16_t    bestN               = this->settings->getConfigEntry<uint16_t>("bestN");

    // for all reads in a tile
    /////////////////////////////////////////////////////////////////////////////
    for (uint64_t i = 0; i < numberOfAlignments; i++) {

        int mateIndex = 1;
        vector<ReadAlignment*> mateAlignments;
        for (auto e: alignmentFiles) {
            mateAlignments.push_back(e->alignments[i]);
            //mateAlignments.push_back(e->get_alignment());
            mateAlignments.back()->setSettings(&readAlignmentSettings);

            int currentReadId = this->sequenceElements->getSeqByMate(mateIndex++).readId;
            mateAlignments.back()->setReadGetter([=] (){
                return this->framework->getReadIterators(i, currentReadId);
            });
            mateAlignments.back()->sort_seeds_by_as();
        }

        vector<vector<seqan::BamAlignmentRecord>> mateRecords(mateAlignments.size(), vector<seqan::BamAlignmentRecord>(0));

        // if the filter file is available and the filter flag is 0 then skip
        // TODO: Improve performance, use getFilterData
        if (this->framework->filterBasecall(lane, tile, i)) continue;

        // compute barcode sequence as it should be written to BC tag
        string barcode = this->format_barcode(mateAlignments[0]->getBarcodeString());

        // Barcode index for the read
        CountType barcodeIndex = mateAlignments[0]->getBarcodeIndex();

        // If read has undetermined barcode and keep_all_barcodes is not set, skip this read
        if ( barcodeIndex == UNDETERMINED && !keepAllBarcodes ) { continue; }
        else if ( barcodeIndex == UNDETERMINED ) barcodeIndex = numBarcodes; // this is the index for the "undetermined" output stream

        // setup QNAME
        // Read name format <instrument‐name>:<run ID>:<flowcell ID>:<lane‐number>:<tile‐number>:<x‐pos>:<y‐pos>
        // readname << "<instrument>:<run-ID>:<flowcell-ID>:" << ln << ":" << tl << ":<xpos>:<ypos>:" << i;
        //TODO: Get coordinates from clocs file, run-ID from runInfo.xml, flowcell ID from runInfo.xml, instrument from runInfo.xml
        stringstream readname;
        readname << "lane." << lane << "|tile." << tile << "|read." << i;

        // Track equivalent alignments for the same mate (similar positions from different seeds -> only keep the best one)
        // TODO: implement equivalent alignment window as user parameter
        PositionType equivalentAlignmentWindow = 10;

        // TODO: Improve the prevention of reporting similar alignments.
        //		set<PositionType> alignmentPositions;
        set<GenomePosType> alignmentPositions;

        // for all mates
        /////////////////////////////////////////////////////////////////////////////
        for (unsigned int mateAlignmentIndex=0; mateAlignmentIndex < mateAlignments.size(); ++mateAlignmentIndex) {

            // Decrease the ReadAlignment cycle since it is automatically increased when loading the file TODO: this should be changed.
            //mateAlignments[mateAlignmentIndex]->cycle -= 1;

            // Init record with information about the read
            seqan::BamAlignmentRecord mate_record;
            mate_record.qName = readname.str();
            mate_record.flag = 0;

            // Set correct segment (paired) flags for the record
            if ( mateAlignments.size() > 1) { // if there are at least two mates already sequenced

                mate_record.flag = addSAMFlag(mate_record.flag, SAMFlag::MULT_SEG);
                if (mateAlignmentIndex == 0) {
                    mate_record.flag |= addSAMFlag(mate_record.flag, SAMFlag::FIRST_SEG);
                } else if (mateAlignmentIndex == mateAlignments.size()-1) {
                    mate_record.flag = addSAMFlag(mate_record.flag, SAMFlag::LAST_SEG);
                } else {
                    mate_record.flag = addSAMFlag(mate_record.flag, SAMFlag::FIRST_SEG);
                    mate_record.flag = addSAMFlag(mate_record.flag, SAMFlag::LAST_SEG);
                }

            }

            // Add read specific information to the BAM record
            mateAlignments[mateAlignmentIndex]->addReadInfoToRecord(mate_record);

            // Alignment disabled or no seeds
            if ( mateAlignments[mateAlignmentIndex]->is_disabled() || mateAlignments[mateAlignmentIndex]->seeds.size() == 0 ) {

                // Don't report disabled reads if their sequences are not kept.
                if ( mateAlignments[mateAlignmentIndex]->is_disabled() && !keepAllSequences )
                    continue;

                // Report unmapped reads if activated
                if ( reportUnmapped ) {
                    mate_record.flag = addSAMFlag(mate_record.flag, SAMFlag::SEG_UNMAPPED);
                    mateRecords[mateAlignmentIndex].push_back(mate_record);
                }
                continue;
            }

            // Variables for output modes
            ScoreType first_seed_score = 0;
            ScoreType last_seed_score = 0;
            CountType num_diff_scores = 0;

            // Number of printed alignments for the current mate.
            unsigned printedMateAlignments = 0;

            // Unique mode interruption
            // TODO: Think about reporting of unmapped and non-unique reads if report-unmapped is activated
            // TODO: this filtering approach is problematic if similar seeds exist ...
            if ( this->is_mode(UNIQUE) && (
                    mateAlignments[mateAlignmentIndex]->seeds.size() > 1 ||
                    (mateAlignments[mateAlignmentIndex]->seeds.size() == 1 && mateAlignments[mateAlignmentIndex]->seeds.front()->getNumPositions() > 1))) {
                continue;
            }

            vector<uint8_t> mapqs = mateAlignments[mateAlignmentIndex]->getMAPQs();
            auto mapqs_it = mapqs.begin();

            // for all seeds
            /////////////////////////////////////////////////////////////////////////////
            for (SeedVecIt it = mateAlignments[mateAlignmentIndex]->seeds.begin(); it != mateAlignments[mateAlignmentIndex]->seeds.end(); ++it, ++mapqs_it) {

                ScoreType curr_seed_score = (*it)->get_as();

                // If no alignment was printed before, the current one has the best "score"
                if ( printedMateAlignments == 0 )
                    first_seed_score = curr_seed_score;

                // Stop in all best mode when AS:i score is lower than the first
                if( this->is_mode(ALLBEST) && first_seed_score > curr_seed_score ) {
                    goto nextmate;
                }


                // Don't write this seed if the user-specified score or softclip ratio is not fulfilled
                CountType softclip_length = (*it)->get_softclip_length();
                if ( curr_seed_score < minAs || softclip_length > maxSoftclipRatio * mateCycles[mateAlignmentIndex]) {
                    continue;
                }

                // get CIGAR-String
                seqan::String<seqan::CigarElement<> > cigar = (*it)->returnSeqanCigarString();

                // Get NM:i value
                unsigned nm = (*it)->get_nm();


                // check if cigar string sums up to read length
                // TODO Potentially conflicts with the 'eachMateAligned' flag if done here.
                unsigned cigarElemSum = 0;
                unsigned deletionSum = 0;
                unsigned supposed_cigar_length = mateCycles[mateAlignmentIndex];

                for (seqan::Iterator<seqan::String<seqan::CigarElement<> > >::Type elem = seqan::begin(cigar); elem != end(cigar); ++elem) {
                    if ((elem->operation == 'M') || (elem->operation == 'I') || (elem->operation == 'S') || (elem->operation == '=') || (elem->operation == 'X'))
                        cigarElemSum += elem->count;

                    if (elem->operation == 'D') {
                        deletionSum += elem->count;
                    }
                }
                if (cigarElemSum != supposed_cigar_length) {
                    it = mateAlignments[mateAlignmentIndex]->seeds.erase(it);
                    it--;
                    continue;
                }
                if (deletionSum >= supposed_cigar_length) {
                    it = mateAlignments[mateAlignmentIndex]->seeds.erase(it);
                    it--;
                    continue;
                }

                // Get positions for the current seed
                vector<GenomePosType> pos_list;

                if ( this->is_mode(ANYBEST) ) {
                    pos_list = (*it)->getPositions(0, 1, this->index); // retrieve only one position from the index
                } else
                    pos_list = (*it)->getPositions(this->index); // retrieve all positions from the index

                // handle all positions
                for ( auto p = pos_list.begin(); p != pos_list.end(); ++p ) {

                    // Stop in any best mode when first alignment was already written
                    if( this->is_mode(ANYBEST) && printedMateAlignments >= 1 ) {
                        goto nextmate;
                    }

                    // Stop in bestn mode if first n alignments were already written
                    if ( this->is_mode(BESTN) && printedMateAlignments >= bestN ) {
                        goto nextmate;
                    }

                    seqan::BamAlignmentRecord record = mate_record;

                    record.rID = CountType(p->gid / 2);

                    record.beginPos = mateAlignments[mateAlignmentIndex]->get_SAM_start_pos(*p, *it, this->index);

                    record.mapQ = *mapqs_it;

                    // skip invalid positions
                    if (record.beginPos < 0 || PositionType(record.beginPos) == numeric_limits<PositionType>::max()) {
                        continue;
                    }

                    // skip positions that were already written (equivalent alignments). This can be done because the best alignment for this position is written first.
                    if ( alignmentPositions.find(GenomePosType(p->gid, record.beginPos - ( record.beginPos % equivalentAlignmentWindow ) )) != alignmentPositions.end() ||
                         alignmentPositions.find(GenomePosType(p->gid, record.beginPos +  (equivalentAlignmentWindow - ( record.beginPos % equivalentAlignmentWindow ) ) ) ) != alignmentPositions.end()) {
                        continue;
                    }

                    record.cigar = cigar;
                    if ( this->index->isReverse(p->gid) )
                        seqan::reverse(record.cigar);

                    if ( printedMateAlignments > 0 ) { // if current seed is secondary alignment
                        record.flag = addSAMFlag(record.flag, SAMFlag::SEC_ALIGNMENT);
                        seqan::clear(record.seq);
                        seqan::clear(record.qual);
                    }

                    if ( this->index->isReverse(p->gid) ) { // if read matched reverse complementary
                        seqan::reverseComplement(record.seq);
                        seqan::reverse(record.qual);
                        record.flag = addSAMFlag(record.flag, SAMFlag::SEQ_RC);
                    }

                    // Dictionary for additional SAM tags
                    seqan::BamTagsDict dict;

                    // Alignment Score
                    seqan::appendTagValue(dict, "AS", curr_seed_score);

                    // Barcode sequence
                    if (barcode!="")
                        seqan::appendTagValue(dict, "BC", barcode);

                    // Number of mismatches
                    seqan::appendTagValue(dict, "NM", nm);

                    // MD:Z string
                    string mdz = (*it)->getMDZString();
                    if ( this->index->isReverse(p->gid))
                        mdz = reverse_mdz(mdz);
                    seqan::appendTagValue(dict, "MD", mdz);

                    record.tags = seqan::host(dict);

                    // fill records list
                    mateRecords[mateAlignmentIndex].push_back(record);

                    // set variables for mode selection
                    if ( last_seed_score != curr_seed_score || num_diff_scores == 0 )
                        ++num_diff_scores;
                    last_seed_score = curr_seed_score;

                    ++printedMateAlignments;
                    alignmentPositions.insert(GenomePosType(p->gid, record.beginPos - ( record.beginPos % equivalentAlignmentWindow )));

                }
            }
            nextmate: {
            // Report unmapped reads if activated
            if ( mateRecords[mateAlignmentIndex].size()==0 && reportUnmapped ) {
                mate_record.flag = addSAMFlag(mate_record.flag, SAMFlag::SEG_UNMAPPED);
                mateRecords[mateAlignmentIndex].push_back(mate_record);
            }
        }
        }

        // Set flags related to the next mate.
        setMateSAMFlags(mateRecords);

        // Write all records as a group to keep suboptimal alignments and paired reads together.
        (*bfos)[barcodeIndex].writeRecords(mateRecords);

        //for (auto e:mateAlignments) delete e;

    }

    //for (auto e:alignmentFiles) delete e;
}

void Exporter::setMateSAMFlags( vector<vector<seqan::BamAlignmentRecord>> & mateRecords ) {
    for ( CountType i=0; i<mateRecords.size(); i++ ) {

        // Stop if not paired
        if ( mateRecords.size() <= 1 )
            break;

        // Set related mate index
        CountType next = i==mateRecords.size()-1 ? 0 : i+1;

        for ( auto & record_pointer : mateRecords[i] ) {

            if ( mateRecords[next].size() > 0 ) {

                CountType mateFlag = mateRecords[next][0].flag;

                // set next mate rID
                record_pointer.rNextId = mateRecords[next][0].rID;

                // set next mate pos
                record_pointer.pNext = mateRecords[next][0].beginPos;

                // other mate entry is reverse complemented
                if ( hasSAMFlag( mateFlag, SAMFlag::SEQ_RC ) ) {
                    record_pointer.flag = addSAMFlag(record_pointer.flag, SAMFlag::NEXT_SEQ_RC);
                }

                // other mate entry is flagged as unmapped
                if ( hasSAMFlag( mateFlag, SAMFlag::SEG_UNMAPPED ) ) {
                    record_pointer.flag = addSAMFlag(record_pointer.flag, SAMFlag::NEXT_SEG_UNMAPPED);

                    record_pointer.pNext = record_pointer.beginPos;
                    record_pointer.rNextId = record_pointer.rID;

                    // the current read is flagged as unmapped but the mate is not
                } else if ( hasSAMFlag(record_pointer.flag, SAMFlag::SEG_UNMAPPED) ) {
                    record_pointer.beginPos = mateRecords[next][0].beginPos;
                    record_pointer.rID = mateRecords[next][0].rID;
                }


                // No record for the mate available (this implies, that it is unmapped)
            } else {
                record_pointer.flag = addSAMFlag(record_pointer.flag, SAMFlag::NEXT_SEG_UNMAPPED);
            }
        }

    }
}

bool Exporter::is_mode(OutputMode alignment_mode) const {
    return this->outputMode == alignment_mode;
}