#ifndef LIVEKIT_MEMORYALIGNER_H
#define LIVEKIT_MEMORYALIGNER_H

#include "Aligner.h"

/**
 * The MemoryAligner is a concrete implementation of the "extendAlignment"-Functionality, that utilizes mostly the memory.
 */
class MemoryAligner : public Aligner {
public:
    MemoryAligner(Configurable *settings, SerializableIndex *index, TileBasedFrameworkInterface *framework);

    void initAlignment() override;

    uint64_t extendAlignment(uint16_t cycle, bool keepAlnFile) override;

    void extendBarcode(uint16_t bc_cycle, uint16_t currentReadLength) override;
};


#endif //LIVEKIT_MEMORYALIGNER_H
