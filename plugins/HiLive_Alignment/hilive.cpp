#include "hilive.h"
#include "MemoryAligner.h"
#include "DiskAligner.h"
#include "BfosFactory.h"
#include "Exporter.h"

using namespace std;

mutex_map<string> fileLocks;

std::shared_ptr<FragmentContainer> Hilive::runPreprocessing(std::shared_ptr<FragmentContainer> inputFragments) {

    // Get Index from InputFragments
    auto index              = (SerializableIndex *) inputFragments->get("FMIndex")->getSerializable("FMIndex");
    auto sequenceElements   = (SerializableSequenceElements *) this->permanentFragment->getSerializable("sequenceElements");

    // Config entries should only be registered in the setConfig() method, but since we need the index for calculation
    // of the anchorLength we make an exception here
    this->registerCalculatedConfigEntry<uint16_t>("anchorLength", this->setAnchorLength(index));
    this->registerConfigEntry<float>("softclipExtensionPenalty",
                                     float(this->getConfigEntry<uint16_t>("mismatchPenalty"))
                                     / this->getConfigEntry<uint16_t>("anchorLength"),
                                     NO_WARNING);
    this->registerConfigEntry<uint16_t>("errorRate", uint16_t(this->getConfigEntry<uint16_t>("anchorLength") / 2),
                                        NO_WARNING);
    this->registerConfigEntry<uint16_t>("seedingInterval",
                                        max((uint16_t) this->getConfigEntry<uint16_t>("anchorLength") / 2, 1),
                                        NO_WARNING);

    this->printConsoleWelcome();

    auto lanes = this->getConfigEntry<vector<uint16_t>>("lanes");
    auto tiles = this->getConfigEntry<vector<uint16_t>>("tiles");

    for (auto lane : lanes)
        for (auto tile : tiles)
            this->tileFragments[lane][tile] = this->framework->createNewFragment("ALIGN");

    BfosFactory bfosFactory((Configurable *)this, index, sequenceElements);

    for (auto cycle : this->getConfigEntry<vector<uint16_t>>("outputCycles"))
        this->cycleBfos[cycle] = bfosFactory.initBfos(cycle);

    return this->framework->createNewFragmentContainer();
}

void Hilive::printConsoleWelcome() {
    auto sequenceElements = (SerializableSequenceElements *) this->permanentFragment->getSerializable("sequenceElements");

    this->out    << endl
            << "------------------------------------------------------------------" << endl
            << "HiLive v " << HiLive_VERSION_MAJOR << "." << HiLive_VERSION_MINOR
            << " PLUGIN VERSION - Realtime Alignment of Illumina Reads" << endl
            << "------------------------------------------------------------------" << endl;

    if (!this->getConfigEntry<string>("tempDir").empty())
        this->out << "Temporary directory:      " << this->getConfigEntry<string>("tempDir") << endl;
    if (this->getConfigEntry<OutputFormat>("outputFormat") == OutputFormat::SAM)
         this->out << "SAM output directory:     ";
    else this->out << "BAM output directory:     ";
    this->out << this->getConfigEntry<string>("outDir") << endl;

    this->out << "Read lengths:             ";
    string barcode_suffix;
    for (uint16_t read = 0; read != sequenceElements->getSeqs().size(); read++) {
        this->out << sequenceElements->getSeqById(read).length;
        barcode_suffix = sequenceElements->getSeqById(read).isBarcode() ? "B" : "R";
        this->out << barcode_suffix << " ";
    }
    this->out << endl;

    this->out << "Min. alignment score:     " << this->getConfigEntry<ScoreType>("minAs") << endl;
    this->out << "Mapping mode:             "
         << to_string(this->getConfigEntry<OutputMode>("mode"), this->getConfigEntry<uint16_t>("bestN")) << endl;
    this->out << "Anchor length:            " << this->getConfigEntry<uint16_t>("anchorLength") << endl;
    this->out << endl;
}

std::shared_ptr<FragmentContainer> Hilive::runCycle(std::shared_ptr<FragmentContainer> inputFragments) {
    int currentCycle        = this->framework->getCurrentCycle();

    if (this->isOutputCycle(currentCycle)) {
        auto index = (SerializableIndex *) inputFragments->get("FMIndex")->getSerializable("FMIndex");
        SerializableSequenceElements *sequenceElements = (SerializableSequenceElements *)this->permanentFragment->getSerializable("sequenceElements");


        BfosFactory bfosFactory((Configurable *)this, index, sequenceElements);

        bfosFactory.finalizeBfos(this->cycleBfos[currentCycle], currentCycle);
        this->out << " done!" << endl;
    }

    return this->framework->createNewFragmentContainer();
}

std::shared_ptr<FragmentContainer> Hilive::HiliveTileContext::runCycleForTile(std::shared_ptr<FragmentContainer> inputFragments) {
    // TODO: Reimplement "continue" functionality

    int currentReadId       = this->framework->getCurrentReadId(),
        currentMateId       = this->framework->getCurrentMateId(),
        currentReadLength   = this->framework->getCurrentReadLength(),
        mateCount           = this->framework->getMateCount(),
        currentReadCycle    = this->framework->getCurrentReadCycle(),
        currentCycle        = this->framework->getCurrentCycle(),
        previousCycle       = currentCycle - 1;

    bool isBarcodeCycle     = this->framework->isBarcodeCycle();

    // Get Index from InputFragments
    auto index = (SerializableIndex *) inputFragments->get("FMIndex")->getSerializable("FMIndex");

    Task t( lane, tile, SequenceElement(currentReadId, currentMateId, currentReadId, currentReadLength), currentReadCycle );

    auto outCycles      = this->plugin->getConfigEntry<vector<uint16_t>>("outputCycles");
    auto alnFileCycles  = this->plugin->getConfigEntry<vector<uint16_t>>("keepAlnFiles");
    auto memorySaveMode = this->plugin->getConfigEntry<bool>("memorySaveMode");

    // When memorySaveMode is enabled, the alnFiles of the previous cycle have to be prevented from deletion
    int alnFileCycle = memorySaveMode ? previousCycle : currentCycle;

    bool isKeepAlnFileCycle = (find(alnFileCycles.begin(), alnFileCycles.end(), alnFileCycle) != alnFileCycles.end())
        || (find(outCycles.begin(), outCycles.end(), alnFileCycle) != outCycles.end())
        || currentReadCycle == currentReadLength; // keep the align file, for a read that is finished

    SerializableSequenceElements *sequenceElements = (SerializableSequenceElements *)this->plugin->permanentFragment->getSerializable("sequenceElements");

    Aligner *aligner = memorySaveMode
                      ? static_cast<Aligner *>(new DiskAligner((Configurable *)this->plugin, index, this->framework))
                      : static_cast<Aligner *>(new MemoryAligner((Configurable *)this->plugin, index, this->framework));

    if (currentCycle == 1) {
        for (auto sequenceElement : sequenceElements->getSeqs()) {
            if (sequenceElement.mate == 0) continue;

            uint32_t num_reads = this->framework->getNumSequences(lane, tile);
            SerializableAlignment *alignment = new SerializableAlignment(
                    this->lane,
                    this->tile,
                    sequenceElement.length,
                    sequenceElement.mate,
                    sequenceElement.readId,
                    num_reads,
                    memorySaveMode,
                    this->plugin->getConfigEntry<uint64_t>("blockSize"),
                    this->plugin->getConfigEntry<uint8_t>("compressedFormat")
            );

            aligner->setAlignment(alignment);
            aligner->initAlignment();

            this->tileFragment->setSerializable("mate_" + to_string(sequenceElement.mate), alignment);
        }
    }

    try {
        if (!isBarcodeCycle) {
            // Get the alignment for this mate from the PermanentFragments
            auto currentAlignment = (SerializableAlignment *) this->tileFragment->getSerializable("mate_" + to_string(currentMateId));
            aligner->setAlignment(currentAlignment);

            // Seed extension if current read is sequence fragment.
            uint64_t num_seeds = aligner->extendAlignment(currentReadCycle, isKeepAlnFileCycle);

            this->out << "Task [" << t << "]: Found " << num_seeds << " seeds." << endl;
        } else {
            // Barcode extension if current read is barcode fragment
            for (int mate = 1; mate <= mateCount; mate++) {
                SequenceElement seqEl = sequenceElements->getSeqByMate(mate);
                CountType current_mate_cycle = t.seqEl.id < seqEl.id ? 0 : seqEl.length;

                auto alignment = (SerializableAlignment *) this->tileFragment->getSerializable(
                    "mate_" + to_string(seqEl.mate)
                );

                aligner->setAlignment(alignment);

                aligner->extendBarcode(
                    current_mate_cycle,
                    sequenceElements->getSeqs()[t.seqEl.id].length
                );
            }
            this->out << "Task [" << t << "]: Extended barcode of " << mateCount << " mates." << endl;
        }
    } catch (const exception &e) { cerr << "Failed to finish task [" << t << "]: " << e.what() << endl; }

    delete aligner;

    bool isOutputCycle = dynamic_cast<Hilive *>(this->plugin)->isOutputCycle(currentCycle);

    if (isOutputCycle) {
        this->out << "Executing output task of lane " << this->lane << " and tile " << this->tile << "." << endl;

        // Get Index from InputFragments
        auto index = (SerializableIndex *) inputFragments->get("FMIndex")->getSerializable("FMIndex");


        vector<SerializableAlignment *> alignmentFiles;
        for (int i = 1; i <= mateCount; i++)
            alignmentFiles.push_back((SerializableAlignment *) this->tileFragment->getSerializable("mate_" + to_string(i)));
        Exporter exporter(
            (Configurable *)this->plugin,
            index,
            this->framework,
            sequenceElements,
            this->lane,
            this->tile,
            currentCycle,
            mateCount,
            alignmentFiles,
            dynamic_cast<Hilive *>(this->plugin)->cycleBfos[currentCycle]
        );

        exporter.writeTileToBam();
    }

    auto outputContainer = this->framework->createNewFragmentContainer();
    outputContainer->add(this->tileFragment);
    return outputContainer;
}

bool Hilive::isOutputCycle(int cycle) {
    vector<uint16_t> outCycles = this->getConfigEntry<vector<uint16_t>>("outputCycles");
    return !(find(outCycles.begin(), outCycles.end(), cycle) == outCycles.end());
}

void Hilive::setConfig() {

    this->permanentFragment = this->framework->createNewFragment("hiliveFragment");

    enum CompressionFormat : uint8_t {
        FORMAT_FWRITE = 0, FORMAT_GZWRITE = 1, FORMAT_LZ4WRITE = 2
    };

    this->registerConfigEntry<uint16_t>("startCycle", 1);

    // needed for SerializableAlignment
    this->registerConfigEntry<string>("tempDir", "./temp/");

    // fallback is the framework-defined output directory
    this->registerConfigEntry<string>("outDir", this->getConfigEntry<string>("outDir"));

    this->registerConfigEntry<uint64_t, string>("blockSize", "64M", Configurable::toBytes());
    this->registerConfigEntry<uint8_t>("compressedFormat", FORMAT_LZ4WRITE);

    this->registerConfigEntry<uint16_t>("threadsPerTile", 1, NO_WARNING);
    this->registerConfigEntry<bool>("memorySaveMode", false, NO_WARNING);

    this->registerConfigEntry<bool>("keepAllAlnFiles", false);
    this->registerConfigEntry<vector<uint16_t>, string>("keepAlnFiles", "", [&](string input) {
        if (this->getConfigEntry<bool>("keepAllAlnFiles")) {
            vector<CountType> keep_aln_cycles(static_cast<unsigned long>(this->framework->getCycleCount()));
            iota(keep_aln_cycles.begin(), keep_aln_cycles.end(), 1);
            return keep_aln_cycles;
        } else if (!input.empty()) return toVector<uint16_t>(',')(input);
        else return vector<uint16_t>{};
    });

    this->registerConfigEntry<AlignmentMode, string>("alignMode", "balanced", [](string input) {
        char first = toupper(input[0]), second = toupper(input[1]), fifth = toupper(input[5]);
        if (first == 'B') return BALANCED;
        if (first == 'A') return ACCURATE;
        if (first == 'F') return FAST;
        if (first == 'V' && (second == 'F' || fifth == 'F')) return VERYFAST;
        if (first == 'V' && (second == 'A' || fifth == 'A')) return VERYACCURATE;
        throw runtime_error("Invalid alignment mode " + input + ".");
    });

    this->registerConfigEntry<bool>("extendedCigar", false, NO_WARNING);
    this->registerConfigEntry<uint16_t>("mismatchPenalty", 6, NO_WARNING);
    this->registerConfigEntry<float>("softclipOpeningPenalty",
                                     float(this->getConfigEntry<uint16_t>("mismatchPenalty")),
                                     NO_WARNING);
    this->registerConfigEntry<uint16_t>("insertionOpeningPenalty", 5, NO_WARNING);
    this->registerConfigEntry<uint16_t>("insertionExtensionPenalty", 3, NO_WARNING);
    this->registerConfigEntry<uint16_t>("deletionOpeningPenalty", 5, NO_WARNING);
    this->registerConfigEntry<uint16_t>("deletionExtensionPenalty", 3, NO_WARNING);
    this->registerConfigEntry<uint16_t>("matchScore", 0, NO_WARNING);

    //needed for ReadAlignment
    this->registerConfigEntry<uint16_t>("maxGapLength", 3, NO_WARNING);

    uint16_t mismatch_penalty = this->getConfigEntry<uint16_t>("mismatchPenalty")
                                + this->getConfigEntry<uint16_t>("matchScore");
    uint16_t deletion_penalty = this->getConfigEntry<uint16_t>("deletionOpeningPenalty")
                                + this->getConfigEntry<uint16_t>("deletionExtensionPenalty");
    uint16_t insertion_penalty = this->getConfigEntry<uint16_t>("insertionOpeningPenalty")
                                 + this->getConfigEntry<uint16_t>("deletionExtensionPenalty")
                                 + this->getConfigEntry<uint16_t>("matchScore");

    uint16_t maxSingleErrorPenalty = max(mismatch_penalty, max(insertion_penalty, deletion_penalty));
    this->registerConfigEntry<OutputMode, string>("mode", "ANYBEST", this->setMode());

    auto readStructure = this->getConfigEntry<vector<pair<int, char>>>("reads");
    auto sequenceElements = this->createSequenceElements(readStructure);
    this->permanentFragment->setSerializable("sequenceElements", sequenceElements);

    float default_error_rate = map<AlignmentMode, float>{
            {VERYFAST,     0.015f},
            {FAST,         0.02f},
            {BALANCED,     0.025f},
            {ACCURATE,     0.03f},
            {VERYACCURATE, 0.035f}
    }[this->getConfigEntry<AlignmentMode>("alignMode")];

    float min_as_default = (this->getConfigEntry<uint16_t>("matchScore") * (sequenceElements->getSeqByMate(1).length)) -
                           (float(sequenceElements->getSeqByMate(1).length) * default_error_rate *
                            maxSingleErrorPenalty);
    this->registerConfigEntry<ScoreType>("minAs", static_cast<const ScoreType &>(min_as_default));
    this->registerConfigEntry<bool>("keepAllSequences", false);

    // needed for AlnOut
    this->registerConfigEntry<bool, bool>("keepAllBarcodes", false, [&](bool value) {
        return this->getConfigEntry<vector<vector<string>>>("barcodeVector").empty() || value;
    });
    this->registerConfigEntry<bool>("forceResort", false);
    this->registerConfigEntry<bool>("reportUnmapped", false);
    this->registerConfigEntry<float>("maxSoftclipRatio", 0.2f);
    this->registerConfigEntry<uint16_t>("maxSoftclipLength", sequenceElements->getSeqByMate(1).length / 2);
    this->registerConfigEntry<OutputFormat, string>("outputFormat", "BAM", [&](string value) {
        if (value[0] == 'S') return OutputFormat::SAM;
        else if (value[0] == 'B') return OutputFormat::BAM;
        else throw runtime_error("Invalid output format: " + value + ".");
    });
    this->registerConfigEntry<vector<uint16_t>, string>("outputCycles",
                                                        to_string(this->framework->getCycleCount()),
                                                        toVector<uint16_t>(','));

}

void Hilive::finalize() {}

function<OutputMode(string)> Hilive::setMode() {
    return [&](string value) {
        if (value.substr(0, 5) != "BESTN" && value.substr(0, 1) != "N")
            this->registerConfigEntry<uint16_t>("bestN", 0, NO_WARNING);

        if (value == "ALL" || value == "A") return OutputMode::ALL; // All hit mode
        else if (value == "UNIQUE" || value == "U") return OutputMode::UNIQUE; // Unique mode
        else if (value == "ALLBEST" || value == "H") return OutputMode::ALLBEST; // All best mode
        else if (value == "ANYBEST" || value == "B") return OutputMode::ANYBEST; // All hit mode
        else if (value.substr(0, 5) == "BESTN" || value.substr(0, 1) == "N") {
            string bestn = value.substr(0, 5) == "BESTN" ? value.substr(5) : value.substr(1);
            if (bestn.find_first_not_of("0123456789") != string::npos)
                throw runtime_error("Invalid alignment mode: " + value + ".");
            try {
                this->registerConfigEntry<uint16_t>("bestN", (uint16_t) atol(bestn.c_str()));
                return OutputMode::BESTN; // Best N scores mode
            } catch (bad_cast &ex) {
                cerr << "Error while casting length " << bestn << " to type uint16_t." << endl;
                throw ex;
            }
        } else throw runtime_error("Invalid alignment mode: " + value + "."); // Unknown mode
    };
}

SerializableSequenceElements *Hilive::createSequenceElements(vector<pair<int, char>> read_structure) {
    uint16_t mates = 1;
    uint16_t readId = 0;

    vector<SequenceElement> temp;

    // Iterate through input vector
    temp.reserve(read_structure.size());
    for (auto &pair : read_structure)
        temp.emplace_back(temp.size(), (pair.second == 'R') ? mates++ : 0, readId++, pair.first);

    auto sequenceElements = new SerializableSequenceElements(temp);
    sequenceElements->setMateCount(static_cast<uint16_t>(this->framework->getMateCount()));

    return sequenceElements;
}

std::function<uint16_t()> Hilive::setAnchorLength(SerializableIndex *index) {
    return [&, index]() {
        uint64_t genome_size = 0;
        for (uint32_t i = 0; i < index->getNumSequences(); i++) {
            genome_size += 2 * index->getSeqLengths()[i];
        }

        // relative number of reads matching a reference of given length randomly (in theory, not in biology)
        float expectation_value = .0001f;
        auto balanced_anchor_length = static_cast<uint16_t>(log(float(genome_size) / expectation_value) / log(4));

        return (uint16_t) floor(map<AlignmentMode, float>{
                {VERYFAST,     1.25f},
                {FAST,         1.125f},
                {BALANCED,     1.0f},
                {ACCURATE,     0.875f},
                {VERYACCURATE, 0.75f}
        }[this->getConfigEntry<AlignmentMode>("alignMode")] * balanced_anchor_length);
    };
}

void Hilive::switchToMemorySaveMode() {
    if (!this->getConfigEntry<bool>("memorySaveMode")) {
        this->out << "Switch to memorySaveMode in HiLive" << endl;

        auto lanes = this->getConfigEntry<vector<uint16_t>>("lanes");
        auto tiles = this->getConfigEntry<vector<uint16_t>>("tiles");
        auto sequenceElements = (SerializableSequenceElements *) this->permanentFragment->getSerializable("sequenceElements");
        auto tempDir = this->getConfigEntry<string>("tempDir");

        // switchToMemorySaveMode for all mates of all tiles of all lanes
        for (uint16_t lane : lanes)
            for (uint16_t tile : tiles) {
                if(this->tileFragments[lane][tile]->isFragmentSerialized())
                    this->tileFragments[lane][tile]->deserialize();
                for (auto sequenceElement : sequenceElements->getSeqs()) {
                    string key = "mate_" + to_string(sequenceElement.mate);
                    if (sequenceElement.mate == 0 || !this->tileFragments[lane][tile]->hasSerializable(key)) continue;
                    auto currentAlignment = (SerializableAlignment *) this->tileFragments[lane][tile]->getSerializable(key);
                    currentAlignment->enableMemorySaveMode(tempDir);
                }
                this->tileFragments[lane][tile]->serialize();
            }

        this->configMap["memorySaveMode"] = true;
    }
}

extern "C" Hilive *create() {
    return new Hilive;
}

extern "C" void destroy(Hilive *plugin) {
    delete plugin;
}
