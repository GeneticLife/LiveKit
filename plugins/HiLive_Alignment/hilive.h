#ifndef HILIVE_HILIVE_H

#define HILIVE_HILIVE_H

#include "../HiLive_sharedLibraries/headers.h"
#include "../HiLive_sharedLibraries/definitions.h"
#include "../HiLive_sharedLibraries/global_variables.h"
#include "../HiLive_sharedLibraries/kindex.h"
#include "../HiLive_sharedLibraries/alnstream.h"
#include "../HiLive_sharedLibraries/alnout.h"
#include "../HiLive_sharedLibraries/parallel.h"
#include "../HiLive_sharedLibraries/tools_static.h"
#include "../../serializables/SerializableSequenceElements.h"

#include "../../framework/Plugin.h"
#include "../../framework/FrameworkInterface.h"
#include "../../framework/fragments/Fragment.cpp"
#include "../../serializables/SerializableIndex.h"
#include "../../serializables/SerializableAlignment.h"

LIVEKIT_PLUGIN(Hilive) {
private:
    void printConsoleWelcome();

    SerializableSequenceElements *createSequenceElements(vector<pair<int, char>> read_structure);

    // lambda functions for setConfig

    /**
     * @return a function that sets the alignment mode for sequencing [(ALL)(UNIQUE)(BESTN[0-9]+)(ALLBEST)(ANYBEST)AU(N[0-9]+)HB]
     * the parameter of the returned function is an Alignment mode as string.
     */
    std::function<OutputMode (std::string)> setMode();
    std::function<uint16_t ()> setAnchorLength(SerializableIndex *index);

public:

    std::map<uint16_t, BamFileOutDeque *> cycleBfos;

    bool isOutputCycle(int cycle);

    std::shared_ptr<FragmentContainer> runPreprocessing(std::shared_ptr<FragmentContainer> inputFragments) override;

    std::shared_ptr<FragmentContainer> runCycle(std::shared_ptr<FragmentContainer> inputFragments) override;

    LIVEKIT_TILE_CONTEXT(HiliveTileContext) {
    public:
        std::shared_ptr<FragmentContainer> runCycleForTile(std::shared_ptr<FragmentContainer> inputFragments) override;
    };

    std::shared_ptr<FragmentContainer> runFullReadPostprocessing(std::shared_ptr<FragmentContainer> inputFragments) override { return inputFragments; };

    void setConfig() override;

    void finalize() override;

    void switchToMemorySaveMode() override;
};

#endif
