#include "BuildIndex.h"

using namespace std;

void BuildIndex::init() {

}

std::shared_ptr<FragmentContainer> BuildIndex::runPreprocessing(std::shared_ptr<FragmentContainer> inputFragments) {
    (void) inputFragments; // UNUSED
    
    this->out
        << "-----------------------------------------------------------------------------------------------" << endl
        << "HiLive Index Builder v" << HiLive_VERSION_MAJOR << "." << HiLive_VERSION_MINOR
        << " PLUGIN VERSION - Build Index for Realtime Alignment of Illumina Reads" << endl
        << "-----------------------------------------------------------------------------------------------" << endl;


    SerializableIndex *index;

    if (this->getConfigEntry<bool>("storeIndex")) {
        string filename = this->getConfigEntry<string>("indexName") + "index.fragment";
        if (boost::filesystem::exists( filename )) {
            FILE *serializedFile = fopen(filename.c_str(), "rb");

            unsigned long size;
            fread(&size, sizeof(unsigned long), 1, serializedFile);

            char *serializedSpace = (char *) malloc(size);
            fread(serializedSpace, size, 1, serializedFile);

            this->out << "Loading FM-index from file " << this->getConfigEntry<string>("indexName") << "..." << endl;

            string serializationDirectory = this->getConfigEntry<string>("indexName");
            index = new SerializableIndex(serializedSpace, serializationDirectory, false);

            free(serializedSpace);
            fclose(serializedFile);

            this->out << "Finished loading FM-index" << endl;

        } else {
            this->out << "Building index from Reference " << this->getConfigEntry<string>("fastaName") << "..." << endl;

            index = new SerializableIndex(
                this->getConfigEntry<string>("fastaName"),
                !this->getConfigEntry<bool>("doNotConvertSpaces"),
                this->getConfigEntry<bool>("trimAfterSpace")
            );

            unsigned long size = index->serializableSize();
            char *serializedSpace = (char *) malloc(sizeof(char) * size);

            string serializationDirectory = this->getConfigEntry<string>("indexName");
            index->backup(serializedSpace, serializationDirectory);

            FILE *serializeFile = fopen(filename.c_str(), "wb");
            fwrite(&size, sizeof(unsigned long), 1, serializeFile);
            fwrite(serializedSpace, size, 1, serializeFile);

            free(serializedSpace);
            fclose(serializeFile);

            this->out << "Finished building FM-index" << endl;
        }
    } else {

        this->out << "Building index from Reference " << this->getConfigEntry<string>("fastaName") << "..." << endl;

        index = new SerializableIndex(
            this->getConfigEntry<string>("fastaName"),
            !this->getConfigEntry<bool>("doNotConvertSpaces"),
            this->getConfigEntry<bool>("trimAfterSpace")
        );

        this->out << "Finished building FM-index" << endl;
    }

    auto fragmentContainer = this->framework->createNewFragmentContainer();
    fragmentContainer->add(this->framework->createNewFragment("FMIndex"));
    shared_ptr<Fragment> fmIndexFragment = fragmentContainer->get("FMIndex");
    fmIndexFragment->setSerializable("FMIndex", index);

    return fragmentContainer;
}

void BuildIndex::finalize() {

}

void BuildIndex::setConfig() {
    this->registerConfigEntry<string>("fastaName", "../tutorial/hiv1.fa");
    this->registerConfigEntry<string>("indexName", "../temp/index");
    this->registerConfigEntry<bool>("storeIndex", true);
    this->registerConfigEntry<bool>("doNotConvertSpaces", false);
    this->registerConfigEntry<bool>("trimAfterSpace", false);
}

extern "C" BuildIndex *create() {
    return new BuildIndex;
}

extern "C" void destroy(BuildIndex *plugin) {
    delete plugin;
}

