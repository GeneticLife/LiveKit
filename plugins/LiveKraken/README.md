# LiveKraken Plugin

LiveKraken is an extension of the Kraken taxonomic sequence classification tool for classifying Illumina sequence data as it is being generated.

Supported Sequencers
-------------
Due to differences in the structure and compression of raw sequencing data, LiveKraken currently only supports sequencers producing gz-compressed bcl (not cbcl) files. We are working on the extension of our approach for further devices.
Supported sequencers:
* Miseq
* HiSeq 1500
* HiSeq 2000/2500/3000/4000/X (untested)

Currently not supported:
* NovaSeq
* MiniSeq
* NextSeq500/550
