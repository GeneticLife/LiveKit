#include "liveKraken.h"

#include "../../framework/fragments/Fragment.cpp"
#include "Kraken/krakenutil.hpp"
#include <fstream>

using namespace std;

void LiveKraken::init() {
    this->permanentFragment = this->framework->createNewFragment("LiveKrakenFragment");

    this->permanentFragment->set("totalClassified", 0);
    this->permanentFragment->set("totalSequences", 0);
    this->permanentFragment->set("totalNucleotides", 0);

    this->permanentFragment->set("numberOfProcessedCycles", 0);

    this->permanentFragment->set("loaded_nt", 0);

    this->permanentFragment->set("processedLength", 0);

    this->permanentFragment->set("currentReadCycle", 0);

    // Serializable maps
    this->permanentFragment->setSerializable("lastKmer", new SerializableMap<int, unsigned long>());
    this->permanentFragment->setSerializable("lastAmbig", new SerializableMap<int, unsigned long>());

    this->permanentFragment->setSerializable("taxa", new SerializableMap<unsigned long, SerializableVector<unsigned long>>());
    this->permanentFragment->setSerializable("ambigList", new SerializableMap<unsigned long, SerializableVector<unsigned long>>());

    this->permanentFragment->setSerializable("hitCounts", new SerializableUnorderedMap<unsigned long, SerializableUnorderedMap<unsigned long, unsigned long>>());
}

shared_ptr<FragmentContainer> LiveKraken::runPreprocessing(shared_ptr<FragmentContainer> inputFragments) {

    (void) inputFragments; //UNUSED

    auto *parentMap = new SerializableUnorderedMap<unsigned long, unsigned long>();

    unordered_map<uint32_t, string> taxLevelMap;
    auto nodesFilename = this->getConfigEntry<string>("nodesFilename");
    if (!nodesFilename.empty()) {
        parentMap = kraken::buildParentMap(nodesFilename, taxLevelMap);
    }

    this->permanentFragment->setSerializable("parentMap", parentMap);

    return this->framework->createNewFragmentContainer();
}

std::shared_ptr<FragmentContainer> LiveKraken::runCycle(std::shared_ptr<FragmentContainer> inputFragments) {

    auto stepSize = this->getConfigEntry<int>("stepSize");
    if (stepSize < 1) {
        cerr << "LiveKraken stepSize has to be greater 0" << endl;
        return inputFragments;
    }
    auto startCycle = this->getConfigEntry<int>("startCycle");
    this->permanentFragment->set("currentReadCycle", this->permanentFragment->get<int>("currentReadCycle") + 1);
    auto currentReadCycle = this->permanentFragment->get<int>("currentReadCycle");

    bool isStepCycle = (currentReadCycle - startCycle) % stepSize == 0;
    bool isGreaterThanStartCycle = currentReadCycle >= startCycle;
    bool isEqualStartCycle = currentReadCycle == startCycle;

    if (this->framework->isBarcodeCycle()) {
        this->permanentFragment->set("processedLength", this->permanentFragment->get<int>("processedLength") + 1);
        this->permanentFragment->set("numberOfProcessedCycles", 0);
        this->permanentFragment->set("currentReadCycle", 0);
        return inputFragments;
    }

    if ((isStepCycle && isGreaterThanStartCycle) || isEqualStartCycle || this->islastCycleOfRead()) {

        timeval tv1, tv2;
        gettimeofday(&tv1, nullptr);

        cout << "------------------------------------------------------------------\nLiveKraken Plugin" << endl;
        cout << "------------------------------------------------------------------ \n";

        // BEGIN DATABASE ------------------------------------------------------------

        if (this->getConfigEntry<bool>("populateMemory"))
            cout << "Loading database... ";

        kraken::QuickFile databaseFile;
        databaseFile.open_file(this->getConfigEntry<string>("databaseFilename"));
        if (this->getConfigEntry<bool>("populateMemory"))
            databaseFile.load_file();
        this->database = new kraken::KrakenDB(databaseFile.ptr());
        kraken::KmerScanner::set_k(this->database->get_k());

        kraken::QuickFile indexFile;
        indexFile.open_file(this->getConfigEntry<string>("indexFilename"));
        if (this->getConfigEntry<bool>("populateMemory"))
            indexFile.load_file();

        kraken::KrakenDBIndex databaseIndex(indexFile.ptr());
        this->database->set_index(&databaseIndex);

        if (this->getConfigEntry<bool>("populateMemory"))
            cout << "complete." << endl;


        // END DATABASE ------------------------------------------------------------
        this->prepareBclClassification();

        gettimeofday(&tv2, nullptr);
        this->reportStats(tv1, tv2);

        this->permanentFragment->set("totalClassified", 0);
        this->permanentFragment->set("totalSequences", 0);
        this->permanentFragment->set("totalNucleotides", 0);

        delete this->database;
    }

    return this->framework->createNewFragmentContainer();
}

shared_ptr<FragmentContainer> LiveKraken::runFullReadPostprocessing(shared_ptr<FragmentContainer> inputFragments) {
    (void) inputFragments; // UNUSED

    unordered_map<uint32_t, string> taxLevelMap;

    auto fastQFiles = this->getConfigEntry<vector<string>>("fastQFiles");
    auto fastAFiles = this->getConfigEntry<vector<string>>("fastAFiles");

    kraken::DNASequenceReader *reader;

    timeval tv1, tv2;

    if (!fastQFiles.empty() || !fastAFiles.empty()) {

        cout << "------------------------------------------------------------------\nLiveKraken Plugin" << endl;
        cout << "------------------------------------------------------------------ \n";

        // BEGIN: Database

        if (this->getConfigEntry<bool>("populateMemory"))
            cout << "Loading database... ";

        kraken::QuickFile databaseFile;
        databaseFile.open_file(this->getConfigEntry<string>("databaseFilename"));
        if (this->getConfigEntry<bool>("populateMemory"))
            databaseFile.load_file();
        this->database = new kraken::KrakenDB(databaseFile.ptr());
        kraken::KmerScanner::set_k(this->database->get_k());

        kraken::QuickFile indexFile;
        indexFile.open_file(this->getConfigEntry<string>("indexFilename"));
        if (this->getConfigEntry<bool>("populateMemory"))
            indexFile.load_file();
        kraken::KrakenDBIndex databaseIndex(indexFile.ptr());
        this->database->set_index(&databaseIndex);

        if (this->getConfigEntry<bool>("populateMemory"))
            cout << "complete." << endl;

        // END: Database

        if (this->checkFileType(fastQFiles, "fastq")) {
            for (auto file : fastQFiles) {

                gettimeofday(&tv1, nullptr);

                reader = new kraken::FastqReader(file, this->getFastQLength(file));

                this->permanentFragment->set("fileInput", "fastq");

                this->processFastFiles(reader, "fastq");

                delete reader;

                gettimeofday(&tv2, nullptr);
                this->reportStats(tv1, tv2);

                this->permanentFragment->set<int>("totalClassified", 0);
                this->permanentFragment->set<int>("totalSequences", 0);
                this->permanentFragment->set<int>("totalNucleotides", 0);
            }
        } else {
            cerr << "Not all files are of type fastq" << endl;
        }

        if (this->checkFileType(fastAFiles, "fasta")) {
            for (auto file : fastAFiles) {
                gettimeofday(&tv1, nullptr);
                reader = new kraken::FastaReader(file);
                this->permanentFragment->set("fileInput", "fasta");
                this->processFastFiles(reader, "fasta");
                delete reader;
                gettimeofday(&tv2, nullptr);
                this->reportStats(tv1, tv2);

                this->permanentFragment->set<int>("totalClassified", 0);
                this->permanentFragment->set<int>("totalSequences", 0);
                this->permanentFragment->set<int>("totalNucleotides", 0);
            }
        } else {
            cerr << "Not all files are of type fasta" << endl;
        }
    }


    return this->framework->createNewFragmentContainer();
}

void LiveKraken::prepareBclClassification() {

    kraken::BCLReader *reader;
    reader = new kraken::BCLReader();

    ostringstream kraken_output_ss, classified_output_ss, unclassified_output_ss;

    kraken::DNASequence dna;

    kraken::WorkUnit work_unit;

    vector<kraken::SequenceStruct> sequencesStruct;
    auto sequence = kraken::SequenceStruct();
    uint64_t sequenceCount = 0;


    auto lanes = this->getConfigEntry<vector<uint16_t>>("targetLanes");
    auto tiles = this->getConfigEntry<vector<uint16_t>>("targetTiles");

    if (lanes[0] == 0) {
        lanes = this->getConfigEntry<vector<uint16_t>>("lanes");
    }
    if (tiles[0] == 0) {
        tiles = this->getConfigEntry<vector<uint16_t>>("tiles");
    }

    for (auto lane : lanes) {
        for (auto tile : tiles) {
            for (int i = 0; i < (int) this->framework->getNumSequences(lane, tile); i++) {
                Read sequencesRead = this->framework->getRead(lane, tile, i, this->framework->getCurrentReadId());

                sequence.sequences.push_back(this->getBaseString(sequencesRead));
                sequence.tile = tile;
                sequence.lane = lane;
                sequenceCount++;
            }
            sequencesStruct.push_back(sequence);
            sequence = kraken::SequenceStruct();
        }

    }

    auto tile = kraken::TileInfo();

    tile.first_cycle = this->permanentFragment->get<int>("numberOfProcessedCycles");
    tile.last_cycle = this->permanentFragment->get<int>("currentReadCycle");

    work_unit.seqs.clear();
    work_unit.runContainer = nullptr;
    size_t total_nt = 0;
    total_nt = reader->nextWorkunit(sequencesStruct, work_unit, tile);

    this->permanentFragment->set("numberOfProcessedCycles", tile.last_cycle);

    if (total_nt == 0)
        return;

    kraken_output_ss.str("");
    classified_output_ss.str("");
    unclassified_output_ss.str("");

    bool printClassified = this->getConfigEntry<bool>("printClassified");
    bool printUnclassified = this->getConfigEntry<bool>("printUnclassified");
    bool printKraken = this->getConfigEntry<bool>("printKraken");

    bool outputAll = this->getConfigEntry<bool>("outputAll");

    bool first = this->framework->getCurrentReadCycle() <= this->getConfigEntry<int>("startCycle");

    for (size_t j = 0; j < sequenceCount; j++) {
        this->classifySequence(work_unit.seqs[j], kraken_output_ss, classified_output_ss, unclassified_output_ss,
                         true, "bcl", j, printClassified, printUnclassified, printKraken, outputAll, first);
    }

    // signal to the reader how many tile sequences were processed
    if (work_unit.runContainer) {
        work_unit.runContainer->increment_count(work_unit.seqs.size());
    }

    delete reader;

    int numberOfProcessedNucleotides = sequenceCount * (tile.last_cycle - tile.first_cycle);
    this->outputResults(kraken_output_ss, classified_output_ss, unclassified_output_ss,
                        sequenceCount, numberOfProcessedNucleotides, "bcl", outputAll);
}

void LiveKraken::processFastFiles(kraken::DNASequenceReader *reader, string fileType) {
    kraken::DNASequence dna;

    kraken::WorkUnit work_unit;
    ostringstream kraken_output_ss, classified_output_ss, unclassified_output_ss;

    while (reader->is_valid()) {
        work_unit.seqs.clear();
        work_unit.runContainer = nullptr;
        size_t total_nt = 0;

        total_nt = reader->next_workunit(work_unit);

        if (total_nt == 0)
            break;

        kraken_output_ss.str("");
        classified_output_ss.str("");
        unclassified_output_ss.str("");

        // sequence count for this batch of reads
        uint64_t seq_count = 0;
        seq_count = work_unit.seqs.size();

        bool printClassified = this->getConfigEntry<bool>("printClassified");
        bool printUnclassified = this->getConfigEntry<bool>("printUnclassified");
        bool printKraken = this->getConfigEntry<bool>("printKraken");

        bool outputAll = this->getConfigEntry<bool>("outputAll");

        for (size_t j = 0; j < work_unit.seqs.size(); j++) {
            this->classifySequence(work_unit.seqs[j], kraken_output_ss, classified_output_ss, unclassified_output_ss,
                                   false, fileType, j, printClassified, printUnclassified, printKraken, outputAll, false);
        }

        // signal to the reader how many tile sequences were processed
        if (work_unit.runContainer) {
            work_unit.runContainer->increment_count(work_unit.seqs.size());
        }

        this->outputResults(kraken_output_ss, classified_output_ss, unclassified_output_ss,
                            seq_count, total_nt, fileType, true);
    }
}

void
LiveKraken::classifySequence(kraken::DNASequence &dna, ostringstream &koss, ostringstream &coss, ostringstream &uoss,
                             bool isBcl, string fileType, int sequenceNumber, bool printClassified,
                             bool printUnclassified, bool printKraken, bool outputAll, bool first) {
    uint32_t call = 0;

    // we can finalize if: Full fasta or fastq sequence given and called
    // or if the BCL file was sufficiently accurately classified
    // false at the moment since we do not stop classifying early
    // bool tax_call = check_tax_level(call, "genus", Parent_map, taxLevel_map);

    // classify the sequence we're given
    if (fileType == "bcl" && outputAll) {
        this->classifyPartialBclSequence(dna, call, sequenceNumber, first);

        if (sequenceNumber == 0)
            this->permanentFragment->set("processedLength", this->permanentFragment->get<int>("processedLength") + dna.readInfo->processed_len);
        this->classifyBclFinalize(dna, sequenceNumber, koss, coss, uoss, call, isBcl, printClassified, printUnclassified, printKraken);
    }
    else if (fileType != "bcl") {
        this->classifyPartialSequence(dna, call);
        this->classifyFinalize(dna, koss, coss, uoss, call, fileType);
    }
}

void LiveKraken::classifyPartialBclSequence(kraken::DNASequence &bclSequence, uint32_t &call, int sequenceNumber,
                                            bool first) {
    uint64_t *kmer_ptr;
    uint32_t taxon = 0;

    uint64_t current_bin_key;
    int64_t current_min_pos = 1;
    int64_t current_max_pos = 0;

    // check !first because k-mer size requirement may not hold for incremental steps

    if (!first || bclSequence.seq.size() >= this->database->get_k()) {
        auto lastKmer = (SerializableMap<int, unsigned long>*) this->permanentFragment->getSerializable("lastKmer");
        auto lastAmbig = (SerializableMap<int, unsigned long>*) this->permanentFragment->getSerializable("lastAmbig");
        kraken::KmerScanner scanner(bclSequence.seq, 0, ~0, !first,
                                    (*lastAmbig)[sequenceNumber],
                                    (*lastKmer)[sequenceNumber]);
        this->permanentFragment->set("loaded_nt", scanner.getLoaded_nt());
        scanner.setLoaded_nt(this->permanentFragment->get<int>("loaded_nt"));

        auto hitCounts = (SerializableUnorderedMap<unsigned long, SerializableUnorderedMap<unsigned long, unsigned long>>*) this->permanentFragment->getSerializable("hitCounts");

        while ((kmer_ptr = scanner.next_kmer()) != nullptr) {
            taxon = 0;
            auto ambigList = (SerializableMap<unsigned long, SerializableVector<unsigned long>>*) this->permanentFragment->getSerializable("ambigList");
            if (scanner.ambig_kmer()) {
                (*ambigList)[sequenceNumber].push_back(1);
            } else {
                (*ambigList)[sequenceNumber].push_back(0);
                uint32_t *val_ptr = this->database->kmer_query(
                        this->database->canonical_representation(*kmer_ptr),
                        &current_bin_key,
                        &current_min_pos, &current_max_pos
                );

                taxon = val_ptr ? *val_ptr : 0;

                if (taxon) {
                    bclSequence.readInfo->hit_counts[taxon]++;
                    if (this->getConfigEntry<bool>("quickMode") &&
                        ++bclSequence.readInfo->hits >= this->getConfigEntry<uint32_t>("minimumHitCount"))
                        break;
                }
            }

            bclSequence.readInfo->last_kmer = *kmer_ptr;

            (*hitCounts)[sequenceNumber][taxon]++;

            (*lastKmer)[sequenceNumber] = *kmer_ptr;
            this->permanentFragment->set("lastKmer", lastKmer);

            (*lastAmbig)[sequenceNumber] = scanner.get_ambig();
            this->permanentFragment->set("lastAmbig", lastAmbig);


            auto taxa = (SerializableMap<unsigned long, SerializableVector<unsigned long>>*) this->permanentFragment->getSerializable("taxa");
            (*taxa)[sequenceNumber].push_back(taxon);
            this->permanentFragment->set("taxa", taxa);
        }

        if (this->getConfigEntry<bool>("quickMode"))
            call = bclSequence.readInfo->hits >= this->getConfigEntry<uint32_t>("minimumHitCount") ? taxon : 0;
        else {
            auto parentMap = (SerializableUnorderedMap<unsigned long, unsigned long>*) this->permanentFragment->getSerializable("parentMap");
            call = kraken::resolveTree((*hitCounts)[sequenceNumber], *parentMap);
        }

        bclSequence.readInfo->first = false;
        this->permanentFragment->set("hitCounts", hitCounts);
    }
}

void LiveKraken::classifyPartialSequence(kraken::DNASequence &dna, uint32_t &call) {
    uint64_t *kmer_ptr;
    uint32_t taxon = 0;

    uint64_t current_bin_key;
    int64_t current_min_pos = 1;
    int64_t current_max_pos = 0;

    // check !first because k-mer size requirement may not hold for incremental steps
    if (!dna.readInfo->first || dna.seq.size() >= this->database->get_k()) {
        kraken::KmerScanner scanner(dna.seq, 0, ~0, !dna.readInfo->first, dna.readInfo->last_ambig,
                                    dna.readInfo->last_kmer);

        while ((kmer_ptr = scanner.next_kmer()) != nullptr) {
            taxon = 0;
            if (scanner.ambig_kmer()) {
                dna.readInfo->ambig_list.push_back(1);
            } else {
                dna.readInfo->ambig_list.push_back(0);
                uint32_t *val_ptr = this->database->kmer_query(
                        this->database->canonical_representation(*kmer_ptr),
                        &current_bin_key,
                        &current_min_pos, &current_max_pos
                );

                taxon = val_ptr ? *val_ptr : 0;

                if (taxon) {
                    dna.readInfo->hit_counts[taxon]++;
                    if (this->getConfigEntry<bool>("quickMode") &&
                        ++dna.readInfo->hits >= this->getConfigEntry<uint32_t>("minimumHitCount"))
                        break;
                }
            }

            dna.readInfo->last_kmer = *kmer_ptr;
            dna.readInfo->last_ambig = scanner.get_ambig();
            dna.readInfo->taxa.push_back(taxon);
        }

        if (this->getConfigEntry<bool>("quickMode"))
            call = dna.readInfo->hits >= this->getConfigEntry<uint32_t>("minimumHitCount") ? taxon : 0;
        else {
            auto parentMap = (SerializableUnorderedMap<unsigned long, unsigned long>*) this->permanentFragment->getSerializable("parentMap");
            call = kraken::resolveTree(dna.readInfo->hit_counts, *parentMap);
        }

        dna.readInfo->first = false;
    }
}

void LiveKraken::classifyBclFinalize(kraken::DNASequence &dna, int sequenceNumber, ostringstream &koss,
                                     ostringstream &coss, ostringstream &uoss, const uint32_t call, bool record_stats,
                                     bool printClassified, bool printUnclassified, bool printKraken) {

    if (call && record_stats) {
        // Write totalClassified in a Fragment
        this->permanentFragment->set("totalClassified", this->permanentFragment->get<int>("totalClassified") + 1);
    }

    if (printUnclassified || printClassified) {
        ostringstream *oss_ptr = call ? &coss : &uoss;
        bool print = call ? printClassified : printUnclassified;
        if (print) {
            (*oss_ptr) << ">" << this->getConfigEntry<string>("run-id") << "\t" << dna.id << endl << dna.seq << endl;
        }
    }

    if (!printKraken)
        return;

    if (call) {
        koss << "C\t";
    } else {
        if (this->getConfigEntry<bool>("onlyClassifiedKrakenOutput"))
            return;

        koss << "U\t";
    }

    int processedLength = this->permanentFragment->get<int>("processedLength");
    koss << dna.id << "\t" << call << "\t" << processedLength << "\t";

    auto taxa = (SerializableMap<unsigned long, SerializableVector<unsigned long>>*) this->permanentFragment->getSerializable("taxa");
    if (this->getConfigEntry<bool>("quickMode")) {
        koss << "Q:" << dna.readInfo->hits;
    } else {
        if ((*taxa)[sequenceNumber].empty())
            koss << "0:0";
        else
            koss << this->hitlistStringBcl(sequenceNumber);
    }

    koss << endl;
}

void LiveKraken::classifyFinalize(const kraken::DNASequence &dna, ostringstream &koss, ostringstream &coss,
                                  ostringstream &uoss,
                                  const uint32_t call, string fileType) {

    if (call) {
        // Write totalClassified in a Fragment
        this->permanentFragment->set("totalClassified", this->permanentFragment->get<int>("totalClassified") + 1);
    }

    bool printClassified = this->getConfigEntry<bool>("printClassified");
    bool printUnclassified = this->getConfigEntry<bool>("printUnclassified");
    bool printKraken = this->getConfigEntry<bool>("printKraken");

    if (printUnclassified || printClassified) {
        ostringstream *oss_ptr = call ? &coss : &uoss;
        bool print = call ? printClassified : printUnclassified;
        if (print) {
            if (fileType == "fastq") {
                (*oss_ptr) << "@" << dna.header_line << endl
                           << dna.seq << endl
                           << "+" << endl
                           << dna.quals << endl;
            } else {
                (*oss_ptr) << ">" << dna.header_line << endl
                           << dna.seq << endl;
            }
        }
    }

    if (!printKraken)
        return;

    if (call) {
        koss << "C\t";
    } else {
        if (this->getConfigEntry<bool>("onlyClassifiedKrakenOutput"))
            return;

        koss << "U\t";
    }

    koss << dna.id << "\t" << call << "\t" << dna.readInfo->processed_len << "\t";

    if (this->getConfigEntry<bool>("quickMode")) {
        koss << "Q:" << dna.readInfo->hits;
    } else {
        if (dna.readInfo->taxa.empty())
            koss << "0:0";
        else
            koss << this->hitlistString(dna.readInfo->taxa, dna.readInfo->ambig_list);
    }

    koss << endl;
}

string LiveKraken::hitlistStringBcl(int sequenceNumber) {

    int64_t last_code;
    int code_count = 1;
    ostringstream hitlist;

    auto taxa = (SerializableMap<unsigned long, SerializableVector<unsigned long>>*) this->permanentFragment->getSerializable("taxa");
    auto ambigList = (SerializableMap<unsigned long, SerializableVector<unsigned long>>*) this->permanentFragment->getSerializable("ambigList");
    if ((*ambigList)[sequenceNumber][0]) { last_code = -1; }
    else { last_code = (*taxa)[sequenceNumber][0]; }

    for (size_t i = 1; i < (*taxa)[sequenceNumber].size(); i++) {
        int64_t code;
        if ((*ambigList)[sequenceNumber][i]) { code = -1; }
        else { code = (*taxa)[sequenceNumber][i]; }

        if (code == last_code) {
            code_count++;
        } else {
            if (last_code >= 0) {
                hitlist << last_code << ":" << code_count << " ";
            } else {
                hitlist << "A:" << code_count << " ";
            }
            code_count = 1;
            last_code = code;
        }
    }
    if (last_code >= 0) {
        hitlist << last_code << ":" << code_count;
    } else {
        hitlist << "A:" << code_count;
    }
    return hitlist.str();
}

string LiveKraken::hitlistString(vector<uint32_t> &taxaVector, vector<uint8_t> &ambig) {

    int64_t last_code;
    int code_count = 1;
    ostringstream hitlist;

    if (ambig[0]) { last_code = -1; }
    else { last_code = taxaVector[0]; }

    auto taxa = (SerializableMap<unsigned long, SerializableVector<unsigned long>>*) this->permanentFragment->getSerializable("taxa");

    for (size_t i = 1; i < (*taxa).size(); i++) {
        int64_t code;
        if (ambig[i]) { code = -1; }
        else { code = taxaVector[i]; }

        if (code == last_code) {
            code_count++;
        } else {
            if (last_code >= 0) {
                hitlist << last_code << ":" << code_count << " ";
            } else {
                hitlist << "A:" << code_count << " ";
            }
            code_count = 1;
            last_code = code;
        }
    }
    if (last_code >= 0) {
        hitlist << last_code << ":" << code_count;
    } else {
        hitlist << "A:" << code_count;
    }
    return hitlist.str();
}

/**
 * A workUnit is a struct that has as a member a RunInfoContainer.
 * This RunInfo Container is passed to this method.
 * @param runInfo holds information about the current Run
 * @param 3 stringstreams that contains the information
 * @param sequenceCount and totalNucleotides are needed for the console output
 */
void LiveKraken::outputResults(ostringstream &krakenOutputSS,
                               ostringstream &classifiedOutputSS, ostringstream &unclassifiedOutputSS,
                               uint64_t sequenceCount, size_t totalNucleotides, const string &fileType, bool outputAll) {
    if (outputAll) {

        // Filenames for the output
        string classifiedOutputFileName = this->getConfigEntry<string>("classifiedOutputFileName");
        string unclassifiedOutputFileName = this->getConfigEntry<string>("unclassifiedOutputFileName");
        string krakenOutputFileName = this->getConfigEntry<string>("krakenOutputFileName");

        string prefix = "./out/LiveKraken/";
        // Check if prefix directory exists and create it otherwise
        boost::filesystem::path dir(prefix);
        if (!boost::filesystem::is_directory(dir) || !boost::filesystem::exists(dir)) {
            boost::filesystem::create_directory(dir);
        }

        if (fileType == "fastq") {
            prefix += "FastQ/";
        } else if (fileType == "fasta") {
            prefix += "FastA/";
        } else if (fileType == "bcl") {
            prefix += to_string(this->framework->getCurrentCycle()) + "/";
        }

        // Check if prefix directory exists and create it otherwise
        boost::filesystem::path dir2(prefix);
        if (!boost::filesystem::is_directory(dir2) || !boost::filesystem::exists(dir2)) {
            boost::filesystem::create_directory(dir2);
        }

        auto classifiedOutput = new std::ofstream((prefix + classifiedOutputFileName).c_str());
        auto unclassifiedOutput = new std::ofstream((prefix + unclassifiedOutputFileName).c_str());
        auto krakenOutput = new std::ofstream((prefix + krakenOutputFileName).c_str());

        // This is needed, because the ostream needs more input than just some
        // fastq entries to show the content in the file
        for (int i = 0; i < 100; i++) {
            classifiedOutputSS << "                   ";
            unclassifiedOutputSS << "                   ";
        }

        // Fill the ostream with the content
        if (this->getConfigEntry<bool>("printKraken"))
            (*krakenOutput) << krakenOutputSS.str();
        if (this->getConfigEntry<bool>("printClassified"))
            (*classifiedOutput) << classifiedOutputSS.str();
        if (this->getConfigEntry<bool>("printUnclassified"))
            (*unclassifiedOutput) << unclassifiedOutputSS.str();

        if (sequenceCount == 1) {
            cout << "Processed " << sequenceCount << " sequence (" << totalNucleotides << " bp) ..." << endl;
        } else {
            cout << "Processed " << sequenceCount << " sequences (" << totalNucleotides << " bp) ..." << endl;
        }

        this->permanentFragment->set("totalSequences", sequenceCount);
        this->permanentFragment->set("totalNucleotides", totalNucleotides);
    }
}

void LiveKraken::reportStats(struct timeval time1, struct timeval time2) {
    time2.tv_usec -= time1.tv_usec;
    time2.tv_sec -= time1.tv_sec;
    if (time2.tv_usec < 0) {
        time2.tv_sec--;
        time2.tv_usec += 1000000;
    }
    double seconds = time2.tv_usec;
    seconds /= 1e6;
    seconds += time2.tv_sec;

    auto totalClassified = this->permanentFragment->get<int>("totalClassified");
    auto totalSequences = this->permanentFragment->get<int>("totalSequences");
    auto totalNucleotides = this->permanentFragment->get<int>("totalNucleotides");

    cout << "\r";
    printf(
            "%llu sequences (%.2f Mbp) processed in %.3fs (%.1f Kseq/m, %.2f Mbp/m).\n",
            (unsigned long long) totalSequences, totalNucleotides / 1.0e6, seconds,
            totalSequences / 1.0e3 / (seconds / 60),
            totalNucleotides / 1.0e6 / (seconds / 60));
    printf("%llu sequences classified (%.2f%%)\n", (unsigned long long) totalClassified,
           totalClassified * 100.0 / totalSequences);
    printf("%llu sequences unclassified (%.2f%%)\n", (unsigned long long) (totalSequences - totalClassified),
           (totalSequences - totalClassified) * 100.0 / totalSequences);
    cout << endl;
}

bool LiveKraken::checkFileType(vector<string> files, string type) {

    for (auto file : files) {
        if (!(file.substr(file.find_last_of(".") + 1) == type)) {
            return false;
        }
    }
    return true;
}

string LiveKraken::getBaseString(Read sequence) {
    string result;

    for (size_t i = 0; i < sequence.size(); i++) {
        bitset<8> binary(sequence.at((int) i));

        int base = this->getBaseFromByte(binary);
        int quality = this->getQualityFromByte(binary);
        if (quality == 0) {
            result.push_back('N');
            continue;
        }

        switch (base) {
            case (0):
                result.push_back('A');
                break;
            case (1):
                result.push_back('C');
                break;
            case (2):
                result.push_back('G');
                break;
            case (3):
                result.push_back('T');
                break;
            default:
                break;
        }
    }
    return result;
}

int LiveKraken::getBaseFromByte(bitset<8> byte) {
    return (int) byte.to_ulong() & 0b11;
}

int LiveKraken::getQualityFromByte(bitset<8> byte) {
    return (int) byte.to_ulong() & 0b11111100;
}

int LiveKraken::getFastQLength(string filename) {
    string line;

    std::ifstream file(filename);

    while (getline(file, line)) {
        if (line[0] == '@') {
            getline(file, line);
            break;
        }
    }

    return line.size();
}

bool LiveKraken::islastCycleOfRead() {
    int currentCycle = this->permanentFragment->get<int>("currentReadCycle");

    auto reads = this->getConfigEntry<vector<pair<int, char>>>("reads");

    for (auto read : reads) {
        if (currentCycle == read.first && read.second == 'R') {
            return true;
        }
    }
    return false;
}

void LiveKraken::setConfig() {
    this->registerConfigEntry<string>("databaseFilename", ""); // d
    this->registerConfigEntry<string>("indexFilename", ""); // i
    this->registerConfigEntry<string>("nodesFilename", ""); // n
    this->registerConfigEntry<bool>("quickMode", false); // q

    this->registerConfigEntry<bool>("printKraken", true);
    this->registerConfigEntry<bool>("printClassified", true); // C
    this->registerConfigEntry<bool>("printUnclassified", true); // U

    this->registerConfigEntry<string>("classifiedOutputFileName", "classified"); // C
    this->registerConfigEntry<string>("unclassifiedOutputFileName", "unclassified"); // U
    this->registerConfigEntry<string>("krakenOutputFileName", "krakenOutput"); // o

    this->registerConfigEntry<bool>("populateMemory", false); // M
    this->registerConfigEntry<bool>("onlyClassifiedKrakenOutput", false); // c
    this->registerConfigEntry<uint32_t>("minimumHitCount", 1); // m

    // FileInput is replaced by FastA and FastQ File input because we process bcl files anyway
    this->registerConfigEntry<vector<string>, string>("fastQFiles", "", Configurable::toVector<string>(','));
    this->registerConfigEntry<vector<string>, string>("fastAFiles", "", Configurable::toVector<string>(',')); // f,b,a

    // BCLParams
    this->registerConfigEntry<int>("startCycle", 32); // s
    this->registerConfigEntry<int>("stepSize", 0); // k
    this->registerConfigEntry<vector<uint16_t>, string>("targetTiles", "0",
                                                   Configurable::toVector<uint16_t>(',')); // x
    this->registerConfigEntry<vector<uint16_t>, string>("targetLanes", "0", Configurable::toVector<uint16_t>(',')); // y

    this->registerConfigEntry<bool>("outputAll", true);
    // END: command line parameter
}

extern "C" LiveKraken *create() {
    return new LiveKraken;
}

extern "C" void destroy(LiveKraken *plugin) {
    delete plugin;
}
