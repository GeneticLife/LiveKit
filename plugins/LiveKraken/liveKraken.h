#ifndef LIVEKIT_LIVEKRAKEN_H
#define LIVEKIT_LIVEKRAKEN_H

#include "../../framework/Plugin.h"
#include "../../framework/FrameworkInterface.h"
#include "../../serializables/SerializableMap.h"
#include "../../serializables/SerializableUnorderedMap.h"
#include "Kraken/krakendb.hpp"
#include "Kraken/seqreader.hpp"
#include "Kraken/kraken_headers.hpp"
#include "Kraken/quickfile.hpp"

#include<unordered_map>
#include <bitset>

/**
 * Plugin that classify the sequences based on it's organism and the database that was entered.
 */
LIVEKIT_PLUGIN(LiveKraken) {
private:

    kraken::KrakenDB *database;

    /**
     * Preparation of the classification. This method create the BCL-Reader, prepare the DNA-Sequences and prepare the output stringStreams.
     */
    void prepareBclClassification();

    /**
     * Process the optional fastQ and fastA-files
     * @param reader The fastq or fasta-reader that contains the work_unit and the correntBufferQueue
     * @param fileType Which file is currently used (fasta, fastq)
     */
    void processFastFiles(kraken::DNASequenceReader *reader, std::string fileType);

    /**
     * Classify either fastq, fasta and bcl-files.
     * @param dna Current sequence
     * @param koss stringStream for the krakenOutput
     * @param coss stringStream for the classifiedOutput
     * @param uoss stringStream for the unclassifiedOutput
     * @param isBcl Flag that indicates if the current DNA-Sequence came from a bcl-File
     * @param fileType Type of the currently used file
     * @param sequenceNumber Current sequence-number
     * @param printClassified Flag that indicates if the classified sequences should be printed in a file
     * @param printUnclassified Flag that indicates if the unclassified sequences should be printed in a file
     * @param printKraken Flag that indicates if the krakenOutput should be printed in a file
     * @param outputAll Flag that indicates if there should be output at all
     * @param first Flag that indicates if the current sequence is the first sequence
     */
    void classifySequence(kraken::DNASequence &dna, std::ostringstream &koss, std::ostringstream &coss,
                          std::ostringstream &uoss, bool isBcl, std::string fileType, int sequenceNumber,
                          bool printClassified, bool printUnclassified, bool printKraken, bool outputAll, bool first);

    /**
     * Get the next k-mer and classify it (for fastq and fastq files). Furthermore additional permanentFragments are set (like taxa and lastAmbig)
     * @param dna Current DNA-Sequence
     * @param call Contains the classifiy-information (if it is classified or not)
     */
    void classifyPartialSequence(kraken::DNASequence &dna, uint32_t &call);

    /**
     * Get the next k-mer and classify it (for bcl files). Furthermore additional permanentFragments are set (like taxa and lastAmbig)
     * @param bclSequence Current DNA-Sequence
     * @param call Contains the classifiy-information (if it is classified or not)
     * @param sequenceNumber Sequence number of the current read
     * @param first Flag that indicates if this sequence is the first sequence
     */
    void classifyPartialBclSequence(kraken::DNASequence &bclSequence, uint32_t &call, int sequenceNumber,
                                    bool first);

    /**
     * Create the output for the bcl classification
     * @param dna Current DNA-Sequence
     * @param sequenceNumber Number of the current DNA-Sequence
     * @param koss stringStream for the krakenOutput
     * @param coss stringStream for the classifiedOutput
     * @param uoss stringStream for the unclassifiedOutput
     * @param call Contains the classifiy-information (if it is classified or not)
     * @param record_stats Flag that indicates if statistics should be showed or not
     * @param printClassified Flag that indicates if the classified sequences should be printed in a file
     * @param printUnclassified Flag that indicates if the unclassified sequences should be printed in a file
     * @param printKraken Flag that indicates if the krakenOutput should be printed in a file
     */
    void classifyBclFinalize(kraken::DNASequence &dna, int sequenceNumber,
                             std::ostringstream &koss,
                             std::ostringstream &coss, std::ostringstream &uoss, const uint32_t call,
                             bool record_stats, bool printClassified, bool printUnclassified, bool printKraken);

    /**
     * Create the output for fastq and fasta files
     * @param dna Current DNA-Sequence
     * @param koss stringStream for the krakenOutput
     * @param coss stringStream for the classifiedOutput
     * @param uoss stringStream for the unclassifiedOutput
     * @param call Contains the classifiy-information (if it is classified or not)
     * @param fileType String that contains either "fastq" or "fasta"
     */
    void classifyFinalize(const kraken::DNASequence &dna, std::ostringstream &koss, std::ostringstream &coss,
                          std::ostringstream &uoss,
                          uint32_t call, std::string fileType);

    /**
     * Calculates the hitList for every bcl sequence
     * @param sequenceNumber Number of the current sequence
     * @return Return the hitList string
     */
    std::string hitlistStringBcl(int sequenceNumber);

    /**
     * Calculates the hitList for every fastq and fasta sequence
     * @param sequenceNumber Number of the current sequence
     * @return Return the hitList string
     */
    std::string hitlistString(std::vector<uint32_t> &taxaVector, std::vector<uint8_t> &ambig);

    /**
     * Write the stringStreams in files.
     * @param krakenOutputSS StringStream for the krakenOutput
     * @param classifiedOutputSS StringStream for the classified DNA-Sequences
     * @param unclassifiedOutputSS StringStreams for the unclassified DNA-Sequences
     * @param sequenceCount Total number of sequences
     * @param totalNucleotides Total number of nucleotides of all sequences
     * @param fileType Contains the current fileType (either fastq, fasta or bcl)
     * @param outputAll Flag that indicates if all stringStreams should be printed or not
     */
    void outputResults(std::ostringstream &krakenOutputSS,
                       std::ostringstream &classifiedOutputSS, std::ostringstream &unclassifiedOutputSS,
                       uint64_t sequenceCount, size_t totalNucleotides, const std::string &fileType, bool outputAll);

    /**
     * Print the statistics
     * @param time1 timeStamp before the sequences where classified
     * @param time2 timeStamp after the sequences where classified
     */
    void reportStats(struct timeval time1, struct timeval time2);

    /**
     * Method that check the fileTyp
     * @param files Input file
     * @param type Type that is compared to the file
     * @return Return true if the fileType is equal to the given type
     */
    bool checkFileType(std::vector<std::string> files, std::string type);

    /**
     * Convert a read to its equivalent string sequence
     * @param sequences Sequences of type Read
     * @return Return the sequences as a string
     */
    std::string getBaseString(Read sequences);

    /**
     * Extract the base of a byte
     * @param byte Current byte
     * @return The Base
     */
    int getBaseFromByte(std::bitset<8> byte);

    /**
     * Extract the quality of a byte
     * @param byte Current byte
     * @return The quality
     */
    int getQualityFromByte(std::bitset<8> byte);

    /**
     * Calculate the number of sequences in a fastq file
     * @param filename Name of the fastq file
     * @return Return the number of sequences in that fastq file
     */
    int getFastQLength(std::string filename);

    /**
     * Determine if the current Cycle is the last cycle of the current read
     * @return True if it is the last cycle of the read
     */
    bool islastCycleOfRead();

#ifdef TEST_DEFINITIONS

    friend class LiveKrakenAccessHelper;

#endif

public:
    void init() override;

    /**
     * Go through all sequences every stepSize-times.
     * @param inputFragments This fragment is not used
     * @return An empty fragment
     */
    std::shared_ptr<FragmentContainer> runCycle(std::shared_ptr<FragmentContainer> inputFragments) override;

    std::shared_ptr<FragmentContainer> runPreprocessing(std::shared_ptr<FragmentContainer> inputFragments) override;

    /**
     * FastQ and FastA files can be classified in this method
     * @param inputFragments This fragment is not used
     * @return An empty fragment
     */
    std::shared_ptr<FragmentContainer>
    runFullReadPostprocessing(std::shared_ptr<FragmentContainer> inputFragments) override;


    void setConfig() override;

    void finalize() override {};
};

#endif //LIVEKIT_LIVEKRAKEN_H
