#include <bitset>
#include <numeric>

#include "QualityControl.h"
#include "../../framework/fragments/Fragment.cpp"
#include "json.hpp"

using json = nlohmann::json;
namespace fs = boost::filesystem;

using namespace std;

/**
 * This function is used to instantiate the Plugin as the framework
 * @returns a pointer to a new instance of the `TestPlugin` class
 */
extern "C" QualityControl *create() {
    return new QualityControl;
}
/**
 * This function deletes the given plugin pointer to free its memory
 */
extern "C" void destroy(QualityControl *plugin) {
    delete plugin;
}

std::shared_ptr<FragmentContainer> QualityControl::runCycle(std::shared_ptr<FragmentContainer> inputFragments) {
    (void) inputFragments;

    unsigned long long overallQuality = 0;
    unsigned long long overallCount = 0;
    unsigned long long allQualities[41] = {0};
    unsigned long long allBaseCounts[5] = {0};

    for (auto &lane : this->getConfigEntry<vector<uint16_t >>("lanes")) {
        unsigned long long laneQuality = 0;
        unsigned long long laneCount = 0;
        unsigned long long laneQualities[41] = {0};
        unsigned long long laneBaseCounts[5] = {0};

        for (auto &tile : this->getConfigEntry<vector<uint16_t >>("tiles")) {
            auto &bcl = this->framework->getMostRecentBcl(lane, tile);
            unsigned long long tileQuality = 0;
            unsigned long long tileCount = 0;

            for (unsigned int i = 0; i < bcl.size(); i++) {
                if (!this->framework->filterBasecall(lane, tile, i)) {
                    auto basecall = (unsigned char) bcl[i];
                    unsigned char currentBase = basecall & 0b11u;
                    unsigned char currentQuality = basecall >> 2u;
                    currentBase = (currentBase == 0 && currentQuality == 0 ? 4 : currentBase);
                    tileQuality += currentQuality;
                    laneBaseCounts[currentBase] += 1;

                    laneQualities[currentQuality]++;
                    tileCount++;
                }
            }

            auto tileQualities = this->getTileQualities(lane, tile);
            auto tileQualityMean = tileQuality / (float) tileCount;
            tileQualities->emplace_back(tileQualityMean);

            auto allTileQualities = this->getTileQualities(0, tile);
            if (tileQualities->size() > allTileQualities->size()) {
                allTileQualities->emplace_back(tileQualityMean);
            } else {
                allTileQualities->back() = (allTileQualities->back() + tileQualityMean) / 2;
            }

            laneCount += tileCount;
            laneQuality += tileQuality;
        }

        this->updateStatisticsVectors(lane, laneQuality, laneCount, laneQualities);
        this->updateCountVectors(lane, laneCount, laneBaseCounts);

        overallQuality += laneQuality;
        overallCount += laneCount;
        for (auto i = 0u; i < 41; i++) {
            allQualities[i] += laneQualities[i];
        }
        for (auto i = 0u; i < 5; i++) {
            allBaseCounts[i] += laneBaseCounts[i];
        }
    }

    this->updateStatisticsVectors(0, overallQuality, overallCount, allQualities);
    this->updateCountVectors(0, overallCount, allBaseCounts);

    if (this->getConfigEntry<bool>("showOutput"))
        this->out << "Mean quality of cycle " << this->framework->getCurrentCycle() << ": "
                  << overallQuality / (float) overallCount << endl;

    if (this->getConfigEntry<bool>("printAfterEveryCycle")) {
        writeCSVsFrom(this->framework->getCurrentCycle() - 1);
    } else {
        auto vec = this->getConfigEntry<vector<uint16_t >>("printAfterCycle");
        for (auto i = 0u; i < vec.size(); i++) {
            if (vec[i] == this->framework->getCurrentCycle())
                writeCSVsFrom(i == 0 ? 0 : vec[i - 1]);
        }
    }

    return this->framework->createNewFragmentContainer();
}

void QualityControl::setConfig() {
    this->registerConfigEntry<vector<uint16_t>, string>("printAfterCycle", "",
                                                        Configurable::toVector<uint16_t>(','),
                                                        EntryOptions::NO_WARNING);
    this->registerConfigEntry<bool>("printAfterEveryCycle", true, EntryOptions::NO_WARNING);
    this->registerConfigEntry<string>("outputDirectory", "./out/LiveQC/", EntryOptions::NO_WARNING);
    this->registerConfigEntry<string>("chartDirectory", "../plugins/LiveQC/charts/", EntryOptions::NO_WARNING);
    this->registerConfigEntry<int>("reloadTimeout", 300000); // 300000 => 5min
    this->registerConfigEntry<bool>("showOutput", true, EntryOptions::NO_WARNING);
}

void QualityControl::init() {
    this->permanentFragment = this->framework->createNewFragment("permanent");
    auto lanes = this->getConfigEntry<vector<uint16_t >>("lanes");
    lanes.push_back(0);

    for (auto &lane : lanes) {
        for (auto &name : this->statisticsVectorNames) {
            auto *vec = new SerializableVector<float>();
            this->permanentFragment->setSerializable(to_string(lane).append(name), vec);
        }
        for (auto &name : this->countVectorNames) {
            auto *vec = new SerializableVector<unsigned long>();
            this->permanentFragment->setSerializable(to_string(lane).append(name), vec);
        }
        for (auto &tile : this->getConfigEntry<vector<uint16_t >>("tiles")) {
            auto *vec = new SerializableVector<float>();
            this->permanentFragment->setSerializable(
                    to_string(lane).append("means").append(to_string(tile)), vec);
        }
    }

    auto outputDirectory = this->getConfigEntry<string>("outputDirectory");
    auto chartDirectory = this->getConfigEntry<string>("chartDirectory");

    if (this->copyDir(fs::path(chartDirectory), fs::path(outputDirectory)))
        cout << "Initialized LiveQC charts" << endl;

    this->writeCSVHeaders();

    this->writeConfigJson();
}

void QualityControl::finalize() {
    if (!this->getConfigEntry<bool>("printAfterEveryCycle")) {
        auto vec = this->getConfigEntry<vector<uint16_t >>("printAfterCycle");
        if (!vec.empty())
            writeCSVsFrom(vec.back());
        else
            writeCSVsFrom(0);
    }
}

void QualityControl::writeCSVHeaders() {
    ofstream dataFile;
    auto lanes = this->getConfigEntry<vector<uint16_t >>("lanes");
    lanes.push_back(0);

    for (auto &lane : lanes) {
        dataFile.open(this->getConfigEntry<string>("outputDirectory") + "qualityReport" + to_string(lane) + ".csv");

        for (auto &name : this->statisticsVectorNames) {
            dataFile << name << ",";
        }

        for (auto &name : this->countVectorNames) {
            dataFile << name << ",";
        }

        for (auto &tile : this->getConfigEntry<vector<uint16_t >>("tiles")) {
            dataFile << "means" << tile << ",";
        }

        dataFile << endl;
        dataFile.close();
    }

}

void QualityControl::writeCSVsFrom(int lastWrittenCycle) {
    ofstream dataFile;
    auto lanes = this->getConfigEntry<vector<uint16_t >>("lanes");
    lanes.push_back(0);

    for (auto &lane : lanes) {
        dataFile.open(this->getConfigEntry<string>("outputDirectory") + "qualityReport" + to_string(lane) + ".csv",
                      ios_base::app);

        auto statisticsVectors = this->getAllStatisticsVectors(lane);
        auto countVectors = this->getAllCountVectors(lane);

        for (unsigned long i = lastWrittenCycle; i < statisticsVectors[0]->size(); i++) {
            for (auto &vec : statisticsVectors) {
                dataFile << vec->at(i) << ",";
            }

            for (auto &vec : countVectors) {
                dataFile << vec->at(i) << ",";
            }

            for (auto &tile : this->getConfigEntry<vector<uint16_t >>("tiles")) {
                auto tmpVec = this->getTileQualities(lane, tile);
                dataFile << tmpVec->at(i) << ",";
            }

            dataFile << endl;
        }
        dataFile.close();
    }
}

void QualityControl::writeConfigJson() {
    json config = {
            {"existingLanes", this->getConfigEntry<vector<uint16_t >>("lanes")},
            {"numCycles",     this->framework->getCycleCount()},
            {"reloadTimeout", this->getConfigEntry<int>("reloadTimeout")}
    };

    ofstream configFile;
    configFile.open(this->getConfigEntry<string>("outputDirectory") + "config.json");
    configFile << setw(4) << config << endl;
    configFile.close();
}

void QualityControl::updateStatisticsVectors(const uint16_t &lane, const unsigned long long &quality,
                                             const unsigned long long &count,
                                             const unsigned long long qualities[41]) {
    auto statisticsVector = this->getAllStatisticsVectors(lane);
    statisticsVector[0]->emplace_back(quality / (float) count);
    statisticsVector[1]->emplace_back(QualityControl::getMedian(qualities, count));
    statisticsVector[2]->emplace_back(QualityControl::getQOne(qualities, count));
    statisticsVector[3]->emplace_back(QualityControl::getQThree(qualities, count));
    statisticsVector[4]->emplace_back(QualityControl::getDOne(qualities, count));
    statisticsVector[5]->emplace_back(QualityControl::getDNine(qualities, count));
    statisticsVector[6]->emplace_back(QualityControl::getMin(qualities));
    statisticsVector[7]->emplace_back(QualityControl::getMax(qualities));
}

void QualityControl::updateCountVectors(const uint16_t &lane, const unsigned long long &count,
                                        const unsigned long long baseCounts[41]) {
    auto countsVector = this->getAllCountVectors(lane);
    countsVector[0]->emplace_back(count);
    countsVector[1]->emplace_back(baseCounts[0]);
    countsVector[2]->emplace_back(baseCounts[1]);
    countsVector[3]->emplace_back(baseCounts[2]);
    countsVector[4]->emplace_back(baseCounts[3]);
    countsVector[5]->emplace_back(baseCounts[4]);
}

bool QualityControl::copyDir(boost::filesystem::path const &source, boost::filesystem::path const &destination) {
    try {
        // Check whether the function call is valid
        if (
                !fs::exists(source) ||
                !fs::is_directory(source)) {
            std::cerr << "Source directory " << source.string()
                      << " does not exist or is not a directory." << '\n';
            return false;
        }
        if (fs::exists(destination)) {
            return true;
        }
        // Create the destination directory
        if (!fs::create_directories(destination)) {
            std::cerr << "Unable to create destination directory"
                      << destination.string() << '\n';
            return false;
        }
    }
    catch (fs::filesystem_error const &e) {
        std::cerr << e.what() << '\n';
        return false;
    }
    // Iterate through the source directory
    for (
            fs::directory_iterator file(source);
            file != fs::directory_iterator(); ++file
            ) {
        try {
            fs::path current(file->path());
            if (fs::is_directory(current)) {
                // Found directory: Recursion
                if (
                        !copyDir(
                                current,
                                destination / current.filename()
                        )
                        ) {
                    return false;
                }
            } else {
                // Found file: Copy
                fs::copy_file(
                        current,
                        destination / current.filename()
                );
            }
        }
        catch (fs::filesystem_error const &e) {
            std::cerr << e.what() << '\n';
        }
    }
    return true;
}
