#ifndef QUALITY_CONTROL_H
#define QUALITY_CONTROL_H

#include <iostream>
#include <bitset>
#include <sys/stat.h>
#include <boost/filesystem/path.hpp>
#include "../../framework/Plugin.h"
#include "../..//framework/FrameworkInterface.h"
#include "../..//serializables/SerializableVector.h"


LIVEKIT_PLUGIN(QualityControl) {
private:
    const std::vector<std::string> statisticsVectorNames = {"means", "median", "qOne", "qThree", "dOne", "dNine", "min",
                                                            "max"};
    const std::vector<std::string> countVectorNames = {"overallCount", "countA", "countC", "countG", "countT",
                                                       "countN"};

    static inline float getMedian(const unsigned long long laneQualities[41], const unsigned long long &totalCount) {
        return getElementAtPosition(laneQualities, (unsigned long) ceil(totalCount / 2.0));
    };

    static inline float getQThree(const unsigned long long laneQualities[41], const unsigned long long &totalCount) {
        return getElementAtPosition(laneQualities, (unsigned long) ceil(totalCount * 3 / 4.0));
    }

    static inline float getQOne(const unsigned long long laneQualities[41], const unsigned long long &totalCount) {
        return getElementAtPosition(laneQualities, (unsigned long) ceil(totalCount / 4.0));
    }

    static inline float getDOne(const unsigned long long laneQualities[41], const unsigned long long &totalCount) {
        return getElementAtPosition(laneQualities, (unsigned long) ceil(totalCount / 10.0));
    }

    static inline float getDNine(const unsigned long long laneQualities[41], const unsigned long long &totalCount) {
        return getElementAtPosition(laneQualities, (unsigned long) ceil(totalCount * 9 / 10.0));
    }

    static inline unsigned char getMin(const unsigned long long laneQualities[41]) {
        for (auto i = 0; i < 41; i++) {
            if (laneQualities[i] > 0)
                return i;
        }
        return 0;
    }

    static inline unsigned char getMax(const unsigned long long laneQualities[41]) {
        for (auto i = 40; i >= 0; i--) {
            if (laneQualities[i] > 0)
                return i;
        }

        return 0;
    }

    static inline float
    getElementAtPosition(const unsigned long long laneQualities[41], const unsigned long long &position) {
        unsigned long offset = 0;
        unsigned char i = 0;
        while (offset <= position) {
            offset += laneQualities[i];
            i++;
        }

        if (offset == position)
            return i;

        return i - 1;
    }

    template<typename T>
    inline std::vector<SerializableVector<T> *>
    getAllVectorsByName(const std::vector<std::string> &names, uint16_t lane) {
        std::vector<SerializableVector<T> *> tmpVectors;
        for (auto &name : names) {
            tmpVectors.push_back(
                    (SerializableVector<T> *) this->permanentFragment->getSerializable(
                            std::to_string(lane).append(name)));
        }
        return tmpVectors;
    }

    std::vector<SerializableVector<float> *> getAllStatisticsVectors(uint16_t lane) {
        return this->getAllVectorsByName<float>(this->statisticsVectorNames, lane);
    }


    std::vector<SerializableVector<unsigned long> *> getAllCountVectors(uint16_t lane) {
        return this->getAllVectorsByName<unsigned long>(this->countVectorNames, lane);
    }

    SerializableVector<float> *getTileQualities(uint16_t lane, uint16_t tile) {
        return (SerializableVector<float> *) this->permanentFragment->getSerializable(
                std::to_string(lane).append("means").append(std::to_string(tile)));
    }

    void writeCSVHeaders();

    void writeCSVsFrom(int lastWrittenCycle);

    void writeConfigJson();

    void
    updateStatisticsVectors(const uint16_t &lane, const unsigned long long &quality, const unsigned long long &count,
                            const unsigned long long qualities[41]);

    void
    updateCountVectors(const uint16_t &lane, const unsigned long long &count, const unsigned long long baseCounts[5]);

    bool copyDir(boost::filesystem::path const &source, boost::filesystem::path const &destination);

public:
    void init() override;

    void setConfig() override;

    std::shared_ptr<FragmentContainer> runCycle(std::shared_ptr<FragmentContainer> inputFragments) override;

    void finalize() override;

    ~QualityControl() override = default;
};

#endif //QUALITY_CONTROL_H

