# LiveQC

## LiveKit Plugin
**LiveQC** is a plugin written for the excellent framework [LiveKit](https://gitlab.com/GeneticLife/LiveKit). It enables you to monitor the quality of your Illumina sequencing run in real-time.

## Installation
To install **LiveQC** by source, you'll need to clone the repo and build the project. Afterwards you need to move the `charts` directory to LiveKit and adapt the `outputDirectory` config path as necessary.

## Configuration options

Can also be empty.

| **Name**                     | **Default**  | **Description**                                                 |
| ---------------------------- | ------------ | --------------------------------------------------------------- |
| printAfterEveryCycle: bool   | `true`       | set to true to update the output csv after every analyzed cycle |
| printAfterCycle: vector<int> | `[]`         | if `printAfterEveryCycle` is disabled, one can control after which cycles LiveQC should output quality data.<br />LiveQC will always output after the last cycle was run. |
| outputDirectory: string      | `./out/LiveQC/` | specify the directory where the charts are, so LiveQC can update the data. |
| chartDirectory: string      | `../plugins/LiveQC/charts/` | specify the directory where the chart libraries are so **LiveQC** can copy these to the output dir |
| reloadTimeout: int           | `300000` = 5min | specify the timeout in ms after which the data should be refreshed and the charts should be reloaded. |
| showOutput: bool             | `true`         | set to true to see command line output when LiveQC is running |

## Visualization

The visualization is currently only available in Firefox because other browsers don't allow the loading of local files.
Since Firefox 68.0 file requests are also blocked but you can allow the requests by disabling the config option `privacy.file_unique_origin` in the `about:config` tab.

I used the following dependencies to create the visualization: 
- [d3](https://d3js.org/) to create the charts.
- [bootstrap](https://getbootstrap.com/) to create the buttons and dropdowns.
- [noUiSlider](https://github.com/leongersen/noUiSlider) to create the slider.
- [xo-linter](https://github.com/xojs/xo) to prettify and check my code.