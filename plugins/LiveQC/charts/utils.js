/* exported translateBy, changeLane, scrollToElement, tileTooltip, baseTooltip */

function type(d) {
	const keys = Object.keys(d);
	for (const key of keys) {
		d[key] = Number(d[key]);
	}

	return d;
}

function translateBy(x, y) {
	return 'translate(' + x + ',' + y + ')';
}

class ChartManager {
	constructor() {
		this.charts = {
			baseQuality: new BaseQuality(),
			tileChart: new TileChart(),
			baseProportion: new BaseProportion()
		};
		this.data = {};
		this.filteredReads = true;
		this.nBases = false;
		this.config = {};
		this.currLane = undefined;
		this.min = undefined;
		this.max = undefined;

		this.cycleDisplay = document.querySelector('#cycleDisplay');
		this.slider = document.querySelector('#slider');
		this.initSlider();

		this.loadConfig();
		this.loadData().then(() => {
			this.loadCharts();
			if (this.data.length < this.config.numCycles)
				setTimeout(reloadPage, this.config.reloadTimeout);
		});
	}

	initSlider() {
		noUiSlider.create(this.slider, {
			start: [0, 100],
			connect: true,
			step: 1,
			range: {
				min: 0,
				max: 100
			},
			pips: {
				mode: 'positions',
				values: [0, 25, 50, 75, 100],
				density: 4
			}
		});

		this.slider.noUiSlider.on('set', values => this.updateMinMax(values));
		this.slider.noUiSlider.on('update', values => this.updateCycleText(values));
	}

	loadConfig() {
		this.config = this.loadConfigJson();

		this.config.existingLanes.unshift(0);

		let laneCollection = '';
		for (const lane of this.config.existingLanes) {
			const laneString = lane === 0 ? 'All Lanes' : `Lane ${lane}`;
			laneCollection += `<button class="dropdown-item" id="lane${lane}" onclick="changeLane(event)">${laneString}</button>`;
		}

		document
			.querySelector('#lanePicker')
			.querySelectorAll('.dropdown-menu')[0].innerHTML = laneCollection;
		this.changeLaneTo(this.config.existingLanes[0]);
	}

	changeLaneTo(newCurrLane) {
		if (this.currLane !== undefined)
			document.querySelector(`#lane${this.currLane.toString()}`).className =
				'dropdown-item';
		this.currLane = newCurrLane;
		document.querySelector(`#lane${this.currLane.toString()}`).className +=
			' active';
		document.querySelector('#laneButton').innerHTML = this.currLane === 0 ? 'All Lanes' : `Lane ${this.currLane}`;
	}

	reloadCharts() {
		console.log('reload');
		this.emptyCharts();
		this.loadCharts();
	}

	loadCharts() {
		for (const chart of Object.values(this.charts)) {
			chart.initChartArea();
			chart.processData(this.data, this.min, this.max);
		}
	}

	emptyCharts() {
		for (const chart of Object.values(this.charts)) {
			chart.remove();
		}
	}

	loadData() {
		return new Promise(resolve => {
			document.querySelector('#refresh').innerHTML =
				'<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...';
			d3.csv(`qualityReport${this.currLane}.csv`, type).then(data => {
				this.data = data;
				this.min = 1;
				this.max = data.length;

				this.cycleDisplay.innerHTML = `Cycle ${this.min} - ${this.max}`;

				this.slider.noUiSlider.updateOptions(
					{
						range: {
							min: this.min,
							max: this.max
						},
						start: [this.min, this.max]
					},
					false
				);

				document.querySelector('#refresh').innerHTML = 'Refresh';
				resolve();
			});
		});
	}

	updateMinMax(values) {
		this.min = Number(values[0]);
		this.max = Number(values[1]);

		this.cycleDisplay.innerHTML = `Cycle ${this.min} - ${this.max}`;

		this.reloadCharts();
	}

	updateCycleText(values) {
		this.min = Number(values[0]);
		this.max = Number(values[1]);

		this.cycleDisplay.innerHTML = `Cycle ${this.min} - ${this.max}`;
	}

	loadConfigJson() {
		try {
			const configFile = new XMLHttpRequest();
			configFile.open('GET', 'config.json', false);
			configFile.overrideMimeType('application/json');
			configFile.send();
			return JSON.parse(configFile.responseText);
		} catch (error) {
			return '';
		}
	}
}

function changeLane(event) {
	manager.changeLaneTo(Number(event.target.id.replace('lane', '')));
	manager.loadData().then(() => manager.reloadCharts());
}

function reloadPage() {
	manager.loadData().then(() => {
		manager.reloadCharts();
		if (manager.data.length < manager.config.numCycles)
			setTimeout(reloadPage, manager.config.reloadTimeout);
	});
}

function scrollToElement(element) {
	const bbox = element.getBoundingClientRect();
	window.scrollTo({
		left: 0,
		top: bbox.top + window.pageYOffset - 150,
		behavior: 'smooth'
	});
}

const tileTooltip = d3
	.select('body')
	.append('div')
	.attr('class', 'tooltip tile')
	.style('opacity', 0);

const baseTooltip = d3
	.select('body')
	.append('div')
	.attr('class', 'tooltip prop')
	.style('opacity', 0);

const manager = new ChartManager();
