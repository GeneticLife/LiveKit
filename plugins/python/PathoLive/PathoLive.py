#!/usr/bin/env python3
from pyplugin import *
import pysam
import os
import pickle
import shutil
import time
import math
import matplotlib
import numpy
import multiprocessing

matplotlib.use('PS')  # PS seems to be faster than Agg, even though .pngs are generated. but why?
import matplotlib.pyplot as plt


class PathoLive(PyPlugin):
    framework = None
    permanentFragment = None
    cycleList = []
    backgrounddict = {}
    bsl_dict = {}
    start_time = time.time()
    program_path = ""
    barcodes = []

    def runPreprocessing(self, inputFragments):
        print()
        print('---------')
        print('PathoLive')
        print('---------')
        print('output cycle: ', self.config["outCycles"])

        self.program_path = os.path.dirname(os.path.realpath(__file__))
        if not os.path.exists(os.path.join(self.config["outDir"])):
            os.makedirs(os.path.join(self.config["outDir"]))

        # parse background coverage file
        print('parsing background data')
        self.backgrounddict = self.unpickle_backgrounddict(
            backgroundfile=os.path.join(self.program_path, 'prelim_data', 'background_coverages.pickle'))

        # parse bsl list
        print('parsing bsl list')
        self.bsl_dict = self.parse_bsl_file(
            bsl_file=os.path.join(self.program_path, 'prelim_data', 'belgian_bsl_scores_always_highest.txt'))

        # get barcodes
        self.barcodes = self.get_barcode_string_vector()
        if(len(self.barcodes) == 0):
            self.barcodes.append('undetermined')

    def runCycle(self, inputFragments):
        cycle = self.framework.getCurrentCycle()
        if cycle in self.config["outCycles"]:
            for barcode in self.barcodes:
                print("PathoLive: run cycle")
                print("Assume hilive generated: " + os.path.join(self.config["outDir"],
                                                                 'hilive_out_cycle{}_{}.sam'.format(cycle, barcode)))

                print("reading SAM/BAM for ", cycle)
                if self.config["outFormat"] == 'SAM':
                    print('parsing samfile for cycle {}'.format(cycle))
                    referencedict, uniquebasesdict, coveragedict, readcountdict = self.parse_sam_bam_file(
                        os.path.join(self.config["outDir"], 'hilive_out_cycle{}_{}.sam'.format(cycle, barcode)),
                        self.backgrounddict, False)
                elif not self.config["outFormat"] == 'SAM':
                    print('parsing bamfile for cycle {}'.format(cycle))
                    referencedict, uniquebasesdict, coveragedict, readcountdict = self.parse_sam_bam_file(
                        os.path.join(self.config["outDir"], 'hilive_out_cycle{}_{}.bam'.format(cycle, barcode)),
                        self.backgrounddict, True)

                # generate data structure for result presentation
                print('generating data for tree of results for cycle {}'.format(cycle))
                try:
                    self.copy_tree_structure(os.path.join(self.program_path, 'prelim_data'),
                                             os.path.join(self.config["outDir"], 'output_cycle{}_{}'.format(cycle, barcode)))
                except FileExistsError:
                    shutil.rmtree(os.path.join(self.config["outDir"], 'output_cycle{}'.format(cycle)))
                    self.copy_tree_structure(os.path.join(self.program_path, 'prelim_data'),
                                             os.path.join(self.config["outDir"], 'output_cycle{}_{}'.format(cycle, barcode)))

                # generate tree
                print('writing data for tree')
                self.print_treedata(readcountdict, uniquebasesdict, self.backgrounddict, self.bsl_dict,
                                    resultfile=os.path.join(self.config["outDir"], 'output_cycle{}_{}'.format(cycle, barcode),
                                                            'Collapsible_Tree_with_Amounts',
                                                            'data',
                                                            'treedata.csv'))
                print(time.time() - self.start_time)
                self.start_time = time.time()

                # generate plots
                print('generating plots')
                worker_args = []
                for refname in readcountdict.keys():
                    try:
                        worker_args.append((referencedict[refname], coveragedict[refname], self.backgrounddict[refname], refname,
                                            os.path.join(self.config["outDir"], 'output_cycle{}_{}'.format(cycle, barcode),
                                                         'Collapsible_Tree_with_Amounts',
                                                         'images')))
                    except KeyError:
                        worker_args.append((referencedict[refname], coveragedict[refname], {}, refname,
                                            os.path.join(self.config["outDir"], 'output_cycle{}_{}'.format(cycle, barcode),
                                                         'Collapsible_Tree_with_Amounts',
                                                         'images')))
                with multiprocessing.Pool(processes=int(self.config.get("numThreads", 1))) as pool:
                    pool.map(coverage_plot, worker_args)

    def unpickle_backgrounddict(self, backgroundfile):
        """
        Loads background dict from pickle-file
        :param self:
        :param backgroundfile: path to backgrounddict
        :return: dictionary of covered viral reference positions from human genome project
        """
        print(backgroundfile)
        backgroundfile_open = open(backgroundfile, 'rb')
        backgrounddict = pickle.load(backgroundfile_open)
        backgroundfile_open.close()
        return backgrounddict

    def parse_sam_bam_file(self, filePath, backgrounddict, is_bam):
        """
        Function that parses the necessary information from generated .bamfile
        :param self:
        :param filePath: path to file of type .sam or .bam - must contain header
        :param backgrounddict: dict of background abundances
        :return: tuple of dicts: referencedict, uniquebasesdict, coveragedict, readcountdict
        :return: referencedict: key = str refname, val = int reflength
        :return: uniquebasesdict: key = str refname, val = int ref positions only covered in foreground
        :return: coveragedict: key = str refname, val = dict; key = reference positions, val = coverage
        :return: readcountdict: key = str refname, val = int number of mapped reads

        """
        readcountdict = {}  # dict with read counts for tree
        coveragedict = {}  # dict with complete coverages for plotting
        uniquebasesdict = {}  # dict containing binary information if a position is covered
        if is_bam:
            access_type = 'rb'
        else:
            access_type = 'r'

        file = pysam.AlignmentFile(filePath, access_type)
        referencedict = dict(zip(file.references, file.lengths))
        for read in file:
            alignment_score = int(read.tags[0][1])

            ref = read.reference_name
            hit_range = read.get_reference_positions()
            for hitpos in hit_range:
                try:
                    if hitpos not in backgrounddict[ref]:  # if a position is also hit in BG: Ignore it
                        try:
                            uniquebasesdict[ref] |= {
                                hitpos}  # add hitpos to unique set of positions only found in FG
                        except KeyError:
                            uniquebasesdict[ref] = set()
                            uniquebasesdict[ref] |= {hitpos}
                except KeyError:  # if backgrounddict does not contain reference
                    try:
                        uniquebasesdict[ref] |= {hitpos}
                    except KeyError:
                        uniquebasesdict[ref] = set()
                        uniquebasesdict[ref] |= {hitpos}
                try:
                    coveragedict[ref][hitpos] += 1
                except KeyError:
                    try:
                        coveragedict[ref][hitpos] = 1
                    except KeyError:
                        coveragedict[ref] = {}
                        coveragedict[ref][hitpos] = 1

            try:
                readcountdict[ref] += 1  # count reads mapping on reference
            except KeyError:
                readcountdict[ref] = 1

        for ref in uniquebasesdict.keys():  # flatten dict of covered bases, count covered bases and return int(len(set))
            uniquebasesdict[ref] = len(uniquebasesdict[ref])

        return referencedict, uniquebasesdict, coveragedict, readcountdict

    def parse_bsl_file(self, bsl_file):
        """
        Generates BSL-Value-Dictionary from textfile
        :param bsl_file: path to BSL List in .txt-format
        :return: dictionary of bsl scores to trivial virus names. May contain different virus names than expected
        """
        with open(bsl_file) as bsl_file:
            bsl_dict = {}
            for line in bsl_file:
                li = line.rstrip()
                l = li.split('\t')
                bsl_dict[str(l[0]).lower()] = l[1]  # switch names to lower case
        return bsl_dict

    def bsl_leveller(self, bsl_dict, speciesname):
        """
        Parses the BSL-scores of species from the provided BSL-dict.
        :param bsl_dict: dictionary of bsl scores to trivial virus names. May contain different virus names than expected,
        mapping is not perfect
        :param speciesname: name of the virus found in the reference database. Not guaranteed to be in same format as
        BSL-dict
        :return: int [1:4] BSL-Score, always lowest (=1) if not defined
        """
        lower_speciesname = speciesname.lower()
        if 'phage' in lower_speciesname:
            return 1
        try:
            return [int(bsl_dict[s]) for s in bsl_dict.keys() if
                    lower_speciesname in s or s.split('viruses')[0] in lower_speciesname][
                       0] + 1
            # either the found virus appears in bsl_dict or the bsl_dict key without viruses appears in found virus.
            # Note: Some groups of viruses are defined on higher level (e.g. adenoviruses on family level).
        except IndexError:
            return 2

    def copy_tree_structure(self, program_path, resultpath):
        """
        Copies files used for html presentation of results to the resultfolder
        :param program_path: str path to program
        :param resultpath: str path to result
        :return: nothing
        """
        shutil.copytree(os.path.join(program_path, 'Collapsible_Tree_with_Amounts'),
                        os.path.join(resultpath, 'Collapsible_Tree_with_Amounts'))

    def print_treedata(self, readcountdict, uniquebasesdict, backgrounddict, bsl_dict, resultfile):
        """
        Writes results into csv file for D3
        :param self:
        :param readcountdict: dict of int coverage of int position of str reference in foreground
        :param uniquebasesdict: dict of int coverage of str reference found in foreground only
        :param backgrounddict: dict of int coverage of str reference found in background
        :param bsl_dict: dict of int bsl-level of str reference identifier (not always exactly matching str reference)
        :param resultfile: str path to file to write results to
        :return: nothing
        """
        outfile = open(resultfile, 'w')
        outfile.write('Category,Level1,Level2,Level3,Level4,AllHits,BSLlvl,,UnambigBases,WeightedScore,Total\n')
        linelist = []
        for key, value in readcountdict.items():
            print(key)
            giname = key.split('|')[1]  # set to 0 for old database format!
            speciesnum = value
            if key not in uniquebasesdict:
                weighted_score = 0
            else:
                if key in backgrounddict and key in uniquebasesdict:
                    weighted_score = uniquebasesdict[key] / len(
                        backgrounddict[key]) * (math.log(
                        speciesnum) + 1)  # ratio of number of unique fore- and background bases
                else:
                    weighted_score = uniquebasesdict[key] * (
                            math.log(speciesnum) + 1)  # add pseudocount to circumvent 0-scores for one read
            try:
                basesnum = uniquebasesdict[key]
            except KeyError:  # references without unique foreground bases are missing in uniquebasesdict
                basesnum = 0
            key = key.replace(',', ';')  # remove commas from species tax name to avoid crashing the csv tree
            if '|species:' not in key:
                speciesname = 'unassigned species'
                speciesname_no_number = 'unassigned species'
            else:
                speciesname = key.split('|species:')[1].split('|')[0]
                speciesname_no_number = speciesname.split(':')[1]
            if '|genus:' not in key:
                genusname = 'unassigned genus'
            else:
                genusname = key.split('|genus:')[1].split('|')[0]
            if '|family:' not in key:
                familyname = 'unassigned family'
            else:
                familyname = key.split('|family:')[1].split('|')[0]
            linelist.append('---Root,' + familyname + ',' + genusname + ',' + speciesname + ',' + giname + ',' + str(
                speciesnum) + ',' + str(self.bsl_leveller(bsl_dict, speciesname_no_number)) + ',,' + str(
                basesnum) + ',' + str(
                int(weighted_score)) + ',0.1,\n')
        linelist.sort()
        for each in linelist:
            outfile.write(each)
        outfile.close()

    def get_barcode_string_vector(self):
        bc_strings = []
        barcode_vector = self.config["barcodeVector"]
        for barcode in barcode_vector:
            bc_strings.append("-".join(barcode))

        return bc_strings[1:] # remove first char ('-')


def coverage_plot(inputdata):
    """
    Plots foreground coverage and background abundance
    :param inputdata: tuple of all input data
    [0]length: int length of reference
    [1]coveragedict: dict; key = str refname, val = dict; key = reference positions, val = coverage
    [2]backgroundcov: dict of covered viral reference positions from human genome project
    [3]refname: str name of reference
    [4]resultpath: str path for results
    :return: nothing
    """
    length = inputdata[0]
    coveragedict = inputdata[1]
    backgroundcov = inputdata[2]
    refname = inputdata[3]
    resultpath = inputdata[4]
    backcovlist = []
    forecovlist = []
    for i in range(length):
        try:
            backcovlist.append(-numpy.log10(backgroundcov[
                                                i] + 1))  # +1 to avoid logarithmic zeros.
        except KeyError:
            backcovlist.append(0)

    for i in range(length):
        try:
            forecovlist.append(numpy.log10(coveragedict[i] + 1))
        except KeyError:
            forecovlist.append(0)

    fig, ax = plt.subplots()

    try:
        plt.title(refname.split('||')[-1].split(':')[2].split('|')[0].replace('_', ' '))
    except IndexError:
        plt.title(refname)  # maybe not necessary anymore
    plt.xlabel('reference position')
    plt.ylabel('background abundance          foreground coverage')
    ax.set_xlim([0, length])
    try:
        limit = max(max(backgroundcov.values()), max(
            coveragedict.values())) + 1
        # sets limit to highest value of fore- and backgrounddict, +1 because we don't want bars to touch the border
    except ValueError:
        limit = max(coveragedict.values()) + 1
    maximized_limit = (len(str(int(limit))))  # calculates the next full power of 10
    ax.set_ylim([-maximized_limit, maximized_limit])
    minor_ticks_list = [math.log(j * 10 ** i, 10) for j in range(2, 10) for i in
                        range(0, maximized_limit)]  # generates list of form log10([2,3,4,5,6,7,8,9,20,30,40...])
    minor_ticks = sorted(
        [-x for x in
         minor_ticks_list] + minor_ticks_list)  # concatenates sorted list of negative and positive ticks
    ax.set_yticks(minor_ticks, minor=True)
    labels = [10 ** i for i in range(maximized_limit, 0, -1)] + [0] + [10 ** i for i in range(1,
                                                                                              maximized_limit + 1)]
    # generates list of form [...100,10,0,10,100...]
    major_ticks = range(-maximized_limit, maximized_limit + 1)
    ax.set_yticks(major_ticks, minor=False)
    ax.set_yticklabels(labels)
    ax.fill_between(range(length), 0, forecovlist, facecolor='green', edgecolor='green')
    ax.fill_between(range(length), 0, backcovlist, facecolor='red', edgecolor='red')
    ax.axhline(y=0, color='black', linewidth=1.5)  # this is just a fix to hide red line slightly above 0
    plt.savefig(
        os.path.join(resultpath, '{}.png'.format(
            str(refname).split('|')[1])))  # name plot by GI number # Livekit: removed '.split('|')[1]'
    plt.close()
