from PluginLibrary import *
import struct
import random
import string
import json

'''
This class is the python implementation of the original Plugin.h (and Plugin.cpp) in the LiveKit repo.
Python plugins must inherit from this class to be able to communicate with the framework.
The communication between this pyhton class and the framework is setup in PythonPlugin.h (and PythonPlugin.cpp) and
based on boost_python.
For more information about plugins look at wiki of the repo or at the documentation of the c++ implementation
'''

class PyPlugin:
    framework = None
    permanentFragment = None
    config = []

    def setFramework(self, framework):
        self.framework = framework

    def setCurrentPluginCycle(self, cycle):
        self.framework.setCurrentPluginCycle(cycle)

    def loadConfig(self, pluginConfigFileName, frameworkConfigFileName):
        pluginConfigFile = open(pluginConfigFileName, 'r')
        frameworkConfigFile = open(frameworkConfigFileName, 'r')
        # configEntries from the plugin config have a higher priority and can overwrite configEntries from the framework
        self.config = json.load(frameworkConfigFile)
        self.config.update(json.load(pluginConfigFile))

    def init(self):
        pass

    def runPreprocessingHook(self, inputFragmentNames):
        self.runPreprocessing(self.getFragmentsFromNames(inputFragmentNames))

    def runPreprocessing(self, inputFragments):
        pass

    def runCycleHook(self, inputFragmentNames):
        self.runCycle(self.getFragmentsFromNames(inputFragmentNames))

    def runCycle(self, inputFragments):
        pass

    def hasTileContext(self):
        return False # The runCycleForTile-Hook is currently not supported for python because of multithreading problems with boost-python

    def runCycleForTileHook(self, inputFragmentNames, lane, tile):
        self.runCycleForTile(self.getFragmentsFromNames(inputFragmentNames), lane, tile)

    def runCycleForTile(self, inputFragments, lane, tile):
        pass # The runCycleForTile-Hook is currently not supported for python because of multithreading problems with boost-python

    def runFullReadPostprocessingHook(self, inputFragmentNames):
        self.runFullReadPostprocessing(self.getFragmentsFromNames(inputFragmentNames))

    def runFullReadPostprocessing(self, inputFragments):
        pass

    def finalize(self):
        pass

    def getFragmentsFromNames(self, fragmentNames):
        neededFragmentNames = list(map(lambda x: x.split('_'), fragmentNames))
        fragments = list(map(lambda x: Fragment(x[0], x[1]), neededFragmentNames))
        return fragments


class Fragment:
    map = {}
    sizes = {}
    hash = ""
    isSerialized = True
    name = ""
    numberOfSerializables = 0
    serializables = b''

    def __init__(self, name, hash=None):
        if hash is None:
            self.hash = self.generateHash()
        else:
            self.hash = hash

        self.name = name

    def generateHash(self):
        return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(10))

    def set(self, name, value):
        if (self.isSerialized):
            self.deserialize()

        byteRep = None
        if type(value) is str:
            byteRep = value.encode('ascii')
        elif type(value) is int:
            byteRep = struct.pack("<i", value)
        else:
            byteRep = bytes(value)

        self.map[name] = byteRep

    def get(self, name):
        if self.isSerialized:
            self.deserialize()

        return self.map[name]

    def getInt(self, name):
        if self.isSerialized:
            self.deserialize()

        return struct.unpack("<i", self.map[name])[0]

    def getString(self, name):
        if self.isSerialized:
            self.deserialize()

        return self.map[name].decode('ascii')

    def erase(self, name):
        if self.isSerialized:
            self.deserialize()

        del self.map[name]

    def serialize(self):
        if self.isSerialized:
            return

        filename = "temp/" + self.name + "_" + self.hash + ".fragment"
        serializeFile = open(filename, "wb")
        serializeFile.write(
            struct.pack("<Q", len(self.map)))

        # serialize primitives

        for name in self.map:
            self.serializeName(name, serializeFile)
            self.serializeValue(self.map[name], serializeFile)

        # serialize saved Serializables
        serializeFile.write(struct.pack("<Q",
                                        self.numberOfSerializables))
        serializeFile.write(self.serializables)

        self.map.clear()
        self.isSerialized = True
        # print("serialize")

    def serializeName(self, name, file):
        file.write(struct.pack("<Q", len(name)))
        file.write(name.encode('ascii'))

    def serializeValue(self, value, file):
        file.write(struct.pack("<Q", len(value)))
        file.write(value)

    def deserialize(self):
        if not self.isSerialized:
            return

        filename = "temp/" + self.name + "_" + self.hash + ".fragment"
        serializedFile = open(filename, "rb")
        sizeBytes = serializedFile.read(8)
        size = struct.unpack("<Q", sizeBytes)[0]

        # deserialize primitives

        for i in range(0, size):
            name = self.deserializeName(serializedFile)
            value = self.deserializeValue(serializedFile)
            self.map[name] = value

        # save Serializables for later use

        sizeBytes = serializedFile.read(8)
        self.numberOfSerializables = struct.unpack("<Q", sizeBytes)[0]
        currPosition = serializedFile.tell()
        serializedFile.seek(0, 2)
        eof = serializedFile.tell()
        serializedFile.seek(currPosition, 0)
        self.serializables = serializedFile.read(eof - currPosition)

        self.isSerialized = False
        # print("deserialize")

    def deserializeName(self, serializedFile):
        sizeBytes = serializedFile.read(8)
        size = struct.unpack("<Q", sizeBytes)[0]
        nameBytes = serializedFile.read(size)
        return nameBytes.decode('ascii')

    def deserializeValue(self, serializedFile):
        sizeBytes = serializedFile.read(8)
        size = struct.unpack("<Q", sizeBytes)[0]
        return serializedFile.read(size)
