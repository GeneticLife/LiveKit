#include "SerializableFactory.h"
#include "SerializableIndex.h"
#include "SerializableVector.h"
#include "SerializableMap.h"
#include "SerializableUnorderedMap.h"
#include "SerializableSequenceElements.h"
#include "SerializableAlignment.h"

#include "../plugins/HiLive_sharedLibraries/global_variables.h"

mutex_map<std::string> fileLocks;

/*
 * blueprint for adding new Serializable%s:

    {"name", [](unsigned long size, char *serializedSpace, std::string serializationDirectory) {
        return new SerializableName(size, serializedSpace, serializationDirectory);
    }}
 */

Serializable *
SerializableFactory::createSerializable(const std::string &name, unsigned long size, char *serializedSpace, std::string &serializationDirectory) {
    return SerializableFactory::knownSerializables.at(name)(size, serializedSpace, serializationDirectory);
}

const std::map<std::string, std::function<Serializable *(unsigned long,
                                                         char *, std::string &serializationDirectory)>> SerializableFactory::knownSerializables{
        {"index",  [](unsigned long size, char *serializedSpace, std::string &serializationDirectory) {
            (void) size; // UNUSED
            return new SerializableIndex(serializedSpace, serializationDirectory);
        }},
        {"vector", [](unsigned long size, char *serializedSpace, std::string &serializationDirectory) {
            return SerializableVectorTypeLess::createSerializableVector(size, serializedSpace, serializationDirectory);
        }},
        {"seqEl",  [](unsigned long size, char *serializedSpace, std::string &serializationDirectory) {
            (void) size; // UNUSED
            return new SerializableSequenceElements(serializedSpace, serializationDirectory);
        }},
        {"align",  [](unsigned long size, char *serializedSpace, std::string &serializationDirectory) {
            (void) size; // UNUSED
            return new SerializableAlignment(serializedSpace, serializationDirectory);
        }},
        {"map", [](unsigned long size, char *serializedSpace, std::string &serializationDirectory) {
            return SerializableMapTypeLess::createSerializableMap(size, serializedSpace, serializationDirectory);
        }},
        {"umap", [](unsigned long size, char *serializedSpace, std::string &serializationDirectory) {
            return SerializableUnorderedMapTypeLess::createSerializableUnorderedMap(size, serializedSpace, serializationDirectory);
        }}
};

void SerializableFactory::deleteSerializable(Serializable *serializable) {
    delete serializable;
}

extern "C" SerializableFactory *create() {
    return new SerializableFactory;
}

extern "C" void destroy(SerializableFactory *factory) {
    delete factory;
}