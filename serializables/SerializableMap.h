#ifndef LIVEKIT_SERIALIZABLEMAP_H
#define LIVEKIT_SERIALIZABLEMAP_H

#include <map>
#include <cstring>
#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include <iostream>
#include <utility>
#include <string>
#include <sstream>
#include "../framework/Serializable.h"
#include "SerializableIndex.h"
#include "SerializableVector.h"

template<typename K, typename V>
class SerializableMap : public std::map<K, V>, public Serializable {
    unsigned long serializedNumElements;

    inline unsigned char getDataType() const { return 255; };

    const std::string name() const override { return "map"; };

public:

    explicit SerializableMap() : serializedNumElements(0) {};

    int getSize() {return serializedNumElements;};

    SerializableMap(unsigned long size, char *serializedSpace, std::string serializationDirectory) {
        this->serializedNumElements = (size - serializableNameSize() - sizeof(unsigned char)) / (sizeof(K) + sizeof(V));
        this->deserialize(serializedSpace, serializationDirectory);
    };

    void serializeData(char *serializedSpace, std::string &serializationDirectory) override {
        (void) serializationDirectory; // UNUSED
        unsigned char dataType = getDataType();
        memcpy(serializedSpace, &dataType, sizeof(unsigned char));
        serializedSpace += sizeof(unsigned char);

        K key;
        V value;

        for(auto entry : (*this)) {
            key = entry.first;
            memcpy(serializedSpace, &key, sizeof(K));
            serializedSpace += sizeof(V);
            value = entry.second;
            memcpy(serializedSpace, &value, sizeof(V));
            serializedSpace += sizeof(K);
        }

        this->serializedNumElements = this->size();
    };

    void deserializeData(char *serializedSpace, std::string &serializationDirectory) override {
        (void) serializationDirectory; // UNUSED
        serializedSpace++;
        K key;
        V value;

        std::vector<K> v;
        for (unsigned long i = 0; i < this->serializedNumElements; i++) {

            memcpy(&key, serializedSpace, sizeof(K));
            serializedSpace += sizeof(V);
            v.push_back(key);

            memcpy(&value, serializedSpace, sizeof(V));

            (*this)[v[i]] = value;

            serializedSpace += sizeof(K);
        }

        this->serializedNumElements = 0;
    };

    void freeData() override {
        this->clear();
    }

    unsigned long serializableDataSize() override {
        return this->size() * (sizeof(K) + sizeof(V)) +
               sizeof(unsigned char); // Elements + 1 Byte Data Type of Map Elements
    };
};

template<>
inline unsigned char SerializableMap<int, float>::getDataType() const { return 0; }

template<>
inline unsigned char SerializableMap<int, int>::getDataType() const { return 1; }

template<>
inline unsigned char SerializableMap<int, unsigned int>::getDataType() const { return 2; }

template<>
inline unsigned char SerializableMap<int, long>::getDataType() const { return 3; }

template<>
inline unsigned char SerializableMap<int, unsigned long>::getDataType() const { return 4; }

template<>
inline unsigned char SerializableMap<int, SequenceElement>::getDataType() const { return 5; }

template<>
inline unsigned char SerializableMap<unsigned long, SerializableVector<unsigned long>>::getDataType() const { return 6; }

class SerializableMapTypeLess {
public:
    static Serializable *createSerializableMap(unsigned long size, char *serializedSpace, std::string serializationDirectory) {
        char *checkType = serializedSpace + 11;
        unsigned char type;
        memcpy(&type, checkType, sizeof(unsigned char));

        switch (type) {
            case 0:
                return new SerializableMap<int, float>(size, serializedSpace, serializationDirectory);
            case 1:
                return new SerializableMap<int, int>(size, serializedSpace, serializationDirectory);
            case 2:
                return new SerializableMap<int, unsigned int>(size, serializedSpace, serializationDirectory);
            case 3:
                return new SerializableMap<int, long>(size, serializedSpace, serializationDirectory);
            case 4:
                return new SerializableMap<int, unsigned long>(size, serializedSpace, serializationDirectory);
            case 5:
                return new SerializableMap<int, SequenceElement>(size, serializedSpace, serializationDirectory);
            case 6:
                return new SerializableMap<unsigned long, SerializableVector<unsigned long>>(size, serializedSpace, serializationDirectory);

            default:
                throw std::runtime_error("unsupported type of map element");
        }
    }
};

#endif //LIVEKIT_SERIALIZABLEMAP_H
