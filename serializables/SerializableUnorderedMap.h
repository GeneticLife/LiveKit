#ifndef LIVEKIT_SERIALIZABLEUNORDEREDMAP_H
#define LIVEKIT_SERIALIZABLEUNORDEREDMAP_H

#include <unordered_map>
#include <cstring>
#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include <iostream>
#include <utility>
#include <string>
#include <sstream>
#include "../framework/Serializable.h"
#include "SerializableIndex.h"

template<typename K, typename V>
class SerializableUnorderedMap : public std::unordered_map<K, V>, public Serializable {
    unsigned long serializedNumElements;

    inline unsigned char getDataType() const { return 255; };

    const std::string name() const override { return "umap"; };

public:

    explicit SerializableUnorderedMap() : serializedNumElements(0) {};

    int getSize() {return serializedNumElements;};

    SerializableUnorderedMap(unsigned long size, char *serializedSpace, std::string &serializationDirectory) {
        this->serializedNumElements = (size - serializableNameSize() - sizeof(unsigned char)) / (sizeof(K) + sizeof(V));
        this->deserialize(serializedSpace, serializationDirectory);
    };

    void serializeData(char *serializedSpace, std::string &serializationDirectory) override {
        (void) serializationDirectory; // UNUSED
        unsigned char dataType = getDataType();
        memcpy(serializedSpace, &dataType, sizeof(unsigned char));
        serializedSpace += sizeof(unsigned char);

        K key;
        V value;

        for(auto entry : (*this)) {
            key = entry.first;
            memcpy(serializedSpace, &key, sizeof(K));
            serializedSpace += sizeof(K);
            value = entry.second;
            memcpy(serializedSpace, &value, sizeof(V));
            serializedSpace += sizeof(V);
        }

        this->serializedNumElements = this->size();
    };

    void deserializeData(char *serializedSpace, std::string &serializationDirectory) override {
        (void) serializationDirectory; // UNUSED
        serializedSpace++;
        K key;
        V value;

        std::vector<int> v;
        for (unsigned long i = 0; i < (this->serializedNumElements); i++) {
            memcpy(&key, serializedSpace, sizeof(K));
            serializedSpace += sizeof(K);
            v.push_back(key);
            memcpy(&value, serializedSpace, sizeof(V));
            serializedSpace += sizeof(V);

            (*this)[v[i]] = value;
        }

        this->serializedNumElements = 0;
    };

    void freeData() override {
        this->clear();
    }

    unsigned long serializableDataSize() override {
        return this->size() * (sizeof(K) + sizeof(V)) +
               sizeof(unsigned char); // Elements + 1 Byte Data Type of Map Elements
    };
};

template<>
inline unsigned char SerializableUnorderedMap<int, float>::getDataType() const { return 0; }

template<>
inline unsigned char SerializableUnorderedMap<int, int>::getDataType() const { return 1; }

template<>
inline unsigned char SerializableUnorderedMap<int, unsigned int>::getDataType() const { return 2; }

template<>
inline unsigned char SerializableUnorderedMap<int, long>::getDataType() const { return 3; }

template<>
inline unsigned char SerializableUnorderedMap<int, unsigned long>::getDataType() const { return 4; }

template<>
inline unsigned char SerializableUnorderedMap<int, SequenceElement>::getDataType() const { return 5; }

template<>
inline unsigned char SerializableUnorderedMap<unsigned long, SerializableUnorderedMap<unsigned long, unsigned long>>::getDataType() const { return 6; }


class SerializableUnorderedMapTypeLess {
public:
    static Serializable *createSerializableUnorderedMap(unsigned long size, char *serializedSpace, std::string serializationDirectory) {
        char *checkType = serializedSpace + 12;
        unsigned char type;
        memcpy(&type, checkType, sizeof(unsigned char));

        switch (type) {
            case 0:
                return new SerializableUnorderedMap<int, float>(size, serializedSpace, serializationDirectory);
            case 1:
                return new SerializableUnorderedMap<int, int>(size, serializedSpace, serializationDirectory);
            case 2:
                return new SerializableUnorderedMap<int, unsigned int>(size, serializedSpace, serializationDirectory);
            case 3:
                return new SerializableUnorderedMap<int, long>(size, serializedSpace, serializationDirectory);
            case 4:
                return new SerializableUnorderedMap<int, unsigned long>(size, serializedSpace, serializationDirectory);
            case 5:
                return new SerializableUnorderedMap<int, SequenceElement>(size, serializedSpace, serializationDirectory);
            case 6:
                return new SerializableUnorderedMap<unsigned long, SerializableUnorderedMap<unsigned long, unsigned long>>(size, serializedSpace, serializationDirectory);
            default:
                throw std::runtime_error("unsupported type of map element");
        }
    }
};

#endif //LIVEKIT_SERIALIZABLEUNORDEREDMAP_H
