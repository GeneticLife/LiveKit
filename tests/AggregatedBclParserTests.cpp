#include <iostream>
#include "../framework/basecalls/data_representation/BclRepresentation.h"
#include "../framework/basecalls/parser/BclParser.h"
#include "../framework/basecalls/parser/AggregatedBclParser.h"
#include "./catch2/catch.hpp"

using namespace std;

TEST_CASE("AggregatedBclParserTests") {
    vector<uint16_t> lanes = {1};
    vector<uint16_t> tiles = {1101};
    BclRepresentation bclRepresentation(lanes, tiles); // 1 lane, 1 tiles
    AggregatedBclParser bclParser(&bclRepresentation, nullptr); // 1 lane, 1 tiles
    string filename = "../tests/mockFiles/AggregatedBaseCalls";

    SECTION("Should be able to parse single cbcl file") {
        bclParser.parse(1, 1101, 1, filename);
        auto bcl = bclParser.bclRepresentation->getBcl(1, 1101, 1); // first lane, first tile and first cycle

        // check if the correct number of reads were parsed
        // 1216 is encoded in the first four bytes of the bcl file (it is also equal to the size of the bcl file - 4)
        REQUIRE(bclParser.bclRepresentation->getBclSize(1, 1101) == 28);

        // check if content is correctly parsed
        // the actual data is '4141 4343 4377 ...'
        REQUIRE(bcl[0] == (char)'\x5F');
        REQUIRE(bcl[1] == (char)'\x96');
        REQUIRE(bcl[2] == (char)'\x02');
        REQUIRE(bcl[3] == (char)'\x96');
        REQUIRE(bcl[4] == (char)'\x31');
        REQUIRE(bcl[5] == (char)'\x96');
        REQUIRE(bcl[6] == (char)'\x00');
    }

    SECTION("Should throw an error if specified file doesn't exist") {
        REQUIRE_THROWS_WITH(bclParser.parse(2, 1101, 1, filename), "Error while reading CBCL file");
    }
    
    SECTION("Should throw an error if specified tile doesn't exist") {
        REQUIRE_THROWS_WITH(bclParser.parse(1, 1106, 1, filename), "The specified tile wasn't found in the BCL file");
    }
}
