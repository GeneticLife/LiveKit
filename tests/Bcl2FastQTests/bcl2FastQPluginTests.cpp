#include "../catch2/catch.hpp"
#include "../fakeit/fakeit.hpp"
#include "../../plugins/Bcl2FastQ/bcl2FastQ.h"
#include "../../framework/Plugin.h"
#include "../../framework/Framework.h"
#include "../../framework/basecalls/data_representation/BclRepresentation.h"

using namespace fakeit;
using namespace std;

TEST_CASE("Bcl2FastQPlugin") {

    // Example Data
    int numberOfSequences = 2;
    std::vector<Read> allSequences;
    Read mockSequence1;
    mockSequence1.push_back(0b10010000); // A
    mockSequence1.push_back(0b10001111); // T
    mockSequence1.push_back(0b10100010); // G
    mockSequence1.push_back(0b00000011); // T
    Read mockSequence2;
    mockSequence2.push_back(0b10100000); // A
    mockSequence2.push_back(0b00110000); // A
    mockSequence2.push_back(0b00001101); // C
    mockSequence2.push_back(0b10010011); // T
    allSequences.push_back(mockSequence1);
    allSequences.push_back(mockSequence2);

    Bcl2FastQ bcl2Fastq;
    bcl2Fastq.setConfig();

    seqan::StringSet<seqan::Dna5String> sequences;
    seqan::StringSet<std::string> qualities;

    seqan::resize(sequences, numberOfSequences);
    seqan::resize(qualities, numberOfSequences);

    Read currentSequence;

    SECTION("Should parse the correct base from a Byte") {

        std::bitset<8> byte;
        int base;

        byte = 0b00000000;
        base = bcl2Fastq.getBaseFromBits(byte);
        REQUIRE(base == 0);

        byte = 0b00000001;
        base = bcl2Fastq.getBaseFromBits(byte);
        REQUIRE(base == 1);

        byte = 0b00000010;
        base = bcl2Fastq.getBaseFromBits(byte);
        REQUIRE(base == 2);

        byte = 0b00000011;
        base = bcl2Fastq.getBaseFromBits(byte);
        REQUIRE(base == 3);
    }

    SECTION("Should parse the correct quality from a Byte") {

        std::bitset<8> byte;
        int quality;

        byte = 0b00000000;
        quality = bcl2Fastq.getQualityFromBits(byte);
        REQUIRE(quality == '!');

        byte = 0b10100001;
        quality = bcl2Fastq.getQualityFromBits(byte);
        REQUIRE(quality == 'I');

        byte = 0b00101010;
        quality = bcl2Fastq.getQualityFromBits(byte);
        REQUIRE(quality == '+');

        byte = 0b01010011;
        quality = bcl2Fastq.getQualityFromBits(byte);
        REQUIRE(quality == '5');
    }

    SECTION("Should parse the bases correctly") {

        currentSequence = allSequences[0];
        bcl2Fastq.addBasesAndQualities(currentSequence, sequences[0], qualities[0]);

        currentSequence = allSequences[1];
        bcl2Fastq.addBasesAndQualities(currentSequence, sequences[1], qualities[0]);

        REQUIRE(sequences[0] == "ATGN");
        REQUIRE(sequences[1] == "AACT");

        seqan::clear(qualities);
        seqan::clear(sequences);
    }

    SECTION("Should parse the qualities correctly") {

        currentSequence = allSequences[0];
        bcl2Fastq.addBasesAndQualities(currentSequence, sequences[0], qualities[0]);

        currentSequence = allSequences[1];
        bcl2Fastq.addBasesAndQualities(currentSequence, sequences[1], qualities[1]);

        REQUIRE(qualities[0] == "EDI!"); // Example: 00001010 -> 10; 10 * 1,492 -> 15; 15 -> '0'
        REQUIRE(qualities[1] == "I-$E");

        seqan::clear(qualities);
        seqan::clear(sequences);
    }

    SECTION("Should append the identifiers from the RunInfo.xml correctly") {

        string manifestFilePath = "../tests/mockFiles/Bcl2FastQTestFiles/Bcl2FastQFramework.json";
        auto *framework = new Framework(manifestFilePath);
        bcl2Fastq.setFramework(new FrameworkInterface(framework));
        bcl2Fastq.setConfigFromFile("../tests/mockFiles/Bcl2FastQTestFiles/fastQHeader.json");

        //This is necessary because the union of the framework configMap and the plugin config Map doesn't work here
        bcl2Fastq.registerConfigEntry<vector<uint16_t>, string>("lanes", "1,2", Configurable::toVector<uint16_t>(','));
        bcl2Fastq.registerConfigEntry<vector<uint16_t>, string>("tiles", "1101,1102,1103,1104,1105,1106,1107,1108,1109,1110,1111,1112,1113,11114,1115,1116", Configurable::toVector<uint16_t>(','));
        bcl2Fastq.registerConfigEntry<int>("laneCount", (int) bcl2Fastq.getConfigEntry<vector<uint16_t>>("lanes").size());
        bcl2Fastq.registerConfigEntry<int>("tileCount", (int) bcl2Fastq.getConfigEntry<vector<uint16_t>>("tiles").size());
        bcl2Fastq.registerConfigEntry<string>("run-id", "181205_C00102_0126_AHT3VGBCX2");
        bcl2Fastq.registerConfigEntry<string>("flowcell-id", "HT3VGBCX2");
        bcl2Fastq.registerConfigEntry<string>("instrument-id", "C00102");


        seqan::StringSet<std::string> ids;

        seqan::resize(ids, numberOfSequences);

        // Get the fragment that stores the position of every read and cast it into a vector
        auto *positionVector = new SerializableVector<float>();
        positionVector->push_back(7.5);
        positionVector->push_back(1.56);

        currentSequence = allSequences[0];
        bcl2Fastq.addIdentifier(ids[0], "C00102", "181205_C00102_0126_AHT3VGBCX2" ,"HT3VGBCX2", 1, 1101, positionVector, 1, 0, true);

        // Get header information from runInfo.xml
        REQUIRE(ids[0] == "C00102:181205_C00102_0126_AHT3VGBCX2:HT3VGBCX2:1:1101:7.50:1.56 1:Y:1:");

        delete framework;
        delete positionVector;
    }
    SECTION("Should append the default identifiers correctly") {

        string manifestFilePath = "../tests/mockFiles/Bcl2FastQTestFiles/fastQHeaderDefault.json";
        auto *framework = new Framework(manifestFilePath);
        bcl2Fastq.setFramework(new FrameworkInterface(framework));
        bcl2Fastq.setConfigFromFile("../tests/mockFiles/Bcl2FastQTestFiles/fastQHeader.json");

        //This is necessary because the union of the framework configMap and the plugin config Map doesn't work here
        bcl2Fastq.registerConfigEntry<vector<uint16_t>, string>("lanes", "1,2", Configurable::toVector<uint16_t>(','));
        bcl2Fastq.registerConfigEntry<vector<uint16_t>, string>("tiles", "1101,1102,1103,1104,1105,1106,1107,1108,1109,1110,1111,1112,1113,11114,1115,1116", Configurable::toVector<uint16_t>(','));
        bcl2Fastq.registerConfigEntry<int>("laneCount", (int) bcl2Fastq.getConfigEntry<vector<uint16_t>>("lanes").size());
        bcl2Fastq.registerConfigEntry<int>("tileCount", (int) bcl2Fastq.getConfigEntry<vector<uint16_t>>("tiles").size());
        bcl2Fastq.registerConfigEntry<string>("run-id", "0");
        bcl2Fastq.registerConfigEntry<string>("flowcell-id", "Default");
        bcl2Fastq.registerConfigEntry<string>("instrument-id", "0");

        seqan::StringSet<std::string> ids;

        seqan::resize(ids, numberOfSequences);

        currentSequence = allSequences[1];

        // Get the fragment that stores the position of every read and cast it into a vector
        auto *positionVector = new SerializableVector<float>();
        positionVector->push_back(4.55);
        positionVector->push_back(2.0);

        bcl2Fastq.addIdentifier(ids[1], "0", "0" ,"Default", 1, 1101, positionVector, 2, 0, false);

        // Compare default header informations
        REQUIRE(ids[1] == "0:0:Default:1:1101:4.55:2.00 2:N:1:");

        delete framework;
    }
}
