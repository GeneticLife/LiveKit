#include <iostream>
#include "../framework/basecalls/data_representation/BclRepresentation.h"
#include "../framework/basecalls/parser/BclParser.h"
#include "../framework/basecalls/parser/CompressedBclParser.h"
#include "./catch2/catch.hpp"

using namespace std;

TEST_CASE("CompressedBclParserTests") {
    vector<uint16_t> lanes = {1};
    vector<uint16_t> tiles = {1101, 1102, 1103};
    BclRepresentation bclRepresentation(lanes, tiles); // 1 lane, 3 tiles
    CompressedBclParser bclParser(&bclRepresentation); // 1 lane, 3 tiles
    string filename = "../tests/mockFiles/CompressedBaseCalls";

    SECTION("Should be able to parse single gzip compressed bcl file") {
        bclParser.parse(1, 1101, 1, filename);
        auto bcl = bclParser.bclRepresentation->getBcl(1, 1101, 1); // first lane, first tile and first cycle

        // check if the correct number of reads were parsed
        // 1216 is encoded in the first four bytes of the bcl file (it is also equal to the size of the bcl file - 4)
        REQUIRE(bclParser.bclRepresentation->getBclSize(1, 1101) == 60);

        // check if content is correctly parsed
        // the actual data is '4141 4343 4377 ...'
        REQUIRE(bcl[0] == '\x41');
        REQUIRE(bcl[1] == '\x41');
        REQUIRE(bcl[2] == '\x43');
        REQUIRE(bcl[3] == '\x43');
        REQUIRE(bcl[4] == '\x43');
        REQUIRE(bcl[5] == '\x77');
    }
}