#include <vector>
#include <boost/filesystem.hpp>
#include "./catch2/catch.hpp"
#include "./fakeit/fakeit.hpp"
#include "../framework/Framework.h"
#include "../framework/fragments/Fragment.cpp"
#include "../serializables/SerializableVector.h"
#include "../serializables/SerializableMap.h"
#include "../serializables/SerializableUnorderedMap.h"

class FragmentAccessHelper {
    Fragment *fragment;
public:
    explicit FragmentAccessHelper(Fragment *f) : fragment(f) {};

    std::string getHash() {
        return this->fragment->hash;
    }

    std::string getNextString(FILE *file) {
        return this->fragment->deserializeName(file);
    }

    std::map<std::string, void *> getMap() {
        return this->fragment->map;
    }

    static Fragment *createFragment(std::string name) {
        // This creates a memory leak but I think its okay because it's only in the test
        return new Fragment(name, new SerializableFactory());
    }
};

using namespace fakeit;

TEST_CASE("FragmentTests") {

    Fragment *fragment = FragmentAccessHelper::createFragment("testFragment");
    Mock<Fragment> fragmentSpecification(*fragment);
    Spy(Method(fragmentSpecification, deserialize));
    FragmentAccessHelper helper(fragment);

    Fragment &mockFragment = fragmentSpecification.get();

    boost::filesystem::create_directories("./temp");

    SECTION("Should set and get primitive types") {
        fragment->set<int>("someInteger", 12345);
        REQUIRE(fragment->get<int>("someInteger") == 12345);

        fragment->set<int>("someInteger", 67890);
        REQUIRE(fragment->get<int>("someInteger") == 67890);

        *fragment->getPointer<int>("someInteger") = 119911;
        REQUIRE(fragment->get<int>("someInteger") == 119911);

        fragment->set<char>("someChar", 'x');
        REQUIRE(fragment->get<char>("someChar") == 'x');
    }

    SECTION("Should initialize and get arrays") {
        long *someArray = fragment->initArray<long>("someArray", 10);
        for (auto i : {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}) {
            *someArray = i * i;
            someArray++;
        }

        REQUIRE(fragment->getPointer<long>("someArray")[5] == 36);

        REQUIRE_THROWS_WITH(fragment->initArray<int>("someArray", 10), "Element already initialized");
    }

    SECTION("Should set and get strings") {
        fragment->set<string>("someString", "ABCDEF");

        REQUIRE(fragment->get<string>("someString") == "ABCDEF");

        string anotherString = "XYZ";
        fragment->set<string>("someString", anotherString);

        REQUIRE(fragment->get<string>("someString") == "XYZ");
    }

    SECTION("Should erase data") {
        fragment->set<int>("someInteger", 12345);
        fragment->initArray<int>("someArray", 5);

        fragment->erase("someInteger");
        fragment->erase("someArray");

        REQUIRE_THROWS_WITH(fragment->get<int>("someInteger"), "Element does not exist");
        REQUIRE_THROWS_WITH(fragment->getPointer<int>("someArray"), "Element does not exist");
    }

    SECTION("Should get and set Serializables") {
        SerializableVector<int> vec(10);

        vec[5] = 34;
        vec.push_back(11);

        fragment->setSerializable("someSerializable", &vec);

        auto *vecAddress = dynamic_cast<SerializableVector<int> *>(fragment->getSerializable("someSerializable"));

        REQUIRE((*vecAddress)[5] == 34);
        REQUIRE((*vecAddress)[10] == 11);
    }

    SECTION("Should set and get strings") {
        fragment->set<string>("someString", "ABCDEF");

        REQUIRE(fragment->get<string>("someString") == "ABCDEF");

        string anotherString = "XYZ";
        fragment->set<string>("someString", anotherString);

        REQUIRE(fragment->get<string>("someString") == "XYZ");
    }

    SECTION("Should serialize primitives") {
        int someInteger = 12345;
        mockFragment.set<int>("someInteger", someInteger);
        char *someArray = fragment->initArray<char>("someArray", 5);
        const char *value = "Hello";
        memcpy(someArray, value, 5);
        mockFragment.set<char>("someChar", 'x');
        mockFragment.set<string>("someText", "abrakadabra");

        mockFragment.serialize();

        REQUIRE(helper.getMap().empty());

        FILE *serializedFile;

        std::string filename = "temp/testFragment_" + helper.getHash() + ".fragment";
        serializedFile = fopen(filename.c_str(), "rb");

        unsigned long size;
        fread(&size, sizeof(unsigned long), 1, serializedFile);

        REQUIRE(size == 4);

        std::string name = helper.getNextString(serializedFile);
        REQUIRE(name == "someArray");

        fread(&size, sizeof(unsigned long), 1, serializedFile);
        REQUIRE(size == 5);
        fseek(serializedFile, -sizeof(unsigned long), SEEK_CUR);
        std::string serializedValue = helper.getNextString(serializedFile);

        REQUIRE(serializedValue == std::string("Hello"));

        mockFragment.deserialize();

        REQUIRE(mockFragment.get<char>("someChar") == 'x');
        REQUIRE(mockFragment.get<int>("someInteger") == 12345);
        REQUIRE(mockFragment.get<string>("someText") == "abrakadabra");

        fclose(serializedFile);
    }

    SECTION("Should remove temporary file after deserialization") {
        std::string filename = "temp/testFragment_" + helper.getHash() + ".fragment";

        fragment->serialize();
        REQUIRE (boost::filesystem::exists(filename.c_str()));
        fragment->deserialize();
        REQUIRE (!boost::filesystem::exists(filename.c_str()));
    }

    SECTION("Should deserialize primitives (when serialized data is accessed)") {
        mockFragment.set<int>("someInteger", 12345);
        mockFragment.set<char>("someChar", 'x');

        mockFragment.serialize();

        // Deserialize manually
        mockFragment.deserialize();

        REQUIRE(mockFragment.get<int>("someInteger") == 12345);
        REQUIRE(mockFragment.get<char>("someChar") == 'x');

        mockFragment.serialize();

        // Lazy deserialization
        REQUIRE(mockFragment.get<int>("someInteger") == 12345);
        REQUIRE(mockFragment.get<char>("someChar") == 'x');

        Verify(Method(fragmentSpecification, deserialize)).Exactly(2_Times);

    }

    SECTION("Should serialize and deserialize SerializableVector") {
        auto vec = new SerializableVector<int>(5);

        vec->at(4) = 99;

        fragment->setSerializable("myVector", vec);

        vec->push_back(123);

        fragment->serialize();

        // Serialization of a fragment should only happen, while it is not being used
        // When a plugin wants to access data inside a Fragment, it has to use its getters.
        // Calling the getters automatically deserializes the fragment data.
        // This ensures, that Serializables inside Fragments are always in a consistent state.

        fragment->deserialize();

        auto vecAddress = dynamic_cast<SerializableVector<int> *>(fragment->getSerializable("myVector"));

        REQUIRE(vecAddress->at(0) == 0);
        REQUIRE(vecAddress->at(4) == 99);
        REQUIRE((*vecAddress)[5] == 123);
    }

    SECTION("Should serialize and deserialize SerializableMap") {
        auto map = new SerializableMap<int, float>();

        (*map)[1] = 2.99f;
        (*map)[4] = 5.543f;
        (*map)[10] = 115.45f;

        fragment->setSerializable("myMap", map);

        fragment->serialize();

        fragment->deserialize();

        auto mapAddress = dynamic_cast<SerializableMap<int, float> *>(fragment->getSerializable("myMap"));

        REQUIRE((*mapAddress)[1] == 2.99f);
        REQUIRE((*mapAddress)[4] == 5.543f);
        REQUIRE((*mapAddress)[10] == 115.45f);
    }

    SECTION("Should serialize and deserialize SerializableUnorderedMap") {
        auto umap = new SerializableUnorderedMap<int, int>();

        (*umap)[1] = 2;
        (*umap)[4] = 5;
        (*umap)[10] = 115;

        fragment->setSerializable("myUMap", umap);

        fragment->serialize();

        fragment->deserialize();

        auto mapAddress = dynamic_cast<SerializableUnorderedMap<int, int> *>(fragment->getSerializable("myUMap"));

        REQUIRE((*mapAddress)[1] == 2);
        REQUIRE((*mapAddress)[4] == 5);
        REQUIRE((*mapAddress)[10] == 115);
    }

}

