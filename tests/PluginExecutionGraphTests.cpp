#include <iostream>
#include <regex>

#include "./catch2/catch.hpp"
#include "../fakeit/fakeit.hpp"

#include "../framework/Plugin.h"
#include "../framework/Framework.h"
#include <boost/graph/graphviz.hpp>

using namespace std;
using namespace fakeit;

class TestPluginWithTileContext : public HelperPlugin {
public:
    explicit TestPluginWithTileContext(PluginSpecification *specification) : HelperPlugin(specification) {};

    bool hasTileContext() override { return true; }
};

TEST_CASE("ExecutionGraphTests") {
    // Capture cout
    streambuf *defaultOutBuffer = cout.rdbuf();
    ostringstream capturedOutBuffer;
    cout.rdbuf(capturedOutBuffer.rdbuf());
    capturedOutBuffer.str("");

    PluginSpecification::FragmentSpecification
            index1 = {"index1", "FMIndex"},
            index2 = {"index2", "FMIndex"},
            align1 = {"align1", "ALIGN"},
            align2 = {"align2", "ALIGN"},
            flow1Input = {"flow1", "FLOW", true },
            flow1Output = {"flow1", "FLOW" },
            flow2 = {"flow2", "FLOW" },
            variants = {"variants", "VARIANTS"};

    Plugin *buildIndex1 = new HelperPlugin(new PluginSpecification("BuildIndex1", "", {}, {
        // OutputFragments
        { "preprocessing", { index1 } }
    })),
    *buildIndex2 = new HelperPlugin(new PluginSpecification("BuildIndex2", "", {}, {
        // OutputFragments
        { "preprocessing", { index2 } }
    })),
    *alignment1 = new TestPluginWithTileContext(new PluginSpecification("Alignment1", "", {
        // InputFragments
        { "preprocessing", { index1 } },
        { "cycle", { index1 } }
    }, {
        // OutputFragments
        { "cycle", { align1 } }
    })),
    *alignment2 = new TestPluginWithTileContext(new PluginSpecification("Alignment2", "", {
        // InputFragments
        { "preprocessing", { index2 } },
        { "cycle", { index2 } }
    }, {
        // OutputFragments
        { "cycle", { align2 } }
    })),
    *pipeline1 = new TestPluginWithTileContext(new PluginSpecification("Pipeline1", "", {
        // InputFragments
        { "cycle", { flow1Input } }
    }, {
        // OutputFragments
        { "cycle", { flow2 }}
    })),
    *pipeline2 = new TestPluginWithTileContext(new PluginSpecification("Pipeline2", "", {
        // InputFragments
        { "cycle", { flow2 } }
    }, {
        // OutputFragments
        { "cycle", { flow1Output }}
    })),
    *variantCalling = new HelperPlugin(new PluginSpecification("Variants", "", {
        // InputFragments
        { "postprocessing", { align1, flow1Output } }
    }, {
        // OutputFragments
        { "postprocessing", { variants } }
    })),
    *vcfExport = new HelperPlugin(new PluginSpecification("Variants", "", {
        // InputFragments
        { "postprocessing", { variants } }
    }, {}));


    std::set<Plugin *> plugins = { buildIndex1, buildIndex2, alignment1, alignment2, pipeline1, pipeline2, variantCalling, vcfExport };

    SECTION("Should add a vertex to the graph") {
        ExecutionGraphBuilder realBuilder({ 1 }, { 1101 }, 1);

        Mock<ExecutionGraphBuilder> mock(realBuilder);
        Fake(Method(mock, registerOutputSpecification));

        ExecutionGraphBuilder &builder = mock.get();

        unsigned int initialNumVertices = builder.graph->getNumVertices();

        Plugin *dummyPlugin = new HelperPlugin(new PluginSpecification("DummyPlugin", "", {}, {
            { "preprocessing", { { "Fragment1", "Fragment", false }, { "Fragment2", "Fragment", false } } },
            { "cycle", { { "Fragment3", "Fragment", true } } }
        }));

        VertexIdentifier identifier = { dummyPlugin, 1, 1101, builder.graph->PRE_CYCLE };

        ExecutionGraphBuilder::Vertex v = builder.addVertex(identifier);

        // After adding the vertex, the graph should contain one more vertex than before
        REQUIRE( builder.graph->getNumVertices() == initialNumVertices + 1);

        // The vertexIdentifier should by accessible by the Vertex
        REQUIRE( builder.graph->getVertexIdentifier(v) == identifier );

        // The vertex should be accessible by the VertexIdentifier
        REQUIRE ( builder.toVertex(identifier) == v);

        // The outputSpecifications of the plugin for the according edge category should be registered
        Verify(Method(mock, registerOutputSpecification).Using(
            { "Fragment1", "Fragment", false }, v, identifier
        )).Once();

        Verify(Method(mock, registerOutputSpecification).Using(
            { "Fragment2", "Fragment", false }, v, identifier
        )).Once();

        VerifyNoOtherInvocations(mock);

        delete dummyPlugin->specification;
        delete dummyPlugin;

    }

    SECTION("Should register outputSpecifications correctly") {
        ExecutionGraphBuilder builder({ 1 }, { 1101 }, 1);

        Plugin *dummyPlugin = new HelperPlugin(new PluginSpecification("DummyPlugin", "", {}, {}));
        VertexIdentifier identifier = { dummyPlugin, 1, 1101, builder.graph->PRE_CYCLE };
        ExecutionGraphBuilder::Vertex v = builder.addVertex(identifier);

        builder.registerOutputSpecification({ "Fragment1", "Fragment", false }, v, identifier);

        builder.registerOutputSpecification({ "Fragment2", "otherType", false }, v, identifier);

        // Specific outputSpecification should exist in the registeredOutputSpecifications-map
        REQUIRE( builder.registeredOutputSpecifications[{
           "Fragment1", 1, 1101, builder.graph->PRE_CYCLE
        }] == ExecutionGraphBuilder::OutputSpecificationValue{ "Fragment", v} );

        // Should throw an error, when the same outputSpecification is added twice
        REQUIRE_THROWS( builder.registerOutputSpecification({ "Fragment1", "Fragment", false }, v, identifier) );

        // Should throw an error, when specification of same name but different type already exists
        REQUIRE_THROWS( builder.registerOutputSpecification({ "Fragment1", "otherType", false }, v, identifier) );

        delete dummyPlugin->specification;
        delete dummyPlugin;
    }

    SECTION("Should search for outputSpecifications") {
        ExecutionGraphBuilder builder({ 1 }, { 1101, 1102 }, 10);
        ExecutionGraphBuilder::Vertex v = 0;

        // The OutputSpecification that we search comes from cycle 5 of tile 1102
        builder.registerOutputSpecification({ "Fragment1", "Fragment", false }, v, { nullptr, 1, 1102, 5 });

        // Other specifications we do not look for
        builder.registerOutputSpecification({ "Fragment1", "Fragment", false }, v, { nullptr, 1, 1101, 5 });
        builder.registerOutputSpecification({ "Fragment2", "Fragment", false }, v, { nullptr, 1, 1102, builder.graph->PRE_CYCLE });

        // Should find the according outputSpecification for the inputSpecification
        REQUIRE( builder.searchForOutputSpecification(
            { "Fragment1", "Fragment", false }, {
                { "Fragment1", builder.graph->ALL_LANES, builder.graph->ALL_TILES, 5},
                { "Fragment1", 1, 1102, builder.graph->PRE_CYCLE},
                { "Fragment1", 1, 1102, 5}
            }
        ) == ExecutionGraphBuilder::OutputSpecificationValue{ "Fragment", v });

        // Should throw an error if the specification can not be found
        REQUIRE_THROWS( builder.searchForOutputSpecification(
            { "notExistingFragment", "Fragment", false }, {
                { "notExistingFragment", 1, 1102, 5}
            }
        ) );
    }

    SECTION("Should add an edge correctly") {
        ExecutionGraphBuilder builder({ 1 }, { 1101, 1102 }, 10);
        ExecutionGraphBuilder::Vertex
            v1 = builder.graph->addVertex({ nullptr, 0, 0, 0 }),
            v2 = builder.graph->addVertex({ nullptr, 0, 0, 1 }),
            v3 = builder.graph->addVertex({ nullptr, 0, 0, 2 }),
            v4 = builder.graph->addVertex({ nullptr, 0, 0, 3 });

        unsigned int initialNumEdges = builder.graph->getNumEdges();

        REQUIRE (!builder.graph->hasEdge(v1, v2));

        // Should add edge with empty fragmentSpecification
        builder.addEdge(v1, v2);

        REQUIRE ( builder.graph->getNumEdges() == initialNumEdges + 1);
        REQUIRE ( builder.graph->hasEdge(v1, v2) );

        // Should add edge with fragmentSpecification
        ExecutionGraphBuilder::Edge edge = builder.addEdge(v3, v4, { "Fragment1", "Fragment", false });

        REQUIRE( builder.graph->getEdgePayload(edge)[0] == PluginSpecification::FragmentSpecification{ "Fragment1", "Fragment", false } );

        // Should add another fragmentSpecification to an existing edge

        edge = builder.addEdge(v3, v4, { "Fragment2", "Fragment", false });
        REQUIRE( builder.graph->getEdgePayload(edge)[1] == PluginSpecification::FragmentSpecification{ "Fragment2", "Fragment", false });
    }

    SECTION("Should not add runCycleForTile steps for Plugins without TileContext") {
        ExecutionGraphBuilder realBuilder({ 1 }, { 1101, 1102 }, 10);
        Mock<ExecutionGraphBuilder> mock(realBuilder);
        Fake(Method(mock, addEdge));

        ExecutionGraphBuilder &builder = mock.get();

        ExecutionGraph *g = builder.build(plugins);

        // BuildIndex has no TileContext, so there are no tile-based vertices
        REQUIRE (builder.pluginVertices.count({buildIndex1, 1, 1101, 5}) == 0);

        // Alignemnt has a TileContext, so there are tile-based vertices
        REQUIRE (builder.pluginVertices.count({alignment1, 1, 1101, 5 }) == 1);

        delete g;
    }

    SECTION("Should setup graph structure correctly") {
        ExecutionGraphBuilder realBuilder({ 1 }, { 1101, 1102 }, 10);
        Mock<ExecutionGraphBuilder> mock(realBuilder);
        Fake(Method(mock, addEdge));

        ExecutionGraphBuilder &builder = mock.get();

        ExecutionGraph *g = builder.build(plugins);

        PluginSpecification::FragmentSpecification emptyFragmentSpecification = {"", "", false};
        // Edges for general execution structure
        {
            // Edge between StartPlugin and Preprocessing step of each plugin
            Verify(Method(mock, addEdge).Using(g->startPluginVertex, _, emptyFragmentSpecification)).Exactly(8_Times);
            Verify(Method(mock, addEdge).Using(
                    g->startPluginVertex,
                    builder.toVertex({buildIndex1, g->ALL_LANES, g->ALL_TILES, g->PRE_CYCLE}),
                    emptyFragmentSpecification
            ));

            // Edge between preprocseeing step and runCycle of each plugin
            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({buildIndex1, g->ALL_LANES, g->ALL_TILES, g->PRE_CYCLE}),
                    builder.toVertex({buildIndex1, g->ALL_LANES, g->ALL_TILES, 1}),
                    emptyFragmentSpecification
            ));

            // Edge between preprocessing step and runCycleForTile of each plugin
            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({alignment1, g->ALL_LANES, g->ALL_TILES, g->PRE_CYCLE}),
                    builder.toVertex({alignment1, 1, 1101, 1}),
                    emptyFragmentSpecification
            ));

            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({alignment1, g->ALL_LANES, g->ALL_TILES, g->PRE_CYCLE}),
                    builder.toVertex({alignment1, 1, 1102, 1}),
                    emptyFragmentSpecification
            ));

            // Edge of each runCycleForTile to runCycle for each tile in each cycle of each plugin
            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({alignment1, 1, 1101, 5}),
                    builder.toVertex({alignment1, g->ALL_LANES, g->ALL_TILES, 5}),
                    emptyFragmentSpecification
            ));

            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({alignment1, 1, 1102, 5}),
                    builder.toVertex({alignment1, g->ALL_LANES, g->ALL_TILES, 5}),
                    emptyFragmentSpecification
            ));

            // Edge from one cycle to the next cycle for runCycle for each Plugin
            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({alignment1, g->ALL_LANES, g->ALL_TILES, 5}),
                    builder.toVertex({alignment1, g->ALL_LANES, g->ALL_TILES, 6}),
                    emptyFragmentSpecification
            ));

            // Edge from one runCycleForTile to the runCycleForTile of the next cycle for each Plugin
            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({alignment1, 1, 1101, 5}),
                    builder.toVertex({alignment1, 1, 1101, 6}),
                    emptyFragmentSpecification
            ));

            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({alignment1, 1, 1102, 5}),
                    builder.toVertex({alignment1, 1, 1102, 6}),
                    emptyFragmentSpecification
            ));

            // Edge from the last runCycle to to the Postprocessing step
            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({alignment1, 1, 1101, 5}),
                    builder.toVertex({alignment1, 1, 1101, 6}),
                    emptyFragmentSpecification
            ));

            // Edge from every Postprocessing step to the endPluginVertex
            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({alignment1, g->ALL_LANES, g->ALL_TILES, g->POST_CYCLE}),
                    g->endPluginVertex,
                    emptyFragmentSpecification
            ));

            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({pipeline1, g->ALL_LANES, g->ALL_TILES, g->POST_CYCLE}),
                    g->endPluginVertex,
                    emptyFragmentSpecification
            ));
        }

        // Edges for fragment dependencies
        {
            // Fragment dependencies between preprocessing hooks
            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({buildIndex1, g->ALL_LANES, g->ALL_TILES, g->PRE_CYCLE}),
                    builder.toVertex({alignment1, g->ALL_LANES, g->ALL_TILES, g->PRE_CYCLE}),
                    index1
            ));

            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({buildIndex2, g->ALL_LANES, g->ALL_TILES, g->PRE_CYCLE}),
                    builder.toVertex({alignment2, g->ALL_LANES, g->ALL_TILES, g->PRE_CYCLE}),
                    index2
            ));

            // Fragment dependencies between preprocessing and runCycle hooks
            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({buildIndex1, g->ALL_LANES, g->ALL_TILES, g->PRE_CYCLE}),
                    builder.toVertex({alignment1, g->ALL_LANES, g->ALL_TILES, 1 }),
                    index1
            ));

            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({buildIndex2, g->ALL_LANES, g->ALL_TILES, g->PRE_CYCLE}),
                    builder.toVertex({alignment2, g->ALL_LANES, g->ALL_TILES, 3 }),
                    index2
            ));

            // Fragment dependencies between preprocessing and runCycleForTile hooks
            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({buildIndex1, g->ALL_LANES, g->ALL_TILES, g->PRE_CYCLE}),
                    builder.toVertex({alignment1, 1, 1101, 1 }),
                    index1
            ));

            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({buildIndex2, g->ALL_LANES, g->ALL_TILES, g->PRE_CYCLE}),
                    builder.toVertex({alignment2, 1, 1102, 4 }),
                    index2
            ));

            // Fragment dependencies between runCycle hooks
            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({pipeline1, g->ALL_LANES, g->ALL_TILES, 4}),
                    builder.toVertex({pipeline2, g->ALL_LANES, g->ALL_TILES, 4}),
                    flow2
            ));

            // Fragment dependencies between runCycleForTile hooks
            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({pipeline1, 1, 1102, 4}),
                    builder.toVertex({pipeline2, 1, 1102, 4}),
                    flow2
            ));

            // Fragment dependencies from a previous cycle
            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({pipeline2, g->ALL_LANES, g->ALL_TILES, 3}),
                    builder.toVertex({pipeline1, g->ALL_LANES, g->ALL_TILES, 4}),
                    flow1Input
            ));

            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({pipeline2, 1, 1101, 3}),
                    builder.toVertex({pipeline1, 1, 1101, 4}),
                    flow1Input
            ));

            // Fragment dependencies between runCycle and postprocessing hooks
            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({alignment1, g->ALL_LANES, g->ALL_TILES, g->POST_CYCLE - 1}),
                    builder.toVertex({variantCalling, g->ALL_LANES, g->ALL_TILES, g->POST_CYCLE}),
                    align1
            ));

            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({pipeline2, g->ALL_LANES, g->ALL_TILES, g->POST_CYCLE - 1}),
                    builder.toVertex({variantCalling, g->ALL_LANES, g->ALL_TILES, g->POST_CYCLE}),
                    flow1Output
            ));


            // Fragment dependencies between postprocessing hooks
            Verify(Method(mock, addEdge).Using(
                    builder.toVertex({variantCalling, g->ALL_LANES, g->ALL_TILES, g->POST_CYCLE}),
                    builder.toVertex({vcfExport, g->ALL_LANES, g->ALL_TILES, g->POST_CYCLE}),
                    variants
            ));
        }

        delete g;

    }

    // Reset cout
    cout.rdbuf(defaultOutBuffer);
}

