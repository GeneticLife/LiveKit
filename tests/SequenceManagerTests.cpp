#include "./catch2/catch.hpp"
#include "../framework/basecalls/management/SequenceManager.h"
#include "../framework/basecalls/data_representation/BclRepresentation.h"
#include <boost/filesystem.hpp>

class SequenceManagerAccessHelper {
    SequenceManager &sequenceManager;
public:
    explicit SequenceManagerAccessHelper(SequenceManager &sequenceManager) : sequenceManager(sequenceManager) {};

    bool isSerialized(uint16_t lane, uint16_t tile){
        return sequenceManager.areSequencesOfTileSerialized(lane, tile);
    }

    // this method does not deserialize the SequenceContainer in case it is serialized
    SequenceContainer& getSequencesRaw(uint16_t lane, uint16_t tile){
        return this->sequenceManager.sequenceRepresentations[lane][tile];
    }
};

TEST_CASE("SequenceManager") {

    boost::filesystem::create_directories("./temp/sequences/");

    std::vector<uint16_t> lanes = {1, 2};
    std::vector<uint16_t> tiles = {1101, 1102};
    std::vector<std::pair<int, char>> readstructure = {std::make_pair(5, 'R')};

    SequenceManager seqManager = SequenceManager(lanes, tiles, readstructure);
    SequenceManagerAccessHelper seqManagerHelper(seqManager);

    const BCL lastBcl = {'A', 'T', 'T', 'C', 'G', 'C'};

    // simulate the lifecycle of the framwork
    for(int i=1; i<=5; i++){    // 5 cycles
        for(auto lane: lanes){
            for(auto tile: tiles){
                if(i == 1)
                    seqManager.getSequences(lane, tile).setSize(lastBcl.size());
                seqManager.getSequences(lane, tile).extendSequences(lastBcl);
            }
        }
    }

    SECTION("Should setup correctly") {
        REQUIRE(*seqManager.getSequences(1, 1101).getSequenceIterators(0, 5).first == 'A');
        REQUIRE(*seqManager.getSequences(1, 1101).getSequenceIterators(1, 5).first == 'T');
        REQUIRE(*seqManager.getSequences(1, 1101).getSequenceIterators(2, 5).first == 'T');
        REQUIRE(*seqManager.getSequences(1, 1101).getSequenceIterators(3, 5).first == 'C');
        REQUIRE(*seqManager.getSequences(1, 1101).getSequenceIterators(4, 5).first == 'G');
        REQUIRE(*seqManager.getSequences(1, 1101).getSequenceIterators(5, 5).first == 'C');
    }

    SECTION("Should serialize tile") {
        seqManager.serializeSequencesOfTile(1, 1101);
        REQUIRE(boost::filesystem::exists("./temp/sequences/1_1101"));

        REQUIRE(seqManagerHelper.isSerialized(1, 1101) == true);
        REQUIRE(seqManagerHelper.isSerialized(1, 1102) == false);

        //REQUIRE(seqManagerHelper.getSequencesRaw(1, 1101).getAllSequences().size() == 0);
    }

    SECTION("Should derserialize correctly"){
        seqManager.serializeSequencesOfTile(1, 1101);
        seqManager.deserializeSequencesOfTile(1, 1101);

        REQUIRE(*seqManager.getSequences(1, 1101).getSequenceIterators(0, 5).first == 'A');
        REQUIRE(*seqManager.getSequences(1, 1101).getSequenceIterators(1, 5).first == 'T');
        REQUIRE(*seqManager.getSequences(1, 1101).getSequenceIterators(2, 5).first == 'T');
        REQUIRE(*seqManager.getSequences(1, 1101).getSequenceIterators(3, 5).first == 'C');
        REQUIRE(*seqManager.getSequences(1, 1101).getSequenceIterators(4, 5).first == 'G');
        REQUIRE(*seqManager.getSequences(1, 1101).getSequenceIterators(5, 5).first == 'C');

        REQUIRE(seqManagerHelper.isSerialized(1, 1101) == false);
    }

    SECTION("Should automatically derserialize serialized data if needed"){
        seqManager.serializeSequencesOfTile(1, 1101);

        REQUIRE(seqManagerHelper.isSerialized(1, 1101) == true);
        //REQUIRE(seqManagerHelper.getSequencesRaw(1, 1101).getAllSequences().size() == 0);
        REQUIRE_THROWS(seqManagerHelper.getSequencesRaw(1, 1101).getSequenceIterators(0, 0)); // this does not deserialize
        REQUIRE(*seqManager.getSequences(1, 1101).getSequenceIterators(0, 5).first == 'A'); // this calls deserialize for the tile
        //REQUIRE(seqManagerHelper.getSequencesRaw(1, 1101).getAllSequences().size() == 6);
        REQUIRE(seqManagerHelper.isSerialized(1, 1101) == false);
    }
}
