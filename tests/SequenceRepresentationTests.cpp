#include "./catch2/catch.hpp"
#include "../framework/basecalls/data_representation/SequenceContainer.h"

class SequenceContainerAccessHelper {
    SequenceContainer &sequenceContainer;
public:
    explicit SequenceContainerAccessHelper(SequenceContainer &sequenceContainer, std::vector<std::pair<int, char>> readStructure) : sequenceContainer(sequenceContainer) {
        this->sequenceContainer.setReadStructure(readStructure);
    };

    void setSequences(std::vector<Sequence> newSequences){
        this->sequenceContainer.sequences =  newSequences;
    }
};

TEST_CASE("SequenceContainer") {

    std::vector<char> nucleotides = {'A', 'T', 'C'};

    SECTION("Should set size in all Sequences") {
        SequenceContainer sequences;
        sequences.setReadStructure({{2, 'R'}});
        sequences.setSize(3);
        sequences.extendSequences(nucleotides);
        sequences.extendSequences(nucleotides);
        for (int i = 0; i < 2; i++) {
            auto sequenceIterators = sequences.getSequenceIterators(i, 2);
            REQUIRE(*sequenceIterators.first == nucleotides[i]);
        }
    }

    SECTION("Should set char in all Sequences for a complex read structure") {
        std::vector<std::pair<int, char>> readStructure = {{2, 'R'}, {1, 'B'}, {1, 'B'}, {2, 'R'}}; // read-structure: 2R 1B 1B 2R
        SequenceContainer sequences;
        sequences.setReadStructure(readStructure);
        sequences.setSize(3);

        // This is a bit dirty but does the trick:
        // The following lines simulate sequences.extendSequence(0, 'T'), but this method is a private function
        sequences.extendSequences(std::vector<char>(1, 'T'));
        sequences.extendSequences(std::vector<char>(1, 'A'));
        sequences.extendSequences(std::vector<char>(1, 'T'));
        sequences.extendSequences(std::vector<char>(1, 'C'));
        sequences.extendSequences(std::vector<char>(1, 'G'));

        // we extended the sequence 5 times
        REQUIRE(sequences.getRead(0, 0, 5).size() == 2);
        REQUIRE(sequences.getRead(0, 1, 5).size() == 1);
        REQUIRE(sequences.getRead(0, 2, 5).size() == 1);
        REQUIRE(sequences.getRead(0, 3, 5).size() == 1); // because we only have inserted 5 nucleotides

        REQUIRE(sequences.getRead(0, 0, 5) == (Read){'T','A'});
        REQUIRE(sequences.getRead(0, 1, 5) == (Read){'T'});
        REQUIRE(sequences.getRead(0, 2, 5) == (Read){'C'});
        REQUIRE(sequences.getRead(0, 3, 5) == (Read){'G'});

        auto iterators = sequences.getSequenceIterators(0, 5);
        auto result = std::vector<uint8_t>(iterators.first, iterators.second);
        REQUIRE(result == Read{'T', 'A', 'T', 'C', 'G'});
    }

    SECTION("Should establish serializing Size correctly") {
        std::vector<std::pair<int, char>> readStructure = {{20, 'R'}, {4, 'B'}, {20, 'R'}}; // read-structure: 20R 4B 20R
        SequenceContainer sequences;
        sequences.setReadStructure(readStructure);
        sequences.setSize(5);

        std::vector<char> longerNucleotides = {'A', 'T', 'C', 'G', 'A'};

        unsigned long baseSize = sizeof(unsigned long);
        // The size of sequences is 5. Each sequence has n = 3 reads
        // Each sequence needs:
        // 1 long for the totalSize
        // 1 long for the number of elements in the vector
        baseSize += 5 * (1 + 1) * sizeof(unsigned long);

        for (int i = 1; i <= 3; i++) {
            sequences.extendSequences(longerNucleotides);
            REQUIRE(sequences.serializableSize() == i * longerNucleotides.size() + baseSize);
        }
    }


    SECTION("Test deserializing") {
        /*
        SequenceContainer sequences;
        std::vector<std::pair<int, char>> readStructure = {{20, 'R'}}; // read-structure: 20R
        sequences.setSize(3);
        sequences.extendSequences(nucleotides);

        // the copy is needed because the data of the original 'sequences' is cleared
        SequenceContainer sequenceCopy;
        SequenceContainerAccessHelper sequenceCopyAH(sequenceCopy, readStructure);
        sequenceCopyAH.setSequences(sequences.getAllSequences());

        // serialize
        auto serializingResult = std::vector<char>(sequences.serializableSize());
        sequences.serialize(serializingResult.data());

        // deserialize
        SequenceContainer newSequences;
        newSequences.deserialize(serializingResult.data());

        for (int i = 0; i < 3; i++) {
            REQUIRE(newSequences.getSequence(i) == sequenceCopy.getSequence(i));
        }
         */
    }
}
